//
//  AddEditViewController.swift
//  ExelToApp
//
//  Created by baps on 28/01/20.
//  Copyright © 2020 Osiya. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI

class AddEditViewController: UIViewController,UITextFieldDelegate,CNContactPickerDelegate {
    static var VC = AddEditViewController()
    var scrollView:UIScrollView!
    var txtField:UITextField!
    var lblTitle:UILabel!
    var btnSubmit:UIButton!
    var Position = 0
    var titlearr = [""]
    var textFieldArr = NSMutableArray()
    var squ = [0]
    var bgImgView:UIImageView!
    var custVarHeading1 = USER_DEFAULTS.string(forKey: "custoVarHeadingOne") ?? ""
    var custVarHeading2 = USER_DEFAULTS.string(forKey: "custoVarHeadingTwo") ?? ""
    var custVarHeading3 = USER_DEFAULTS.string(forKey: "custoVarHeadingThree") ?? ""
    var custVararr1 = USER_DEFAULTS.array(forKey: "arrayCustomVarOne") as NSArray?
    var custVararr2 = USER_DEFAULTS.array(forKey: "arrayCustomVarTwo") as NSArray?
    var custVararr3 = USER_DEFAULTS.array(forKey: "arrayCustomVarThree") as NSArray?
    var votedarr = USER_DEFAULTS.array(forKey: "arrayVoted")! as NSArray
    var u_id = USER_DEFAULTS.string(forKey: "u_id") ?? ""
    var cp_id = USER_DEFAULTS.string(forKey: "cp_id") ?? ""
    var no = 0
    var commentArray = NSMutableArray()
    var commDataArray = NSMutableArray()
    
    var viewDatePicker:UIView!
    var datePicker:UIDatePicker!
    var btnCanceldp:UIButton!
    var btnSelectdp:UIButton!
    var btnImportContact:UIButton!
    
    var textView:UITextView!
    
    var popupView:UIView!
    var viewBack:UIView!
    var topView:UIView!
    var lblHeading:UILabel!
    var btn:UIButton!
    
    var contactVal:CNContact!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AddEditViewController.VC = self
        HomeViewController.VC.btnIocn.isHidden = true
        no = -1;
        if(USER_DEFAULTS.string(forKey: "case") ?? "" == "add"){
            self.title = "Add Person"
        }else{
            self.title = "Edit Person"
            no = Int(USER_DEFAULTS.string(forKey: "case") ?? "")!
        }
        print("no~~~~~~~~\(USER_DEFAULTS.string(forKey: "case"))-",no)
        bgImgView = UIImageView()
        bgImgView.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT)
        bgImgView.image = UIImage(named: "bcakground.jpg")
        view.addSubview(bgImgView)
        setUpView()

    }
    
    func setUpView() {
        scrollView = UIScrollView()
        scrollView.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT)
        scrollView.backgroundColor = UIColor.clear
        scrollView.bounces = false
        scrollView.contentSize.height = 2800
        view.addSubview(scrollView)
        
        titlearr = ["Personal First Name","Personal Middle Name","Personal Last Name","Residence House Number","Residence House Fraction","Residence Apartment Number","Residence Street Direction","Residence Street Name","Residence City","Residence State","Residence ZipCode5","Personal Sex","Personal Race","Registration Political Party Code","Personal Age","Home Phone","Mobile Phone","Last Voted","Email","Residence Walk List Order","Favorability","Yard Sign","Comments","Rating","Voted","Jurisdiction Parish","Jurisdiction Ward","Jurisdiction Precinct","Custom Variable1","Custom Variable2","Custom Variable3","excel id","new_comments","voter_status"];
        
        squ = [0,23,1,2,16,17,15,3,4,18,19,5,6,7,8,9,31,10,21,11,12,13,14,24,22,25,26,27,28,29,30,20,32,33];
        let headingArr = jsonObj.value(forKey: "heading") as? NSArray ?? NSArray()
        
        btnImportContact = UIButton(type: .system)
        btnImportContact.setButton(X: scrollView.frame.width/2-(scrollView.frame.width/2)/2, Y: 10, Width: scrollView.frame.width/2, Height: 30, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Import Contact", Align: .center, Font: .systemFont(ofSize: 18.0))
        btnImportContact.tag = 4
        btnImportContact.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        scrollView.addSubview(btnImportContact)
        
        if (no>=0) {
            Position = 50;
            btnImportContact.isHidden = false
        }else{
            Position = 10
            btnImportContact.isHidden = true
        }
        
        for i in 0..<titlearr.count {
            //print("titlearr.count---\(Position)-\(i)")
            lblTitle = UILabel()
            lblTitle.setLabel(X: 15, Y: CGFloat(Position), Width: SWIDTH-30, Height: 20, TextColor: .black, BackColor: .clear, Text: titlearr[i], TextAlignment: .left, Font: .systemFont(ofSize: 15))
            scrollView.addSubview(lblTitle)
            
            txtField = UITextField()
            txtField.setTextField(X: 15, Y: CGFloat(Position+25), Width: SWIDTH-30, Height: 40, TextColor: .black, BackColor: .white)
            txtField.borderStyle = .roundedRect
            txtField.font = UIFont.systemFont(ofSize: 15)
            txtField.placeholder = titlearr[i]
            scrollView.addSubview(txtField)
            
            if i == 22{
                if(USER_DEFAULTS.string(forKey: "case") ?? "" == "add"){
                    //textView.text = "";
                    txtField.isEnabled = true
                    //textView.isHidden = true;
                }else{
                    textView = UITextView(frame: CGRect(x: 15, y: CGFloat(Position+25), width: SWIDTH-30, height: 100)) //Frame: : 0, 20, 320, 548
                    textView.text = "Comment"
                    if (textView.text == "Comment") {
                        textView.textColor = UIColor.lightGray
                    } else {
                        textView.textColor = UIColor.black
                    }
                    textView.font = UIFont.systemFont(ofSize: 15.0)
                    textView.isScrollEnabled = true
                    textView.alwaysBounceVertical = true
                    textView.isEditable = false
                    textView.clipsToBounds = true
                    textView.keyboardDismissMode = .interactive
                    textView.keyboardType = .default
                    textView.layer.cornerRadius = 5.0
                    scrollView.addSubview(textView)
                    Position+=60;
                }
            }
            textFieldArr.add(txtField)
            
            if((titlearr.count - 4 ) < i){
                txtField.isHidden = true;
                lblTitle.isHidden = true;
                txtField.text = "";
            }
            if(i == 28){
                if(custVarHeading1 != ""){
                    lblTitle.text = custVarHeading1
                    txtField.placeholder = ""
                    Position+=80;
                }else{
                    lblTitle.isHidden = true
                    txtField.isHidden = true
                }
            }else if (i==29){
                if(custVarHeading2 != ""){
                    lblTitle.text = custVarHeading2
                    txtField.placeholder = ""
                    Position+=80;
                }else{
                    lblTitle.isHidden = true
                    txtField.isHidden = true
                }
            }else if (i==30){
                if(custVarHeading3 != ""){
                    lblTitle.text = custVarHeading3
                    txtField.placeholder = ""
                    Position+=80;
                }else{
                    lblTitle.isHidden = true
                    txtField.isHidden = true
                }
            }else{
                Position+=80;
            }
            
            if(i == 11 || i == 12 || i == 13 || i == 17 || i == 20 || i == 21 || i == 28 || i == 29 || i == 30  || i == 24){
                txtField.delegate = self;
            }
        }
        print("Position~~~~~~`",Position)
        btnSubmit = UIButton(type: .system)
        btnSubmit.setButton(X: SWIDTH/2-75, Y: CGFloat(Position-100), Width: 150, Height: 35, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Add Person", Align: .center, Font: .boldSystemFont(ofSize: 18))
        btnSubmit.tag = 1
        btnSubmit.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        scrollView.addSubview(btnSubmit)
        
        if(no >= 0){
            let results = jsonObj.value(forKey: "data") as! NSArray
            let obj = results[no] as! NSArray
            
            for j in 0..<headingArr.count{
                let k = squ[j]
                let txt = textFieldArr[j] as? UITextField
                txt?.text = obj[k] as? String
                if(j == 20){
                    var fav = obj[k] as? String
                    if fav!.caseInsensitiveCompare("Satisfied") == .orderedSame{
                        fav = "FAVOR"
                    } else if fav!.caseInsensitiveCompare("Not Satisfied") == .orderedSame{
                        fav = "NOT FAVOR"
                    } else if fav!.caseInsensitiveCompare("Undecided") == .orderedSame {
                        fav = "UNDECIDED"
                    } else if fav!.caseInsensitiveCompare("Never Contacted") == .orderedSame {
                        fav = "NOT AVAILABLE"
                    }
                    txt?.text = fav
                }
                if(j == 21){
                    var yard = obj[k] as? String
                    if yard!.caseInsensitiveCompare("Open") == .orderedSame{
                        yard = "YES"
                    }else if yard!.caseInsensitiveCompare("Closed") == .orderedSame {
                        yard = "NO"
                    }
                    txt?.text = yard
                }
                if j == 22{
                    let dic = obj[32] as! NSDictionary
                    let commArr = dic.value(forKey: "data") as! NSArray
                    commentArray.removeAllObjects()
                    commDataArray.removeAllObjects()
                    for i in 0..<commArr.count{
                        let c_Array = NSMutableArray()
                        let data = commArr[i] as! NSArray
                        var custVar1detail = "";
                        var custVar2detail = "";
                        var custVar3detail = "";
                        let d9 = data[9] as? String ?? ""
                        let d10 = data[10] as? String ?? ""
                        let d11 = data[10] as? String ?? ""
                        if (d9 != "") {custVar1detail = "\n\(data[9]) : \(data[12])"}
                        if (d10 != "") {custVar2detail = "\n\(data[10]) : \(data[13])"}
                        if (d11 != "") {custVar3detail = "\n\(data[11]) : \(data[14])"}
                        let str = "Date : \(data[2])\nUser : \(data[8])\nComment : \(data[1])\nFavorability : \(data[4])\nYard Sign : \(data[5])\(custVar1detail)\(custVar2detail)\(custVar3detail)\n\n"
                        commentArray.add(str)
                        textView?.text = commentArray[i] as? String
                        textView.textColor = UIColor.black
                        c_Array.add(data[0]);c_Array.add(data[1]);c_Array.add(data[2]);c_Array.add(data[3]);c_Array.add(data[4]);c_Array.add(data[5]);c_Array.add(data[6]);c_Array.add(data[7]);c_Array.add(data[8]);c_Array.add(data[9]);c_Array.add(data[10]);c_Array.add(data[11]);c_Array.add(data[12]);c_Array.add(data[13]);c_Array.add(data[14]);
                        
                        commDataArray.add(c_Array)
                    }
                }
                if(j == 32) {
                    txt?.text = ""
                }
                
            }
            btnSubmit.setTitle("Update", for: .normal)
        }
        
        viewDatePicker = UIView()
        viewDatePicker.frame = CGRect(x: 30, y: 180, width: SWIDTH-60, height: 260)
        viewDatePicker.backgroundColor = APPPPOPUPCOLOR
        viewDatePicker.layer.cornerRadius = 10.0
        view.addSubview(viewDatePicker)
        
        datePicker = UIDatePicker()
        datePicker.frame = CGRect(x: 0, y: 25, width: viewDatePicker.frame.width, height: viewDatePicker.frame.height-65)
        datePicker.backgroundColor = UIColor.clear
        datePicker.maximumDate = Date()
        viewDatePicker.addSubview(datePicker)
        
        btnCanceldp = UIButton()
        btnCanceldp.setButton(X: viewDatePicker.frame.width-30, Y: 5, Width: 24, Height: 24, TextColor: APPBUTTONCOLOR, BackColor: .clear, Title: "X", Align: .center, Font: .boldSystemFont(ofSize: 22.0))
        btnCanceldp.tag = 3
        btnCanceldp.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        viewDatePicker.addSubview(btnCanceldp)
        
        btnSelectdp = UIButton()
        btnSelectdp.setButton(X: viewDatePicker.frame.width/2-60, Y: datePicker.frame.maxY+5, Width: 120, Height: 30, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Select", Align: .center, Font: .systemFont(ofSize: 16.0))
        btnSelectdp.tag = 2
        btnSelectdp.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        viewDatePicker.addSubview(btnSelectdp)
        viewDatePicker.isHidden = true
        
        
        viewBack = UIView()
        viewBack.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT)
        viewBack.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        view.addSubview(viewBack)
        
        popupView = UIView()
        popupView.frame = CGRect(x: 10, y: view.frame.height/2-150, width: view.frame.width-20, height: 140)
        popupView.backgroundColor = UIColor.white
        popupView.layer.cornerRadius = 10.0
        popupView.clipsToBounds = true
        viewBack.addSubview(popupView)
        
        topView = UIView()
        topView.frame = CGRect(x: 0, y: 0, width: popupView.frame.width, height: 50)
        topView.backgroundColor = APPBUTTONCOLOR
        popupView.addSubview(topView)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 16, Y: 5, Width: popupView.frame.width-40, Height: 40, TextColor: .white, BackColor: .clear, Text: "Do You Want To Merge The Contact Information For This Case?", TextAlignment: .center, Font: .boldSystemFont(ofSize: 16))
        lblHeading.numberOfLines = 0
        lblHeading.lineBreakMode = .byWordWrapping
        topView.addSubview(lblHeading)
        
        btn = UIButton(type: .system)
        btn.setButton(X: popupView.frame.width-30, Y: 5, Width: 24, Height: 30, TextColor: .white, BackColor: .clear, Title: "X", Align: .center, Font: .boldSystemFont(ofSize: 22))
        btn.tag = 5
        btn.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        topView.addSubview(btn)
        
        btn = UIButton(type: .system)
        btn.setButton(X: 50, Y: topView.frame.maxY+10, Width: popupView.frame.width-100, Height: 30, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Yes", Align: .center, Font: .systemFont(ofSize: 17))
        btn.tag = 6
        btn.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        popupView.addSubview(btn)
        
        btn = UIButton(type: .system)
        btn.setButton(X: 50, Y: topView.frame.maxY+50, Width: popupView.frame.width-100, Height: 30, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Keep Searching", Align: .center, Font: .systemFont(ofSize: 17))
        btn.tag = 7
        btn.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        popupView.addSubview(btn)
        
        viewBack.isHidden = true
        
        
        
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        let NAVHEIGHT = self.navigationController!.navigationBar.frame.maxY
        
        let txtdate = textFieldArr[17] as? UITextField
        if txtdate == textField {
            viewDatePicker.isHidden = false
            return false
        }
        
        let txtSex = self.textFieldArr[11] as? UITextField
        if txtSex == textField {
            isSelectPopup = 0
            let sexView = PopUpView()
            sexView.isHidden = false
            UIView.transition(with: sexView, duration: 0.3, options: .transitionCrossDissolve, animations: {
                sexView.frame = CGRect(x: 0, y: NAVHEIGHT, width: SWIDTH, height: SHEIGHT-64)
                self.view.addSubview(sexView)
            })
            return false
        }
        let txtRace = self.textFieldArr[12] as? UITextField
        if txtRace == textField {
            isSelectPopup = 1
            let raceView = PopUpView()
            raceView.isHidden = false
            UIView.transition(with: raceView, duration: 0.3, options: .transitionCrossDissolve, animations: {
                raceView.frame = CGRect(x: 0, y: NAVHEIGHT, width: SWIDTH, height: SHEIGHT-64)
                self.view.addSubview(raceView)
            })
            return false
        }
        let txtParty = self.textFieldArr[13] as? UITextField
        if txtParty == textField {
            isSelectPopup = 2
            let partyView = PopUpView()
            partyView.isHidden = false
            UIView.transition(with: partyView, duration: 0.3, options: .transitionCrossDissolve, animations: {
                partyView.frame = CGRect(x: 0, y: NAVHEIGHT, width: SWIDTH, height: SHEIGHT-64)
                self.view.addSubview(partyView)
            })
            return false
        }
        let txtFav = self.textFieldArr[20] as? UITextField
        if txtFav == textField {
            isSelectPopup = 3
            let favView = PopUpView()
            favView.isHidden = false
            UIView.transition(with: favView, duration: 0.3, options: .transitionCrossDissolve, animations: {
                favView.frame = CGRect(x: 0, y: NAVHEIGHT, width: SWIDTH, height: SHEIGHT-64)
                self.view.addSubview(favView)
            })
            return false
        }
        let txtYard = self.textFieldArr[21] as? UITextField
        if txtYard == textField {
            isSelectPopup = 4
            let yardView = PopUpView()
            yardView.isHidden = false
            UIView.transition(with: yardView, duration: 0.3, options: .transitionCrossDissolve, animations: {
                yardView.frame = CGRect(x: 0, y: NAVHEIGHT, width: SWIDTH, height: SHEIGHT-64)
                self.view.addSubview(yardView)
            })
            return false
        }
        let txtVoted = self.textFieldArr[24] as? UITextField
        if txtVoted == textField {
            isSelectPopup = 5
            let votedView = PopUpView()
            votedView.isHidden = false
            UIView.transition(with: votedView, duration: 0.3, options: .transitionCrossDissolve, animations: {
                votedView.frame = CGRect(x: 0, y: NAVHEIGHT, width: SWIDTH, height: SHEIGHT-64)
                self.view.addSubview(votedView)
            })
            return false
        }
        let txtCustVar1 = self.textFieldArr[28] as? UITextField
        if txtCustVar1 == textField {
            isSelectPopup = 6
            isSelectCustomVar = 0
            let custView = PopUpView()
            custView.custVarMode = 1
            custView.lblHeading.text = custVarHeading1
            custView.isHidden = false
            UIView.transition(with: custView, duration: 0.3, options: .transitionCrossDissolve, animations: {
                custView.frame = CGRect(x: 0, y: NAVHEIGHT, width: SWIDTH, height: SHEIGHT-64)
                self.view.addSubview(custView)
            })
            return false
        }
        let txtCustVar2 = self.textFieldArr[29] as? UITextField
        if txtCustVar2 == textField {
            isSelectPopup = 6
            isSelectCustomVar = 0
            let custView = PopUpView()
            custView.custVarMode = 2
            custView.lblHeading.text = custVarHeading2
            custView.isHidden = false
            UIView.transition(with: custView, duration: 0.3, options: .transitionCrossDissolve, animations: {
                custView.frame = CGRect(x: 0, y: NAVHEIGHT, width: SWIDTH, height: SHEIGHT-64)
                self.view.addSubview(custView)
            })
            return false
        }
        let txtCustVar3 = self.textFieldArr[30] as? UITextField
        if txtCustVar3 == textField {
            isSelectPopup = 6
            isSelectCustomVar = 0
            let custView = PopUpView()
            custView.custVarMode = 3
            custView.lblHeading.text = custVarHeading3
            custView.isHidden = false
            UIView.transition(with: custView, duration: 0.3, options: .transitionCrossDissolve, animations: {
                custView.frame = CGRect(x: 0, y: NAVHEIGHT, width: SWIDTH, height: SHEIGHT-64)
                self.view.addSubview(custView)
            })
            return false
        }
        return true
    }
    
    @objc func onClickButton(_ sender: UIButton) {
        if sender.tag == 1 { //on Click Update&Submit
            if(no < 0){
                updateView()
            }else{
                updateEditView()
            }
        }
        if sender.tag == 2{ //on Click Date Picker Select
            let txtdate = textFieldArr[17] as? UITextField
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd/yyyy"
            txtdate?.text = formatter.string(from: datePicker.date)
            viewDatePicker.isHidden = true
        }
        if sender.tag == 3{ //on Click Date Picker Cancel
            viewDatePicker.isHidden = true
        }
        if sender.tag == 4{ //on Click Import Contact
            let contactPicker = CNContactPickerViewController()
            contactPicker.delegate = self
            present(contactPicker, animated: true)
        }
        if sender.tag == 5{ //on Click Cross popup
            viewBack.isHidden = true
        }
        if sender.tag == 6{ //on Click Yes
            
            for phoneNumber in contactVal.phoneNumbers {
                guard let phoneNumber = phoneNumber as? CNLabeledValue else {
                    continue
                }
                let lable :String  =  CNLabeledValue<NSString>.localizedString(forLabel: phoneNumber.label ?? "" )
                print("\(lable)  \(phoneNumber.value)")
                //print("contact~~~~~~~~",phoneNumber.label)
                
                if lable == "home"{
                    var phone = phoneNumber.value.stringValue.withoutSpecialCharacters
                    phone = phone.replacingOccurrences(of: " ", with: "")
                    phone = phone.replacingOccurrences(of: "-", with: "")
                    phone = phone.replacingOccurrences(of: "(", with: "")
                    phone = phone.replacingOccurrences(of: ")", with: "")
                    let phno = String(phone.suffix(10))
                    //harr.append(phno)
                    let txt = self.textFieldArr[15] as? UITextField
                    txt?.text = phno
                }
                if lable == "mobile"{
                    var phone = phoneNumber.value.stringValue.withoutSpecialCharacters
                    phone = phone.replacingOccurrences(of: " ", with: "")
                    phone = phone.replacingOccurrences(of: "-", with: "")
                    phone = phone.replacingOccurrences(of: "(", with: "")
                    phone = phone.replacingOccurrences(of: ")", with: "")
                    let phno = String(phone.suffix(10))
                    let txt = self.textFieldArr[16] as? UITextField
                    txt?.text = phno
                }
                
            }
            
            
            for email in contactVal.emailAddresses {
                guard let email = email as? CNLabeledValue else {
                    continue
                }
                let selectedEmail = "\(email.value)"
                let txt = self.textFieldArr[18] as? UITextField
                if !(selectedEmail == "") {
                    txt?.text = selectedEmail
                }
            }
            
            viewBack.isHidden = true
        }
        if sender.tag == 7{ //on Click Keep Searching
            let contactPicker = CNContactPickerViewController()
            contactPicker.delegate = self
            present(contactPicker, animated: true)
        }
    }
    
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        contactVal = contact
        let dispatchAfter = DispatchTimeInterval.seconds(Int(1.0))
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + dispatchAfter, execute: {
            self.viewBack.isHidden = false
        })
    }
    
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        print("Cancelled")
    }

    
    
    
    func updateView() {
        let heding = jsonObj.value(forKey: "heading") as! NSArray
        let results = jsonObj.value(forKey: "data") as! NSArray
        let obj = results[0] as! NSArray
        let dic = obj[32] as! NSDictionary
        let commHeding = dic.value(forKey: "heading") as! NSArray
        let allDataArr = NSMutableArray()
        let heading = NSMutableArray()
        if(no < 0){ heading.add("cid")}
        for i in 0..<heding.count {
            let k = squ[i]
            heading.add(heding[k])
        }
        let chidchid2 = NSMutableArray()
        if(no < 0){chidchid2.add(cp_id)}
        for j in 0..<heding.count{
            let txt = textFieldArr[j] as? UITextField
            if(j == 20){
                let txtFav = self.textFieldArr[20] as? UITextField
                var fav = txtFav!.text
                if fav!.caseInsensitiveCompare("Satisfied") == .orderedSame{
                    fav = "FAVOR"
                } else if fav!.caseInsensitiveCompare("Not Satisfied") == .orderedSame{
                    fav = "NOT FAVOR"
                } else if fav!.caseInsensitiveCompare("Undecided") == .orderedSame {
                    fav = "UNDECIDED"
                } else if fav!.caseInsensitiveCompare("Never Contacted") == .orderedSame {
                    fav = "NOT AVAILABLE"
                }
                chidchid2.add(fav!)
            }else if(j == 21){
                let txt = self.textFieldArr[21] as? UITextField
                var yard = txt!.text
                if yard!.caseInsensitiveCompare("Open") == .orderedSame{
                    yard = "YES"
                }else if yard!.caseInsensitiveCompare("Closed") == .orderedSame {
                    yard = "NO"
                }
                chidchid2.add(yard!)
            }else if(j == 22){
                chidchid2.add("")
            }else if(j == 32){
                addComment()
                var commDic: [AnyHashable : Any] = [:]
                commDic = ["heading": commHeding,"data": commDataArray]
                chidchid2.add(commDic)
            }else{
                chidchid2.add(txt!.text ?? "")
            }
            
        }
        allDataArr.add(chidchid2)
        print("allDataArr~~~~",allDataArr)
        var grade: [AnyHashable : Any] = [:]
        grade = ["data": allDataArr,"status": "true","msg": "Successfully","cid": cp_id,"id": u_id,"heading": heading]
        print("grade~~~~",grade)
        var _: Error? = nil
        var jsonData: Data? = nil
        do {
            jsonData = try JSONSerialization.data(withJSONObject: grade, options: .prettyPrinted)
        } catch {
        }
        var jsonString: String? = nil
        if let jsonData = jsonData {
            jsonString = String(data: jsonData, encoding: .utf8)
        }
        print("jsonString~~~~",jsonString)
        let postData = jsonString?.data(using: .ascii, allowLossyConversion: true)
        if postData != nil {
            let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
            let documentsDirectory = paths[0]
            let filePath = "\(documentsDirectory)/\("filename.doc")"
            do {
                try postData?.write(to: URL(fileURLWithPath: filePath), options: .atomic)
            } catch {
                print(error)
            }
        }
        callUpdateExcel(jsonData: postData!)
    }
    
    func updateEditView() {
        print(no)
        let heding = jsonObj.value(forKey: "heading") as! NSArray
        let results = jsonObj.value(forKey: "data") as! NSArray
        let obj = results[no] as! NSArray
        let dic = obj[32] as! NSDictionary
        let commHeding = dic.value(forKey: "heading") as! NSArray
        let allDataArr = NSMutableArray()
        //let heading = NSMutableArray()
//       // heading.add("cid")
//        for i in 0..<heding.count {
//            let k = squ[i]
//            heading.add(heding[k])
//        }
        for i in 0..<results.count{
            let chidchid2 = NSMutableArray()
            let valus3 = results[i] as! NSArray
            for j in 0..<valus3.count{
                if(i == no){
                    if j == 0 {
                        let txt = textFieldArr[0] as? UITextField
                        let str = txt?.text
                        chidchid2.add(str ?? "")
                    } else if j == 1 {
                        let txt = textFieldArr[2] as? UITextField
                        let str = txt?.text
                        chidchid2.add(str ?? "")
                    } else if j == 2 {
                        let txt = textFieldArr[3] as? UITextField
                        let str = txt?.text
                        chidchid2.add(str ?? "")
                    } else if j == 3 {
                        let txt = textFieldArr[7] as? UITextField
                        let str = txt?.text
                        chidchid2.add(str ?? "")
                    } else if j == 4 {
                        let txt = textFieldArr[8] as? UITextField
                        let str = txt?.text
                        chidchid2.add(str ?? "")
                    } else if j == 5 {
                        let txt = textFieldArr[11] as? UITextField
                        let str = txt?.text
                        chidchid2.add(str ?? "")
                    }else if j == 6 {
                        let txt = textFieldArr[12] as? UITextField
                        let str = txt?.text
                        chidchid2.add(str ?? "")
                    } else if j == 7 {
                        let txt = textFieldArr[13] as? UITextField
                        let str = txt?.text
                        chidchid2.add(str ?? "")
                    } else if j == 8 {
                        let txt = textFieldArr[14] as? UITextField
                        let str = txt?.text
                        chidchid2.add(str ?? "")
                    } else if j == 9 {
                        let txt = textFieldArr[15] as? UITextField
                        let str = txt?.text
                        chidchid2.add(str ?? "")
                    } else if j == 10 {
                        let txt = textFieldArr[17] as? UITextField
                        let str = txt?.text
                        chidchid2.add(str ?? "")
                    }else if j == 11 {
                        let txt = textFieldArr[19] as? UITextField
                        let str = txt?.text
                        chidchid2.add(str ?? "")
                    } else if j == 12 {
                        let txt = textFieldArr[20] as? UITextField
                        var str = txt?.text
                        if txt?.text?.caseInsensitiveCompare("Satisfied") == .orderedSame {
                            str = "FAVOR"
                        } else if txt?.text?.caseInsensitiveCompare("Not Satisfied") == .orderedSame {
                            str = "NOT FAVOR"
                        } else if txt?.text?.caseInsensitiveCompare("Undecided") == .orderedSame {
                            str = "UNDECIDED"
                        } else if txt?.text?.caseInsensitiveCompare("Never Contacted") == .orderedSame {
                            str = "NOT AVAILABLE"
                        }
                        chidchid2.add(str ?? "")
                    }else if j == 13 {
                        let txt = textFieldArr[21] as? UITextField
                        var str = txt?.text
                        if txt?.text?.caseInsensitiveCompare("Open") == .orderedSame {
                            str = "YES"
                        } else if txt?.text?.caseInsensitiveCompare("Closed") == .orderedSame {
                            str = "NO"
                        }
                        chidchid2.add(str ?? "")
                    } else if j == 14 {
                        let txt = textFieldArr[22] as? UITextField
                        let str = txt?.text
                        chidchid2.add(str ?? "")
                    } else if j == 15 {
                        let txt = textFieldArr[6] as? UITextField
                        let str = txt?.text
                        chidchid2.add(str ?? "")
                    } else if j == 16 {
                        let txt = textFieldArr[4] as? UITextField
                        let str = txt?.text
                        chidchid2.add(str ?? "")
                    } else if j == 17 {
                        let txt = textFieldArr[5] as? UITextField
                        let str = txt?.text
                        chidchid2.add(str ?? "")
                    } else if j == 18 {
                        let txt = textFieldArr[9] as? UITextField
                        let str = txt?.text
                        chidchid2.add(str ?? "")
                    }else if j == 19 {
                        let txt = textFieldArr[10] as? UITextField
                        let str = txt?.text
                        chidchid2.add(str ?? "")
                    } else if j == 20 {
                        let txt = textFieldArr[31] as? UITextField
                        let str = txt?.text
                        chidchid2.add(str ?? "")
                    } else if j == 21 {
                        let txt = textFieldArr[18] as? UITextField
                        let str = txt?.text
                        chidchid2.add(str ?? "")
                    } else if j == 22 {
                        let txt = textFieldArr[24] as? UITextField
                        let str = txt?.text
                        chidchid2.add(str ?? "")
                    } else if j == 23 {
                        let txt = textFieldArr[1] as? UITextField
                        let str = txt?.text
                        chidchid2.add(str ?? "")
                    }else if j == 24 {
                        let txt = textFieldArr[23] as? UITextField
                        let str = txt?.text
                        chidchid2.add(str ?? "")
                    } else if j == 25 {
                        let txt = textFieldArr[25] as? UITextField
                        let str = txt?.text
                        chidchid2.add(str ?? "")
                    } else if j == 26 {
                        let txt = textFieldArr[26] as? UITextField
                        let str = txt?.text
                        chidchid2.add(str ?? "")
                    } else if j == 27 {
                        let txt = textFieldArr[27] as? UITextField
                        let str = txt?.text
                        chidchid2.add(str ?? "")
                    } else if j == 28 {
                        let txt = textFieldArr[28] as? UITextField
                        let str = txt?.text
                        chidchid2.add(str ?? "")
                    }else if j == 29 {
                        let txt = textFieldArr[29] as? UITextField
                        let str = txt?.text
                        chidchid2.add(str ?? "")
                    } else if j == 30 {
                        let txt = textFieldArr[30] as? UITextField
                        let str = txt?.text
                        chidchid2.add(str ?? "")
                    } else if j == 31 {
                        let txt = textFieldArr[16] as? UITextField
                        let str = txt?.text
                        chidchid2.add(str ?? "")
                    }else if(j == 32){
                        addComment()
                        
                        var commDic: [AnyHashable : Any] = [:]
                        commDic = ["heading": commHeding,"data": commDataArray]
                        print("commDataArray--edit",commDataArray)
                        chidchid2.add(commDic)
                    }else{
                        let txt = textFieldArr[j] as? UITextField
                        chidchid2.add(txt?.text ?? "")
                    }
                }else{
                    chidchid2.add(valus3[j])
                }
            }
            
            allDataArr.add(chidchid2)
        }
        
        
        
        var grade: [AnyHashable : Any] = [:]
        grade = ["data": allDataArr,"status": "true","msg": "Successfully","cid": cp_id,"heading": heding]
        jsonObj = grade as NSDictionary
        //print("grade~~~~~~~~~",grade)
        
        var sendreq: [AnyHashable : Any] = [:]
        var result = jsonObj.value(forKey: "data") as? [AnyHashable]
        var bp: [AnyHashable] = []
        if let no = result?[no] {
            bp.append(no)
        }
        print("bp~~~~~~~~~",bp)
        //print("heding~~~~~~~~~",heding)
       // print("heading~~~~~~~~~",heading)
        
        sendreq = ["data": bp,"status": "true","msg": "Successfully","cid": cp_id,"id": u_id,"heading": heding]
        var _: Error? = nil
        var jsonData: Data? = nil
        do {
            jsonData = try JSONSerialization.data(withJSONObject: sendreq, options: .prettyPrinted)
        } catch {
        }
        var jsonString: String? = nil
        if let jsonData = jsonData {
            jsonString = String(data: jsonData, encoding: .utf8)
        }
        let postData = jsonString?.data(using: .ascii, allowLossyConversion: true)
        if postData != nil {
            let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
            let documentsDirectory = paths[0]
            let filePath = "\(documentsDirectory)/\("filename.doc")"
            do {
                try postData?.write(to: URL(fileURLWithPath: filePath), options: .atomic)
            } catch {
                print(error)
            }
        }
        callUpdateExcel(jsonData: postData!)
    }
    
    func addComment() {
        let addCommArr = NSMutableArray()
        let currentDate: Date = Date()
        let stringDate: String = currentDate.app_stringFromDate1()
        
        addCommArr.add("")
        let txtcomm = self.textFieldArr[22] as? UITextField
        addCommArr.add(txtcomm?.text ?? "")
        addCommArr.add(stringDate)
        addCommArr.add(u_id)
        
        let txtFav = self.textFieldArr[20] as? UITextField
        var fav = txtFav!.text
        if fav!.caseInsensitiveCompare("Satisfied") == .orderedSame || fav!.caseInsensitiveCompare("FAVOR") == .orderedSame{
            fav = "FAVOR"
        } else if fav!.caseInsensitiveCompare("Not Satisfied") == .orderedSame || fav!.caseInsensitiveCompare("NOT FAVOR") == .orderedSame{
            fav = "NOT FAVOR"
        } else if fav!.caseInsensitiveCompare("Undecided") == .orderedSame || fav!.caseInsensitiveCompare("UNDECIDED") == .orderedSame{
            fav = "UNDECIDED"
        } else if fav!.caseInsensitiveCompare("Never Contacted") == .orderedSame || fav!.caseInsensitiveCompare("NOT AVAILABLE") == .orderedSame{
            fav = "NOT AVAILABLE"
        } else {
            fav = ""
        }
        let txt = self.textFieldArr[21] as? UITextField
        var yard = txt!.text
        if yard!.caseInsensitiveCompare("Open") == .orderedSame || yard!.caseInsensitiveCompare("YES") == .orderedSame{
            yard = "YES"
        }else if yard!.caseInsensitiveCompare("Closed") == .orderedSame || yard!.caseInsensitiveCompare("NO") == .orderedSame{
            yard = "NO"
        }else if yard!.caseInsensitiveCompare("PLACED") == .orderedSame {
            yard = "PLACED"
        } else {
            yard = ""
        }
        addCommArr.add(fav!)
        addCommArr.add(yard!)
        addCommArr.add(HomeViewController.VC.exel_Id)
        addCommArr.add(cp_id)
        addCommArr.add(USER_DEFAULTS.string(forKey: "userName") ?? "")
        addCommArr.add(custVarHeading1)
        addCommArr.add(custVarHeading2)
        addCommArr.add(custVarHeading3)
        let txtc1 = self.textFieldArr[28] as? UITextField
        let txtc2 = self.textFieldArr[29] as? UITextField
        let txtc3 = self.textFieldArr[30] as? UITextField
        addCommArr.add(txtc1?.text ?? "")
        addCommArr.add(txtc2?.text ?? "")
        addCommArr.add(txtc3?.text ?? "")
        addCommArr.add(false)
        commDataArray.insert(addCommArr, at: 0)
        print("commDataArray~~",commDataArray)
//        commentArray.removeAllObjects()
//        for i in 0..<commDataArray.count{
//            let data = commDataArray[i] as! NSArray
//            var custVar1detail = "";
//            var custVar2detail = "";
//            var custVar3detail = "";
//            let d9 = data[9] as? String ?? ""
//            let d10 = data[10] as? String ?? ""
//            let d11 = data[10] as? String ?? ""
//            if (d9 != "") {custVar1detail = "\n\(data[9]) : \(data[12])"}
//            if (d10 != "") {custVar2detail = "\n\(data[10]) : \(data[13])"}
//            if (d11 != "") {custVar3detail = "\n\(data[11]) : \(data[14])"}
//            let str = "Date : \(data[2])\nUser : \(data[8])\nComment : \(data[1])\nFavorability : \(data[4])\nYard Sign : \(data[5])\(custVar1detail)\(custVar2detail)\(custVar3detail)"
//            //commentArray.add(str)
//           // commTableView.reloadData()
//        }
    }
    func comment(_ str: String?, type isSel: Bool) -> multiSelcted? {
        let comment = multiSelcted()
        comment.txt = str ?? ""
        comment.isSelected = isSel
        return comment
    }
    
    func callUpdateExcel(jsonData:Data){
        let authToken = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
        let param = ["authToken":authToken,"id":u_id,"cid":cp_id]
        var urlStr = ""
        if(no < 0){
            urlStr = "insert_jsonsheet?"
        }else{
            urlStr = "upload_jsonsheet?"
        }
        postWithImage(url_string: urlStr, parameters: param, filePathKey: "file", jsonData: jsonData, CompletionHandler: { json, error in
            if let json = json {
                print("json~~~~~~~",json)
                let statusmassege = json.value(forKey: "status") as! Bool
                if (statusmassege == true){
                    let authToken = json.value(forKey: "authToken") as! String
                    USER_DEFAULTS.set(authToken, forKey: "authToken")
                    if(self.no >= 0){
                        let data = json.value(forKey: "data") as! NSArray
                        let obj = data[0] as! NSArray
                        let dic = obj[32] as! NSDictionary
                        let commArr = dic.value(forKey: "data") as! NSArray
                        self.commentArray.removeAllObjects()
                        self.commDataArray.removeAllObjects()
                        for i in 0..<commArr.count{
                            let c_Array = NSMutableArray()
                            let data = commArr[i] as! NSArray
                            var custVar1detail = "";
                            var custVar2detail = "";
                            var custVar3detail = "";
                            let d9 = data[9] as? String ?? ""
                            let d10 = data[10] as? String ?? ""
                            let d11 = data[11] as? String ?? ""
                            if self.custVarHeading1 != ""{
                                if (d9 != "") {custVar1detail = "\n\(data[9]) : \(data[12])"}
                            }else{
                                custVar1detail = ""
                            }
                            if self.custVarHeading2 != ""{
                                if (d10 != "") {custVar2detail = "\n\(data[10]) : \(data[13])"}
                            }else{
                                custVar2detail = ""
                            }
                            if self.custVarHeading3 != ""{
                                if (d11 != "") {custVar3detail = "\n\(data[11]) : \(data[14])"}
                            }else{
                                custVar3detail = ""
                            }
                            var str = ""
                            if isAppMyPeople {
                                str = "Date : \(data[2])\nUser : \(data[8])\nComment : \(data[1])\nSatisfaction : \(data[4])\nCase Status : \(data[5])\(custVar1detail)\(custVar2detail)\(custVar3detail)"
                            } else {
                                str = "Date : \(data[2])\nUser : \(data[8])\nComment : \(data[1])\nFavorability : \(data[4])\nYard Sign : \(data[5])\(custVar1detail)\(custVar2detail)\(custVar3detail)"
                            }
                            self.commentArray.add(self.comment(str, type: false)!)
                            c_Array.add(data[0]);c_Array.add(data[1]);c_Array.add(data[2]);c_Array.add(data[3]);c_Array.add(data[4]);c_Array.add(data[5]);c_Array.add(data[6]);c_Array.add(data[7]);c_Array.add(data[8]);c_Array.add(data[9]);c_Array.add(data[10]);c_Array.add(data[11]);c_Array.add(data[12]);c_Array.add(data[13]);c_Array.add(data[14]);
                            self.commDataArray.add(c_Array)
                        }
                        
                        let heading = json["heading"] as? [AnyHashable]
                        var results: [AnyHashable]? = []
                        if(isMapHome == 1){
                            results = mapjsonObj.value(forKey: "data") as? [AnyHashable]
                        }else{
                            results = jsonObj.value(forKey: "data") as? [AnyHashable]
                        }
                        var baap: [AnyHashable] = []
                        for i in 0..<(results?.count ?? 0) {
                            var chidchid2: [AnyHashable] = []
                            var valus3 = results?[i] as? [AnyHashable]
                            for j in 0..<(valus3?.count ?? 0) {
                                if i == self.no {
                                    chidchid2.append(obj[j] as! AnyHashable)
                                } else {
                                    if let aValus3 = valus3?[j] {
                                        chidchid2.append(aValus3)
                                    }
                                }
                            }
                            baap.append(chidchid2)
                        }
                        var grade: [AnyHashable : Any] = [:]
                        grade = ["data": baap,"status": "true","msg": "Successfully","cid": self.cp_id,"heading": heading!]
                        if(isMapHome == 1){
                            mapjsonObj = grade as NSDictionary
                        }else{
                            jsonObj = grade as NSDictionary
                        }
                    }
                    DispatchQueue.main.async {
                        var msg = "Case Update."
                        if(self.no < 0){
                            msg = "Do You Want To Add More User?"
                        }
                        let heding = jsonObj.value(forKey: "heading") as! NSArray
                        let alert = UIAlertController(title: "Message", message: msg, preferredStyle: .alert)
                        if self.no < 0 {
                            let saveServer = UIAlertAction(title: "YES", style: .default, handler: { action in
                                for i in 0..<heding.count {
                                    (self.textFieldArr[i] as? UITextField)?.text = ""
                                }
                            })
                            alert.addAction(saveServer)
                        }
                        let No = UIAlertAction(title: self.no < 0 ? "NO" : "OK", style: .default, handler: { action in
                            HomeViewController.VC.SetData()
                            self.navigationController?.popToRootViewController(animated: true)
                        })
                        alert.addAction(No)
                        self.present(alert, animated: true)
                    }
                }else{
                    var is_changed: String? = nil
                    if let object = json["is_changed"] {
                        is_changed = "\(object)"
                    }
                    if is_changed?.isEqual("1") ?? false {
                        logoutAlertMassege()
                    } else {
                        self.alert(message: "No Record Found!", title: "Alert!")
                    }
                }
            }
        })
    }
    
}
