//
//  RearViewController.swift
//  ExelToApp
//
//  Created by baps on 14/01/20.
//  Copyright © 2020 Osiya. All rights reserved.
//

import UIKit

class RearViewController: UIViewController,SWRevealViewControllerDelegate {
    
    var bgImgView:UIImageView!
    var scrollView: UIScrollView!
    var btnView:UIView!
    var btnSearchBy:UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        rootView?.activityStopAnimating()
        type = 0
        self.setButtonView()
        NotificationCenter.default.post(name: Notification.Name("onClickRearView"), object: nil)
    }
    
    
    func setUpView() {
        print("setUpView======------",SWIDTH)
        btnView = UIView()
        btnView.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT)
        self.view.addSubview(btnView)
        
        bgImgView = UIImageView()
        bgImgView.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT)
        bgImgView.image = UIImage(named: "bcakground.jpg")
        btnView.addSubview(bgImgView)
        
        scrollView = UIScrollView(frame: CGRect(x: 0, y: STATUSBARHEIGHT, width: btnView.frame.width, height: btnView.frame.height))
        scrollView.backgroundColor = UIColor.clear
        scrollView.bounces = false
        scrollView.contentSize.height = SHEIGHT
        btnView.addSubview(scrollView)
        scrollView.setContentOffset(.zero, animated: true)
        
        let titlearr = ["Search By","Alphabetically","Filter","Residential","Mailing","Favorability","Map","Name","Precinct","Rating","Street Name","Yard Sign Location","Add Person","All Campaigns","Send CSV","Campaign Manager","Send To Others","Poll Watching Mode","My Reports","Import My Contacts","All Notification","Logout"]
        
        for i in 0..<titlearr.count {
            //btnSearchBy = UIButton()
            btnSearchBy = UIButton(type: .custom)
            btnSearchBy.setButton(X: 20, Y: CGFloat(i*40), Width: self.view.frame.width/2, Height: 32, TextColor: .white, BackColor: .clear, Title: titlearr[i], Align: .left ,Font: .systemFont(ofSize: 16))
            
            if(i == 0 || i == 12 || i == 13 || i == 14 || i == 17 || i == 18 || i == 19 || i == 20 || i == 21){
                btnSearchBy.titleLabel?.font = UIFont(name: "Helvetica-Bold", size: 16)
                btnSearchBy.frame.size = CGSize(width: btnSearchBy.frame.width+20, height: btnSearchBy.frame.height)
                btnSearchBy.backgroundColor = APPBUTTONCOLOR1
            }else if(i == 3 || i == 4){
                btnSearchBy.titleLabel?.font = UIFont(name: "Helvetica-Bold", size: 14)
                btnSearchBy.frame.origin.x = btnSearchBy.frame.minX+40
                btnSearchBy.frame.size = CGSize(width: btnSearchBy.frame.width-20, height: btnSearchBy.frame.height)
                btnSearchBy.backgroundColor = UIColor(red: 78/255.0, green: 170/255.0, blue: 219/255.0, alpha: 1.0)
                if i == 3{
                    isResidential = 2
                }
                if i == 4{
                    isResidential = 0
                }
            }else if (i == 2){//down-arrow.png
                btnSearchBy.titleLabel?.font = UIFont(name: "Helvetica-Bold", size: 14)
                btnSearchBy.frame.origin.x = btnSearchBy.frame.minX+20
                btnSearchBy.backgroundColor = UIColor(red: 78/255.0, green: 170/255.0, blue: 219/255.0, alpha: 1.0)
                btnSearchBy.setImage(UIImage(named: "down-arrow.png"), for: .normal)
                btnSearchBy.setImage(UIImage(named: "up-Arrow"), for: .selected)
                btnSearchBy.tintColor = UIColor.white
                btnSearchBy.imageEdgeInsets = UIEdgeInsets(top: 0, left: btnSearchBy.frame.width-35, bottom: 0, right: 5)
                btnSearchBy.titleEdgeInsets = UIEdgeInsets(top: 0, left: -22, bottom: 0, right: 0)
            }else{
                btnSearchBy.titleLabel?.font = UIFont(name: "Helvetica-Bold", size: 14)
                btnSearchBy.frame.origin.x = btnSearchBy.frame.minX+20
                btnSearchBy.backgroundColor = UIColor(red: 78/255.0, green: 170/255.0, blue: 219/255.0, alpha: 1.0)
            }
            btnSearchBy.layer.cornerRadius = 8;
            btnSearchBy.tag = i;
            btnSearchBy.contentHorizontalAlignment = .left
            btnSearchBy.contentEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0);
            btnSearchBy.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
            scrollView.addSubview(btnSearchBy)
        }
        let dispatchAfter = DispatchTimeInterval.seconds(Int(0.2))
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + dispatchAfter, execute: {
            self.setButtonView()
        })
    }
    
    @objc func onClickButton(_ sender: UIButton) {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "onClickRearView"), object: nil)
        if sender.tag == 2 {
            sender.isSelected = !sender.isSelected
            if sender.isSelected{
                sender.backgroundColor = UIColor.black.withAlphaComponent(0.05)
            }
            let logVC = LoginViewController.VC
            print("logVC.isSelectCrossTab",logVC)
            if(logVC.isSelectCrossTab){
                logVC.isSelectCrossTab = false
            }else{
                logVC.isSelectCrossTab = true
            }
            setButtonView()
        }else{
            sender.isSelected = !sender.isSelected
            if sender.isSelected{
                sender.backgroundColor = UIColor.black.withAlphaComponent(0.05)
            }
            self.revealViewController().revealToggle(animated: true)
            let dispatchAfter = DispatchTimeInterval.seconds(Int(0.2))
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + dispatchAfter, execute: {
                HomeViewController.VC.onClickRearViewButton(tag: sender.tag)
            })
        }
    }
    
    func setButtonView() {
        
        var j = 0;
        let logVC = LoginViewController.VC
        print("---------setButtonView---------",logVC.isMapReq)
        for vw in scrollView.subviews {
            var frame = vw.frame
            frame.origin.y = CGFloat(10 + j * 38)
            vw.frame = frame
            let btn = vw as? UIButton
           // print("vw.frame~~\(j)~~",vw.frame)
            if btn?.tag == 0 || btn?.tag == 12 || btn?.tag == 13 || btn?.tag == 14 || btn?.tag == 17 || btn?.tag == 18 || btn?.tag == 19 || btn?.tag == 20 || btn?.tag == 21 {
                btn?.backgroundColor = UIColor(red: 22 / 255.0, green: 128 / 255.0, blue: 196 / 255.0, alpha: 1)
            } else {
                btn?.backgroundColor = UIColor(red: 78 / 255.0, green: 170 / 255.0, blue: 229 / 255.0, alpha: 1)
            }
            if (!logVC.isMapReq && vw.tag == 6) || (!logVC.isAlphabeticallyReq && (vw.tag == 15 || vw.tag == 16 || vw.tag == 1)) || (!logVC.isCrosstabReq && vw.tag == 2) || (!logVC.isFavorabilityReq && vw.tag == 5) || (!logVC.isMyReportsReq && vw.tag == 18) || (!logVC.isAddPersonReq && vw.tag == 12) || (!logVC.isNameReq && vw.tag == 7) || (!logVC.isPollWatchingModeReq && vw.tag == 17) || (!logVC.isPrecinctReq && vw.tag == 8) || (!logVC.isRatingReq && vw.tag == 9) || (!logVC.isSendReportReq && (vw.tag == 14)) || (!logVC.isStreetNameReq && vw.tag == 10) || (!logVC.isYardSignLocation && vw.tag == 11) || (!((logVC.usertype == "admin") || (logVC.usertype == "4")) && vw.tag == 13) || (isAppMyPeople && (vw.tag == 17))  || (!logVC.isSelectCrossTab && (vw.tag == 3 || vw.tag == 4)) || (!logVC.isImportMyContacts && vw.tag == 19) {
                vw.isHidden = true
            //    print("j~~true~~~~",j)aj_vote
            } else {
                vw.isHidden = false
                j += 1
              //  print("j~~~false~~~~",j)
            }
            
            if isAppMyPeople {
                let but = vw as? UIButton
                if vw.tag == 13 {
                    but?.setTitle("All Districts", for: .normal)
                }
            }
        }
    }
}
