//
//  MapViewController.swift
//  ExelToApp
//
//  Created by baps on 28/01/20.
//  Copyright © 2020 Osiya. All rights reserved.
//

import UIKit
import MapKit
import Cluster
let METERS_PER_MILE = 1000000

class MapViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {
    
    static var VC = MapViewController()
    var mapView:MKMapView!
    var manager = ClusterManager()
    let region = (center: CLLocationCoordinate2D(latitude: 37.787994, longitude: -122.407437), delta: 0.1)
    var page = "1"
    var resultArr = NSArray()
    var mapFilter = 0
    var bgImgView:UIImageView!
    var u_id = USER_DEFAULTS.string(forKey: "u_id") ?? ""
    var cp_id = USER_DEFAULTS.string(forKey: "cp_id") ?? ""
    let detailButton: UIButton = UIButton(type: .detailDisclosure)
    var btnTag = 0
    
    var mainView:UIView!
    var lblHeading:UILabel!
    var lblHeading1:UILabel!
    var popUpView:UIView!
    var topView:UIView!
    var btnCross:UIButton!
    var txtParish:UITextField!
    var txtWard:UITextField!
    var txtPrecinct:UITextField!
    
    var popupView1:UIView!
    var TableView: UITableView = UITableView()
    let cellReuseIdentifier = "cell"
    
    var pollView:UIView!
    var votedView:UIView!
    var btnPYes:UIButton!
    var btnPNo:UIButton!
    var btnPSendCase:UIButton!
    var btnSave:UIButton!
    var btnBackToMap:UIButton!
    
    var pollWatchMode = 0
    
    var btnVoted:UIButton!
    var btnNotVoted:UIButton!
    
    var lblAllCount:UILabel!
    var btnNext:UIButton!
    var btnPrevious:UIButton!
    var Next = ""
    var Previous = ""
    var isSelectVotedBtn = false
    
    var field_startDate = ""
    var field_endDate = ""
    var selectedUserID = ""
    var search_field_value = ""
    var field_name = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        MapViewController.VC = self
        bgImgView = UIImageView()
        bgImgView.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT)
        bgImgView.image = UIImage(named: "bcakground.jpg")
        view.addSubview(bgImgView)
        let NAVHEIGHT = self.navigationController!.navigationBar.frame.maxY
        mapView = MKMapView()
        view.addSubview(mapView)
        mapView.anchor(top: self.view.topAnchor, paddingTop: NAVHEIGHT, bottom: self.view.bottomAnchor, paddingBottom: 0, left: self.view.leftAnchor, paddingLeft: 0, right: self.view.rightAnchor, paddingRight: 0, width: 0, height: 0)
        mapView.delegate = self
        manager.maxZoomLevel = 15
        manager.clusterPosition = .center
        if mapType == 1 || mapType == 3{
            callMapSearch()
        }
        if mapType == 2 {
            setUpView()
        }
        
        
        lblAllCount = UILabel()
        lblAllCount.setLabel(X: 0, Y: NAVHEIGHT, Width: SWIDTH, Height: 30, TextColor: .black, BackColor: .clear, Text: "", TextAlignment: .center, Font: .systemFont(ofSize: 17.0))
        view.addSubview(lblAllCount)
        lblAllCount.isHidden = true
        
        btnNext = UIButton(type: .custom)
        btnNext.setButton(X: SWIDTH-64, Y: SHEIGHT-68, Width: 48, Height: 48, TextColor: .clear, BackColor: .clear, Title: "", Align: .center, Font: .boldSystemFont(ofSize: 22))
        btnNext.tag = 13
        btnNext.setImage(UIImage(named: "rightArrow.png"), for: .normal)
        btnNext.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        view.addSubview(btnNext)
        
        btnPrevious = UIButton(type: .custom)
        btnPrevious.setButton(X: 16, Y: SHEIGHT-68, Width: 48, Height: 48, TextColor: .clear, BackColor: .clear, Title: "", Align: .center, Font: .boldSystemFont(ofSize: 22))
        btnPrevious.tag = 14
        btnPrevious.setImage(UIImage(named: "LeftArrow.png"), for: .normal)
        btnPrevious.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        view.addSubview(btnPrevious)
        
        btnPrevious.isHidden = true
        btnNext.isHidden = true
        
    }
    
    func setUpView() {
        
        let NAVHEIGHT = self.navigationController!.navigationBar.frame.maxY
        
        

        mainView = UIView()
        mainView.frame = CGRect(x: 0, y: NAVHEIGHT, width: SWIDTH, height: SHEIGHT-LoginViewController.VC.NAVHEIGHT-STATUSBARHEIGHT)
        mainView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        view.addSubview(mainView)
        
        
        popUpView = UIView()
        popUpView.frame = CGRect(x: 10, y: 80, width: mainView.frame.width-20, height: 450)
        popUpView.backgroundColor = UIColor.white
        popUpView.layer.cornerRadius = 10.0
        popUpView.clipsToBounds = true
        mainView.addSubview(popUpView)
        
        topView = UIView()
        topView.frame = CGRect(x: 0, y: 0, width: popUpView.frame.width, height: 50)
        topView.backgroundColor = APPBUTTONCOLOR
        popUpView.addSubview(topView)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 30, Y: 0, Width: topView.frame.width-60, Height: 50, TextColor: .white, BackColor: .clear, Text: "Poll Watching Mode", TextAlignment: .center, Font: .boldSystemFont(ofSize: 18.0))
        lblHeading.numberOfLines = 0
        lblHeading.lineBreakMode = .byWordWrapping
        topView.addSubview(lblHeading)
        
        btnCross = UIButton(type: .system)
        btnCross.setButton(X: topView.frame.width-30, Y: 5, Width: 24, Height: 24, TextColor: .white, BackColor: .clear, Title: "X", Align: .center, Font: .boldSystemFont(ofSize: 22))
        btnCross.tag = 1
        btnCross.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        topView.addSubview(btnCross)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 15, Y: topView.frame.maxY+10, Width: popUpView.frame.width-30, Height: 20, TextColor: .black, BackColor: .clear, Text: "County/Parish#", TextAlignment: .center, Font: .systemFont(ofSize: 15.0))
        popUpView.addSubview(lblHeading)
        
        txtParish = UITextField()
        txtParish.setTextField(X: 15, Y: lblHeading.frame.maxY+5, Width: popUpView.frame.width-30, Height: 40, TextColor: .black, BackColor: .clear)
        txtParish.borderStyle = .roundedRect
        txtParish.font = UIFont.systemFont(ofSize: 15)
        txtParish.placeholder = "Select Parish"
        txtParish.text = "All"
        txtParish.delegate = self
        txtParish.textAlignment = .center
        popUpView.addSubview(txtParish)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 15, Y: txtParish.frame.maxY+10, Width: popUpView.frame.width-30, Height: 20, TextColor: .black, BackColor: .clear, Text: "Ward", TextAlignment: .center, Font: .systemFont(ofSize: 15.0))
        popUpView.addSubview(lblHeading)
        
        txtWard = UITextField()
        txtWard.setTextField(X: 15, Y: txtParish.frame.maxY+35, Width: popUpView.frame.width-30, Height: 40, TextColor: .black, BackColor: .clear)
        txtWard.borderStyle = .roundedRect
        txtWard.font = UIFont.systemFont(ofSize: 15)
        txtWard.placeholder = "Select Ward"
        txtWard.text = "All"
        txtWard.delegate = self
        txtWard.textAlignment = .center
        popUpView.addSubview(txtWard)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 15, Y: txtWard.frame.maxY+10, Width: popUpView.frame.width-30, Height: 20, TextColor: .black, BackColor: .clear, Text: "Precinct", TextAlignment: .center, Font: .systemFont(ofSize: 15.0))
        popUpView.addSubview(lblHeading)
        
        txtPrecinct = UITextField()
        txtPrecinct.setTextField(X: 15, Y: lblHeading.frame.maxY+5, Width: popUpView.frame.width-30, Height: 40, TextColor: .black, BackColor: .clear)
        txtPrecinct.borderStyle = .roundedRect
        txtPrecinct.font = UIFont.systemFont(ofSize: 15)
        txtPrecinct.placeholder = "Select Precinct"
        txtPrecinct.text = "All"
        txtPrecinct.delegate = self
        txtPrecinct.textAlignment = .center
        popUpView.addSubview(txtPrecinct)
        
        btnVoted = UIButton()
        btnVoted.setButton(X: popUpView.frame.width/2-(popUpView.frame.width/2)/2, Y: txtPrecinct.frame.maxY+10, Width: popUpView.frame.width/2, Height: 40, TextColor: .black, BackColor: .clear, Title: " Voted", Align: .left,Font: .systemFont(ofSize: 14))
        btnVoted.setImage(UIImage(named: "diselect.png"), for: .normal)
        btnVoted.setImage(UIImage(named: "select.png"), for: .selected)
        btnVoted.tag = 2
        btnVoted.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        popUpView.addSubview(btnVoted)
        
        btnNotVoted = UIButton()
        btnNotVoted.setButton(X: popUpView.frame.width/2-(popUpView.frame.width/2)/2, Y: txtPrecinct.frame.maxY+55, Width: popUpView.frame.width/2, Height: 40, TextColor: .black, BackColor: .clear, Title: " Not Voted", Align: .left,Font: .systemFont(ofSize: 14))
        btnNotVoted.setImage(UIImage(named: "diselect.png"), for: .normal)
        btnNotVoted.setImage(UIImage(named: "select.png"), for: .selected)
        btnNotVoted.tag = 3
        btnNotVoted.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        popUpView.addSubview(btnNotVoted)
        
        
        btnCross = UIButton(type: .system)
        btnCross.setButton(X: popUpView.frame.width/2-(popUpView.frame.width/2)/2, Y: txtPrecinct.frame.maxY+120, Width: popUpView.frame.width/2, Height: 35, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Submit", Align: .center, Font: .systemFont(ofSize: 16))
        btnCross.tag = 4
        btnCross.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        popUpView.addSubview(btnCross)
        
        popupView1 = UIView()
        popupView1.frame = CGRect(x: 10, y: 5, width: mainView.frame.width-20, height: mainView.frame.height-10)
        popupView1.backgroundColor = UIColor.white
        popupView1.layer.cornerRadius = 10.0
        popupView1.layer.shadowRadius  = 1.5
        popupView1.layer.shadowColor   = UIColor(red: 176.0/255.0, green: 199.0/255.0, blue: 226.0/255.0, alpha: 1.0).cgColor
        popupView1.layer.shadowOffset  = CGSize(width: 0.0, height: 0.0)
        popupView1.layer.shadowOpacity = 0.9
        popupView1.layer.masksToBounds = false
        popupView1.clipsToBounds = true
        mainView.addSubview(popupView1)
        
        
        topView = UIView()
        topView.frame = CGRect(x: 0, y: 0, width: popupView1.frame.width, height: 50)
        topView.backgroundColor = APPBUTTONCOLOR
        popupView1.addSubview(topView)
        
        lblHeading1 = UILabel()
        lblHeading1.setLabel(X: 30, Y: 0, Width: topView.frame.width-60, Height: 50, TextColor: .white, BackColor: .clear, Text: "", TextAlignment: .center, Font: .boldSystemFont(ofSize: 18.0))
        lblHeading1.numberOfLines = 0
        lblHeading1.lineBreakMode = .byWordWrapping
        topView.addSubview(lblHeading1)
        
        btnCross = UIButton(type: .system)
        btnCross.setButton(X: topView.frame.width-30, Y: 5, Width: 24, Height: 24, TextColor: .white, BackColor: .clear, Title: "X", Align: .center, Font: .boldSystemFont(ofSize: 22))
        btnCross.tag = 5
        btnCross.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        topView.addSubview(btnCross)
        
        TableView.frame = CGRect(x: 0, y: topView.frame.maxY+10, width: popupView1.frame.width, height: popupView1.frame.height-110)
        TableView.backgroundColor = UIColor.white
        TableView.delegate = self
        TableView.dataSource = self
        TableView.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        popupView1.addSubview(TableView)
        TableView.tableFooterView = UIView()
        TableView.layer.cornerRadius = 10.0
        
        btnCross = UIButton()
        btnCross.setButton(X: popupView1.frame.width/4, Y: TableView.frame.maxY+10, Width: popupView1.frame.width/4, Height: 30, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Cancel", Align: .center, Font: .systemFont(ofSize: 17.0))
        btnCross.tag = 6
        btnCross.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        popupView1.addSubview(btnCross)
        
        btnCross = UIButton()
        btnCross.setButton(X: popupView1.frame.width/2+5, Y: TableView.frame.maxY+10, Width: popupView1.frame.width/4, Height: 30, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Select", Align: .center, Font: .systemFont(ofSize: 17.0))
        btnCross.tag = 7
        btnCross.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        popupView1.addSubview(btnCross)
        popupView1.isHidden = true
        
        pollView = UIView()
        pollView.frame = CGRect(x: 0, y: SHEIGHT/2.8, width: SWIDTH, height: SHEIGHT/1.8)
        pollView.backgroundColor = UIColor(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        mainView.addSubview(pollView)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 0, Y: 8, Width: pollView.frame.width, Height: 25, TextColor: .black, BackColor: .clear, Text: "Poll Watching Mode", TextAlignment: .center, Font: .boldSystemFont(ofSize: 18.0))
        pollView.addSubview(lblHeading)
        
        votedView = UIView()
        votedView.frame = CGRect(x: pollView.frame.width/2-(pollView.frame.width/3)/2, y: lblHeading.frame.maxY+15, width: pollView.frame.width/3, height: 70)
        votedView.backgroundColor = APPBUTTONCOLOR
        votedView.layer.cornerRadius = 15.0
        pollView.addSubview(votedView)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 0, Y: 5, Width: votedView.frame.width, Height: 20, TextColor: .black, BackColor: .clear, Text: "Voted?", TextAlignment: .center, Font: .systemFont(ofSize: 17.0))
        votedView.addSubview(lblHeading)
        
        btnPYes = UIButton()
        btnPYes.setButton(X: 10, Y: lblHeading.frame.maxY+5, Width: votedView.frame.width/2, Height: 35, TextColor: .white, BackColor: .clear, Title: " Yes", Align: .left,Font: .systemFont(ofSize: 14))
        btnPYes.setImage(UIImage(named: "unchecked.png"), for: .normal)
        btnPYes.setImage(UIImage(named: "checked.png"), for: .selected)
        btnPYes.tag = 8
        btnPYes.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        votedView.addSubview(btnPYes)
        
        btnPNo = UIButton()
        btnPNo.setButton(X: btnPYes.frame.maxX+5, Y: lblHeading.frame.maxY+5, Width: votedView.frame.width/2, Height: 35, TextColor: .white, BackColor: .clear, Title: " No", Align: .left,Font: .systemFont(ofSize: 14))
        btnPNo.setImage(UIImage(named: "unchecked.png"), for: .normal)
        btnPNo.setImage(UIImage(named: "checked.png"), for: .selected)
        btnPNo.tag = 9
        btnPNo.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        votedView.addSubview(btnPNo)
        
        
        btnPSendCase = UIButton(type: .system)
        btnPSendCase.setButton(X: pollView.frame.width/2-(pollView.frame.width/1.5)/2, Y: votedView.frame.maxY+15, Width: pollView.frame.width/1.5, Height: 40, TextColor: .black, BackColor: .clear, Title: "Send Case", Align: .center,Font: .systemFont(ofSize: 14))
        btnPSendCase.layer.cornerRadius = 10.0
        btnPSendCase.layer.borderColor = UIColor.lightGray.cgColor
        btnPSendCase.layer.borderWidth = 1.0
        btnPSendCase.tag = 10
        btnPSendCase.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        pollView.addSubview(btnPSendCase)
        
        btnSave = UIButton(type: .system)
        btnSave.setButton(X: pollView.frame.width/2-(pollView.frame.width/1.5)/2, Y: btnPSendCase.frame.maxY+10, Width: pollView.frame.width/1.5, Height: 40, TextColor: .black, BackColor: .clear, Title: "Save", Align: .center,Font: .systemFont(ofSize: 14))
        btnSave.layer.cornerRadius = 10.0
        btnSave.layer.borderColor = UIColor.lightGray.cgColor
        btnSave.layer.borderWidth = 1.0
        btnSave.tag = 11
        btnSave.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        pollView.addSubview(btnSave)
        
        btnBackToMap = UIButton(type: .system)
        btnBackToMap.setButton(X: pollView.frame.width/2-(pollView.frame.width/1.5)/2, Y: btnSave.frame.maxY+10, Width: pollView.frame.width/1.5, Height: 40, TextColor: .black, BackColor: .clear, Title: "Back To Map", Align: .center,Font: .systemFont(ofSize: 14))
        btnBackToMap.layer.cornerRadius = 10.0
        btnBackToMap.layer.borderColor = UIColor.lightGray.cgColor
        btnBackToMap.layer.borderWidth = 1.0
        btnBackToMap.tag = 12
        btnBackToMap.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        pollView.addSubview(btnBackToMap)
        
        pollView.isHidden = true
        
        
    }
    
    @objc func onClickButton(_ sender: UIButton) {
        if sender.tag == 1{ //on Click Cross
            mainView.isHidden = true
        }else if sender.tag == 5 || sender.tag == 6{ //on Click Cross/Cancel PopupView
            popupView1.isHidden = true
        }else if sender.tag == 2 || sender.tag == 3{ //on Click Voted-NotVoted
            sender.isSelected = !sender.isSelected
        }else if sender.tag == 4{ //on Click Submit
            mainView.isHidden = true
            callMapSearch()
        }else if sender.tag == 7{ //on Click slect parsh precinct ward
            var nsm = NSMutableArray()
            if pollWatchMode == 1 {
                nsm = parishArray
            }else if pollWatchMode == 2 {
                nsm = wardArray
            }else {
                nsm = precinctArray
            }
            var j = 0
            var select = ""
            for i in 0..<(nsm.count) {
                let obj = nsm[i] as? multiSelcted
                if ((obj?.isSelected)!){
                    if j == 0 {
                        if let txt = obj?.txt {
                            select = "\(txt)"
                        }
                    } else {
                        if let txt = obj?.txt {
                            select = "\(select),\(txt)"
                        }
                    }
                    j += 1
                }
            }
            if j == 0{
                HomeViewController.VC.alert(message: "Select At Least One", title: "Alert")
            }else{
                if pollWatchMode == 1 {
                    txtParish.text = select.contains("All") ? "All" : select
                    txtWard.text = "All"
                    txtPrecinct.text = "All"
                    for i in 0..<wardArray.count{
                        let comObj = wardArray[i] as! multiSelcted
                        comObj.isSelected = true
                    }
                    c_v.callPollPerishnWardUpdateList(u_id: u_id, cp_id: cp_id)
                }else if pollWatchMode == 2 {
                    txtWard.text = select.contains("All") ? "All" : select
                    txtPrecinct.text = "All"
                    c_v.callPollPerishnWardUpdateList(u_id: u_id, cp_id: cp_id)
                }else {
                    txtPrecinct.text = select.contains("All") ? "All" : select
                }
                popupView1.isHidden = true
            }
        }else if sender.tag == 8{ //on Click Yes
            sender.isSelected = !sender.isSelected
            isSelectVotedBtn = true
            btnPNo.isSelected = false
            isSelectedVotedPoll = "Yes"
        }else if sender.tag == 9{ //on Click No
            sender.isSelected = !sender.isSelected
            isSelectVotedBtn = true
            btnPYes.isSelected = false
            isSelectedVotedPoll = "No"
        }else if sender.tag == 10{ //on Click Send Case
            NotificationCenter.default.post(name: Notification.Name("onclickMap"), object: nil, userInfo: ["type":"1"])
            isSelectVotedBtn = false
        }else if sender.tag == 11{ //on Click Save
            NotificationCenter.default.post(name: Notification.Name("onclickMap"), object: nil, userInfo: ["type":"2"])
            isSelectVotedBtn = false
        }else if sender.tag == 12{ //on Click Back To Map
            self.lblAllCount.isHidden = false
            if isSelectVotedBtn{
                NotificationCenter.default.post(name: Notification.Name("onclickMap"), object: nil, userInfo: ["type":"3","isSelectVoted":"\(isSelectVotedBtn)"])
                self.mainView.isHidden = true
            }else{
                isSelectVotedBtn = false
                NotificationCenter.default.post(name: Notification.Name("onclickMap"), object: nil, userInfo: ["type":"3","isSelectVoted":"\(isSelectVotedBtn)"])
                self.mainView.isHidden = true
            }
            
        }else if sender.tag == 13{ //on Click Next
            page = Next;
            manager.removeAll()
            callMapSearch()
        }else if sender.tag == 14{ //on Click Previeus
            page = Previous
            manager.removeAll()
            callMapSearch()
        }
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        
        if txtParish == textField {
            pollWatchMode = 1
            lblHeading1.text = "County/Parish#"
            if (popupView1.isHidden == true) {
                popupView1.isHidden = false;
            }else{
                popupView1.isHidden = true;
            }
            TableView.reloadData()
        }
        if txtWard == textField {
            pollWatchMode = 2
            lblHeading1.text = "Ward"
            if (popupView1.isHidden == true) {
                popupView1.isHidden = false;
            }else{
                popupView1.isHidden = true;
            }
            TableView.reloadData()
        }
        if txtPrecinct == textField {
            pollWatchMode = 3
            lblHeading1.text = "Precinct"
            if (popupView1.isHidden == true) {
                popupView1.isHidden = false;
            }else{
                popupView1.isHidden = true;
            }
            TableView.reloadData()
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if pollWatchMode == 1 {
            return parishArray.count
        }else if pollWatchMode == 2 {
            return wardArray.count
        }else {
            return precinctArray.count
        }
    }
    
    func newCross(_ str: String?, type isSel: Bool) -> multiSelcted? {
        let comment = multiSelcted()
        comment.txt = str ?? ""
        comment.isSelected = isSel
        return comment
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as UITableViewCell!
        var obj: multiSelcted! = nil
        if pollWatchMode == 1 {
            obj = parishArray[indexPath.row] as? multiSelcted
            // print("obj.txt~~~~~",obj.txt)
            cell.textLabel?.text = obj.txt
            cell.imageView?.image = UIImage(named: obj.isSelected == false ? "diselect128.png" : "select128.png")
        }else if pollWatchMode == 2 {
            obj = wardArray[indexPath.row] as? multiSelcted
            cell.textLabel?.text = obj.txt
            cell.imageView?.image = UIImage(named: obj.isSelected == false ? "diselect128.png" : "select128.png")
        }else if pollWatchMode == 3 {
            obj = precinctArray[indexPath.row] as? multiSelcted
            cell.textLabel?.text = obj.txt
            cell.imageView?.image = UIImage(named: obj.isSelected == false ? "diselect128.png" : "select128.png")
        }
        
        cell.textLabel?.textColor = UIColor.black
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var obj = NSMutableArray()
        if pollWatchMode == 1 {
            obj = parishArray
        }else if pollWatchMode == 2 {
            obj = wardArray
        }else if pollWatchMode == 3 {
            obj = precinctArray
        }
        var com = obj[indexPath.row] as! multiSelcted
        if (com.txt == "All") {
            let comObj = obj[0] as! multiSelcted
            if indexPath.row == 0 && comObj.isSelected == false {
                for i in 0..<obj.count {
                    let comObj = obj[i] as! multiSelcted
                    comObj.isSelected = i == 0 ? !(comObj.isSelected) : true
                }
            } else {
                for i in 0..<obj.count {
                    let comObj = obj[i] as! multiSelcted
                    comObj.isSelected = i == 0 ? !(comObj.isSelected) : false
                }
            }
        } else {
            com.isSelected = !com.isSelected
            com = obj[0] as! multiSelcted
            if (com.txt == "All") {
                com.isSelected = false
            }
        }
        TableView.reloadData()
    }
    
    func addAnnotations() {
        var int1 = 0
        var resArray = [Any]()
        for i in 0..<resultArr.count{
            let values = self.resultArr[i] as! NSArray
            let late = "\(values[1])"
            let late1 = Double(late)
            let long = "\(values[2])"
            let long1 = Double(long)
            print("late1--long1---",late1,long1)
            if late1 != nil && long1 != nil{
                resArray.append(self.resultArr[i] as! NSArray)
                int1 = i
            }
        }
        print("resArray---",resArray)
        let totalCount = int1 == 0 ? 1 : int1
        print("totalCount",totalCount)
        let annotations: [Annotation] = (0..<resArray.count).map { i in
            let annotation = Annotation()
            let values = resArray[i] as! NSArray
            let str = "\(values[3]),\(values[5]),\(values[6]),\(values[4])"
            let late = "\(values[1])"
            let late1 = Double(late)
            let long = "\(values[2])"
            let long1 = Double(long)
            print("late1---\(late1)\n long1----\(long1)")
            if late1 != nil && long1 != nil{
                annotation.coordinate = CLLocationCoordinate2D(latitude: late1!, longitude: long1!)
                annotation.title = "\(values[7])"
                annotation.subtitle = str
            }
            return annotation
        }
        
        self.manager.add(annotations)
        self.manager.reload(mapView: self.mapView)
    }
    
    func setRegion(lat:Double,long:Double) {
        var zoomLocation = CLLocationCoordinate2D()
        zoomLocation.latitude = lat
        zoomLocation.longitude = long
        let viewRegion = MKCoordinateRegion(center: zoomLocation, latitudinalMeters: CLLocationDistance(0.5 * Double(METERS_PER_MILE)), longitudinalMeters: CLLocationDistance(0.5 * Double(METERS_PER_MILE)))
        mapView.setRegion(viewRegion, animated: true)
        
    }
    
    func callMapSearch(){
        let authToken = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
        var urlStr = ""
        var parameter = ""
        if mapType == 1{ // CrossTab
            urlStr = "crosstabsearch_for_all"
            parameter = "\(paramStr)&isMap=1&page=\(page)&authToken=\(authToken)"
        }
        if mapType == 2{ // Poll Watching Mode
            var vote = ""
            if btnVoted.isSelected == true && btnNotVoted.isSelected == true {
                vote = "yes,no"
            }else if btnVoted.isSelected == true{
                vote = "yes"
            }else if btnNotVoted.isSelected == true{
                vote = "no"
            }
            mapFilter = 1
            urlStr = "all_precinct_parish_results_formap_v3"
            parameter = "cid=\(cp_id)&id=\(u_id)&parish_id=\(txtParish.text ?? "")&precinct_id=\(txtPrecinct.text ?? "")&ward=\(txtWard.text ?? "")&search_firstname=&search_lastname=&voted=\(vote)&poll_mode=YES&page=\(page)&authToken=\(authToken)"
        }
        if mapType == 3{ // My Reports mail
            urlStr = "my_Reports_for_map"
            parameter = "cid=\(cp_id)&id=\(u_id)&selected_user_id=\(selectedUserID)&startdate=\(field_startDate)&enddate=\(field_endDate)&search_field_value=\(search_field_value)&field_name=\(field_name)&page=\(page)&authToken=\(authToken)"
            
        }
        postWithoutImage(url_string: urlStr, parameters: parameter,CompletionHandler: { json, error in
            if let json = json {
                let statusmassege = json.value(forKey: "status") as! Bool
                if (statusmassege == true){
                    let authToken = json.value(forKey: "authToken") as! String
                    USER_DEFAULTS.set(authToken, forKey: "authToken")
                    self.resultArr = json.value(forKey: "data") as! NSArray
                    print("json~~~~~map~",json)
                    if let object = json["next"] {
                        self.Next = "\(object)"
                    }
                    if let object = json["previous"] {
                        self.Previous = "\(object)"
                    }
                    
                    if self.Previous.contains("null") {
                        self.btnPrevious.isHidden = true
                    } else {
                        self.btnPrevious.isHidden = false
                    }
                    if self.Next.contains("null") {
                        self.btnNext.isHidden = true
                    } else {
                        self.btnNext.isHidden = false
                    }
                    for i in 0..<self.resultArr.count{
                        let values = self.resultArr[i] as! NSArray
                        if values[1] as! String != ""{
                            self.setRegion(lat: (values[1] as AnyObject).doubleValue, long: (values[2] as AnyObject).doubleValue)
                            break
                        }
                    }
                    self.mapView.removeAnnotations(self.mapView.annotations)
                    self.lblAllCount.isHidden = false
                    self.lblAllCount.text = "\(self.resultArr.count)"
                    self.addAnnotations()
                }else{
                    var is_changed: String? = nil
                    if let object = json["is_changed"] {
                        is_changed = "\(object)"
                    }
                    if is_changed?.isEqual("1") ?? false {
                        logoutAlertMassege()
                    } else {
                        self.alert(message: "No Record Found!", title: "Alert!")
                    }
                }
            }
        })
    }
    
    func callUserData(ids:Int){
        let authToken = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
        postWithoutImage(url_string: "get_user_data", parameters: "excel_id=\(ids)&id=\(u_id)&cid=\(cp_id)&authToken\(authToken)",CompletionHandler: { json, error in
            if let json = json {
                let statusmassege = json.value(forKey: "status") as! Bool
                if (statusmassege == true){
                    let authToken = json.value(forKey: "authToken") as! String
                    USER_DEFAULTS.set(authToken, forKey: "authToken")
                    mapjsonObj = json
                    let NAVHEIGHT = self.navigationController!.navigationBar.frame.maxY
                    let homeView = Home()
                    homeView.btnBackToList.setTitle("Back To Map", for: .normal)
                    homeView.btnPrevious.isHidden = true
                    homeView.btnNext.isHidden = true
                    homeView.SetData()
                    homeView.frame = CGRect(x: 0, y: NAVHEIGHT, width: SWIDTH, height: SHEIGHT)
                    self.view.addSubview(homeView)
                    self.setUpView()
                    self.popUpView.isHidden = true
                    self.mainView.frame = CGRect(x: 0, y: SHEIGHT/2.8+NAVHEIGHT, width: SWIDTH, height: SHEIGHT/1.8)
                    self.pollView.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: self.mainView.frame.height)
                    self.mainView.isHidden = mapType == 2 ?  false : true
                    self.pollView.isHidden = mapType == 2 ?  false : true
                    
                    var results = NSArray()
                    results = mapjsonObj.value(forKey: "data") as! NSArray
                    let obj = results[0] as! NSArray
                    if obj.count > 22 {
                        let voted = "\(obj[22])"
                        if voted.caseInsensitiveCompare("YES") == .orderedSame {
                            self.btnPYes.isSelected = true
                        } else if voted.caseInsensitiveCompare("NO") == .orderedSame {
                            self.btnPNo.isSelected = true
                        } else {
                            self.btnPYes.isSelected = false
                            self.btnPNo.isSelected = false
                        }
                    }
                    
                    
                    
                }else{
                    var is_changed: String? = nil
                    if let object = json["is_changed"] {
                        is_changed = "\(object)"
                    }
                    if is_changed?.isEqual("1") ?? false {
                        logoutAlertMassege()
                    } else {
                        self.alert(message: "No Record Found!", title: "Alert!")
                    }
                }
            }
        })
    }
}

extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? ClusterAnnotation {
            let count = annotation.annotations.count
            var colour = UIColor()
            if count > 1000 {
                colour = UIColor(red: 153/255, green: 13/255, blue: 13/255, alpha: 1.0)
            } else if count > 500 {
                colour = UIColor(red: 128/255, green: 23/255, blue: 51/255, alpha: 1.0)
            } else if count > 200 {
                colour = UIColor(red: 153/255, green: 70/255, blue: 16/255, alpha: 1.0)
            } else if count > 100 {
                colour = UIColor(red: 65/255, green: 153/255, blue: 25/255, alpha: 1.0)
            } else if count > 50 {
                colour = UIColor(red: 21/255, green: 153/255, blue: 90/255, alpha: 1.0)
            } else {
                colour = UIColor(red: 8/255, green: 126/255, blue: 154/255, alpha: 1.0)
            }
            return StyledClusterAnnotationView(annotation: annotation, reuseIdentifier: "cluster", style: .color(colour, radius: 25.0))
        } else {
            let defaultPinID = "pin"
            var pinView: MKPinAnnotationView? = nil
            pinView = mapView.dequeueReusableAnnotationView(withIdentifier: defaultPinID) as? MKPinAnnotationView
            if pinView == nil {
                pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: defaultPinID)
                var int1 = 0
                for i in 0..<resultArr.count{
                    let values = self.resultArr[i] as! NSArray
                    let late = "\(values[1])"
                    let late1 = Double(late)
                    let long = "\(values[2])"
                    let long1 = Double(long)
                    if late1 != nil && long1 != nil{
                        int1 = i
                    }
                }
                for i in 0..<int1{
                    let values = self.resultArr[i] as! [String]
                    if (values[5] == "F") {
                        pinView!.pinTintColor = UIColor(red: 1, green: 0, blue: 1, alpha: 1)
                    } else if (values[5] == "M") {
                        pinView!.pinTintColor = UIColor.blue
                    } else {
                        pinView!.pinTintColor = UIColor.purple
                    }
                    if mapFilter == 1 && values.count > 8{
                        if (values[8] == "YES") {
                            pinView!.pinTintColor = UIColor.green
                        } else if (values[8] == "NO") {
                            pinView!.pinTintColor = UIColor.red
                        } else {
                            pinView!.pinTintColor = UIColor.purple
                        }
                    }
                }
            }
            let detailButton: UIButton = UIButton(type: .detailDisclosure)
            detailButton.tag = btnTag
            detailButton.addTarget(self, action: #selector(mapCallOutPressed(_:)), for: .touchUpInside)
            pinView!.rightCalloutAccessoryView = detailButton;
            pinView!.canShowCallout = true;
            return pinView
        }
    }
    
    
    @IBAction func mapCallOutPressed(_ sender: Any) {
        let values = resultArr[btnTag] as! NSArray
        let exelId = "\(values[0])"
        callUserData(ids:Int(exelId)!)
        self.lblAllCount.isHidden = true
        isMapHome = 1
//        if onSameCordinate(CLLocationCoordinate2DMake(CLLocationDegrees((values[1] as? NSNumber)?.doubleValue), CLLocationDegrees((values[2] as? NSNumber)?.doubleValue))) > 1 {
//            lblTable.text = String(format: "%i Pins", Int(samePin.count))
//            tableParentView.hidden = false
//            tableView.reloadData()
//            btnTableOk.hidden = true
//        } else {
//            dview.userdata((values[0] as? NSNumber)?.intValue)
//            dview.viewPollWatchingMode.hidden = !(nav.maptype == 1)
//        }
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        manager.reload(mapView: mapView) { finished in
            print("regionDidChangeAnimated~~~~~",finished)
        }
    }
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        let searchValue = view.annotation?.title ?? ""
        var currentIndex = 0
        var values = [""]
        for i in 0..<resultArr.count{
            values = self.resultArr[i] as! [String]
            if values[7] == searchValue {
                btnTag = currentIndex
                print("Found \(values[7]) for index \(currentIndex)")
                break
            }
            currentIndex += 1
        }
        guard let annotation = view.annotation else { return }
        if let cluster = annotation as? ClusterAnnotation {
            var zoomRect = MKMapRect.null
            for annotation in cluster.annotations {
                let annotationPoint = MKMapPoint(annotation.coordinate)
                let pointRect = MKMapRect(x: annotationPoint.x, y: annotationPoint.y, width: 0, height: 0)
                if zoomRect.isNull {
                    zoomRect = pointRect
                } else {
                    zoomRect = zoomRect.union(pointRect)
                }
            }
            mapView.setVisibleMapRect(zoomRect, animated: true)
        }
    }
    
    func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
        print("~~~~~~~didAdd~~~~~~",views.count)
        views.forEach { $0.alpha = 0 }
        UIView.animate(withDuration: 0.35, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: [], animations: {
            views.forEach { $0.alpha = 1 }
        }, completion: nil)
    }
}

extension MapViewController: ClusterManagerDelegate {
    func cellSize(for zoomLevel: Double) -> Double? {
        print("~~~~~~~cellSize~~~~~~")
        return nil // default
    }
    func shouldClusterAnnotation(_ annotation: MKAnnotation) -> Bool {
        print("~~~~~~~shouldClusterAnnotation~~~~~~")
        return !(annotation is ClusterAnnotation)
    }
}
