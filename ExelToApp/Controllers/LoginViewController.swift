//
//  ViewController.swift
//  ExelToApp
//
//  Created by baps on 09/01/20.
//  Copyright © 2020 Osiya. All rights reserved.
//

import UIKit
import MapKit
import SafariServices
import Speech
import Contacts


class LoginViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate,SWRevealViewControllerDelegate,SFSafariViewControllerDelegate,SFSpeechRecognizerDelegate,UIGestureRecognizerDelegate {
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var passView: UIView!
    @IBOutlet weak var campignView: UIView!
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtCampName: UITextField!
    @IBOutlet weak var lblMsg: UILabel!
    
    static var VC = LoginViewController()
    
    var locationManager: CLLocationManager!
    
    var scrollView: UIScrollView!
    var btnView:UIView!
    var btnSearchBy:UIButton!
    
    var campView:UIView!
    var lblTitleList:UILabel!
    var campTableview: UITableView = UITableView()
    let cellReuseIdentifier1 = "Cell"
    
    var tableDataArray = NSArray()
    
    var logoView:UIView!
    var bgImgView:UIImageView!
    var lblCampaignName:UILabel!
    var btnVoice:UIButton!
    var logoImg:UIImageView!
    var btnGo:UIButton!
    
    var viewVoiceBack:UIView!
    var viewVoice:UIView!
    var topVoice:UIView!
    var imgVoice:UIImageView!
    var txtvoiceSearch:UITextField!
    var btnVoiceSearch:UIButton!
    var lblHeading:UILabel!
    
    
    
    var dropImgView:UIImageView!
    
    var lat = ""
    var long = ""
    
    var navcon: UINavigationController!
    var window: UIWindow?
    
    
    var AllCampaign = ""
    
    var isMapReq = Bool()
    var isAddPersonReq = Bool()
    var isSendReportReq = Bool()
    var isAlphabeticallyReq = Bool()
    var isCrosstabReq = Bool()
    var isFavorabilityReq = Bool()
    var isMyReportsReq = Bool()
    var isNameReq = Bool()
    var isPollWatchingModeReq = Bool()
    var isPrecinctReq = Bool()
    var isRatingReq = Bool()
    var isStreetNameReq = Bool()
    var isYardSignLocation = Bool()
    var isSelectCrossTab = Bool()
    var isImportMyContacts = Bool()
    var usertype = String()
    var NAVHEIGHT = CGFloat()
    
    let pulsator = Pulsator()
    
    let audioEngine = AVAudioEngine()
    let speechRecognizer: SFSpeechRecognizer? = SFSpeechRecognizer()
    let request = SFSpeechAudioBufferRecognitionRequest()
    var recognitionTask: SFSpeechRecognitionTask?
    var isRecording = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        requestSpeechAuthorization()
        LoginViewController.VC = self
        self.navigationController?.navigationBar.isHidden = true
        callHeader()
        if (CLLocationManager.locationServicesEnabled()){
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
        
        
        isImportContactAll = false
//        isICShowAI = true
        
        
        
        
        if(USER_DEFAULTS.string(forKey: "uname") != nil){
            txtUserName.text = USER_DEFAULTS.string(forKey: "uname") ?? ""
        }
        if(USER_DEFAULTS.string(forKey: "pass") != nil){
            txtPassword.text = USER_DEFAULTS.string(forKey: "pass") ?? ""
        }
        if(USER_DEFAULTS.string(forKey: "cname") != nil){
            txtCampName.text = USER_DEFAULTS.string(forKey: "cname") ?? ""
        }
        
        //-------- Set border color of view ---------
        nameView.layer.borderColor = UIColor(red: 100/255, green: 193/255, blue: 235/255, alpha: 1.0).cgColor
        passView.layer.borderColor = UIColor(red: 100/255, green: 193/255, blue: 235/255, alpha: 1.0).cgColor
        campignView.layer.borderColor = UIColor(red: 100/255, green: 193/255, blue: 235/255, alpha: 1.0).cgColor
        
        setUpView()
        
        btnCheckBox.setImage(UIImage(named: "diselect10.png"), for: .normal)
        btnCheckBox.setImage(UIImage(named: "select10.png"), for: .selected)
        btnCheckBox.contentMode = .scaleToFill
        
        if AllCampaign == "All" {
            self.campView.isHidden = false
            self.callCampaign(u_id: USER_DEFAULTS.string(forKey: "u_id") ?? "")
        }
        
        if AllCampaign == "Icon" {
            logoView.isHidden = false
            lblCampaignName.text = USER_DEFAULTS.string(forKey: "titlecampaign") ?? ""
        }
        
        if isAppMyPeople{
            lblMsg.text = "Enter Your Username, Password And  District Name."
            lblTitleList.text = "District List"
            txtCampName.placeholder = "District Or Group Name"
        }
        
        //let contact = searchForContactUsingPhoneNumber()
        //print("contact~~~~~~",contact)
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        
    }
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        print("gestureRecognizerShouldBegin")
        return false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
//    
//    func searchForContactUsingPhoneNumber(){
//        
//        var result: [CNContact] = []
//        
//        //print("self.contacts~~~~~~~",self.contacts)
//        
//        for contact in self.contacts {
////        for contact in self.contacts {
////            if (!contact.phoneNumbers.isEmpty) {
////                let phoneNumberToCompareAgainst = phoneNumber.components(separatedBy: NSCharacterSet.decimalDigits.inverted).joined(separator: "")
////                //phoneNumber.componentsSeparatedByCharactersIn(NSCharacterSet.decimalDigitCharacterSet.inverted).joinWithSeparator("")
////                for phoneNumber in contact.phoneNumbers {
////                    if let phoneNumberStruct = phoneNumber.value as? CNPhoneNumber {
////                        let phoneNumberString = phoneNumberStruct.stringValue
////                        let phoneNumberToCompare = phoneNumberString.components(separatedBy: NSCharacterSet.decimalDigits.inverted).joined(separator: "")
////                        if phoneNumberToCompare == phoneNumberToCompareAgainst {
////                            result.append(contact)
////                        }
////                    }
////                }
//        }
//        print("result~~~~~~~",result)
//        
//        //return result
    
    
    
    
    //MARK: - Check Authorization Status
    func requestSpeechAuthorization() {
        SFSpeechRecognizer.requestAuthorization { authStatus in
            OperationQueue.main.addOperation {
                switch authStatus {
                case .authorized: break
                case .denied: break
                case .restricted: break
                case .notDetermined: break
                @unknown default:
                    return
                }
            }
        }
    }
    //MARK: - Recognize Speech
    func recordAndRecognizeSpeech() {
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playAndRecord, mode: .default, options: .mixWithOthers)
            try AVAudioSession.sharedInstance().setActive(true)
        } catch let exceptn{
            NSLog("exception: \(exceptn.localizedDescription)")
        }
        let node = audioEngine.inputNode
        //avoiding the crash
        if(node.inputFormat(forBus: 0).channelCount == 0){
            NSLog("Not enough available inputs!")
            alert(message: "There is a problem with the audio", title: "Alert")
            return
        }
        let recordingFormat = node.outputFormat(forBus: 0)
        node.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { buffer, _ in
            self.request.append(buffer)
        }
        audioEngine.prepare()
        do {
            try audioEngine.start()
        } catch {
            //self.alert(message: "There has been an audio engine error.", title: "Speech Recognizer Error")
            return print(error)
        }
        guard let myRecognizer = SFSpeechRecognizer() else {
            self.alert(message: "Speech recognition is not supported for your current locale.", title: "Speech Recognizer Error")
            return
        }
        if !myRecognizer.isAvailable {
            self.alert(message: "Speech recognition is not currently available. Check back at a later time.", title: "Speech Recognizer Error")
            // Recognizer is not available right now
            return
        }
        recognitionTask = speechRecognizer?.recognitionTask(with: request, resultHandler: { result, error in
            if let result = result {
                
                let bestString = result.bestTranscription.formattedString
                var lastString: String = ""
                for segment in result.bestTranscription.segments {
                    let indexTo = bestString.index(bestString.startIndex, offsetBy: segment.substringRange.location)
                    lastString = String(bestString[indexTo...])
                }
                print("lastString~~~~",bestString)
                self.txtvoiceSearch.text = bestString
            } else if let error = error {
                self.alert(message: "here has been a speech recognition error.", title: "Speech Recognizer Error")
                print(error)
            }
        })
    }
    
    
    
    func setUpView() {
        NAVHEIGHT = self.navigationController?.navigationBar.frame.size.height ?? 64.0
        //--------------------This View For Campaign List--------------------------------
        campView = UIView()
        campView.frame = CGRect(x: 0, y: STATUSBARHEIGHT, width: SWIDTH, height: SHEIGHT-STATUSBARHEIGHT)
        self.view.addSubview(campView)
        campView.layer.borderWidth = 1.0
        campView.layer.borderColor = nameView.layer.borderColor
        
        bgImgView = UIImageView()
        bgImgView.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT-STATUSBARHEIGHT)
        bgImgView.image = UIImage(named: "bcakground.jpg")
        campView.addSubview(bgImgView)
        
        lblTitleList = UILabel()
        lblTitleList.setLabel(X: 0, Y: 10, Width: SWIDTH, Height: 30, TextColor: .white, BackColor: .clear, Text: "Campaign List", TextAlignment: .center, Font: .systemFont(ofSize: 28.0))
        campView.addSubview(lblTitleList)
        
        campTableview.frame = CGRect(x: 0, y: lblTitleList.frame.maxY+10, width: SWIDTH, height: SHEIGHT-STATUSBARHEIGHT-30)
        campTableview.delegate = self
        campTableview.dataSource = self
        campTableview.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier1)
        campView.addSubview(campTableview)
        campTableview.tableFooterView = UIView()
        campTableview.backgroundColor = UIColor.clear
        campView.isHidden = true
        
        campTableview.translatesAutoresizingMaskIntoConstraints = false
        campTableview.topAnchor.constraint(equalTo: lblTitleList.bottomAnchor, constant: 5).isActive = true
        campTableview.leftAnchor.constraint(equalTo: campView.leftAnchor, constant: 0).isActive = true
        campTableview.rightAnchor.constraint(equalTo: campView.rightAnchor, constant: 0).isActive = true
        campTableview.bottomAnchor.constraint(equalTo: campView.bottomAnchor, constant: 0).isActive = true
        
        //--------------------This View For Logo And Go Button--------------------------------
        
        logoView = UIView()
        logoView.frame = CGRect(x: 0, y: STATUSBARHEIGHT, width: SWIDTH, height: SHEIGHT-STATUSBARHEIGHT)
        self.view.addSubview(logoView)
        
        bgImgView = UIImageView()
        bgImgView.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT-STATUSBARHEIGHT)
        bgImgView.image = UIImage(named: "bcakground.jpg")
        logoView.addSubview(bgImgView)
        
        lblCampaignName = UILabel()
        lblCampaignName.setLabel(X: 0, Y: 44, Width: SWIDTH, Height: 30, TextColor: .white, BackColor: .clear, Text: "C_Name", TextAlignment: .center, Font: .systemFont(ofSize: 28.0))
        logoView.addSubview(lblCampaignName)
        
        btnVoice = UIButton()
        btnVoice.setButton(X: 0, Y: lblCampaignName.frame.maxY+8, Width: SWIDTH, Height: 100, TextColor: .white, BackColor: .clear, Title: "", Align: .center, Font: .systemFont(ofSize: 16))
        btnVoice.setImage(UIImage(named: "microphone.png"), for: .normal)
        btnVoice.tag = 28
        btnVoice.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        logoView.addSubview(btnVoice)
        
        logoImg = UIImageView()
        logoImg.frame = CGRect(x: 0, y: 0, width: SWIDTH*0.75, height: SHEIGHT-STATUSBARHEIGHT*0.75)
        logoImg.image = UIImage(named: "icon.png")
        logoImg.center = logoView.center
        logoImg.contentMode = .scaleAspectFit
        logoView.addSubview(logoImg)
        
        btnGo = UIButton()
        btnGo = UIButton(type: .system)
        btnGo.setButton(X: SWIDTH/2-100, Y: SHEIGHT-150, Width: 200, Height: 50, TextColor: .white, BackColor: .clear, Title: "Go", Align: .center, Font: .systemFont(ofSize: 16))
        btnGo.backgroundColor = APPBUTTONCOLOR1
        btnGo.titleLabel?.font = .boldSystemFont(ofSize: 28.0)
        btnGo.tag = 27
        btnGo.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        logoView.addSubview(btnGo)
        logoView.isHidden = true
        
        //--------------------This View For Side Menu Buttons--------------------------------
        
        btnView = UIView()
        btnView.frame = CGRect(x: 0, y: STATUSBARHEIGHT, width: SWIDTH, height: SHEIGHT-STATUSBARHEIGHT)
        self.view.addSubview(btnView)
        
        bgImgView = UIImageView()
        bgImgView.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT-STATUSBARHEIGHT)
        bgImgView.image = UIImage(named: "bcakground.jpg")
        btnView.addSubview(bgImgView)
        
        scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: btnView.frame.width, height: btnView.frame.height))
        scrollView.backgroundColor = UIColor.clear
        scrollView.bounces = false
        scrollView.contentSize.height = 700
        btnView.addSubview(scrollView)
        
        let titlearr = ["Search By","Alphabetically","Filter","Residential","Mailing","Favorability","Map","Name","Precinct","Rating","Street Name","Yard Sign Location","Add Person","All Campaigns","Send CSV","Campaign Manager","Send To Others","Poll Watching Mode","My Reports","Import My Contacts","All Notification","Logout"]
        
        for i in 0..<titlearr.count {
            btnSearchBy = UIButton(type: .custom)
            btnSearchBy.setButton(X: 20, Y: CGFloat(i*40), Width: SWIDTH*0.6, Height: 32, TextColor: .white, BackColor: .clear, Title: titlearr[i], Align: .left, Font: .systemFont(ofSize: 16))
            
            if(i == 0 || i == 12 || i == 13 || i == 14 || i == 17 || i == 18 || i == 19 || i == 20 || i == 21){
                btnSearchBy.titleLabel?.font = UIFont(name: "Helvetica-Bold", size: 16)
                btnSearchBy.frame.size = CGSize(width: btnSearchBy.frame.width+20, height: btnSearchBy.frame.height)
                btnSearchBy.backgroundColor = APPBUTTONCOLOR1
            }else if(i == 3 || i == 4){
                btnSearchBy.titleLabel?.font = UIFont(name: "Helvetica-Bold", size: 14)
                btnSearchBy.frame.origin.x = btnSearchBy.frame.minX+40
                btnSearchBy.frame.size = CGSize(width: btnSearchBy.frame.width-20, height: btnSearchBy.frame.height)
                btnSearchBy.backgroundColor = UIColor(red: 78/255.0, green: 170/255.0, blue: 219/255.0, alpha: 1.0)
            }else if (i == 2){//down-arrow.png
                btnSearchBy.titleLabel?.font = UIFont(name: "Helvetica-Bold", size: 14)
                btnSearchBy.frame.origin.x = btnSearchBy.frame.minX+20
                btnSearchBy.backgroundColor = UIColor(red: 78/255.0, green: 170/255.0, blue: 219/255.0, alpha: 1.0)
                btnSearchBy.setImage(UIImage(named: "down-arrow.png"), for: .normal)
                btnSearchBy.setImage(UIImage(named: "up-Arrow"), for: .selected)
                btnSearchBy.tintColor = UIColor.white
                btnSearchBy.imageEdgeInsets = UIEdgeInsets(top: 0, left: btnSearchBy.frame.width-35, bottom: 0, right: 5)
                btnSearchBy.titleEdgeInsets = UIEdgeInsets(top: 0, left: -22, bottom: 0, right: 0)
            }else{
                btnSearchBy.titleLabel?.font = UIFont(name: "Helvetica-Bold", size: 14)
                btnSearchBy.frame.origin.x = btnSearchBy.frame.minX+20
                btnSearchBy.backgroundColor = UIColor(red: 78/255.0, green: 170/255.0, blue: 219/255.0, alpha: 1.0)
            }
            btnSearchBy.layer.cornerRadius = 8;
            btnSearchBy.tag = i;
            btnSearchBy.contentHorizontalAlignment = .left
            btnSearchBy.contentEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0);
            btnSearchBy.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
            scrollView.addSubview(btnSearchBy)
            print("titlearr--\(i)--[\(titlearr[i])]")
        }
        btnView.isHidden = true
        
        
        viewVoiceBack = UIView()
        viewVoiceBack.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT)
        viewVoiceBack.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        logoView.addSubview(viewVoiceBack)
        
        viewVoice = UIView()
        viewVoice.frame = CGRect(x: 40, y: 250, width: SWIDTH-80, height: 240)
        viewVoice.backgroundColor = UIColor.white
        viewVoice.layer.cornerRadius = 10.0
        viewVoice.clipsToBounds = true
        viewVoice.center = viewVoiceBack.center
        viewVoiceBack.addSubview(viewVoice)
        
        topVoice = UIView()
        topVoice.frame = CGRect(x: 0, y: 0, width: viewVoice.frame.width, height: 50)
        topVoice.backgroundColor = APPBUTTONCOLOR
        viewVoice.addSubview(topVoice)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 0, Y: 2, Width: viewVoice.frame.width, Height: 46, TextColor: .white, BackColor: .clear, Text: "Voice Search", TextAlignment: .center, Font: .boldSystemFont(ofSize: 18))
        topVoice.addSubview(lblHeading)
        
        btnVoiceSearch = UIButton(type: .system)
        btnVoiceSearch.setButton(X: topVoice.frame.width-30, Y: 13, Width: 24, Height: 24, TextColor: .white, BackColor: .clear, Title: "X", Align: .center, Font: .boldSystemFont(ofSize: 22))
        btnVoiceSearch.tag = 29
        btnVoiceSearch.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        topVoice.addSubview(btnVoiceSearch)
        
        imgVoice = UIImageView()
        imgVoice.setImageView(X: viewVoice.frame.width/2-30, Y: topVoice.frame.maxY+10, Width: 60, Height: 60, img: UIImage(named: "microphone.png")!)
        imgVoice.setImageColor(color: APPBUTTONCOLOR)
        viewVoice.addSubview(imgVoice)
        
        btnVoiceSearch = UIButton(type: .system)
        btnVoiceSearch.setButton(X: viewVoice.frame.width/2-30, Y: topVoice.frame.maxY+10, Width: 60, Height: 60, TextColor: .white, BackColor: .clear, Title: "", Align: .center, Font: .boldSystemFont(ofSize: 22))
        btnVoiceSearch.tag = 30
        btnVoiceSearch.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        viewVoice.addSubview(btnVoiceSearch)
        
        txtvoiceSearch = UITextField()
        txtvoiceSearch.setTextField(X: 20, Y: imgVoice.frame.maxY+20, Width: viewVoice.frame.width-40, Height: 35, TextColor: .black, BackColor: .clear)
        txtvoiceSearch.borderStyle = .roundedRect
        txtvoiceSearch.placeholder = "Tap Mic Or Type Name"
        viewVoice.addSubview(txtvoiceSearch)
        
        btnVoiceSearch = UIButton(type: .system)
        btnVoiceSearch.setButton(X: viewVoice.frame.width/2-(viewVoice.frame.width/3)/2, Y: txtvoiceSearch.frame.maxY+10, Width: viewVoice.frame.width/3, Height: 35, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Go", Align: .center, Font: .systemFont(ofSize: 16.0))
        btnVoiceSearch.tag = 31
        btnVoiceSearch.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        viewVoice.addSubview(btnVoiceSearch)
        viewVoiceBack.isHidden = true
        
        
        
    }
    
    func setButtonView() {
        var j = 0;
        isMapReq = USER_DEFAULTS.bool(forKey: "isMapReq");
        isAddPersonReq = USER_DEFAULTS.bool(forKey: "isAddPersonReq");
        isSendReportReq = USER_DEFAULTS.bool(forKey: "isSendReportReq");
        isAlphabeticallyReq = USER_DEFAULTS.bool(forKey: "isAlphabeticallyReq")
        isCrosstabReq = USER_DEFAULTS.bool(forKey: "isCrosstabReq");
        isFavorabilityReq = USER_DEFAULTS.bool(forKey: "isFavorabilityReq");
        isMyReportsReq = USER_DEFAULTS.bool(forKey: "isMyReportsReq");
        isNameReq = USER_DEFAULTS.bool(forKey: "isNameReq");
        isPollWatchingModeReq = USER_DEFAULTS.bool(forKey: "isPollWatchingModeReq");
        isPrecinctReq = USER_DEFAULTS.bool(forKey: "isPrecinctReq");
        isRatingReq = USER_DEFAULTS.bool(forKey: "isRatingReq");
        isStreetNameReq = USER_DEFAULTS.bool(forKey: "isStreetNameReq");
        isYardSignLocation = USER_DEFAULTS.bool(forKey: "isYardSignLocation");
        usertype = USER_DEFAULTS.string(forKey: "usertype") ?? ""
        isSelectCrossTab = USER_DEFAULTS.bool(forKey: "isSelectCrossTab")//
        isImportMyContacts = USER_DEFAULTS.bool(forKey: "isImportMyContacts")
        
        for vw in scrollView.subviews {
            var frame = vw.frame
            frame.origin.y = CGFloat(10 + j * 40)
            vw.frame = frame
            let btn = vw as? UIButton
//            let btn = vw as? UIButton
//            if vw.tag == 2{
//                btn?.isSelected = false
            
            if btn?.tag == 0 || btn?.tag == 12 || btn?.tag == 13 || btn?.tag == 14 || btn?.tag == 17 || btn?.tag == 18 || btn?.tag == 19 || btn?.tag == 20 || btn?.tag == 21 {
                btn?.backgroundColor = UIColor(red: 22 / 255.0, green: 128 / 255.0, blue: 196 / 255.0, alpha: 1)
            } else {
                btn?.backgroundColor = UIColor(red: 78 / 255.0, green: 170 / 255.0, blue: 229 / 255.0, alpha: 1)
            }
            if (!isMapReq && vw.tag == 6) || (!isAlphabeticallyReq && (vw.tag == 15 || vw.tag == 16 || vw.tag == 1)) || (!isCrosstabReq && vw.tag == 2) || (!isFavorabilityReq && vw.tag == 5) || (!isMyReportsReq && vw.tag == 18) || (!isAddPersonReq && vw.tag == 12) || (!isNameReq && vw.tag == 7) || (!isPollWatchingModeReq && vw.tag == 17) || (!isPrecinctReq && vw.tag == 8) || (!isRatingReq && vw.tag == 9) || (!isSendReportReq && (vw.tag == 14)) || (!isStreetNameReq && vw.tag == 10) || (!isYardSignLocation && vw.tag == 11) || (!((usertype == "admin") || (usertype == "4")) && vw.tag == 13) || (isAppMyPeople && (vw.tag == 17))  || (!isSelectCrossTab && (vw.tag == 3 || vw.tag == 4)) || (!isImportMyContacts && vw.tag == 19){
                vw.isHidden = true
            } else {
                vw.isHidden = false
                j += 1
            }
            
            if isAppMyPeople {
                let but = vw as? UIButton
                if vw.tag == 13 {
                    but?.setTitle("All Districts", for: .normal)
                }
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return tableDataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier1) as UITableViewCell!
        let arr = tableDataArray[indexPath.row] as! NSArray
        cell.textLabel?.text = arr[1] as? String
        cell.textLabel?.textColor = UIColor.white
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let arr = tableDataArray[indexPath.row] as! NSArray
//        print("arr~~~~~",arr)
        self.lblCampaignName.text = arr[1] as? String
        USER_DEFAULTS.set(self.lblCampaignName.text ?? "", forKey: "titlecampaign")
        USER_DEFAULTS.set(arr[0], forKey: "cp_id")
        campView.isHidden = true
        logoView.isHidden = false
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        let location = locations.last! as CLLocation
        lat = "\(location.coordinate.latitude)"
        long = "\(location.coordinate.longitude)"
    }
    

    @IBAction func onClickButton(_ sender: UIButton) {
        print("~~~~~~~onClickButton~~~~~~")
        if (sender.tag == 0 || sender.tag == 1 || sender.tag == 3 || sender.tag == 4 || sender.tag == 5 || sender.tag == 6 || sender.tag == 7 || sender.tag == 8 || sender.tag == 9 || sender.tag == 10 || sender.tag == 11 || sender.tag == 12 || sender.tag == 13 || sender.tag == 14 || sender.tag == 15 || sender.tag == 16 || sender.tag == 17 || sender.tag == 18 || sender.tag == 19 || sender.tag == 20 || sender.tag == 21){
            sender.isSelected = !sender.isSelected
        }
        
        if sender.isSelected{
            sender.backgroundColor = UIColor.black.withAlphaComponent(0.05)
        }
        if sender.tag == 2 {
            sender.isSelected = !sender.isSelected
            if sender.isSelected{
                sender.backgroundColor = UIColor.black.withAlphaComponent(0.05)
            }
            
            if(isSelectCrossTab){
                USER_DEFAULTS.set(false, forKey: "isSelectCrossTab")
            }else{
                USER_DEFAULTS.set(true, forKey: "isSelectCrossTab")
            }
            setButtonView()
            
        }else if sender.tag == 22 { // CheckBox
            sender.isSelected = !sender.isSelected
        }else if sender.tag == 23 { // Accept Terms Of Service
            showTutorial("https://termsfeed.com/terms-service/2865d2e6285b117edc95e8ef2a150192")
            
        }else if sender.tag == 24 { // LogIn
            if(txtUserName.text == ""  || txtPassword.text == "" ){
                self.alert(message: "Please Insert All Fields", title: "Message")
            }else if(!btnCheckBox.isSelected){
                self.alert(message: "Please Accept Terms Of Service ", title: "Alert")
            }else{
                callLogin(userName: txtUserName.text!, password: txtPassword.text!, campName: txtCampName.text!)
            }
        }else if sender.tag == 25 { // Contact Us To RegisterMyPeople123.com
            showTutorial("https://www.lapublicopinion.net/")
        }else if sender.tag == 26 { // MyPeople123.com
            showTutorial("https://www.lapublicopinion.net/")
        }else if sender.tag == 27 { // On Click Go
            setButtonView()
            logoView.isHidden = true
            btnView.isHidden = false
            //getContact()
            callcustom_variable_all_value()
        }else if sender.tag == 13 { // On Click All Campaign/District
            logoView.isHidden = true
            btnView.isHidden = true
            campView.isHidden = false
            callCampaign(u_id: USER_DEFAULTS.string(forKey: "u_id")!)
        }else if sender.tag == 21 { // On Click Logout
            logoView.isHidden = true
            btnView.isHidden = true
            campView.isHidden = true
        }else if sender.tag == 28 { // On Click Voice Button
            viewVoiceBack.isHidden = false
            txtvoiceSearch.text = ""
        }else if sender.tag == 29 { // On Click close voice view
            txtvoiceSearch.text = ""
            viewVoiceBack.isHidden = true
            recognitionTask?.finish()
            recognitionTask = nil
            // stop audio
            request.endAudio()
            audioEngine.stop()
            audioEngine.inputNode.removeTap(onBus: 0)
        }else if sender.tag == 30 { // On Click MIC
            txtvoiceSearch.text = ""
            if isRecording == true {
                print("stop")
                recognitionTask?.finish()
                recognitionTask = nil
                // stop audio
                request.endAudio()
                audioEngine.stop()
                audioEngine.inputNode.removeTap(onBus: 0)
                isRecording = false
            } else {
                print("start")
                self.recordAndRecognizeSpeech()
                isRecording = true
            }
            
            imgVoice.layer.superlayer?.insertSublayer(pulsator, below: imgVoice.layer)
            pulsator.position = imgVoice.layer.position
            pulsator.numPulse = 4
            pulsator.radius = 50
            pulsator.animationDuration = 4
            pulsator.backgroundColor = APPBUTTONCOLOR.cgColor
            pulsator.start()
            
            let dispatchAfter = DispatchTimeInterval.seconds(Int(5.0))
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + dispatchAfter, execute: {
                self.callcustom_variable_all_value()
                self.pulsator.stop()
                self.recognitionTask?.finish()
                self.recognitionTask = nil
                // stop audio
                self.request.endAudio()
                self.audioEngine.stop()
                self.audioEngine.inputNode.removeTap(onBus: 0)
                self.isRecording = false
                self.pulsator.numPulse = 0
                self.pulsator.radius = 0
                self.pulsator.animationDuration = 0
                print("stop audio")
            })
            
        }else if sender.tag == 31 { // On Click Go voive search
            viewVoiceBack.isHidden = true
            type = 13
            setButtonView()
            USER_DEFAULTS.set(self.txtvoiceSearch.text ?? "", forKey: "voiceSearch")
            let frontNavigationController:UINavigationController
            let rearNavigationController:UINavigationController
            let revealController = SWRevealViewController()
            var mainRevealController = SWRevealViewController()
            frontNavigationController =  UINavigationController(rootViewController: HomeViewController())
            rearNavigationController = UINavigationController(rootViewController: RearViewController())
            rearNavigationController.navigationBar.isHidden = true
            revealController.frontViewController = frontNavigationController
            revealController.rearViewController = rearNavigationController
            revealController.delegate = self
            mainRevealController  = revealController
            navigationController?.pushViewController(mainRevealController, animated: true)
            
        }else if sender.tag == 0 { // On Click Search by
            
        }else {
            let frontNavigationController:UINavigationController
            let rearNavigationController:UINavigationController
            let revealController = SWRevealViewController()
            var mainRevealController = SWRevealViewController()
            frontNavigationController =  UINavigationController(rootViewController: HomeViewController())
            rearNavigationController = UINavigationController(rootViewController: RearViewController())
            rearNavigationController.navigationBar.isHidden = true
            revealController.frontViewController = frontNavigationController
            revealController.rearViewController = rearNavigationController
            revealController.delegate = self
            mainRevealController  = revealController
            let dispatchAfter1 = DispatchTimeInterval.seconds(Int(0.2))
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + dispatchAfter1, execute: {
                self.navigationController?.pushViewController(mainRevealController, animated: true)
            })
            
            
            let dispatchAfter = DispatchTimeInterval.seconds(Int(1.0))
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + dispatchAfter, execute: {
                HomeViewController.VC.onClickRearViewButton(tag: sender.tag)
            })
        }
    }
    
    func showTutorial(_ url_str: String) {
        if let url = URL(string: url_str) {
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = true
            
            let vc = SFSafariViewController(url: url, configuration: config)
            present(vc, animated: true)
        }
    }
    
    // callHeader
    func callHeader(){
        print("callHeader")
        postMethod(url_string: "getHeader_Token", CompletionHandler: { json, error in
            if let json = json {
                print("json~~~~~~~",json)
                let statusmassege = json.value(forKey: "status") as! Bool
                if (statusmassege == true){
                    let jData = json.object(forKey: "data") as! NSDictionary
                    let hValue = jData.object(forKey: "header_value") as? String ?? ""
                    USER_DEFAULTS.set(hValue, forKey: "header_value")
                }else{
                    self.alert(message: error!, title: "Alert!")
                }
            }
        })
    }
    
    // callLogin
    func callLogin(userName:String,password:String,campName:String){
        let deviceID = UserDefaults.standard.value(forKey: "deviceID") as? String ?? ""
        let notification_id = UserDefaults.standard.value(forKey: "token") as? String ?? ""
      //  self.view.activityStartAnimating()
        postWithoutImage(url_string: "login", parameters: "user=\(userName)&password=\(password)&cp_name=\(campName)&device_id=\(deviceID)&device_type=ios&ip_address=&latitude=\(lat)&longitude=\(long)&notification_id=\(notification_id)",CompletionHandler: { json, error in
            if let json = json {
                print("json~~~~~~~",json)
                let statusmassege = json.value(forKey: "status") as! Bool
                if (statusmassege == true){
                    let jsonData = json.value(forKey: "data") as! NSArray
                    let dataDic = jsonData[0] as! NSDictionary
                    let u_id = dataDic.object(forKey: "id") as? String ?? ""
                    let cp_id = dataDic.object(forKey: "cp_id") as? String ?? ""
                    let usertype = dataDic.object(forKey: "usertype") as? String ?? ""
                    let userName = dataDic.object(forKey: "Username") as? String ?? ""
                    print("userName-------",userName)
                    let authToken = json.value(forKey: "authToken") as! String
                    USER_DEFAULTS.set(authToken, forKey: "authToken")
                    USER_DEFAULTS.set(u_id, forKey: "u_id")
                    USER_DEFAULTS.set(cp_id, forKey: "cp_id")
                    USER_DEFAULTS.set(usertype, forKey: "usertype")
                    USER_DEFAULTS.set(userName, forKey: "userName")
                    USER_DEFAULTS.set(self.txtUserName.text!, forKey: "uname")
                    USER_DEFAULTS.set(self.txtPassword.text!, forKey: "pass")
                    USER_DEFAULTS.set(self.txtCampName.text ?? "", forKey: "cname")
                    
                    let UserRights = json.value(forKey: "UserRightsSection") as! NSDictionary
                    USER_DEFAULTS.set(UserRights.object(forKey: "Add Person"), forKey: "isAddPersonReq")
                    USER_DEFAULTS.set(false, forKey: "isAlphabeticallyReq")
                    USER_DEFAULTS.set(false, forKey: "isMapReq")
                    USER_DEFAULTS.set(UserRights.object(forKey: "Send Reports"), forKey: "isSendReportReq")
                    USER_DEFAULTS.set(UserRights.object(forKey: "Edit Cases"), forKey: "isEditReq")
                    USER_DEFAULTS.set(UserRights.object(forKey: "Crosstab"), forKey: "isCrosstabReq")
                    USER_DEFAULTS.set(false, forKey: "isFavorabilityReq")
                    USER_DEFAULTS.set(UserRights.object(forKey: "My Reports"), forKey: "isMyReportsReq")
                    USER_DEFAULTS.set(UserRights.object(forKey: "Name"), forKey: "isNameReq")
                    USER_DEFAULTS.set(UserRights.object(forKey: "Poll Watching Mode"), forKey: "isPollWatchingModeReq")
                    USER_DEFAULTS.set(false, forKey: "isPrecinctReq")
                    USER_DEFAULTS.set(false, forKey: "isRatingReq")
                    USER_DEFAULTS.set(UserRights.object(forKey: "Street Name"), forKey: "isStreetNameReq")
                    USER_DEFAULTS.set(UserRights.object(forKey: "Yard Sign Location"), forKey: "isYardSignLocationReq")
                    USER_DEFAULTS.set(false, forKey: "isSelectCrossTab")
                    USER_DEFAULTS.set(UserRights.object(forKey: "Import My Contacts"), forKey: "isImportMyContacts")
                    
                    if(usertype == "admin" || usertype == "4"){
                        self.callCampaign(u_id: u_id)
                    }else{
                        self.logoView.isHidden = false
                        self.lblCampaignName.text = self.txtCampName.text
                        USER_DEFAULTS.set(self.lblCampaignName.text ?? "", forKey: "titlecampaign")
                    }
                }else{
                    self.alert(message: error!, title: "Alert!")
                }
                self.view.activityStopAnimating()
            }
            
        })
    }
    
    // callCampaign
    func callCampaign(u_id:String){
        let authToken = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
       // self.view.activityStartAnimating()
        postWithoutImage(url_string: "get_all_campaign", parameters: "id=\(u_id)&authToken=\(authToken)",CompletionHandler: { json, error in
            if let json = json {
                print("json~~~~~~~",json)
                let statusmassege = json.value(forKey: "status") as! Bool
                if (statusmassege == true){
                    self.tableDataArray = json.value(forKey: "data") as! NSArray
                    let authToken = json.value(forKey: "authToken") as! String
                    USER_DEFAULTS.set(authToken, forKey: "authToken")
                    self.campView.isHidden = false
                    self.campTableview.reloadData()
                }else{
                    self.alert(message: error!, title: "Alert!")
                }
                self.view.activityStopAnimating()
            }
            
        })
    }
    
    func callcustom_variable_all_value(){
        let authToken = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
        postWithoutImage(url_string: "custom_variable_all_value", parameters: "cid=\(USER_DEFAULTS.string(forKey: "cp_id")!)&id=\(USER_DEFAULTS.string(forKey: "u_id")!)&authToken=\(authToken)",CompletionHandler: { json, error in
            if let json = json {
                print("json~~~~~~~",json)
                let statusmassege = json.value(forKey: "status") as! Bool
                if (statusmassege == true){
                    let authToken = json.value(forKey: "authToken") as! String
                    USER_DEFAULTS.set(authToken, forKey: "authToken")
                    USER_DEFAULTS.set(nil, forKey: "custoVarHeadingOne")
                    USER_DEFAULTS.set(nil, forKey: "arrayCustomVarOne")
                    USER_DEFAULTS.set(nil, forKey: "custoVarHeadingTwo")
                    USER_DEFAULTS.set(nil, forKey: "arrayCustomVarTwo")
                    USER_DEFAULTS.set(nil, forKey: "custoVarHeadingThree")
                    USER_DEFAULTS.set(nil, forKey: "arrayCustomVarThree")
                    USER_DEFAULTS.set(nil, forKey: "arrayVoted")
                    
                    let data = json.object(forKey: "data") as! NSArray
                    var variable_valuesArr = NSArray()
                    var variable_number = String()
                    var custoVarHeading = ""
                    let strArr1 = NSMutableArray()
                    let strArr2 = NSMutableArray()
                    let strArr3 = NSMutableArray()
                    for i in 0..<data.count{
                        let arr = data[i] as! NSArray
                        for i in 0..<arr.count{
                            custoVarHeading = arr[1] as! String
                            variable_number = arr[2] as! String
                            variable_valuesArr = arr[4] as! NSArray;
                        }
                        if (variable_number == "1") {
                            USER_DEFAULTS.set(custoVarHeading, forKey: "custoVarHeadingOne")
                            for i in 0..<variable_valuesArr.count{
                                let str = variable_valuesArr[i] as! String
                                strArr1.add(str)
                                USER_DEFAULTS.set(strArr1, forKey: "arrayCustomVarOne")
                            }
                        }else if (variable_number == "2") {
                            USER_DEFAULTS.set(custoVarHeading, forKey: "custoVarHeadingTwo")
                            for i in 0..<variable_valuesArr.count{
                                let str = variable_valuesArr[i] as! String
                                strArr2.add(str)
                                USER_DEFAULTS.set(strArr2, forKey: "arrayCustomVarTwo")
                            }
                        }else if (variable_number == "3") {
                            USER_DEFAULTS.set(custoVarHeading, forKey: "custoVarHeadingThree")
                            for i in 0..<variable_valuesArr.count{
                                let str = variable_valuesArr[i] as! String
                                strArr3.add(str)
                                USER_DEFAULTS.set(strArr3, forKey: "arrayCustomVarThree")
                            }
                        }
                    }
                    
                    let votedArr = NSMutableArray()
                    let voted = json.object(forKey: "voted") as! NSArray
                    for i in 0..<voted.count{
                        let arr = voted[i] as! String
                        votedArr.add(arr)
                        USER_DEFAULTS.set(votedArr, forKey: "arrayVoted")
                    }
                    
                    let iSImportContactStatus = json.object(forKey: "iSImportContactStatus") as? Int ?? -1
                    print("iSImportContactStatus....",iSImportContactStatus)
                    USER_DEFAULTS.set(iSImportContactStatus, forKey: "iSImportContactStatus")
                    
                }else{
                    USER_DEFAULTS.set(nil, forKey: "custoVarHeadingOne")
                    USER_DEFAULTS.set(nil, forKey: "arrayCustomVarOne")
                    USER_DEFAULTS.set(nil, forKey: "custoVarHeadingTwo")
                    USER_DEFAULTS.set(nil, forKey: "arrayCustomVarTwo")
                    USER_DEFAULTS.set(nil, forKey: "custoVarHeadingThree")
                    USER_DEFAULTS.set(nil, forKey: "arrayCustomVarThree")
                    USER_DEFAULTS.set(nil, forKey: "arrayVoted")
                    
                    var is_changed: String? = nil
                    if let object = json["is_changed"] {
                        is_changed = "\(object)"
                    }
                    if is_changed?.isEqual("1") ?? false {
                        logoutAlertMassege()
                    } else {
                       self.alert(message: "No Record Found!", title: "Alert!")
                    }
                    
                    
                }
            }
        })
    }
}
