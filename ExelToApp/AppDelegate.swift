//
//  AppDelegate.swift
//  ExelToApp
//
//  Created by baps on 09/01/20.
//  Copyright © 2020 Osiya. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import UserNotifications


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var navcon: UINavigationController!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        print("~~~~~~didFinishLaunchingWithOptions~~~~~~~")
        IQKeyboardManager.shared.enable = true
        USER_DEFAULTS.set(0, forKey: "importContactCount")
        USER_DEFAULTS.set(0, forKey: "phoneImportContact")
        
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        UserDefaults.standard.set(deviceID, forKey: "deviceID")
        
        FirebaseApp.configure()
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.badge, .sound],
                                           categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        Messaging.messaging().delegate = self
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let destinationController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        navcon = UINavigationController(rootViewController: destinationController)
        window?.rootViewController = navcon
        window?.makeKeyAndVisible()
        
        
        return true
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return .portrait
    }

    func applicationWillResignActive(_ application: UIApplication) {
        print("~~~~~~applicationWillResignActive~~~~~~~")
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        print("~~~~~~applicationDidEnterBackground~~~~~~~")
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        print("~~~~~~applicationWillEnterForeground~~~~~~~")
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        print("~~~~~~applicationDidBecomeActive~~~~~~~")
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        print("~~~~~~applicationWillTerminate~~~~~~~")
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

}

extension AppDelegate: UNUserNotificationCenterDelegate,MessagingDelegate{
    
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        UserDefaults.standard.set(fcmToken, forKey: "token")
        registerDevice(fcmToken)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    private func application(application: UIApplication,
                             didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        // print("didRegisterForRemoteNotificationsWithDeviceToken")
        Messaging.messaging().apnsToken = deviceToken.base64EncodedData(options: .lineLength64Characters)
    }
    
    func registerDevice(_ token: String){
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        print("open app userinfo", userInfo)
        
        let aps = userInfo["aps"] as? [AnyHashable : Any]
        let alert = aps?["alert"] as? [AnyHashable : Any]
        var body: String? = nil
        if let object = alert?["body"] {
            body = "\(object)"
        }
        var title: String? = nil
        if let object = alert?["title"] {
            title = "\(object)"
        }
        
        print("body~~~~~~\(body ?? "")~~~~~~~~title~~~~~~\(title ?? "")")
        
        let alert1 = UIAlertController(title: title, message: body, preferredStyle: .alert)
        let OK = UIAlertAction(title: "OK", style: .default, handler: { action in
            
        })
        alert1.addAction(OK)
        self.window?.rootViewController?.present(alert1, animated: true)
        
        
        
        completionHandler([.alert, .badge, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        print("app is closed")
        let state = UIApplication.shared.applicationState
        
        if state == .background {
            print("~~~~~~~background~~~~~~~")
            // background
        }
        else if state == .active {
            print("~~~~~~~foreground~~~~~~~")
            // foreground
        }else if state == .inactive {
            print("~~~~~~~inactive~~~~~~~")
            // foreground
        }
        print("response  :",response)
        let userInfo = response.notification.request.content.userInfo
        if let aps = userInfo["aps"] as? NSDictionary {
            if let alertMessage = aps["alert"] as? NSDictionary {
                let title = alertMessage.value(forKey: "title") as? String ?? ""
                let body = alertMessage.value(forKey: "body") as? String ?? ""
                
                let alert = UIAlertController(title: title, message: body, preferredStyle: UIAlertController.Style.alert)
                let action = UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
                    print("handle ok logic")
//                    let rootVC = self.storyboard.instantiateViewController(withIdentifier: "SendRatingViewController") as! SendRatingViewController
//                    let nvc = self.storyboard.instantiateViewController(withIdentifier: "NavigationViewController") as! NavigationViewController
//                    nvc.viewControllers = [rootVC]
//                    UIApplication.shared.keyWindow?.rootViewController = nvc
//                    self.nav.popToRootViewController(animated: true)
                })
                alert.addAction(action)
                self.window?.rootViewController?.present(alert, animated: true, completion: nil)
            }
        }
        completionHandler()
    }
}

