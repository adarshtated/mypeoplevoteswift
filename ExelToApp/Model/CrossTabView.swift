//
//  CrossTabView.swift
//  ExelToApp
//
//  Created by baps on 16/01/20.
//  Copyright © 2020 Osiya. All rights reserved.
//

import UIKit
var c_v = CrossTabView()

class multiSelcted: NSObject {
    var txt = ""
    var isSelected = Bool()
}

class CrossTabView: UIView,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UISearchBarDelegate {
    
    
    var scrollView:UIScrollView!
    
    var lblHeader:UILabel!
    var lblTitle:UILabel!
    var txtField:UITextField!
    var btnCancel:UIButton!
    var btnGo:UIButton!
    var Position = 0
    var titlearr = [""]
    var textFieldArr = NSMutableArray()
    
    //static var c_v = CrossTabView()
    
    var isSelectSendMail = false
    var isSelectGo = Bool()
    
    var popUpView:UIView!
    var btnView:UIView!
    var btns:UIButton!
    var lblTotalNoOP:UILabel!
    var lblTotalNoOfRecord:UILabel!
    var btnPos = 0
    var arrowImg:UIImageView!
    var totalRecord = ""
    
    var crossTabMode = 0
    
    var popupView1:UIView!
    var TableView: UITableView = UITableView()
    let cellReuseIdentifier = "cell"
    var btnCancel1:UIButton!
    var btnSelect:UIButton!
    
    var topView:UIView!
    var lblHeading:UILabel!
    var lblHeading1:UILabel!
    var btnCross:UIButton!
    
    var strNameView:UIView!
    var stTableView: UITableView = UITableView()
    var searchBar:UISearchBar!
    var btnAtoZ:UIButton!
    var btnClose:UIButton!
    var cellReuseIdentifier1 = "Cell"
    
    
    var raceArr = ["All","A","B","H","I","O","W"]
    var partyArr = ["All","DEM","REP","OTH"]
    var genderArr = ["All","Male","Female"]
    var favArr = ["All","Favorable","Not Favorable","Undecided","Not Available"]
    var yardArr = ["All","Yes","No","Placed"]
    
    var raceArr1 = NSMutableArray()
    var partyArr1 =  NSMutableArray()
    var genderArr1 = NSMutableArray()
    var favArr1 = NSMutableArray()
    var yardArr1 = NSMutableArray()
    
    var cityArr = NSMutableArray()
    var zip5Arr = NSMutableArray()
    var zip4Arr = NSMutableArray()
    var ratingArr = NSMutableArray()
    
    var cityZipcodeRatingDic = NSDictionary()
    
    var u_id = USER_DEFAULTS.string(forKey: "u_id") ?? ""
    var cp_id = USER_DEFAULTS.string(forKey: "cp_id") ?? ""
    
    var custVarHeading1 = USER_DEFAULTS.string(forKey: "custoVarHeadingOne") ?? ""
    var custVarHeading2 = USER_DEFAULTS.string(forKey: "custoVarHeadingTwo") ?? ""
    var custVarHeading3 = USER_DEFAULTS.string(forKey: "custoVarHeadingThree") ?? ""
    
    var custVararr1 = USER_DEFAULTS.array(forKey: "arrayCustomVarOne") as NSArray?
    var custVararr2 = USER_DEFAULTS.array(forKey: "arrayCustomVarTwo") as NSArray?
    var custVararr3 = USER_DEFAULTS.array(forKey: "arrayCustomVarThree") as NSArray?
    var votedarr = USER_DEFAULTS.array(forKey: "arrayVoted")! as NSArray
    
    var custVarArr1 = NSMutableArray()
    var custVarArr2 = NSMutableArray()
    var custVarArr3 = NSMutableArray()
    var votedArr  = NSMutableArray()
    
    var usertype = USER_DEFAULTS.string(forKey: "usertype") ?? ""
    
    var isSteetName = 0
    var strDirection = ""
    var strName = ""
    
    var concatArr = [String]()
    var strNameArr = NSMutableArray()
    var strDirArr = NSMutableArray()
    var strCountArr = NSMutableArray()
    var arrayStreetNameBySection = NSMutableArray()
    var arrayStreetNameByRow = NSMutableArray()
    var arrayStreetCountBySection = NSMutableArray()
    var arrayStreetDirBySection = NSMutableArray()
    var arrayAotz = NSMutableArray()
    
    var selectedCrosstabValue = [String]()
    var changePosition = 0
    var isFirstTime = 1
    var isDynamic = ""
    var isFirstTimeSelect = true
    
    
    var Custom_Variable1 = 0
    var Custom_Variable2 = 0
    var Custom_Variable3 = 0
    var Favorability = 0
    var Jurisdiction_Parish = 0
    var Jurisdiction_Precinct = 0
    var Jurisdiction_Ward = 0
    var Personal_Race = 0
    var Personal_Sex = 0
    var Rating = 0
    var Registration_PoliticalPartyCode = 0
    var Residence_City = 0
    var Residence_ZipCode4 = 0
    var Residence_ZipCode5 = 0
    var Yard_Sign = 0
    var voted = 0
    
    var isSelectPopup = 0
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
        
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpView()
    }
    
    func setUpView() {
        
        self.strNameArr.removeAllObjects()
        self.strDirArr.removeAllObjects()
        self.strCountArr.removeAllObjects()
        self.concatArr.removeAll()
        self.genderArr1.removeAllObjects()
        self.raceArr1.removeAllObjects()
        self.partyArr1.removeAllObjects()
        self.favArr1.removeAllObjects()
        self.yardArr1.removeAllObjects()
        self.ratingArr.removeAllObjects()
        self.cityArr.removeAllObjects()
        self.zip5Arr.removeAllObjects()
        self.zip4Arr.removeAllObjects()
        parishArray.removeAllObjects()
        wardArray.removeAllObjects()
        precinctArray.removeAllObjects()
        self.votedArr.removeAllObjects()
        self.custVarArr1.removeAllObjects()
        self.custVarArr2.removeAllObjects()
        self.custVarArr3.removeAllObjects()
        
        setMyPeopleApp()
        scrollView = UIScrollView()
        scrollView.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT)
        scrollView.backgroundColor = UIColor.white
        scrollView.bounces = false
        scrollView.contentSize.height = 850
        self.addSubview(scrollView)
        
        lblHeader = UILabel()
        lblHeader.setLabel(X: 5, Y: 5, Width: SWIDTH, Height: 25, TextColor: .black, BackColor: .clear, Text: "Filter", TextAlignment: .center, Font: .boldSystemFont(ofSize: 18))
        scrollView.addSubview(lblHeader)
        
        btnCancel = UIButton()
        btnCancel.setButton(X: SWIDTH-40, Y: 0, Width: 40, Height: 40, TextColor: APPBUTTONCOLOR, BackColor: .clear, Title: "X", Align: .center, Font: .boldSystemFont(ofSize: 25))
        btnCancel.tag = 2
        btnCancel.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        scrollView.addSubview(btnCancel)
        
        titlearr = ["Age Range","Age Range1","Last Name","Street","City","Zipcode 5","Zipcode 4","Rating","Race","Party Code","Gender","Favorability","Yard Sign","County/Parish#","Ward","Precinct","Voted","custVar1","custVar2","custVar3"]
        Position = 50
        for i in 0..<titlearr.count {
            
            if i == 0 || i == 1{
                lblTitle = UILabel()
                lblTitle.setLabel(X: 5, Y: CGFloat(Position), Width: SWIDTH/3, Height: 30, TextColor: .black, BackColor: .clear, Text: titlearr[i], TextAlignment: .left, Font: .systemFont(ofSize: 15))
                scrollView.addSubview(lblTitle)
                
                txtField = UITextField()
                txtField.setTextField(X: lblTitle.frame.maxX+5, Y: CGFloat(Position), Width: (SWIDTH/1.6)/2-15, Height: 30, TextColor: .black, BackColor: .clear)
                txtField.text = "0"
                
                if i == 1{
                    lblTitle.setLabel(X: lblTitle.frame.maxX+(SWIDTH/1.6)/2-10, Y: CGFloat(Position), Width: 25, Height: 30, TextColor: .black, BackColor: .clear, Text: "To", TextAlignment: .center, Font: .systemFont(ofSize: 15))
                    txtField.setTextField(X: lblTitle.frame.maxX, Y: CGFloat(Position), Width: (SWIDTH/1.6)/2-10, Height: 30, TextColor: .black, BackColor: .clear)
                    txtField.text = "150"
                    Position+=35;
                }
                txtField.borderStyle = .roundedRect
                txtField.font = UIFont.systemFont(ofSize: 15)
                txtField.placeholder = titlearr[i]
                scrollView.addSubview(txtField)
                textFieldArr.add(txtField)
            }else{
                lblTitle = UILabel()
                lblTitle.setLabel(X: 5, Y: CGFloat(Position), Width: SWIDTH/3, Height: 30, TextColor: .black, BackColor: .clear, Text: titlearr[i], TextAlignment: .left, Font: .systemFont(ofSize: 15))
                scrollView.addSubview(lblTitle)
                
                txtField = UITextField()
                txtField.setTextField(X: lblTitle.frame.maxX+5, Y: CGFloat(Position), Width: SWIDTH/1.6, Height: 30, TextColor: .black, BackColor: .clear)
                txtField.borderStyle = .roundedRect
                txtField.font = UIFont.systemFont(ofSize: 15)
                txtField.placeholder = titlearr[i]
                scrollView.addSubview(txtField)
                textFieldArr.add(txtField)
                print("textFieldArr.....",textFieldArr)
                arrowImg = UIImageView()
                arrowImg.setImageView(X: txtField.frame.maxX-28, Y: CGFloat(Position), Width: 24, Height: 24, img: UIImage(named: "downArrow.png")!)
                scrollView.addSubview(arrowImg)
                
                
                if(i == 7){
                    if isAppMyPeople{
                        lblTitle.text = "Group"
                        txtField.placeholder = "Group"
                    }
                }
                if(i == 9){
                    txtField.placeholder = "Political Party Code"
                }
                if(i == 3){
                    txtField.placeholder = "Street Name"
                }
                
                if(i == 11){
                    if isAppMyPeople{
                        lblTitle.text = "Satisfaction"
                        txtField.placeholder = "Satisfaction"
                    }
                }
                if(i == 12){
                    if isAppMyPeople{
                        lblTitle.text = "Case Status"
                        txtField.placeholder = "Case Status"
                    }
                }
                if(i == 2){
                    arrowImg.isHidden = true
                }
                
                if(i == 17){
                    if(custVarHeading1 != ""){
                        lblTitle.text = custVarHeading1
                        txtField.placeholder = ""
                        Position+=35;
                    }else{
                        lblTitle.isHidden = true
                        txtField.isHidden = true
                        arrowImg.isHidden = true
                    }
                }else if (i==18){
                    if(custVarHeading2 != ""){
                        lblTitle.text = custVarHeading2
                        txtField.placeholder = ""
                        Position+=35;
                    }else{
                        lblTitle.isHidden = true
                        txtField.isHidden = true
                        arrowImg.isHidden = true
                    }
                }else if (i==19){
                    if(custVarHeading3 != ""){
                        lblTitle.text = custVarHeading3
                        txtField.placeholder = ""
                        Position+=35;
                    }else{
                        lblTitle.isHidden = true
                        txtField.isHidden = true
                        arrowImg.isHidden = true
                    }
                }else{
                    Position+=35;
                }
                if(i == 3 || i == 4 || i == 5 || i == 6 || i == 7 || i == 8 || i == 9 || i == 10 || i == 11 || i == 12 || i == 13 || i == 14 || i == 15 || i == 16 || i == 17 || i == 18 || i == 19){
                    txtField.delegate = self
                }
            }
        }
        btnGo = UIButton()
        btnGo.setButton(X: SWIDTH/2-75, Y: CGFloat(Position)+20, Width: 150, Height: 35, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Go", Align: .center, Font: .boldSystemFont(ofSize: 18))
        btnGo.tag = 1
        btnGo.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        scrollView.addSubview(btnGo)
        
        
        popupView1 = UIView()
        popupView1.frame = CGRect(x: 10, y: 80, width: SWIDTH-20, height: SHEIGHT/2)
        popupView1.backgroundColor = UIColor.white
        popupView1.layer.cornerRadius = 10.0
        popupView1.layer.shadowRadius  = 1.5
        popupView1.layer.shadowColor   = UIColor(red: 176.0/255.0, green: 199.0/255.0, blue: 226.0/255.0, alpha: 1.0).cgColor
        popupView1.layer.shadowOffset  = CGSize(width: 0.0, height: 0.0)
        popupView1.layer.shadowOpacity = 0.9
        popupView1.layer.masksToBounds = false
        popupView1.clipsToBounds = true
        self.addSubview(popupView1)
        
        topView = UIView()
        topView.frame = CGRect(x: 0, y: 0, width: popupView1.frame.width, height: 50)
        topView.backgroundColor = APPBUTTONCOLOR
        popupView1.addSubview(topView)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 0, Y: 10, Width: popupView1.frame.width, Height: 30, TextColor: .white, BackColor: .clear, Text: "", TextAlignment: .center, Font: .boldSystemFont(ofSize: 18))
        topView.addSubview(lblHeading)
        
        btnCross = UIButton(type: .system)
        btnCross.setButton(X: popupView1.frame.width-30, Y: 5, Width: 24, Height: 24, TextColor: .white, BackColor: .clear, Title: "X", Align: .center, Font: .boldSystemFont(ofSize: 22))
        btnCross.tag = 3
        btnCross.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        topView.addSubview(btnCross)
        
        TableView.frame = CGRect(x: 5, y: topView.frame.maxY+10, width: popupView1.frame.width-10, height: popupView1.frame.height-120)
        TableView.backgroundColor = UIColor.white
        TableView.delegate = self
        TableView.dataSource = self
        TableView.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        popupView1.addSubview(TableView)
        TableView.tableFooterView = UIView()
        TableView.layer.cornerRadius = 10.0
        
        btnCancel1 = UIButton()
        btnCancel1.setButton(X: popupView1.frame.width/4, Y: TableView.frame.maxY+5, Width: popupView1.frame.width/4, Height: 30, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Submit", Align: .center, Font: .systemFont(ofSize: 17.0))
        btnCancel1.tag = 4
        btnCancel1.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        popupView1.addSubview(btnCancel1)
        
        btnSelect = UIButton()
        btnSelect.setButton(X: btnCancel1.frame.maxX+5, Y: TableView.frame.maxY+5, Width: popupView1.frame.width/4, Height: 30, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Cancel", Align: .center, Font: .systemFont(ofSize: 17.0))
        btnSelect.tag = 6
        btnSelect.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        popupView1.addSubview(btnSelect)
        popupView1.isHidden = true
        
        
        strNameView = UIView()
        strNameView.frame = CGRect(x: 5, y: 10, width: SWIDTH-10, height: SHEIGHT-84)
        strNameView.backgroundColor = UIColor.white
        strNameView.layer.cornerRadius = 10.0
        strNameView.clipsToBounds = true
        self.addSubview(strNameView)
        
        topView = UIView()
        topView.frame = CGRect(x: 0, y: 0, width: strNameView.frame.width, height: 40)
        topView.backgroundColor = APPBUTTONCOLOR
        strNameView.addSubview(topView)
        
        lblHeading1 = UILabel()
        lblHeading1.setLabel(X: 0, Y: 10, Width: strNameView.frame.width, Height: 30, TextColor: .white, BackColor: .clear, Text: "", TextAlignment: .center, Font: .boldSystemFont(ofSize: 18))
        topView.addSubview(lblHeading1)
        
        btnCancel = UIButton(type: .system)
        btnCancel.setButton(X: strNameView.frame.width-30, Y: 8, Width: 24, Height: 24, TextColor: .white, BackColor: .clear, Title: "X", Align: .center, Font: .boldSystemFont(ofSize: 22))
        btnCancel.tag = 3
        btnCancel.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        topView.addSubview(btnCancel)
        
        
        searchBar = UISearchBar()
        searchBar.frame = CGRect(x: 0, y: topView.frame.maxY, width: strNameView.frame.width, height: 50)
        searchBar.placeholder = "Search Street Name"
        searchBar.backgroundImage = UIImage(named: "trans.png")
        searchBar.backgroundColor = APPBUTTONCOLOR
        searchBar.delegate = self
        strNameView.addSubview(searchBar)
        
        stTableView.frame = CGRect(x: 0, y: searchBar.frame.maxY, width: strNameView.frame.width, height: strNameView.frame.height-150)
        stTableView.backgroundColor = UIColor.white
        stTableView.delegate = self
        stTableView.dataSource = self
        stTableView.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        strNameView.addSubview(stTableView)
        stTableView.tableFooterView = UIView()
        stTableView.layer.cornerRadius = 10.0
        strNameView.isHidden = true
        
        btnAtoZ = UIButton()
        btnAtoZ.setButton(X: strNameView.frame.width-50, Y: searchBar.frame.maxY+2, Width: 40, Height: 30, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "#", Align: .center, Font: .systemFont(ofSize: 17.0))
        btnAtoZ.tag = 5
        btnAtoZ.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        strNameView.addSubview(btnAtoZ)
        
        btnClose = UIButton()
        btnClose.setButton(X: strNameView.frame.width/2-50, Y: stTableView.frame.maxY+10, Width: 100, Height: 30, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Close", Align: .center, Font: .systemFont(ofSize: 17.0))
        btnClose.tag = 7
        btnClose.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        strNameView.addSubview(btnClose)
        
        let keyboardDoneButtonView = UIToolbar()
        keyboardDoneButtonView.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClicked(_:)))
        keyboardDoneButtonView.items = [flexBarButton, doneButton]
        searchBar.inputAccessoryView = keyboardDoneButtonView
        
        
        
        
        
        
        
        print("isSelectPollParishWard~~~~~~1",isSelectPollParishWard)
        if isSelectPollParishWard == 2 {
            //callPollPerishnWardList(u_id: u_id, cp_id: cp_id)
            //callZipcodeRatingCity(dir: "", strName: "")
            callCrosstabSearchFilter()
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.onClickRearView), name: NSNotification.Name(rawValue: "onClickRearView"), object: nil)
       
    }
    @objc func onClickRearView(notif: NSNotification) {
        self.isHidden = true
    }
    
    @IBAction func doneClicked(_ sender: Any) {
        print("Done Clicked.")
        self.endEditing(true)
    }
    
    func setMyPeopleApp() {
        if isAppMyPeople {
            favArr = ["All","Satisfied","Not Satisfied","Undecided","Never Contacted"]
            yardArr = ["All","Open","Closed"]
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("------textDidChange------")
        var arr = NSMutableArray()
        var arrDir = NSMutableArray()
        var arrCount = NSMutableArray()
        var arrstName = NSMutableArray()
        arrayStreetNameBySection.removeAllObjects()
        arrayStreetDirBySection.removeAllObjects()
        arrayStreetCountBySection.removeAllObjects()
        arrayStreetNameByRow.removeAllObjects()
        arrayAotz.removeAllObjects()
        
        var streetNameArr : [streetName] = []
        for i in 0..<strNameArr.count {
            let str = streetName()
            str.strName = strNameArr[i] as! String
            str.strCount = strCountArr[i] as! String
            str.strDir = strDirArr[i] as! String
            str.strCombine = concatArr[i]
            streetNameArr.append(str)
        }
        var sortedArray = btnAtoZ.titleLabel?.text == "#" ? streetNameArr.sorted{ ($0.strCombine).localizedCaseInsensitiveCompare($1.strCombine) == ComparisonResult.orderedAscending} : streetNameArr.sorted{ ($0.strCount).localizedCaseInsensitiveCompare($1.strCount) == ComparisonResult.orderedAscending}
        sortedArray = sortedArray.filter { ($0.strCombine).localizedCaseInsensitiveContains(searchText) }
        
        print("strName~~~~\(searchText)~")
        
        for i in 0..<sortedArray.count{
            let strName = sortedArray[i].strName
            print("strName~~~~`",strName)
        }
        
        var str = ""
        if (sortedArray.count > 0) {
            str = String(sortedArray[0].strCombine.prefix(1))
            arrayAotz.add(str)
            arrayStreetNameBySection.add(arr)
            arrayStreetDirBySection.add(arrDir)
            arrayStreetCountBySection.add(arrCount)
            arrayStreetNameByRow.add(arrstName)
        }
        for i in 0..<sortedArray.count{
            let str1 = String(sortedArray[i].strCombine.prefix(1))
            let strName = sortedArray[i].strCombine
            let strDir = sortedArray[i].strDir
            let strCount = sortedArray[i].strCount
            let stName = sortedArray[i].strName
            if str != str1{
                arr = NSMutableArray()
                arrayStreetNameBySection.add(arr)
                arrDir = NSMutableArray()
                arrayStreetDirBySection.add(arrDir)
                arrCount = NSMutableArray()
                arrayStreetCountBySection.add(arrCount)
                arrstName = NSMutableArray()
                arrayStreetNameByRow.add(arrstName)
                str = str1
                arrayAotz.add(str)
            }
            arr.add(strName)
            arrDir.add(strDir)
            arrCount.add(strCount)
            arrstName.add(stName)
        }
        stTableView.reloadData()
        if searchText == ""{
            if btnAtoZ.titleLabel?.text == "#"{
                searchBar.text = ""
                self.streetNameDropDown()
            }else{
                searchBar.text = ""
                self.streetCountDropDown()
            }
        }
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.endEditing(true)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let txtRace = self.textFieldArr[8] as? UITextField
        let txtSex = self.textFieldArr[10] as? UITextField
        let txtParty = self.textFieldArr[9] as? UITextField
        let txtFav = self.textFieldArr[11] as? UITextField
        let txtYard = self.textFieldArr[12] as? UITextField
        let txtRating = self.textFieldArr[7] as? UITextField
        let txtStrName = self.textFieldArr[3] as? UITextField
        let txtCity = self.textFieldArr[4] as? UITextField
        let txtZip5 = self.textFieldArr[5] as? UITextField
        let txtZip4 = self.textFieldArr[6] as? UITextField
        let txtParish = self.textFieldArr[13] as? UITextField
        let txtWard = self.textFieldArr[14] as? UITextField
        let txtPrecinct = self.textFieldArr[15] as? UITextField
        let txtVoted = self.textFieldArr[16] as? UITextField
        let txtCustVar1 = self.textFieldArr[17] as? UITextField
        let txtCustVar2 = self.textFieldArr[18] as? UITextField
        let txtCustVar3 = self.textFieldArr[19] as? UITextField
        
        textField.resignFirstResponder()
        popupView1.frame = CGRect(x: 10, y: 80, width: SWIDTH-20, height: SHEIGHT/2)
        TableView.frame = CGRect(x: 5, y: topView.frame.maxY+10, width: popupView1.frame.width-10, height: popupView1.frame.height-120)
        btnCancel1.frame = CGRect(x: popupView1.frame.width/4, y: TableView.frame.maxY+10, width: popupView1.frame.width/4, height: 30)
        btnSelect.frame = CGRect(x: btnCancel1.frame.maxX+5, y: TableView.frame.maxY+10, width: popupView1.frame.width/4, height: 30)
        
        if txtStrName == textField {
            crossTabMode = 1
            strNameView.isHidden = false
            streetNameDropDown()
            lblHeading1.text = "Street Name"
            return false
        }
        if txtCity == textField {
            crossTabMode = 2
            if (popupView1.isHidden == true) {
                popupView1.isHidden = false;
                lblHeading.text = "City"
            }else{
                popupView1.isHidden = true;
            }
            TableView.reloadData()
            return false
        }
        if txtZip5 == textField {
            crossTabMode = 3
            if (popupView1.isHidden == true) {
                popupView1.isHidden = false;
                lblHeading.text = txtZip5?.placeholder
            }else{
                popupView1.isHidden = true;
            }
            TableView.reloadData()
            return false
        }
        if txtZip4 == textField {
            crossTabMode = 4
            if (popupView1.isHidden == true) {
                popupView1.isHidden = false;
                lblHeading.text = txtZip4?.placeholder
            }else{
                popupView1.isHidden = true;
            }
            TableView.reloadData()
            return false
        }
        if txtRating == textField {
            crossTabMode = 5
            if (popupView1.isHidden == true) {
                popupView1.isHidden = false;
                lblHeading.text = isAppMyPeople ? "Group" : "Rating"
            }else{
                popupView1.isHidden = true;
            }
            TableView.reloadData()
            return false
        }
        
        if txtRace == textField {
            crossTabMode = 6
            if (popupView1.isHidden == true) {
                popupView1.isHidden = false;
                lblHeading.text = "Race"
            }else{
                popupView1.isHidden = true;
            }
            TableView.reloadData()
            return false
        }
        if txtParty == textField {
            crossTabMode = 7
            if (popupView1.isHidden == true) {
                popupView1.isHidden = false;
                lblHeading.text = "Political Party Code"
            }else{
                popupView1.isHidden = true;
            }
            TableView.reloadData()
            return false
        }
        if txtSex == textField {
            crossTabMode = 8
            if (popupView1.isHidden == true) {
                popupView1.isHidden = false;
                lblHeading.text = "Gender"
            }else{
                popupView1.isHidden = true;
            }
            TableView.reloadData()
            return false
        }
        if txtFav == textField {
            crossTabMode = 9
            if (popupView1.isHidden == true) {
                popupView1.isHidden = false;
                lblHeading.text = isAppMyPeople ? "Satisfaction" : "Favorability"
            }else{
                popupView1.isHidden = true;
            }
            TableView.reloadData()
            return false
        }
        if txtYard == textField {
            crossTabMode = 10
            if (popupView1.isHidden == true) {
                popupView1.isHidden = false;
                lblHeading.text = isAppMyPeople ? "Case Status" : "Yard Sign"
            }else{
                popupView1.isHidden = true;
            }
            TableView.reloadData()
            return false
        }
        if txtVoted == textField {
            crossTabMode = 14
            if (popupView1.isHidden == true) {
                popupView1.isHidden = false;
                lblHeading.text = "Voted"
            }else{
                popupView1.isHidden = true;
            }
            TableView.reloadData()
            return false
        }
        if txtCustVar1 == textField {
            crossTabMode = 15
            if (popupView1.isHidden == true) {
                popupView1.isHidden = false;
                lblHeading.text = custVarHeading1
            }else{
                popupView1.isHidden = true;
            }
            TableView.reloadData()
            return false
        }
        if txtCustVar2 == textField {
            crossTabMode = 16
            if (popupView1.isHidden == true) {
                popupView1.isHidden = false;
                lblHeading.text = custVarHeading2
            }else{
                popupView1.isHidden = true;
            }
            TableView.reloadData()
            return false
        }
        if txtCustVar3 == textField {
            crossTabMode = 17
            if (popupView1.isHidden == true) {
                popupView1.isHidden = false;
                lblHeading.text = custVarHeading3
            }else{
                popupView1.isHidden = true;
            }
            TableView.reloadData()
            return false
        }
        if txtParish == textField {
            popupView1.frame = CGRect(x: 10, y: 0, width: SWIDTH-20, height: SHEIGHT-50)
            TableView.frame = CGRect(x: 5, y: topView.frame.maxY+10, width: popupView1.frame.width-10, height: popupView1.frame.height-150)
            btnCancel1.frame = CGRect(x: popupView1.frame.width/4, y: TableView.frame.maxY+5, width: popupView1.frame.width/4, height: 30)
            btnSelect.frame = CGRect(x: btnCancel1.frame.maxX+5, y: TableView.frame.maxY+5, width: popupView1.frame.width/4, height: 30)
            crossTabMode = 11
            if (popupView1.isHidden == true) {
                popupView1.isHidden = false;
                lblHeading.text = "County/Parish#"
            }else{
                popupView1.isHidden = true;
            }
            TableView.reloadData()
            return false
        }
        if txtWard == textField {
            popupView1.frame = CGRect(x: 10, y: 0, width: SWIDTH-20, height: SHEIGHT-50)
            TableView.frame = CGRect(x: 5, y: topView.frame.maxY+10, width: popupView1.frame.width-10, height: popupView1.frame.height-150)
            btnCancel1.frame = CGRect(x: popupView1.frame.width/4, y: TableView.frame.maxY+5, width: popupView1.frame.width/4, height: 30)
            btnSelect.frame = CGRect(x: btnCancel1.frame.maxX+5, y: TableView.frame.maxY+5, width: popupView1.frame.width/4, height: 30)
            crossTabMode = 12
            if (popupView1.isHidden == true) {
                popupView1.isHidden = false;
                lblHeading.text = "Ward"
            }else{
                popupView1.isHidden = true;
            }
            TableView.reloadData()
            return false
        }
        if txtPrecinct == textField {
            popupView1.frame = CGRect(x: 10, y: 0, width: SWIDTH-20, height: SHEIGHT-50)
            TableView.frame = CGRect(x: 5, y: topView.frame.maxY+10, width: popupView1.frame.width-10, height: popupView1.frame.height-150)
            btnCancel1.frame = CGRect(x: popupView1.frame.width/4, y: TableView.frame.maxY+5, width: popupView1.frame.width/4, height: 30)
            btnSelect.frame = CGRect(x: btnCancel1.frame.maxX+5, y: TableView.frame.maxY+5, width: popupView1.frame.width/4, height: 30)
            crossTabMode = 13
            if (popupView1.isHidden == true) {
                popupView1.isHidden = false;
                lblHeading.text = "Precinct"
            }else{
                popupView1.isHidden = true;
            }
            TableView.reloadData()
            return false
        }
        return true
    }
    
    func setUpBtnView() {
        print("totalRecord~~~~",totalRecord)
        popUpView = UIView()
        popUpView.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT)
        popUpView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        self.addSubview(popUpView)
         print("popUpView~~~~",popUpView.frame)
        
        
        btnView = UIView()
        var hit = CGFloat()
        if (isSelectSendMail == false) {
            hit = 275
            if usertype == "3"{
                hit = 235
            }
        }else{
            hit = 355
        }
        btnView.frame = CGRect(x: 15, y: 60, width: SWIDTH-30, height: hit)
        btnView.backgroundColor = UIColor.white
        btnView.layer.cornerRadius = 10.0
        btnView.clipsToBounds = true
        popUpView.addSubview(btnView)
        print("btnView~~~~",btnView.frame)
        
        topView = UIView()
        topView.frame = CGRect(x: 0, y: 0, width: popUpView.frame.width, height: 67)
        topView.backgroundColor = APPBUTTONCOLOR
        btnView.addSubview(topView)
        
        lblTotalNoOP = UILabel()
        lblTotalNoOP.setLabel(X: 0, Y: 4, Width: btnView.frame.width, Height: 20, TextColor: .white, BackColor: .clear, Text: "Total Number Of People", TextAlignment: .center, Font: .boldSystemFont(ofSize: 17))
        topView.addSubview(lblTotalNoOP)
        
        lblTotalNoOfRecord = UILabel()
        lblTotalNoOfRecord.setLabel(X: 0, Y: lblTotalNoOP.frame.maxY+5, Width: btnView.frame.width, Height: 35, TextColor: .white, BackColor: .clear, Text: totalRecord, TextAlignment: .center, Font: .systemFont(ofSize: 14))
        lblTotalNoOfRecord.numberOfLines = 0
        topView.addSubview(lblTotalNoOfRecord)
        
        btnCross = UIButton(type: .system)
        btnCross.setButton(X: btnView.frame.width-40, Y: 18, Width: 30, Height: 30, TextColor: .white, BackColor: .clear, Title: "X", Align: .center, Font: .boldSystemFont(ofSize: 22))
        btnCross.tag = 7
        btnCross.addTarget(self, action: #selector(onClickCrossPopUpButton(_:)), for: .touchUpInside)
        topView.addSubview(btnCross)
        
        btnPos = Int(topView.frame.maxY+10)
        var titleArr = [""]
        var str = ""
        var str1 = ""
        if (isSelectSendMail == false) {
            titleArr = ["Alphabetical List","Walk List","Back To Crosstab","Map","Export CSV By Email"]
            if usertype == "3"{
                titleArr = ["Alphabetical List","Walk List","Back To Crosstab","Map"]
            }
        }else{
            str = isResidential == 1 ? "Alphabetically Residential Address" : "Alphabetically Mailing Address"
            str1 = isResidential == 1 ? "Walk List Residential" : "Walk List Mailing"
            titleArr = ["Alphabetical List","Walk List","Back To Crosstab","Map","Export CSV By Email","\(str)","\(str1)"]
            
        }
        
        for i in 0..<titleArr.count {
            btns = UIButton(type: .system)
            btns.setButton(X: 0, Y: CGFloat(btnPos), Width: btnView.frame.width, Height: 40, TextColor: APPBUTTONCOLOR, BackColor: .clear, Title: titleArr[i], Align: .center, Font: .systemFont(ofSize: 15.0))
            btns.layer.borderWidth = 1.0
            btns.layer.borderColor = UIColor(red: 225.0/255.0, green: 232.0/255.0, blue: 237.0/255.0, alpha: 1.0).cgColor
            
            if(i == 5 && titleArr[i] == str){
                btns.setTitleColor(UIColor.black, for: .normal)
            }
            if(i == 6 && titleArr[i] == str1){
                btns.setTitleColor(UIColor.black, for: .normal)
            }
            
            btns.tag = i
            btns.addTarget(self, action: #selector(onClickCrossPopUpButton(_:)), for: .touchUpInside)
            btnView.addSubview(btns)
            btnPos+=39;
//            if(titleArr[i] == "Cancel"){
//                btns.tag = 7
//                btns.layer.borderWidth = 0.0
//                btns.layer.borderColor = UIColor.clear.cgColor
//            }
            if(titleArr[i] == "Export CSV By Email"){
                arrowImg = UIImageView()
                arrowImg.setImageView(X: btns.frame.maxX-38, Y: btns.frame.minY+3, Width: 34, Height: 34, img: isSelectSendMail == false ? UIImage(named: "downArrow.png")! : UIImage(named: "upArrow.png")!)
                btnView.addSubview(arrowImg)
            }
            
        }
    }
    
    @objc func onClickButton(_ sender: UIButton) {
       // print("onClickButton~~~~~~~~")
        let txtAgeLow = self.textFieldArr[0] as? UITextField
        let txtAgeHigh = self.textFieldArr[1] as? UITextField
        let txtLastName = self.textFieldArr[2] as? UITextField
        let txtRace = self.textFieldArr[8] as? UITextField
        let txtSex = self.textFieldArr[10] as? UITextField
        let txtParty = self.textFieldArr[9] as? UITextField
        let txtFav = self.textFieldArr[11] as? UITextField
        let txtYard = self.textFieldArr[12] as? UITextField
        
        let txtRating = self.textFieldArr[7] as? UITextField
        let txtStrName = self.textFieldArr[3] as? UITextField
        let txtCity = self.textFieldArr[4] as? UITextField
        let txtZip5 = self.textFieldArr[5] as? UITextField
        let txtZip4 = self.textFieldArr[6] as? UITextField
        
        let txtParish = self.textFieldArr[13] as? UITextField
        let txtWard = self.textFieldArr[14] as? UITextField
        let txtPrecinct = self.textFieldArr[15] as? UITextField
        let txtVoted = self.textFieldArr[16] as? UITextField
        let txtCustVar1 = self.textFieldArr[17] as? UITextField
        let txtCustVar2 = self.textFieldArr[18] as? UITextField
        let txtCustVar3 = self.textFieldArr[19] as? UITextField
        
        if (sender.tag == 2) { // Cancel crosstab popup
            self.isHidden = true
            HomeViewController.VC.navigationItem.rightBarButtonItem = HomeViewController.VC.btnEditPerson
        }
        if (sender.tag == 1) { // On Click Go
            type = 9
            
            
            paramStr = "Personal_Race=\(txtRace?.text ?? "")&Personal_Age=\(txtAgeLow?.text ?? ""),\(txtAgeHigh?.text ?? "")&Personal_Sex=\(txtSex?.text ?? "")&Registration_PoliticalPartyCode=\(txtParty?.text ?? "")&Favorability=\(txtFav?.text ?? "")&Yard_Sign=\(txtYard?.text ?? "")&ratings=\(txtRating?.text ?? "")"
            paramStr = "\(paramStr)&Residence_StreetName=\(strName)&Residence_City=\(txtCity?.text ?? "")&zipcode4=\(txtZip4?.text ?? "")&zipcode5=\(txtZip5?.text ?? "")&parish_id=\(txtParish?.text ?? "")&precinct_id=\(txtPrecinct?.text ?? "")&ward=\(txtWard?.text ?? "")&Custom_Variable1=\(txtCustVar1?.text ?? "")&Custom_Variable2=\(txtCustVar2?.text ?? "")&Custom_Variable3=\(txtCustVar3?.text ?? "")&Residence_StreetDirection=\(strDirection)&voted=\(txtVoted?.text ?? "")&Personal_LastName=\(txtLastName?.text ?? "")&isResidential=\(isResidential)"
            print("paramStr~~~~~\(type)~~~",paramStr)
            callGetExcel(u_id: u_id, cp_id: cp_id, startRow: 0)
            scrollView.isHidden = true
            isSelectSendMail = false
        }
        if (sender.tag == 3) { // Cancel select popup
            strNameView.isHidden = true
            popupView1.isHidden = true
        }
        if (sender.tag == 6) {
            popupView1.isHidden = true
            ClearTextValueAll()
            //print("selectedCrosstabValue~~~~~0~",selectedCrosstabValue)
            selectedCrosstabValue.removeLast()
           // print("selectedCrosstabValue~~~~~1~",selectedCrosstabValue)
        }
        if (sender.tag == 5) { // onClick A-Z
            if btnAtoZ.titleLabel?.text == "ABC"{
                searchBar.text = ""
                self.streetNameDropDown()
                btnAtoZ.setTitle("#", for: .normal)
            }else{
                searchBar.text = ""
                self.streetCountDropDown()
                btnAtoZ.setTitle("ABC", for: .normal)
            }
        }
        if (sender.tag == 7) { // onClick Close street name search popup
//            let txtStrName = self.textFieldArr[3] as? UITextField
//            txtStrName?.text = ""
            strDirection = ""
            strName = ""
            ClearTextValueAll()
            print("selectedCrosstabValue~~~~~07~",selectedCrosstabValue)
            selectedCrosstabValue.removeLast()
            print("selectedCrosstabValue~~~~~17~",selectedCrosstabValue)
            strNameView.isHidden = true
        }
        if (sender.tag == 4) { // select popup
            print("~~~~~~~~select popup~~~~~~~")
            
//            (Residence_City == 0 || Residence_ZipCode5 == 0 || Residence_ZipCode4 == 0 || Rating == 0 || Personal_Race == 0 || Registration_PoliticalPartyCode == 0 || Personal_Sex == 0 || Favorability == 0 || Yard_Sign == 0 || Jurisdiction_Parish == 0 || Jurisdiction_Ward == 0 || Jurisdiction_Precinct == 0 || Custom_Variable1 == 0 || Custom_Variable2 == 0 || Custom_Variable3 == 0 || voted == 0)
            
            var nsm = NSMutableArray()
            if crossTabMode == 2 {
                isSelectPopup = Residence_City
                nsm = cityArr
                if (!selectedCrosstabValue.contains("city")){
                    selectedCrosstabValue.append("city")
                    for i in 0..<selectedCrosstabValue.count{
                        if selectedCrosstabValue[i] == "city"{
                            changePosition = i
                        }
                    }
                }else{
                    for i in 0..<selectedCrosstabValue.count{
                        if selectedCrosstabValue[i] == "city"{
                            changePosition = i
                        }
                    }
                }
                
            }else if crossTabMode == 3 {
                isSelectPopup = Residence_ZipCode5
                nsm = zip5Arr
                if (!selectedCrosstabValue.contains("Zipcode5")){
                    selectedCrosstabValue.append("Zipcode5")
                    for i in 0..<selectedCrosstabValue.count{
                        if selectedCrosstabValue[i] == "Zipcode5"{
                            changePosition = i
                        }
                    }
                }else{
                    for i in 0..<selectedCrosstabValue.count{
                        if selectedCrosstabValue[i] == "Zipcode5"{
                            changePosition = i
                        }
                    }
                }
            }else if crossTabMode == 4 {
                isSelectPopup = Residence_ZipCode4
                nsm = zip4Arr
                if (!selectedCrosstabValue.contains("Zipcode4")){
                    selectedCrosstabValue.append("Zipcode4")
                    for i in 0..<selectedCrosstabValue.count{
                        if selectedCrosstabValue[i] == "Zipcode4"{
                            changePosition = i
                        }
                    }
                }else{
                    for i in 0..<selectedCrosstabValue.count{
                        if selectedCrosstabValue[i] == "Zipcode4"{
                            changePosition = i
                        }
                    }
                }
            }else if crossTabMode == 5 {
                isSelectPopup = Rating
                nsm = ratingArr
                if (!selectedCrosstabValue.contains("rating")){
                    selectedCrosstabValue.append("rating")
                    for i in 0..<selectedCrosstabValue.count{
                        if selectedCrosstabValue[i] == "rating"{
                            changePosition = i
                        }
                    }
                }else{
                    for i in 0..<selectedCrosstabValue.count{
                        if selectedCrosstabValue[i] == "rating"{
                            changePosition = i
                        }
                    }
                }
            }else if crossTabMode == 6 {
                isSelectPopup = Personal_Race
                nsm = raceArr1
                if (!selectedCrosstabValue.contains("race")){
                    selectedCrosstabValue.append("race")
                    for i in 0..<selectedCrosstabValue.count{
                        if selectedCrosstabValue[i] == "race"{
                            changePosition = i
                        }
                    }
                }else{
                    for i in 0..<selectedCrosstabValue.count{
                        if selectedCrosstabValue[i] == "race"{
                            changePosition = i
                        }
                    }
                }
            }else if crossTabMode == 7 {
                isSelectPopup = Registration_PoliticalPartyCode
                nsm = partyArr1
                if (!selectedCrosstabValue.contains("party")){
                    selectedCrosstabValue.append("party")
                    for i in 0..<selectedCrosstabValue.count{
                        if selectedCrosstabValue[i] == "party"{
                            changePosition = i
                        }
                    }
                }else{
                    for i in 0..<selectedCrosstabValue.count{
                        if selectedCrosstabValue[i] == "party"{
                            changePosition = i
                        }
                    }
                }
            }else if crossTabMode == 8 {
                isSelectPopup = Personal_Sex
                nsm = genderArr1
                if (!selectedCrosstabValue.contains("gender")){
                    selectedCrosstabValue.append("gender")
                    for i in 0..<selectedCrosstabValue.count{
                        if selectedCrosstabValue[i] == "gender"{
                            changePosition = i
                        }
                    }
                }else{
                    for i in 0..<selectedCrosstabValue.count{
                        if selectedCrosstabValue[i] == "gender"{
                            changePosition = i
                        }
                    }
                }
            }else if crossTabMode == 9 {
                isSelectPopup = Favorability
                nsm = favArr1
                if (!selectedCrosstabValue.contains("favorability")){
                    selectedCrosstabValue.append("favorability")
                    for i in 0..<selectedCrosstabValue.count{
                        if selectedCrosstabValue[i] == "favorability"{
                            changePosition = i
                        }
                    }
                }else{
                    for i in 0..<selectedCrosstabValue.count{
                        if selectedCrosstabValue[i] == "favorability"{
                            changePosition = i
                        }
                    }
                }
            }else if crossTabMode == 10 {
                isSelectPopup = Yard_Sign
                nsm = yardArr1
                if (!selectedCrosstabValue.contains("yardsign")){
                    selectedCrosstabValue.append("yardsign")
                    for i in 0..<selectedCrosstabValue.count{
                        if selectedCrosstabValue[i] == "yardsign"{
                            changePosition = i
                        }
                    }
                }else{
                    for i in 0..<selectedCrosstabValue.count{
                        if selectedCrosstabValue[i] == "yardsign"{
                            changePosition = i
                        }
                    }
                }
            }else if crossTabMode == 11 {
                isSelectPopup = Jurisdiction_Parish
                nsm = parishArray
                if (!selectedCrosstabValue.contains("parish")){
                    selectedCrosstabValue.append("parish")
                    for i in 0..<selectedCrosstabValue.count{
                        if selectedCrosstabValue[i] == "parish"{
                            changePosition = i
                        }
                    }
                }else{
                    for i in 0..<selectedCrosstabValue.count{
                        if selectedCrosstabValue[i] == "parish"{
                            changePosition = i
                        }
                    }
                }
            }else if crossTabMode == 12 {
                isSelectPopup = Jurisdiction_Ward
                nsm = wardArray
                if (!selectedCrosstabValue.contains("ward")){
                    selectedCrosstabValue.append("ward")
                    for i in 0..<selectedCrosstabValue.count{
                        if selectedCrosstabValue[i] == "ward"{
                            changePosition = i
                        }
                    }
                }else{
                    for i in 0..<selectedCrosstabValue.count{
                        if selectedCrosstabValue[i] == "ward"{
                            changePosition = i
                        }
                    }
                }
            }else if crossTabMode == 13 {
                isSelectPopup = Jurisdiction_Precinct
                nsm = precinctArray
                if (!selectedCrosstabValue.contains("precinct")){
                    selectedCrosstabValue.append("precinct")
                    for i in 0..<selectedCrosstabValue.count{
                        if selectedCrosstabValue[i] == "precinct"{
                            changePosition = i
                        }
                    }
                }else{
                    for i in 0..<selectedCrosstabValue.count{
                        if selectedCrosstabValue[i] == "precinct"{
                            changePosition = i
                        }
                    }
                }
            }else if crossTabMode == 14 {
                isSelectPopup = voted
                nsm = votedArr
                if (!selectedCrosstabValue.contains("voted")){
                    selectedCrosstabValue.append("voted")
                    for i in 0..<selectedCrosstabValue.count{
                        if selectedCrosstabValue[i] == "voted"{
                            changePosition = i
                        }
                    }
                }else{
                    for i in 0..<selectedCrosstabValue.count{
                        if selectedCrosstabValue[i] == "voted"{
                            changePosition = i
                        }
                    }
                }
            }else if crossTabMode == 15 {
                isSelectPopup = Custom_Variable1
                nsm = custVarArr1
                if (!selectedCrosstabValue.contains("custvar1")){
                    selectedCrosstabValue.append("custvar1")
                    for i in 0..<selectedCrosstabValue.count{
                        if selectedCrosstabValue[i] == "custvar1"{
                            changePosition = i
                        }
                    }
                }else{
                    for i in 0..<selectedCrosstabValue.count{
                        if selectedCrosstabValue[i] == "custvar1"{
                            changePosition = i
                        }
                    }
                }
            }else if crossTabMode == 16 {
                isSelectPopup = Custom_Variable2
                nsm = custVarArr2
                if (!selectedCrosstabValue.contains("custvar2")){
                    selectedCrosstabValue.append("custvar2")
                    for i in 0..<selectedCrosstabValue.count{
                        if selectedCrosstabValue[i] == "custvar2"{
                            changePosition = i
                        }
                    }
                }else{
                    for i in 0..<selectedCrosstabValue.count{
                        if selectedCrosstabValue[i] == "custvar2"{
                            changePosition = i
                        }
                    }
                }
            }else {
                isSelectPopup = Custom_Variable3
                nsm = custVarArr3
                if (!selectedCrosstabValue.contains("custvar3")){
                    selectedCrosstabValue.append("custvar3")
                    for i in 0..<selectedCrosstabValue.count{
                        if selectedCrosstabValue[i] == "custvar3"{
                            changePosition = i
                        }
                    }
                }else{
                    for i in 0..<selectedCrosstabValue.count{
                        if selectedCrosstabValue[i] == "custvar3"{
                            changePosition = i
                        }
                    }
                }
            }
            
            var j = 0
            var select = ""
            //var favArr = isAppMyPeople ? ["All","ST","NSF","UND","NC"] : ["All","FA","NF","UA","NA"]
           // var YardArr = isAppMyPeople ? ["All","Open","Closed"] : ["All","Yes","No","Placed"]
            for i in 0..<(nsm.count) {
                let obj = nsm[i] as? multiSelcted
                if crossTabMode == 9 {
                    if ((obj?.isSelected)!){
                        var str = ""
                        print("obj!.txt~~~~~~",obj!.txt)
                        if obj!.txt == "Favor" || obj!.txt == "FAVOR"{
                            str = "FA"
                        }else if obj!.txt == "Not Favor" || obj!.txt == "NOT FAVOR"{
                            str = "NF"
                        }else if obj!.txt == "Undecided"{
                            str = "UA"
                        }else if obj!.txt == "Not Available"{
                            str = "NA"
                        }else{
                            str = obj!.txt
                        }
                        if j == 0 {
                            select = "\(str)"
                        } else {
                            select = "\(select),\(str)"
                        }
                        j += 1
                    }
                }else if crossTabMode == 8 {
                    if ((obj?.isSelected)!){
                        var str = ""
                        if obj!.txt == "Male"{
                            str = "M"
                        }else if obj!.txt == "Female"{
                            str = "F"
                        }else{
                            str = obj!.txt
                        }
                        if j == 0 {
                            select = "\(str)"
                        } else {
                            select = "\(select),\(str)"
                        }
                        j += 1
                    }
                }else{
                    if ((obj?.isSelected)!){
                        if j == 0 {
                            if let txt = obj?.txt {
                                select = "\(txt)"
                            }
                        } else {
                            if let txt = obj?.txt {
                                select = "\(select),\(txt)"
                            }
                        }
                        j += 1
                    }
                }
            }
            let txtRace = self.textFieldArr[8] as? UITextField
            let txtSex = self.textFieldArr[10] as? UITextField
            let txtParty = self.textFieldArr[9] as? UITextField
            let txtFav = self.textFieldArr[11] as? UITextField
            let txtYard = self.textFieldArr[12] as? UITextField
            let txtRating = self.textFieldArr[7] as? UITextField
            let txtCity = self.textFieldArr[4] as? UITextField
            let txtZip5 = self.textFieldArr[5] as? UITextField
            let txtZip4 = self.textFieldArr[6] as? UITextField
            let txtParish = self.textFieldArr[13] as? UITextField
            let txtWard = self.textFieldArr[14] as? UITextField
            let txtPrecinct = self.textFieldArr[15] as? UITextField
            let txtVoted = self.textFieldArr[16] as? UITextField
            let txtCustVar1 = self.textFieldArr[17] as? UITextField
            let txtCustVar2 = self.textFieldArr[18] as? UITextField
            let txtCustVar3 = self.textFieldArr[19] as? UITextField
            if j == 0{
                HomeViewController.VC.alert(message: "Select At Least One", title: "Alert")
                
            }else{
                print("select---\(changePosition)--\(crossTabMode)---",select)
                if crossTabMode == 2 {
                    txtCity?.text = select.contains("All") ? "All" : select
                }else if crossTabMode == 3 {
                    txtZip5?.text = select.contains("All") ? "All" : select
                }else if crossTabMode == 4 {
                    txtZip4?.text = select.contains("All") ? "All" : select
                }else if crossTabMode == 5 {
                    txtRating?.text = select.contains("All") ? "All" : select
                }else if crossTabMode == 6 {
                    txtRace?.text = select.contains("All") ? "All" : select
                }else if crossTabMode == 7 {
                    txtParty?.text = select.contains("All") ? "All" : select
                }else if crossTabMode == 8 {
                    txtSex?.text = select.contains("All") ? "All" : select
                }else if crossTabMode == 9 {
                    txtFav?.text = select.contains("All") ? "All" : select
                }else if crossTabMode == 10 {
                    txtYard?.text = select.contains("All") ? "All" : select
                }else if crossTabMode == 11 {
                    txtParish?.text = select.contains("All") ? "All" : select
                    //callPollPerishnWardUpdateList(u_id: u_id, cp_id: cp_id)
                }else if crossTabMode == 12 {
                    txtWard?.text = select.contains("All") ? "All" : select
                    //callPollPerishnWardUpdateList(u_id: u_id, cp_id: cp_id)
                }else if crossTabMode == 13 {
                    txtPrecinct?.text = select.contains("All") ? "All" : select
                }else if crossTabMode == 14 {
                    txtVoted?.text = select.contains("All") ? "All" : select
                }else if crossTabMode == 15 {
                    txtCustVar1?.text = select.contains("All") ? "All" : select
                }else if crossTabMode == 16 {
                    txtCustVar2?.text = select.contains("All") ? "All" : select
                }else {
                    txtCustVar3?.text = select.contains("All") ? "All" : select
                }
                popupView1.isHidden = true
                RefreshValue()
                
            }
            
        }
    }
    
    @objc func onClickCrossPopUpButton(_ sender: UIButton) {
        
        if sender.tag != 0{
            HomeViewController.VC.navigationItem.rightBarButtonItem = HomeViewController.VC.btnEditPerson
        }
        
        let txtAgeLow = self.textFieldArr[0] as? UITextField
        let txtAgeHigh = self.textFieldArr[1] as? UITextField
        let txtlastName = self.textFieldArr[2] as? UITextField
        let txtStrName = self.textFieldArr[3] as? UITextField
        let txtRace = self.textFieldArr[8] as? UITextField
        let txtSex = self.textFieldArr[10] as? UITextField
        let txtParty = self.textFieldArr[9] as? UITextField
        let txtFav = self.textFieldArr[11] as? UITextField
        let txtYard = self.textFieldArr[12] as? UITextField
        
        let txtRating = self.textFieldArr[7] as? UITextField
        let txtCity = self.textFieldArr[4] as? UITextField
        let txtZip5 = self.textFieldArr[5] as? UITextField
        let txtZip4 = self.textFieldArr[6] as? UITextField
        
        let txtParish = self.textFieldArr[13] as? UITextField
        let txtWard = self.textFieldArr[14] as? UITextField
        let txtPrecinct = self.textFieldArr[15] as? UITextField
        let txtVoted = self.textFieldArr[16] as? UITextField
        let txtCustVar1 = self.textFieldArr[17] as? UITextField
        let txtCustVar2 = self.textFieldArr[18] as? UITextField
        let txtCustVar3 = self.textFieldArr[19] as? UITextField
        
        if sender.tag == 0 { // Alphabetical List
            self.isHidden = true
            NotificationCenter.default.post(name: Notification.Name("nameOfNotification"), object: nil, userInfo: ["type":"0"])
        }
        if sender.tag == 1 { // Walk List
            type = 14
            paramStr = "\(paramStr)&isWalklist=1"
            HomeViewController.VC.callGetExcel(u_id: u_id, cp_id: cp_id, startRow: 0)
            self.isHidden = true
        }
        if sender.tag == 2 { // Back to crosstab
            scrollView.isHidden = false
            popUpView.isHidden = true
        }
        if sender.tag == 3 { // Map
            
            paramStr = "cid=\(cp_id)&id=\(u_id)&Personal_LastName=\(txtlastName?.text ?? "")&zipcode5=\(txtZip5?.text ?? "")&zipcode4=\(txtZip4?.text ?? "")&Residence_StreetDirection=\(strDirection)&Residence_StreetName=\(strName)&Residence_City=\(txtCity?.text ?? "")&Yard_Sign=\(txtYard?.text ?? "")&Personal_Race=\(txtRace?.text ?? "")&Personal_Age=\(txtAgeLow!.text ?? ""),\(txtAgeHigh!.text ?? "")&Personal_Sex=\(txtSex?.text ?? "")&Registration_PoliticalPartyCode=\(txtParty?.text ?? "")&Favorability=\(txtFav?.text ?? "")&ratings=\(txtRating?.text ?? "")&Custom_Variable1=\(txtCustVar1?.text ?? "")&Custom_Variable2=\(txtCustVar2?.text ?? "")&Custom_Variable3=\(txtCustVar3?.text ?? "")&voted=\(txtVoted?.text ?? "")"
            paramStr = "\(paramStr)&parish_id=\(txtParish?.text ?? "")&precinct_id=\(txtPrecinct?.text ?? "")&ward=\(txtWard?.text ?? "")&isResidential=\(isResidential)"

            
            let vc = MapViewController()
            //self.isHidden = true
            mapType = 1
            HomeViewController.VC.navigationController?.pushViewController(vc, animated: true)
        }
        if sender.tag == 4 { // Send Mail
            popUpView.isHidden = false
            if (isSelectSendMail == false) {
                isSelectSendMail = true
            }else{
                isSelectSendMail = false
            }
            popUpView.removeFromSuperview()
            setUpBtnView()
        }
        if sender.tag == 5 { // Send Mail-Alphabetically
            paramStr = "Personal_Race=\(txtRace?.text ?? "")&Personal_Age=\(txtAgeLow?.text ?? ""),\(txtAgeHigh?.text ?? "")&Personal_Sex=\(txtSex?.text ?? "")&Registration_PoliticalPartyCode=\(txtParty?.text ?? "")&Favorability=\(txtFav?.text ?? "")&Yard_Sign=\(txtYard?.text ?? "")&ratings=\(txtRating?.text ?? "")"
            paramStr = "\(paramStr)&Residence_StreetName=\(strName)&Residence_City=\(txtCity?.text ?? "")&zipcode4=\(txtZip4?.text ?? "")&zipcode5=\(txtZip5?.text ?? "")&parish_id=\(txtParish?.text ?? "")&precinct_id=\(txtPrecinct?.text ?? "")&ward=\(txtWard?.text ?? "")&Custom_Variable1=\(txtCustVar1?.text ?? "")&Custom_Variable2=\(txtCustVar2?.text ?? "")&Custom_Variable3=\(txtCustVar3?.text ?? "")&Residence_StreetDirection=\(strDirection)&voted=\(txtVoted?.text ?? "")&Personal_LastName=\(txtlastName?.text ?? "")&isResidential=\(isResidential)"
                paramStr = "\(paramStr)&isWalklist=0&isSendmail=1&page=1"
            
            HomeViewController.VC.callSendCSVApi(typ: "", id: 10)
            self.isHidden = true
        }
        if sender.tag == 6 { // Send Mail-Walk List
            paramStr = "Personal_Race=\(txtRace?.text ?? "")&Personal_Age=\(txtAgeLow?.text ?? ""),\(txtAgeHigh?.text ?? "")&Personal_Sex=\(txtSex?.text ?? "")&Registration_PoliticalPartyCode=\(txtParty?.text ?? "")&Favorability=\(txtFav?.text ?? "")&Yard_Sign=\(txtYard?.text ?? "")&ratings=\(txtRating?.text ?? "")"
            paramStr = "\(paramStr)&Residence_StreetName=\(strName)&Residence_City=\(txtCity?.text ?? "")&zipcode4=\(txtZip4?.text ?? "")&zipcode5=\(txtZip5?.text ?? "")&parish_id=\(txtParish?.text ?? "")&precinct_id=\(txtPrecinct?.text ?? "")&ward=\(txtWard?.text ?? "")&Custom_Variable1=\(txtCustVar1?.text ?? "")&Custom_Variable2=\(txtCustVar2?.text ?? "")&Custom_Variable3=\(txtCustVar3?.text ?? "")&Residence_StreetDirection=\(strDirection)&voted=\(txtVoted?.text ?? "")&Personal_LastName=\(txtlastName?.text ?? "")&isResidential=\(isResidential)"
                paramStr = "\(paramStr)&isWalklist=1&isSendmail=1&page=1"
            HomeViewController.VC.mailDic = nil
            HomeViewController.VC.callSendCSVApi(typ: "", id: 10)
            self.isHidden = true
        }
        if sender.tag == 7 { // Cancel
            self.isHidden = true
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if crossTabMode == 1{
            return arrayStreetNameBySection.count
        }else {
            return 1
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if crossTabMode == 1 {
            let arr = arrayStreetNameBySection[section] as! NSArray
            return arr.count
        }else if crossTabMode == 2 {
            return cityArr.count
        }else if crossTabMode == 3 {
            return zip5Arr.count
        }else if crossTabMode == 4 {
            return zip4Arr.count
        }else if crossTabMode == 5 {
            return ratingArr.count
        }else if crossTabMode == 6 {
            return raceArr1.count
        }else if crossTabMode == 7 {
            return partyArr1.count
        }else if crossTabMode == 8 {
            return genderArr1.count
        }else if crossTabMode == 9 {
            return favArr1.count
        }else if crossTabMode == 10 {
            return yardArr1.count
        }else if crossTabMode == 11 {
            return parishArray.count
        }else if crossTabMode == 12 {
            return wardArray.count
        }else if crossTabMode == 13 {
            return precinctArray.count
        }else if crossTabMode == 14 {
            return votedArr.count
        }else if crossTabMode == 15 {
            return custVarArr1.count
        }else if crossTabMode == 16 {
            return custVarArr2.count
        }else {
            return custVarArr3.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            
            let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! UITableViewCell
            var obj: multiSelcted! = nil
            if crossTabMode == 1 {
                let arr = arrayStreetNameBySection[indexPath.section] as! NSArray
                let arrCount = arrayStreetCountBySection[indexPath.section] as! NSArray
                cell.textLabel?.text = "\(arr[indexPath.row]) (\(arrCount[indexPath.row]))"
                cell.textLabel?.textColor = APPBUTTONCOLOR
            }else if crossTabMode == 2 {
                obj = cityArr[indexPath.row] as? multiSelcted
                cell.textLabel?.text = obj.txt
                cell.imageView?.image = UIImage(named: obj.isSelected == false ? "diselect128.png" : "select128.png")
            }else if crossTabMode == 3 {
                obj = zip5Arr[indexPath.row] as? multiSelcted
                cell.textLabel?.text = obj.txt
                cell.imageView?.image = UIImage(named: obj.isSelected == false ? "diselect128.png" : "select128.png")
            }else if crossTabMode == 4 {
                obj = zip4Arr[indexPath.row] as? multiSelcted
                cell.textLabel?.text = obj.txt
                cell.imageView?.image = UIImage(named: obj.isSelected == false ? "diselect128.png" : "select128.png")
            }else if crossTabMode == 5 {
                obj = ratingArr[indexPath.row] as? multiSelcted
                cell.textLabel?.text = obj.txt
                cell.imageView?.image = UIImage(named: obj.isSelected == false ? "diselect128.png" : "select128.png")
            }else if crossTabMode == 6 {
                obj = raceArr1[indexPath.row] as? multiSelcted
                cell.textLabel?.text = obj.txt
                cell.imageView?.image = UIImage(named: obj.isSelected == false ? "diselect128.png" : "select128.png")
            }else if crossTabMode == 7 {
                obj = partyArr1[indexPath.row] as? multiSelcted
                print("obj.txt~~~~~~~",obj.txt)
                cell.textLabel?.text = obj.txt
                cell.imageView?.image = UIImage(named: obj.isSelected == false ? "diselect128.png" : "select128.png")
            }else if crossTabMode == 8 {
                obj = genderArr1[indexPath.row] as? multiSelcted
                cell.textLabel?.text = obj.txt
                cell.imageView?.image = UIImage(named: obj.isSelected == false ? "diselect128.png" : "select128.png")
            }else if crossTabMode == 9 {
                obj = favArr1[indexPath.row] as? multiSelcted
                cell.textLabel?.text = obj.txt
                cell.imageView?.image = UIImage(named: obj.isSelected == false ? "diselect128.png" : "select128.png")
            }else if crossTabMode == 10 {
                obj = yardArr1[indexPath.row] as? multiSelcted
                cell.textLabel?.text = obj.txt
                cell.imageView?.image = UIImage(named: obj.isSelected == false ? "diselect128.png" : "select128.png")
            }else if crossTabMode == 11 {
                obj = parishArray[indexPath.row] as? multiSelcted
                cell.textLabel?.text = obj.txt
                cell.imageView?.image = UIImage(named: obj.isSelected == false ? "diselect128.png" : "select128.png")
            }else if crossTabMode == 12 {
                obj = wardArray[indexPath.row] as? multiSelcted
                cell.textLabel?.text = obj.txt
                cell.imageView?.image = UIImage(named: obj.isSelected == false ? "diselect128.png" : "select128.png")
            }else if crossTabMode == 13 {
                obj = precinctArray[indexPath.row] as? multiSelcted
                cell.textLabel?.text = obj.txt
                cell.imageView?.image = UIImage(named: obj.isSelected == false ? "diselect128.png" : "select128.png")
            }else if crossTabMode == 14 {
                obj = votedArr[indexPath.row] as? multiSelcted
                cell.textLabel?.text = obj.txt
                cell.imageView?.image = UIImage(named: obj.isSelected == false ? "diselect128.png" : "select128.png")
            }else if crossTabMode == 15 {
                obj = custVarArr1[indexPath.row] as? multiSelcted
                cell.textLabel?.text = obj.txt
                cell.imageView?.image = UIImage(named: obj.isSelected == false ? "diselect128.png" : "select128.png")
            }else if crossTabMode == 16 {
                obj = custVarArr2[indexPath.row] as? multiSelcted
                cell.textLabel?.text = obj.txt
                cell.imageView?.image = UIImage(named: obj.isSelected == false ? "diselect128.png" : "select128.png")
            }else if crossTabMode == 17 {
                obj = custVarArr3[indexPath.row] as? multiSelcted
                cell.textLabel?.text = obj.txt
                cell.imageView?.image = UIImage(named: obj.isSelected == false ? "diselect128.png" : "select128.png")
            }
            
            cell.textLabel?.textColor = APPBUTTONCOLOR
            cell.backgroundColor = .clear
            cell.selectionStyle = .none
            return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if crossTabMode == 1 {
            let arr = arrayStreetNameBySection[indexPath.section] as! NSArray
            let txtStrName = self.textFieldArr[3] as? UITextField
            txtStrName?.text = "\(arr[indexPath.row])"
            let arrDir = arrayStreetDirBySection[indexPath.section] as! NSArray
            strDirection = "\(arrDir[indexPath.row])"
            let arrName = arrayStreetNameByRow[indexPath.section] as! NSArray
            strName = "\(arrName[indexPath.row])"
            strNameView.isHidden = true
            if (!selectedCrosstabValue.contains("streetname")){
                selectedCrosstabValue.append("streetname")
                for i in 0..<selectedCrosstabValue.count{
                    if selectedCrosstabValue[i] == "streetname"{
                        changePosition = i
                    }
                }
            }else{
                for i in 0..<selectedCrosstabValue.count{
                    if selectedCrosstabValue[i] == "streetname"{
                        changePosition = i
                    }
                }
            }
            isSelectPopup = 1
            RefreshValue()
            //callCrosstabSearchFilter()
        }else{
            var obj = NSMutableArray()
            if crossTabMode == 2 {
                obj = cityArr
            }else if crossTabMode == 3 {
                obj = zip5Arr
            }else if crossTabMode == 4 {
                obj = zip4Arr
            }else if crossTabMode == 5 {
                obj = ratingArr
            }else if crossTabMode == 6 {
                obj = raceArr1
            }else if crossTabMode == 7 {
                obj = partyArr1
            }else if crossTabMode == 8 {
                obj = genderArr1
            }else if crossTabMode == 9 {
                obj = favArr1
            }else if crossTabMode == 10 {
                obj = yardArr1
            }else if crossTabMode == 11 {
                obj = parishArray
            }else if crossTabMode == 12 {
                obj = wardArray
            }else if crossTabMode == 13 {
                obj = precinctArray
            }else if crossTabMode == 14 {
                obj = votedArr
            }else if crossTabMode == 15 {
                obj = custVarArr1
            }else if crossTabMode == 16 {
                obj = custVarArr2
            }else if crossTabMode == 17 {
                obj = custVarArr3
            }
            var com = obj[indexPath.row] as! multiSelcted
            if (com.txt == "All") {
                let comObj = obj[0] as! multiSelcted
                if indexPath.row == 0 && comObj.isSelected == false {
                    for i in 0..<obj.count {
                        let comObj = obj[i] as! multiSelcted
                        comObj.isSelected = i == 0 ? !(comObj.isSelected) : true
                    }
                } else {
                    for i in 0..<obj.count {
                        let comObj = obj[i] as! multiSelcted
                        comObj.isSelected = i == 0 ? !(comObj.isSelected) : false
                    }
                }
            } else {
                com.isSelected = !com.isSelected
                com = obj[0] as! multiSelcted
                if (com.txt == "All") {
                    com.isSelected = false
                }
            }
            TableView.reloadData()
        }
        
        
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        if crossTabMode == 1{
            return arrayAotz as? [String]
        }else{
            return [""]
        }
    }
    
    func newCross(_ str: String?, type isSel: Bool) -> multiSelcted? {
        let comment = multiSelcted()
        comment.txt = str ?? ""
        comment.isSelected = isSel
        return comment
    }
    
    func callPollPerishnWardList(u_id:String,cp_id:String){
        
        let authToken = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
        postWithoutImage(url_string: "jurisdiction_group_by_v2", parameters: "cid=\(cp_id)&id=\(u_id)&poll_mode=YES&authToken=\(authToken)",CompletionHandler: { json, error in
            if let json = json {
                print("json~~~1~~~~",json)
                let statusmassege = json.value(forKey: "status") as! Bool
                if (statusmassege == true){
                    let authToken = json.value(forKey: "authToken") as! String
                    USER_DEFAULTS.set(authToken, forKey: "authToken")
                    //(one.nsm_Pricenct[k] as? Comment)?.isSelected = true
                    parishArray.removeAllObjects()
                    precinctArray.removeAllObjects()
                    wardArray.removeAllObjects()
                    let perishDic = json.value(forKey: "parish") as! NSDictionary
                    let perishArr = perishDic.value(forKey: "values") as! NSArray
                    parishArray.add(self.newCross("All", type: true)!)
                    for i in 0..<perishArr.count{
                        let obj = perishArr[i] as! NSArray
                        for i in 0..<obj.count{
                            parishArray.add(self.newCross(obj[i] as? String, type: true)!)
                        }
                    }
                    let precinctDic = json.value(forKey: "precinct") as! NSDictionary
                    let precinctArr = precinctDic.value(forKey: "values") as! NSArray
                    precinctArray.add(self.newCross("All", type: true)!)
                    for i in 0..<precinctArr.count{
                        let obj = precinctArr[i] as! NSArray
                        for i in 0..<obj.count{
                            precinctArray.add(self.newCross(obj[i] as? String, type: true)!)
                        }
                    }
                    let wardDic = json.value(forKey: "ward") as! NSDictionary
                    let wardArr = wardDic.value(forKey: "values") as! NSArray
                    wardArray.add(self.newCross("All", type: true)!)
                    for i in 0..<wardArr.count{
                        let obj = wardArr[i] as! NSArray
                        for i in 0..<obj.count{
                            wardArray.add(self.newCross(obj[i] as? String, type: true)!)
                        }
                    }
//                    let txtParish = self.textFieldArr[13] as? UITextField
//                    //txtParish?.text = "All"
//                    let txtward = self.textFieldArr[14] as? UITextField
                    //txtward?.text = "All"
                    
                    
                    self.callPollPerishnWardUpdateList(u_id: u_id, cp_id: cp_id)
                }else{
                    var is_changed: String? = nil
                    if let object = json["is_changed"] {
                        is_changed = "\(object)"
                    }
                    if is_changed?.isEqual("1") ?? false {
                        logoutAlertMassege()
                    } else {
                        HomeViewController.VC.alert(message: "No Record Found!", title: "Alert!")
                    }
                }
            }
        })
    }
    
    func callPollPerishnWardUpdateList(u_id:String,cp_id:String){
        let authToken = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
        var select = ""
        var j = 0
        for i in 0..<parishArray.count{
            let obj = parishArray[i] as! multiSelcted
            if (obj.isSelected){
                if(j==0){
                    select = obj.txt
                }else{
                    select = "\(select),\(obj.txt)"
                }
                j = j+1
            }
        }
        var select1 = ""
        var k = 0
        for i in 0..<wardArray.count{
            let obj = wardArray[i] as! multiSelcted
            if (obj.isSelected){
                if(k==0){
                    select1 = obj.txt
                }else{
                    select1 = "\(select1),\(obj.txt)"
                }
                k = k+1
            }
        }
        if select1.contains("All"){
            select1 = ""
        }
        
        postWithoutImage(url_string: "jurisdiction_precinct_ward_group_by_v2", parameters: "cid=\(cp_id)&id=\(u_id)&parish_id=\(select)&ward_id=\(select1)&poll_mode=YES&authToken=\(authToken)",CompletionHandler: { json, error in
            if let json = json {
                print("json~~~123~~~~",json)
                let statusmassege = json.value(forKey: "status") as! Bool
                if (statusmassege == true){
                    let authToken = json.value(forKey: "authToken") as! String
                    USER_DEFAULTS.set(authToken, forKey: "authToken")
                    //(one.nsm_Pricenct[k] as? Comment)?.isSelected = true
                    let precinctDic = json.value(forKey: "precinct") as! NSDictionary
                    let precinctArr = precinctDic.value(forKey: "values") as! NSArray
                    precinctArray.removeAllObjects()
                    if select1 == ""{
                        wardArray.removeAllObjects()
                    }
                    
                    precinctArray.add(self.newCross("All", type: true)!)
                    for i in 0..<precinctArr.count{
                        let obj = precinctArr[i] as! NSArray
                        for i in 0..<obj.count{
                            precinctArray.add(self.newCross(obj[i] as? String, type: true)!)
                        }
                    }

                    let wardDic = json.value(forKey: "ward") as! NSDictionary
                    let wardArr = wardDic.value(forKey: "values") as! NSArray
                    if select1 == ""{
                        wardArray.add(self.newCross("All", type: true)!)
                        for i in 0..<wardArr.count{
                            let obj = wardArr[i] as! NSArray
                            for i in 0..<obj.count{
                                wardArray.add(self.newCross(obj[i] as? String, type: true)!)
                            }
                        }
                    }
                    
                    
                    
//                    let txtward = self.textFieldArr[14] as? UITextField
//                    txtward?.text = "All"
                   // let txtPrecinct = self.textFieldArr[15] as? UITextField
                   // txtPrecinct?.text = "All"
                    
                }else{
                    var is_changed: String? = nil
                    if let object = json["is_changed"] {
                        is_changed = "\(object)"
                    }
                    if is_changed?.isEqual("1") ?? false {
                        logoutAlertMassege()
                    } else {
                        HomeViewController.VC.alert(message: "No Record Found!", title: "Alert!")
                    }
                }
            }
        })
    }
    
    func callGetExcel(u_id:String,cp_id:String,startRow:Int){
        let authToken = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
        var urlStr = ""
        var parameter = ""
        if type == 9{
            urlStr = "crosstabsearch_for_all"
            parameter = "cid=\(cp_id)&id=\(u_id)&startrow=\(startRow)&endrow=\(noRow)&\(paramStr)&authToken=\(authToken)"
        }
        postWithoutImage(url_string: urlStr, parameters: parameter,CompletionHandler: { json, error in
            if let json = json {
                print("json~~~~~~~",json)
                let statusmassege = json.value(forKey: "status") as! Bool
                if (statusmassege == true){
                    let authToken = json.value(forKey: "authToken") as! String
                    USER_DEFAULTS.set(authToken, forKey: "authToken")
                    jsonObj = json
                    HomeViewController.VC.SetData()
                    USER_DEFAULTS.set(0, forKey: "case1")
                    self.setUpBtnView()
                    if(type == 9 && startRow == 0){
                        let no1 = json["Total_rows"] as! Int
                        let no0 = (json["Total_campaign_rows"] as! NSString).floatValue
                        let total = (Float(no1)/no0)*100
                        self.totalRecord = NSString(format: "%@ \n %.3f%% Of Database", String(no1),total) as String
                        self.lblTotalNoOfRecord.text = NSString(format: "%@ \n %.3f%% Of Database", String(no1),total) as String
                    }
                }else{
                    self.isHidden = true
                    var is_changed: String? = nil
                    if let object = json["is_changed"] {
                        is_changed = "\(object)"
                    }
                    if is_changed?.isEqual("1") ?? false {
                        logoutAlertMassege()
                    } else {
                        HomeViewController.VC.alert(message: "No Record Found!", title: "Alert!")
                    }
                }
            }
        })
    }
    
    func callCrosstabSearchFilter(){
        print("~~~~~callCrosstabSearchFilter~~~~~~~`")
        let txtAgeLow = self.textFieldArr[0] as? UITextField
        let txtAgeHigh = self.textFieldArr[1] as? UITextField
        let txtpersonalLastName = self.textFieldArr[2] as? UITextField
        let txtRace = self.textFieldArr[8] as? UITextField
        let txtSex = self.textFieldArr[10] as? UITextField
        let txtParty = self.textFieldArr[9] as? UITextField
        let txtFav = self.textFieldArr[11] as? UITextField
        let txtYard = self.textFieldArr[12] as? UITextField
        let txtRating = self.textFieldArr[7] as? UITextField
        let txtCity = self.textFieldArr[4] as? UITextField
        let txtZip5 = self.textFieldArr[5] as? UITextField
        let txtZip4 = self.textFieldArr[6] as? UITextField
        let txtParish = self.textFieldArr[13] as? UITextField
        let txtWard = self.textFieldArr[14] as? UITextField
        let txtPrecinct = self.textFieldArr[15] as? UITextField
        let txtVoted = self.textFieldArr[16] as? UITextField
        let txtCustVar1 = self.textFieldArr[17] as? UITextField
        let txtCustVar2 = self.textFieldArr[18] as? UITextField
        let txtCustVar3 = self.textFieldArr[19] as? UITextField
        
        
        //crosstabsearch_filter
        
        let authToken = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
        postWithoutImage(url_string: "crosstabsearch_filter", parameters: "id=\(u_id)&cid=\(cp_id)&isMap=0&isResidential=\(isResidential)&Personal_Age=\(txtAgeLow?.text ?? ""),\(txtAgeHigh?.text ?? "")&Personal_Sex=\(txtSex?.text ?? "")&Registration_PoliticalPartyCode=\(txtParty?.text ?? "")&Favorability=\(txtFav?.text ?? "")&Yard_Sign=\(txtYard?.text ?? "")&ratings=\(txtRating?.text ?? "")&zipcode4=\(txtZip4?.text ?? "")&zipcode5=\(txtZip5?.text ?? "")&parish_id=\(txtParish?.text ?? "")&precinct_id=\(txtPrecinct?.text ?? "")&ward=\(txtWard?.text ?? "")&Custom_Variable1=\(txtCustVar1?.text ?? "")&Custom_Variable2=\(txtCustVar2?.text ?? "")&Custom_Variable3=\(txtCustVar3?.text ?? "")&Residence_City=\(txtCity?.text ?? "")&Personal_LastName=\(txtpersonalLastName?.text ?? "")&Personal_Race=\(txtRace?.text ?? "")&voted=\(txtVoted?.text ?? "")&isCanvaser=0&Residence_StreetName=\(strName)&Residence_StreetDirection=\(strDirection)&isStreetName=\(isSteetName)&isFirstTime=\(isFirstTime)&isHeading=0&heading_name=&authToken=\(authToken)",CompletionHandler: { json, error in
            if let json = json {
                print("crosstabsearch_filter~~~~~~",json)
                let statusmassege = json.value(forKey: "status") as! Bool
                if (statusmassege == true){
                    self.isFirstTime = 0
                    let authToken = json.value(forKey: "authToken") as! String
                    USER_DEFAULTS.set(authToken, forKey: "authToken")
                    let data = json.object(forKey: "data") as! NSDictionary
                    let field_status = json.object(forKey: "field_status") as! NSDictionary
                    self.Custom_Variable1 = field_status.object(forKey: "Custom_Variable1") as? Int ?? 2
                    self.Custom_Variable2 = field_status.object(forKey: "Custom_Variable2") as? Int ?? 2
                    self.Custom_Variable3 = field_status.object(forKey: "Custom_Variable3") as? Int ?? 2
                    self.Favorability = field_status.object(forKey: "Favorability") as? Int ?? 2
                    self.Jurisdiction_Parish = field_status.object(forKey: "Jurisdiction_Parish") as? Int ?? 2
                    self.Jurisdiction_Ward = field_status.object(forKey: "Jurisdiction_Ward") as? Int ?? 2
                    self.Jurisdiction_Precinct = field_status.object(forKey: "Jurisdiction_Precinct") as? Int ?? 2
                    self.Personal_Race = field_status.object(forKey: "Personal_Race") as? Int ?? 2
                    self.Personal_Sex = field_status.object(forKey: "Personal_Sex") as? Int ?? 2
                    self.Rating = field_status.object(forKey: "Rating") as? Int ?? 2
                    self.Registration_PoliticalPartyCode = field_status.object(forKey: "Registration_PoliticalPartyCode") as? Int ?? 2
                    self.Residence_City = field_status.object(forKey: "Residence_City") as? Int ?? 2
                    self.Residence_ZipCode4 = field_status.object(forKey: isResidential == 1 ? "Residence_ZipCode4" : "Mail_ZipCode4") as? Int ?? 2
                    self.Residence_ZipCode5 = field_status.object(forKey: isResidential == 1 ? "Residence_ZipCode5" : "Mail_ZipCode5") as? Int ?? 2
                    self.Yard_Sign = field_status.object(forKey: "Yard_Sign") as? Int ?? 2
                    self.voted = field_status.object(forKey: "voted") as? Int ?? 2
                    
                    print("self.voted~~~~~~",self.voted)
                    
                    if (data.object(forKey: "Jurisdiction_Parish") is NSNull && data.object(forKey: "Jurisdiction_Ward") is NSNull && data.object(forKey: "Jurisdiction_Precinct") is NSNull && data.object(forKey: "Residence_City") is NSNull && data.object(forKey: "Residence_ZipCode4") is NSNull && data.object(forKey: "Residence_ZipCode5") is NSNull && data.object(forKey: "Personal_Sex") is NSNull && data.object(forKey: "Personal_Race") is NSNull && data.object(forKey: "Registration_PoliticalPartyCode") is NSNull && data.object(forKey: "Favorability") is NSNull && data.object(forKey: "Yard_Sign") is NSNull && data.object(forKey: "Rating") is NSNull && data.object(forKey: "Custom_Variable1") is NSNull && data.object(forKey: "Custom_Variable2") is NSNull && data.object(forKey: "Custom_Variable3") is NSNull && data.object(forKey: "voted") is NSNull){
                        HomeViewController.VC.alert(message: "No Record Found!", title: "Alert!")
                    }
                    
                    if (!self.selectedCrosstabValue.contains("streetname")){
                        self.strNameArr.removeAllObjects()
                        self.strDirArr.removeAllObjects()
                        self.strCountArr.removeAllObjects()
                        self.concatArr.removeAll()
                        let strDirNameArr = json.value(forKey: "street_direction_name") as! NSArray
                        
                        self.isDynamic = json.value(forKey: "isDynamic") as? String ?? ""
                        print("isDynamic~~~~~~~",self.isDynamic)
                        
                        
                        
                        for i in 0..<strDirNameArr.count{
                            let dic = strDirNameArr[i] as! NSDictionary
                            self.strNameArr.add(dic.value(forKey: "StreetName") as? String ?? "")
                            let trimmed = dic.value(forKey: "StreetDirection") as? String ?? "".trimmingCharacters(in: CharacterSet.whitespaces)
                            self.strDirArr.add(trimmed)
                            self.strCountArr.add(dic.value(forKey: "StreetCount") as? String ?? "")
                            var trimmedString = dic.value(forKey: "concatString") as? String ?? "".trimmingCharacters(in: .whitespaces)
                            trimmedString = trimmedString.replacingOccurrences(of: "  ", with: " ")
                            trimmedString = trimmedString.replacingOccurrences(of: "   ", with: " ")
                            self.concatArr.append(trimmedString)
                        }
                    }
                    if !(self.selectedCrosstabValue.contains("gender")) && self.Personal_Sex == 1{
                        self.genderArr1.removeAllObjects()
                        print("gender~~~~~~~~~~~1~~~",self.genderArr1)
                        let Personal_Sex = data.object(forKey: "Personal_Sex") as? String ?? ""
                        
                        var array = Personal_Sex.components(separatedBy: ",")
                        if array == [""]{
                            array.removeAll()
                        }
                        self.genderArr1.add(self.newCross("All", type: false)!)
                        for i in 0..<array.count{
                            self.genderArr1.add(self.newCross(array[i], type: false)!)
                        }
                    }
                    if !(self.selectedCrosstabValue.contains("race")) && self.Personal_Race == 1{
                        self.raceArr1.removeAllObjects()
                        let Personal_Race = data.object(forKey: "Personal_Race") as? String ?? ""
                        var array = Personal_Race.components(separatedBy: ",")
                        if array == [""]{
                            array.removeAll()
                        }
                        self.raceArr1.add(self.newCross("All", type: false)!)
                        for i in 0..<array.count{
                            self.raceArr1.add(self.newCross(array[i], type: false)!)
                        }
                    }
                    if !(self.selectedCrosstabValue.contains("party")) && self.Registration_PoliticalPartyCode == 1{
                        self.partyArr1.removeAllObjects()
                        let PartyCodeArr = data.object(forKey: "Registration_PoliticalPartyCode") as? String ?? ""
                        var array = PartyCodeArr.components(separatedBy: ",")
                        if array == [""]{
                            array.removeAll()
                        }
                        self.partyArr1.add(self.newCross("All", type: false)!)
                        for i in 0..<array.count{
                            self.partyArr1.add(self.newCross(array[i], type: false)!)
                        }
                    }
                    if !self.selectedCrosstabValue.contains("favorability") && self.Favorability == 1{
                        self.favArr1.removeAllObjects()
                        let Favorability = data.object(forKey: "Favorability") as? String ?? ""
                        var array = Favorability.components(separatedBy: ",")
                        if array == [""]{
                            array.removeAll()
                        }
                        self.favArr1.add(self.newCross("All", type: false)!)
                        for i in 0..<array.count{
                            self.favArr1.add(self.newCross(array[i], type: false)!)
                        }
                    }
                    if !self.selectedCrosstabValue.contains("yardsign") && self.Yard_Sign == 1{
                        self.yardArr1.removeAllObjects()
                        let Yard_Sign = data.object(forKey: "Yard_Sign") as? String ?? ""
                        var array = Yard_Sign.components(separatedBy: ",")
                        if array == [""]{
                            array.removeAll()
                        }
                        self.yardArr1.add(self.newCross("All", type: false)!)
                        for i in 0..<array.count{
                            self.yardArr1.add(self.newCross(array[i], type: false)!)
                        }
                    }
                    if !self.selectedCrosstabValue.contains("rating") && self.Rating == 1{
                        self.ratingArr.removeAllObjects()
                        let ratingArr = data.object(forKey: "Rating") as? String ?? ""
                        var array = ratingArr.components(separatedBy: ",")
                        if array == [""]{
                            array.removeAll()
                        }
                        self.ratingArr.add(self.newCross("All", type: false)!)
                        for i in 0..<array.count{
                            self.ratingArr.add(self.newCross(array[i], type: false)!)
                        }
                    }
                    if !self.selectedCrosstabValue.contains("city") && self.Residence_City == 1{
                        self.cityArr.removeAllObjects()
                        let cityArr = data.object(forKey: "Residence_City") as? String ?? ""
                        var array = cityArr.components(separatedBy: ",")
                        if array == [""]{
                            array.removeAll()
                        }
                        self.cityArr.add(self.newCross("All", type: false)!)
                        for i in 0..<array.count{
                            self.cityArr.add(self.newCross(array[i], type: false)!)
                        }
                    }
                   // print(isResidential,"isResidential!!!!!!!!!!")
                    if !self.selectedCrosstabValue.contains("Zipcode5") && self.Residence_ZipCode5 == 1{
                        print(isResidential,"isResidential!!!!!!!!!!")
                        self.zip5Arr.removeAllObjects()
                        self.zip5Arr.add(self.newCross("All", type: false)!)
                        let zip5Arr = data.object(forKey: isResidential == 1 ? "Residence_ZipCode5" : "Mail_ZipCode5") as? String ?? ""
                        //print("zip5Arr~~~~~~~~~~",zip5Arr)
                        var array = zip5Arr.components(separatedBy: ",")
                        if array == [""]{
                            array.removeAll()
                        }
                        for i in 0..<array.count{
                            self.zip5Arr.add(self.newCross(array[i], type: false)!)
                        }
                    }
                    if !self.selectedCrosstabValue.contains("Zipcode4") && self.Residence_ZipCode4 == 1{
                        self.zip4Arr.removeAllObjects()
                        let zip4Arr = data.object(forKey: isResidential == 1 ? "Residence_ZipCode4" : "Mail_ZipCode4") as? String ?? ""
                        var array = zip4Arr.components(separatedBy: ",")
                        if array == [""]{
                            array.removeAll()
                        }
                        self.zip4Arr.add(self.newCross("All", type: false)!)
                        for i in 0..<array.count{
                            self.zip4Arr.add(self.newCross(array[i], type: false)!)
                        }
                    }
                    if !self.selectedCrosstabValue.contains("parish") && self.Jurisdiction_Parish == 1{
                        parishArray.removeAllObjects()
                        let perishArr = data.value(forKey: "Jurisdiction_Parish") as? String ?? ""
                        var array = perishArr.components(separatedBy: ",")
                        if array == [""]{
                            array.removeAll()
                        }
                        parishArray.add(self.newCross("All", type: false)!)
                        for i in 0..<array.count{
                            parishArray.add(self.newCross(array[i], type: false)!)
                        }
                    }
                    if !self.selectedCrosstabValue.contains("ward") && self.Jurisdiction_Ward == 1{
                        wardArray.removeAllObjects()
                        let wardArr = data.value(forKey: "Jurisdiction_Ward") as? String ?? ""
                        var array = wardArr.components(separatedBy: ",")
                        if array == [""]{
                            array.removeAll()
                        }
                        wardArray.add(self.newCross("All", type: false)!)
                        for i in 0..<array.count{
                            wardArray.add(self.newCross(array[i], type: false)!)
                        }
                    }
                    if !self.selectedCrosstabValue.contains("precinct") && self.Jurisdiction_Precinct == 1{
                        precinctArray.removeAllObjects()
                        let precinctArr = data.value(forKey: "Jurisdiction_Precinct") as? String ?? ""
                        var array = precinctArr.components(separatedBy: ",")
                        if array == [""]{
                            array.removeAll()
                        }
                        precinctArray.add(self.newCross("All", type: false)!)
                        for i in 0..<array.count{
                            precinctArray.add(self.newCross(array[i], type: false)!)
                        }
                    }
                    if !self.selectedCrosstabValue.contains("voted") && self.voted == 1{
                        self.votedArr.removeAllObjects()
                        let voted = data.object(forKey: "voted") as? String ?? ""
                        var array = voted.components(separatedBy: ",")
                        if array == [""]{
                            array.removeAll()
                        }
                        self.votedArr.add(self.newCross("All", type: false)!)
                        for i in 0..<array.count{
                            self.votedArr.add(self.newCross(array[i], type: false)!)
                        }
                    }
                    if !self.selectedCrosstabValue.contains("custvar1") && self.Custom_Variable1 == 1{
                        self.custVarArr1.removeAllObjects()
                        let custVar1 = data.object(forKey: "Custom_Variable1") as? String ?? ""
                        var array = custVar1.components(separatedBy: ",")
                        if array == [""]{
                            array.removeAll()
                        }
                        self.custVarArr1.add(self.newCross("All", type: false)!)
                        for i in 0..<array.count{
                            self.custVarArr1.add(self.newCross(array[i], type: false)!)
                        }
                    }
                    if !self.selectedCrosstabValue.contains("custvar2") && self.Custom_Variable2 == 1{
                        self.custVarArr2.removeAllObjects()
                        let custVar2 = data.object(forKey: "Custom_Variable2") as? String ?? ""
                        var array = custVar2.components(separatedBy: ",")
                        if array == [""]{
                            array.removeAll()
                        }
                        self.custVarArr2.add(self.newCross("All", type: false)!)
                        for i in 0..<array.count{
                            self.custVarArr2.add(self.newCross(array[i], type: false)!)
                        }
                    }
                    if !self.selectedCrosstabValue.contains("custvar3") && self.Custom_Variable3 == 1{
                        self.custVarArr3.removeAllObjects()
                        let custVar3 = data.object(forKey: "Custom_Variable3") as? String ?? ""
                        var array = custVar3.components(separatedBy: ",")
                        if array == [""]{
                            array.removeAll()
                        }
                        self.custVarArr3.add(self.newCross("All", type: false)!)
                        for i in 0..<array.count{
                            self.custVarArr3.add(self.newCross(array[i], type: false)!)
                        }
                    }
                    if self.isDynamic == "0"{
                         self.isFirstTimeSelect = false
                    }
                }else{
                    var is_changed: String? = nil
                    if let object = json["is_changed"] {
                        is_changed = "\(object)"
                    }
                    if is_changed?.isEqual("1") ?? false {
                        logoutAlertMassege()
                    } else {
                        HomeViewController.VC.alert(message: "No Record Found!", title: "Alert!")
                    }
                }
            }
        })
    }
    
    func streetNameDropDown(){
        var arr = NSMutableArray()
        var arrDir = NSMutableArray()
        var arrCount = NSMutableArray()
        var arrstName = NSMutableArray()
        arrayStreetNameBySection.removeAllObjects()
        arrayStreetDirBySection.removeAllObjects()
        arrayStreetCountBySection.removeAllObjects()
        arrayStreetNameByRow.removeAllObjects()
        arrayAotz.removeAllObjects()
        var streetNameArr : [streetName] = []
        for i in 0..<strNameArr.count {
            let str = streetName()
            str.strName = strNameArr[i] as! String
            str.strCount = strCountArr[i] as! String
            str.strDir = strDirArr[i] as! String
            str.strCombine = concatArr[i]
            streetNameArr.append(str)
        }
        let sortedArray = streetNameArr.sorted{ ($0.strCombine).localizedCaseInsensitiveCompare($1.strCombine) == ComparisonResult.orderedAscending}
        var str = ""
        if (sortedArray.count > 0) {
            str = String(sortedArray[0].strCombine.prefix(1)).uppercased()
            arrayAotz.add(str)
            arrayStreetNameBySection.add(arr)
            arrayStreetDirBySection.add(arrDir)
            arrayStreetCountBySection.add(arrCount)
            arrayStreetNameByRow.add(arrstName)
        }
        for i in 0..<sortedArray.count{
            let str1 = String(sortedArray[i].strCombine.prefix(1)).uppercased()
            let strName = sortedArray[i].strCombine
            let strDir = sortedArray[i].strDir
            let strCount = sortedArray[i].strCount
            let stName = sortedArray[i].strName
            if str != str1{
                arr = NSMutableArray()
                arrayStreetNameBySection.add(arr)
                arrDir = NSMutableArray()
                arrayStreetDirBySection.add(arrDir)
                arrCount = NSMutableArray()
                arrayStreetCountBySection.add(arrCount)
                arrstName = NSMutableArray()
                arrayStreetNameByRow.add(arrstName)
                str = str1
                arrayAotz.add(str)
            }
            arr.add(strName)
            arrDir.add(strDir)
            arrCount.add(strCount)
            arrstName.add(stName)
        }
        stTableView.reloadData()
    }
    
    func streetCountDropDown(){
        var arr = NSMutableArray()
        var arrDir = NSMutableArray()
        var arrCount = NSMutableArray()
        var arrstName = NSMutableArray()
        arrayStreetNameBySection.removeAllObjects()
        arrayStreetDirBySection.removeAllObjects()
        arrayStreetCountBySection.removeAllObjects()
        arrayStreetNameByRow.removeAllObjects()
        arrayAotz.removeAllObjects()
        var streetNameArr : [streetName] = []
        for i in 0..<strNameArr.count {
            let str = streetName()
            str.strName = strNameArr[i] as! String
            str.strCount = strCountArr[i] as! String
            str.strDir = strDirArr[i] as! String
            str.strCombine = concatArr[i]
            streetNameArr.append(str)
        }
        let sortedArray = streetNameArr.sorted{ ($0.strCount).localizedStandardCompare($1.strCount) == .orderedDescending}
        var str = ""
        if (sortedArray.count > 0) {
            str = String(sortedArray[0].strCombine.prefix(1)).uppercased()
            arrayAotz.add(str)
            arrayStreetNameBySection.add(arr)
            arrayStreetDirBySection.add(arrDir)
            arrayStreetCountBySection.add(arrCount)
            arrayStreetNameByRow.add(arrstName)
        }
        for i in 0..<sortedArray.count{
            let str1 = String(sortedArray[i].strCombine.prefix(1)).uppercased()
            let strName = sortedArray[i].strCombine
            let strDir = sortedArray[i].strDir
            let strCount = sortedArray[i].strCount
            let stName = sortedArray[i].strName
            if str != str1{
                arr = NSMutableArray()
                arrayStreetNameBySection.add(arr)
                arrDir = NSMutableArray()
                arrayStreetDirBySection.add(arrDir)
                arrCount = NSMutableArray()
                arrayStreetCountBySection.add(arrCount)
                arrstName = NSMutableArray()
                arrayStreetNameByRow.add(arrstName)
                str = str1
                arrayAotz.add(str)
            }
            arr.add(strName)
            arrDir.add(strDir)
            arrCount.add(strCount)
            arrstName.add(stName)
        }
        stTableView.reloadData()
    }
    
    func RefreshValue() {
        print("~~~~~RefreshValue~~~~~~~`")
        var str = ""
        let txtStrName = self.textFieldArr[3] as? UITextField
        let txtRace = self.textFieldArr[8] as? UITextField
        let txtSex = self.textFieldArr[10] as? UITextField
        let txtParty = self.textFieldArr[9] as? UITextField
        let txtFav = self.textFieldArr[11] as? UITextField
        let txtYard = self.textFieldArr[12] as? UITextField
        let txtRating = self.textFieldArr[7] as? UITextField
        let txtCity = self.textFieldArr[4] as? UITextField
        let txtZip5 = self.textFieldArr[5] as? UITextField
        let txtZip4 = self.textFieldArr[6] as? UITextField
        let txtParish = self.textFieldArr[13] as? UITextField
        let txtWard = self.textFieldArr[14] as? UITextField
        let txtPrecinct = self.textFieldArr[15] as? UITextField
        let txtVoted = self.textFieldArr[16] as? UITextField
        let txtCustVar1 = self.textFieldArr[17] as? UITextField
        let txtCustVar2 = self.textFieldArr[18] as? UITextField
        let txtCustVar3 = self.textFieldArr[19] as? UITextField
        let streetName = strName
        let StrName  = txtStrName?.text
        let Race     = txtRace?.text
        let Sex      = txtSex?.text
        let Party    = txtParty?.text
        let Fav      = txtFav?.text
        let Yard     = txtYard?.text
        let Rating1   = txtRating?.text
        let City     = txtCity?.text
        let Zip5     = txtZip5?.text
        let Zip4     = txtZip4?.text
        let Parish   = txtParish?.text
        let Ward     = txtWard?.text
        let Precinct = txtPrecinct?.text
        let Voted    = txtVoted?.text
        let CustVar1 = txtCustVar1?.text
        let CustVar2 = txtCustVar2?.text
        let CustVar3 = txtCustVar3?.text
        
        //print("~StrName~~~~\(StrName)~Race~~\(Race)~~Sex~~\(Sex)~Party~\(Party)~Fav~~\(Fav)~Yard~~\(Yard)~Rating~~\(Rating)")
        //print("changePosition~~~~~~",changePosition)
        
        var j = 0
        if selectedCrosstabValue.count > 0{
            str = selectedCrosstabValue[changePosition]
        }
        //print("str~~~~~~",str,changePosition,selectedCrosstabValue.count)
        for i in changePosition..<selectedCrosstabValue.count{
            print("changePosition~~~~~",changePosition,i,j)
            if changePosition >= 0{
                if "streetname" == selectedCrosstabValue[i > 0 ? i-j : i]{
                    txtStrName?.text = ""
                    strName = ""
                }
                if "gender" == selectedCrosstabValue[i > 0 ? i-j : i]{
                    txtSex?.text = ""
                }
                if "race" == selectedCrosstabValue[i > 0 ? i-j : i]{
                    txtRace?.text = ""
                }
                if "party" == selectedCrosstabValue[i > 0 ? i-j : i]{
                    txtParty?.text = ""
                }
                if "favorability" == selectedCrosstabValue[i > 0 ? i-j : i]{
                    txtFav?.text = ""
                }
                if "yardsign" == selectedCrosstabValue[i > 0 ? i-j : i]{
                    txtYard?.text = ""
                }
                if "rating" == selectedCrosstabValue[i > 0 ? i-j : i]{
                    txtRating?.text = ""
                }
                if "city" == selectedCrosstabValue[i > 0 ? i-j : i]{
                    txtCity?.text = ""
                }
                if "Zipcode5" == selectedCrosstabValue[i > 0 ? i-j : i]{
                    txtZip5!.text = ""
                }
                if "Zipcode4" == selectedCrosstabValue[i > 0 ? i-j : i]{
                    txtZip4!.text = ""
                }
                if "parish" == selectedCrosstabValue[i > 0 ? i-j : i]{
                    txtParish?.text = ""
                }
                if "ward" == selectedCrosstabValue[i > 0 ? i-j : i]{
                    txtWard?.text = ""
                }
                if "precinct" == selectedCrosstabValue[i > 0 ? i-j : i]{
                    txtPrecinct?.text = ""
                }
                if "voted" == selectedCrosstabValue[i > 0 ? i-j : i]{
                    txtVoted?.text = ""
                }
                if "custvar1" == selectedCrosstabValue[i > 0 ? i-j : i]{
                    txtCustVar1?.text = ""
                    print("~~~custvar1~~~")
                }
                if "custvar2" == selectedCrosstabValue[i > 0 ? i-j : i]{
                    txtCustVar2?.text = ""
                    print("~~~custvar2~~~")
                }
                if "custvar3" == selectedCrosstabValue[i > 0 ? i-j : i]{
                    txtCustVar3?.text = ""
                    print("~~~custvar3~~~")
                }
                //print("selectedCrosstabValue...0",selectedCrosstabValue)
                selectedCrosstabValue.remove(at: i > 0 ? i-j : i)
                print("selectedCrosstabValue...1",selectedCrosstabValue)
            }else{
                
            }
            j += 1
        }
        if changePosition >= 0{
            print("str~~~~~",str,selectedCrosstabValue)
            if !selectedCrosstabValue.contains(str){
                selectedCrosstabValue.append(str)
            }
            
            
            if str.contains("streetname"){
                strName = streetName
                txtStrName?.text = StrName
            }
            if str.contains("gender"){
                txtSex?.text = Sex
            }
            if str.contains("race"){
                txtRace?.text = Race
            }
            if str.contains("party"){
                txtParty?.text = Party
            }
            if str.contains("favorability"){
                txtFav?.text = Fav
            }
            if str.contains("yardsign"){
                txtYard?.text = Yard
            }
            if str.contains("rating"){
                txtRating?.text = Rating1
            }
            if str.contains("city"){
                txtCity?.text = City
            }
            if str.contains("Zipcode5"){
                txtZip5!.text = Zip5
            }
            if str.contains("Zipcode4"){
                txtZip4!.text = Zip4
            }
            if str.contains("parish"){
                txtParish?.text = Parish
            }
            if str.contains("ward"){
                txtWard?.text = Ward
            }
            if str.contains("precinct"){
                txtPrecinct?.text = Precinct
            }
            if str.contains("voted"){
                txtVoted?.text = Voted
            }
            if str.contains("custvar1"){
                txtCustVar1?.text = CustVar1
            }
            if str.contains("custvar2"){
                txtCustVar2?.text = CustVar2
            }
            if str.contains("custvar3"){
                txtCustVar3?.text = CustVar3
            }
        }
        
        if (isSelectPopup == 0){
            print("isDynamic~~~~~~~~~~0000")
        }else if (self.isDynamic == "2"){
            print("isDynamic~~~~~~~~~~2222")
        }else{
            print("isDynamic~~~~~~~~~1111")
//            let dispatchAfter = DispatchTimeInterval.seconds(Int(1.0))
//            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + dispatchAfter, execute: {
                self.callCrosstabSearchFilter()
//            })
            
        }
        
    }
    
    func ClearTextValueAll(){
        print("~~~~~ClearTextValueAll~~~~~~~`")
        let txtStrName = self.textFieldArr[3] as? UITextField
        let txtRace = self.textFieldArr[8] as? UITextField
        let txtSex = self.textFieldArr[10] as? UITextField
        let txtParty = self.textFieldArr[9] as? UITextField
        let txtFav = self.textFieldArr[11] as? UITextField
        let txtYard = self.textFieldArr[12] as? UITextField
        let txtRating = self.textFieldArr[7] as? UITextField
        let txtCity = self.textFieldArr[4] as? UITextField
        let txtZip5 = self.textFieldArr[5] as? UITextField
        let txtZip4 = self.textFieldArr[6] as? UITextField
        let txtParish = self.textFieldArr[13] as? UITextField
        let txtWard = self.textFieldArr[14] as? UITextField
        let txtPrecinct = self.textFieldArr[15] as? UITextField
        let txtVoted = self.textFieldArr[16] as? UITextField
        let txtCustVar1 = self.textFieldArr[17] as? UITextField
        let txtCustVar2 = self.textFieldArr[18] as? UITextField
        let txtCustVar3 = self.textFieldArr[19] as? UITextField
        
        if crossTabMode == 1 {
            txtStrName?.text = ""
            for i in 0..<selectedCrosstabValue.count{
                if selectedCrosstabValue[i] == "streetname"{
                    changePosition = i
                }
            }
        }else if crossTabMode == 2 {
            isSelectPopup = Residence_City
            txtCity?.text = ""
            for i in 0..<selectedCrosstabValue.count{
                if selectedCrosstabValue[i] == "city"{
                    changePosition = i
                }
            }
            for i in 0..<cityArr.count{
                let comObj = self.cityArr[i] as! multiSelcted
                comObj.isSelected = false
            }
            
            
        }else if crossTabMode == 3 {
            isSelectPopup = Residence_ZipCode5
            txtZip5?.text = ""
            for i in 0..<selectedCrosstabValue.count{
                if selectedCrosstabValue[i] == "Zipcode5"{
                    changePosition = i
                }
            }
            for i in 0..<zip5Arr.count{
                let comObj = self.zip5Arr[i] as! multiSelcted
                comObj.isSelected = false
            }
            
        }else if crossTabMode == 4 {
            isSelectPopup = Residence_ZipCode4
            txtZip4?.text = ""
            for i in 0..<selectedCrosstabValue.count{
                if selectedCrosstabValue[i] == "Zipcode4"{
                    changePosition = i
                }
            }
            for i in 0..<zip4Arr.count{
                let comObj = self.zip4Arr[i] as! multiSelcted
                comObj.isSelected = false
            }
            
        }else if crossTabMode == 5 {
            isSelectPopup = Rating
            txtRating?.text = ""
            for i in 0..<selectedCrosstabValue.count{
                if selectedCrosstabValue[i] == "rating"{
                    changePosition = i
                }
            }
            for i in 0..<ratingArr.count{
                let comObj = self.ratingArr[i] as! multiSelcted
                comObj.isSelected = false
            }
            
        }else if crossTabMode == 6 {
            isSelectPopup = Personal_Race
            txtRace?.text = ""
            for i in 0..<selectedCrosstabValue.count{
                if selectedCrosstabValue[i] == "race"{
                    changePosition = i
                }
            }
            
            for i in 0..<raceArr1.count{
                let comObj = self.raceArr1[i] as! multiSelcted
                comObj.isSelected = false
            }
            
        }else if crossTabMode == 7 {
            isSelectPopup = Registration_PoliticalPartyCode
            txtParty?.text = ""
            for i in 0..<selectedCrosstabValue.count{
                if selectedCrosstabValue[i] == "party"{
                    changePosition = i
                }
            }
            for i in 0..<partyArr1.count{
                let comObj = self.partyArr1[i] as! multiSelcted
                comObj.isSelected = false
            }
            
        }else if crossTabMode == 8 {
            isSelectPopup = Personal_Sex
            txtSex?.text = ""
            for i in 0..<selectedCrosstabValue.count{
                if selectedCrosstabValue[i] == "gender"{
                    changePosition = i
                }
            }
            for i in 0..<self.genderArr1.count{
                let comObj = self.genderArr1[i] as! multiSelcted
                comObj.isSelected = false
            }
        }else if crossTabMode == 9 {
            isSelectPopup = Favorability
            txtFav?.text = ""
            for i in 0..<selectedCrosstabValue.count{
                if selectedCrosstabValue[i] == "favorability"{
                    changePosition = i
                }
            }
            for i in 0..<favArr1.count{
                let comObj = self.favArr1[i] as! multiSelcted
                comObj.isSelected = false
            }
            
        }else if crossTabMode == 10 {
            isSelectPopup = Yard_Sign
            txtYard?.text = ""
            for i in 0..<selectedCrosstabValue.count{
                if selectedCrosstabValue[i] == "yardsign"{
                    changePosition = i
                }
            }
            for i in 0..<yardArr1.count{
                let comObj = self.yardArr1[i] as! multiSelcted
                comObj.isSelected = false
            }
            
            
        }else if crossTabMode == 11 {
            isSelectPopup = Jurisdiction_Parish
            txtParish?.text = ""
            for i in 0..<selectedCrosstabValue.count{
                if selectedCrosstabValue[i] == "parish"{
                    changePosition = i
                }
            }
            for i in 0..<parishArray.count{
                let comObj = parishArray[i] as! multiSelcted
                comObj.isSelected = false
            }
            
        }else if crossTabMode == 12 {
            isSelectPopup = Jurisdiction_Ward
            txtWard?.text = ""
            for i in 0..<selectedCrosstabValue.count{
                if selectedCrosstabValue[i] == "ward"{
                    changePosition = i
                }
            }
            for i in 0..<wardArray.count{
                let comObj = wardArray[i] as! multiSelcted
                comObj.isSelected = false
            }
            
        }else if crossTabMode == 13 {
            isSelectPopup = Jurisdiction_Precinct
            txtPrecinct?.text = ""
            for i in 0..<selectedCrosstabValue.count{
                if selectedCrosstabValue[i] == "precinct"{
                    changePosition = i
                }
            }
            for i in 0..<precinctArray.count{
                let comObj = precinctArray[i] as! multiSelcted
                comObj.isSelected = false
            }
            
        }else if crossTabMode == 14 {
            isSelectPopup = voted
            txtVoted?.text = ""
            for i in 0..<selectedCrosstabValue.count{
                if selectedCrosstabValue[i] == "voted"{
                    changePosition = i
                }
            }
            for i in 0..<votedArr.count{
                let comObj = self.votedArr[i] as! multiSelcted
                comObj.isSelected = false
            }
            
        }else if crossTabMode == 15 {
            isSelectPopup = Custom_Variable1
            txtCustVar1?.text = ""
            for i in 0..<selectedCrosstabValue.count{
                if selectedCrosstabValue[i] == "custvar1"{
                    changePosition = i
                }
            }
            for i in 0..<custVarArr1.count{
                let comObj = self.custVarArr1[i] as! multiSelcted
                comObj.isSelected = false
            }
            
        }else if crossTabMode == 16 {
            isSelectPopup = Custom_Variable2
            txtCustVar2?.text = ""
            for i in 0..<selectedCrosstabValue.count{
                if selectedCrosstabValue[i] == "custvar2"{
                    changePosition = i
                }
            }
            for i in 0..<custVarArr2.count{
                let comObj = self.custVarArr2[i] as! multiSelcted
                comObj.isSelected = false
            }
            
        }else {
            isSelectPopup = Custom_Variable3
            txtCustVar3?.text = ""
            for i in 0..<selectedCrosstabValue.count{
                if selectedCrosstabValue[i] == "custvar3"{
                    changePosition = i
                }
            }
            for i in 0..<custVarArr3.count{
                let comObj = self.custVarArr3[i] as! multiSelcted
                comObj.isSelected = false
            }
        }
        RefreshValue()
    }
}


