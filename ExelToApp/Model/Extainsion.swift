//
//  Extainsion.swift
//  ExelToApp
//
//  Created by baps on 09/01/20.
//  Copyright © 2020 Osiya. All rights reserved.
//

import UIKit

//================ extension for alert view ===================
extension UIViewController {
    func alert(message: String, title: String ) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
}

//================ extension for image show through URL ===================
let imageCache = NSCache<AnyObject, AnyObject>()
extension UIImageView {
    public func imageFromServerURL(urlString: String) {
        if let imageFromCache = imageCache.object(forKey: urlString as AnyObject) as? UIImage{
            self.image = imageFromCache
            return
        }
        URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, response, error) -> Void in
            if error != nil {
                print(error!)
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                self.image = image
            })
        }).resume()
    }
    
    
    public func imageFromServerURLBlackLoader(urlString: String) {
        if let imageFromCache = imageCache.object(forKey: urlString as AnyObject) as? UIImage{
            self.image = imageFromCache
            return
        }
        URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, response, error) -> Void in
            if error != nil {
                print(error!)
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                self.image = image
            })
        }).resume()
    }
}


//================ extension for image show through URL ===================
extension UIImageView {
    func downloadImageFrom(link:String, contentMode: UIView.ContentMode) {
        URLSession.shared.dataTask( with: NSURL(string:link)! as URL, completionHandler: {
            (data, response, error) -> Void in
            DispatchQueue.main.async() {
                self.contentMode =  contentMode
                if let data = data { self.image = UIImage(data: data) }
            }
        }).resume()
    }
}

//================ extension for UIActivityIndicatorView ===================
extension UIView{
    
    func activityStartAnimating() {
        let backgroundView = UIView()
        backgroundView.isHidden = false
        backgroundView.frame = CGRect.init(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height)
        backgroundView.backgroundColor = UIColor.clear
        backgroundView.tag = 475647
        var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
        activityIndicator.isHidden = false
        activityIndicator = UIActivityIndicatorView(frame: CGRect.init(x: 0, y: 0, width: 100, height: 100))
        activityIndicator.center = self.center
        activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
        activityIndicator.color = UIColor.black//UIColor(red:0.00, green:0.00, blue:0.50, alpha:1.0)
        activityIndicator.startAnimating()
        activityIndicator.hidesWhenStopped = true
        self.isUserInteractionEnabled = false
        backgroundView.addSubview(activityIndicator)
        self.addSubview(backgroundView)
    }
    
    func activityStopAnimating() {
        
        if let background = viewWithTag(475647){
            background.removeFromSuperview()
            background.isHidden = true
        }
        self.isUserInteractionEnabled = true
    }
    
}

//================ extension for Date ===================
extension Date {
    func app_stringFromDate() -> String{
        let dateFormatter:DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        let strdt = dateFormatter.string(from: self as Date)
        if let dtDate = dateFormatter.date(from: strdt){
            return dateFormatter.string(from: dtDate)
        }
        return "--"
    }
    
    func app_stringFromDate1() -> String{
        let dateFormatter:DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM/dd/yyyy"
        let strdt = dateFormatter.string(from: self as Date)
        if let dtDate = dateFormatter.date(from: strdt){
            return dateFormatter.string(from: dtDate)
        }
        return "--"
    }
}

//================ extension for convert url to Image ===================
extension UIImage {
    convenience init?(url: URL?) {
        guard let url = url else { return nil }
        
        do {
            let data = try Data(contentsOf: url)
            self.init(data: data)
        } catch {
            print("Cannot load image from url: \(url) with error: \(error)")
            return nil
        }
    }
}

extension Dictionary {
    mutating func merge(dict: [Key: Value]){
        for (k, v) in dict {
            updateValue(v, forKey: k)
        }
    }
}

extension UIViewController {
    
    func presentAlertWithTitle(title: String, message: String, options: String..., completion: @escaping (Int) -> Void) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, option) in options.enumerated() {
            alertController.addAction(UIAlertAction.init(title: option, style: .default, handler: { (action) in
                completion(index)
            }))
        }
        self.present(alertController, animated: true, completion: nil)
    }
}

//= == =============TextField Padding === ================
public func txtPaddingVw(txt:UITextField) {
    let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 5))
    txt.leftViewMode = .always
    txt.leftView = paddingView
}

extension UIButton {
    func setButton(X:CGFloat,Y:CGFloat,Width:CGFloat,Height:CGFloat,TextColor:UIColor,BackColor:UIColor,Title:String,Align:ContentHorizontalAlignment,Font:UIFont) {
        self.frame = CGRect(x: X, y: Y, width: Width, height: Height)
        self.setTitleColor(TextColor, for: .normal)
        self.setTitle(Title, for: .normal)
        self.backgroundColor = BackColor
        self.layer.cornerRadius = 5.0
        self.clipsToBounds = true
        self.contentHorizontalAlignment = Align
        self.titleLabel?.font = Font
    }
    func setButton1(TextColor:UIColor,BackColor:UIColor,Title:String,Align:ContentHorizontalAlignment,Font:UIFont) {
        
        self.setTitleColor(TextColor, for: .normal)
        self.setTitle(Title, for: .normal)
        self.backgroundColor = BackColor
        self.layer.cornerRadius = 5.0
        self.clipsToBounds = true
        self.contentHorizontalAlignment = Align
        self.titleLabel?.font = Font
    }
    func setButtonDesboard(X:CGFloat,Y:CGFloat,Width:CGFloat,Height:CGFloat,TextColor:UIColor,BackColor:UIColor,Title:String) {
        
        self.frame = CGRect(x: X, y: Y, width: Width, height: Height)
        self.layer.cornerRadius = Height/3
        self.layer.masksToBounds = true
        self.layer.borderWidth = 3
        self.layer.borderColor = TextColor.cgColor
        self.setTitleColor(TextColor, for: .normal)
        self.setTitle(Title, for: .normal)
        self.backgroundColor = BackColor
        self.clipsToBounds = true
    }
}

extension UILabel {
    func setLabel(X:CGFloat,Y:CGFloat,Width:CGFloat,Height:CGFloat,TextColor:UIColor,BackColor:UIColor,Text:String,TextAlignment:NSTextAlignment,Font:UIFont) {
        
        self.frame = CGRect(x: X, y: Y, width: Width, height: Height)
        self.backgroundColor = BackColor
        self.text = Text
        self.textColor = TextColor
        self.font = Font
        self.textAlignment = TextAlignment
        self.clipsToBounds = true
    }
}

extension UITextField {
    func setTextField(X:CGFloat,Y:CGFloat,Width:CGFloat,Height:CGFloat,TextColor:UIColor,BackColor:UIColor) {
        
        self.frame = CGRect(x: X, y: Y, width: Width, height: Height)
        self.textColor = TextColor
        self.backgroundColor = BackColor
//        self.layer.cornerRadius = 5
//        self.layer.masksToBounds = true
//        self.layer.borderWidth = 2
//        self.layer.borderColor = TextColor.cgColor
        
    }
    
    class func connectTextFields(fields: [UITextField]) -> Void{
        guard let last = fields.last else {
            return
        }
        for i in 0 ..< fields.count - 1 {
            fields[i].returnKeyType = .next
            fields[i].addTarget(fields[i+1], action: #selector(UIResponder.becomeFirstResponder), for: .editingDidEndOnExit)
        }
        last.returnKeyType = .done
        last.addTarget(last, action: #selector(UIResponder.resignFirstResponder), for: .editingDidEndOnExit)
    }
    
}
extension UIImageView {
    
    func setImageView(X:CGFloat,Y:CGFloat,Width:CGFloat,Height:CGFloat,img:UIImage) {
        self.frame = CGRect(x: X, y: Y, width: Width, height: Height)
        self.image = img
        self.clipsToBounds = true
        self.contentMode = UIView.ContentMode.scaleAspectFit
    }
}

extension UIScrollView {
    func scrollToTop() {
        let desiredOffset = CGPoint(x: 0, y: -contentInset.top)
        setContentOffset(desiredOffset, animated: true)
    }
}


extension UIView {
    func anchor(top: NSLayoutYAxisAnchor?, paddingTop: CGFloat, bottom: NSLayoutYAxisAnchor?, paddingBottom: CGFloat, left: NSLayoutXAxisAnchor?, paddingLeft: CGFloat, right: NSLayoutXAxisAnchor?, paddingRight: CGFloat, width: CGFloat, height: CGFloat) {
        translatesAutoresizingMaskIntoConstraints = false
        if let top = top {
            topAnchor.constraint(equalTo: top, constant: paddingTop).isActive = true
        }
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: -paddingBottom).isActive = true
        }
        if let right = right {
            rightAnchor.constraint(equalTo: right, constant: -paddingRight).isActive = true
        }
        if let left = left {
            leftAnchor.constraint(equalTo: left, constant: paddingLeft).isActive = true
        }
        if width != 0 {
            widthAnchor.constraint(equalToConstant: width).isActive = true
        }
        if height != 0 {
            heightAnchor.constraint(equalToConstant: height).isActive = true
        }
    }
}

extension UIView {
    
    public func removeAllConstraints() {
        var _superview = self.superview
        
        while let superview = _superview {
            for constraint in superview.constraints {
                if let first = constraint.firstItem as? UIView, first == self {
                    superview.removeConstraint(constraint)
                }
                if let second = constraint.secondItem as? UIView, second == self {
                    superview.removeConstraint(constraint)
                }
            }
            _superview = superview.superview
        }
        self.removeConstraints(self.constraints)
        self.translatesAutoresizingMaskIntoConstraints = true
    }
}

//extension UIView {
//    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
//        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
//        let mask = CAShapeLayer()
//        mask.path = path.cgPath
//        self.layer.mask = mask
//    }
//}

extension UIView {
    
    func roundCorners(corners:CACornerMask, radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.maskedCorners = corners
    }
}

extension UIImageView {
    func setImageColor(color: UIColor) {
        let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
}

extension String {
    
    func slice(from: String, to: String) -> String? {
        
        return (range(of: from)?.upperBound).flatMap { substringFrom in
            (range(of: to, range: substringFrom..<endIndex)?.lowerBound).map { substringTo in
                String(self[substringFrom..<substringTo])
            }
        }
    }
}

extension HomeViewController:UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

extension String {
    var withoutSpecialCharacters: String {
        return self.components(separatedBy: CharacterSet.symbols).joined(separator: "")
    }
}
