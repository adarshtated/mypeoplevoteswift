//
//  MyReportsView.swift
//  ExelToApp
//
//  Created by baps on 01/02/20.
//  Copyright © 2020 Osiya. All rights reserved.
//

import UIKit

class MyReportsView: UIView ,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate{
    var mainView:UIView!
    var lblHeading:UILabel!
    var popUpView:UIView!
    var txtStartDate:UITextField!
    var txtEndDate:UITextField!
    var txtSelectUSer:UITextField!
    var txtUserType:UITextField!
    var btnCancel:UIButton!
    var btnOk:UIButton!
    var topView:UIView!
    
    var userView:UIView!
    var tableView:UITableView!
    let cellReuseIdentifier = "cell"
    var jsonUser = NSDictionary()
    
    var firstNameArr = NSMutableArray()
    var lastNameArr = NSMutableArray()
    var userArr = NSMutableArray()
    var userTypArr = NSMutableArray()
    var userFavorArr = NSMutableArray()
    var datePicker:UIDatePicker!
    var datePicker1:UIDatePicker!
    var resultView:UIView!
    var btnResult:UIButton!
    var buttonView:UIView!
    
    var u_id = USER_DEFAULTS.string(forKey: "u_id") ?? ""
    var cp_id = USER_DEFAULTS.string(forKey: "cp_id") ?? ""
    
    let bordeColor = UIColor(red: 22.0/255.0 ,green:128.0/255.0 ,blue:296.0/255.0 ,alpha:1.0).cgColor
    
    var selectedUserID = ""
    var search_field_value = ""
    var field_name = ""
    
    var usertype = USER_DEFAULTS.string(forKey: "usertype") ?? ""
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
        
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpView()
    }
    
    func setUpView() {
        mainView = UIView()
        mainView.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT)
        mainView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.addSubview(mainView)
        
        popUpView = UIView()
        popUpView.frame = CGRect(x: 15, y: mainView.frame.height/2-200, width: mainView.frame.width-30, height: 235)
        popUpView.backgroundColor = UIColor.white
        popUpView.layer.cornerRadius = 10.0
        popUpView.clipsToBounds = true
        mainView.addSubview(popUpView)
        
        topView = UIView()
        topView.frame = CGRect(x: 0, y: 0, width: popUpView.frame.width, height: 50)
        topView.backgroundColor = APPBUTTONCOLOR
        popUpView.addSubview(topView)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 0, Y: 10, Width: popUpView.frame.width, Height: 30, TextColor: .white, BackColor: .clear, Text: "Productivity Report", TextAlignment: .center, Font: .boldSystemFont(ofSize: 18))
        topView.addSubview(lblHeading)
        
        btnCancel = UIButton(type: .system)
        btnCancel.setButton(X: popUpView.frame.width-30, Y: 5, Width: 24, Height: 24, TextColor: .white, BackColor: .clear, Title: "X", Align: .center, Font: .boldSystemFont(ofSize: 22))
        btnCancel.tag = 2
        btnCancel.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        topView.addSubview(btnCancel)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 5, Y: topView.frame.maxY+10, Width: popUpView.frame.width-10, Height: 30, TextColor: APPBUTTONCOLOR, BackColor: .clear, Text: "Date Range", TextAlignment: .center, Font: .boldSystemFont(ofSize: 16))
        lblHeading.layer.borderWidth = 1.0
        lblHeading.layer.borderColor = bordeColor
        lblHeading.roundCorners(corners: [.layerMinXMinYCorner,.layerMaxXMinYCorner], radius: 5)
        popUpView.addSubview(lblHeading)
        
        txtStartDate = UITextField()
        txtStartDate.setTextField(X: 5, Y: lblHeading.frame.maxY-1, Width: popUpView.frame.width/2-5, Height: 35, TextColor: .black, BackColor: .clear)
        txtStartDate.layer.borderWidth = 1.0
        txtStartDate.layer.borderColor = bordeColor
        txtStartDate.placeholder = "Select Start Date"
        txtStartDate.textAlignment = .center
        txtStartDate.font = UIFont.systemFont(ofSize: 15)
        popUpView.addSubview(txtStartDate)
        
        txtEndDate = UITextField()
        txtEndDate.setTextField(X: txtStartDate.frame.maxX-1, Y: lblHeading.frame.maxY-1, Width: popUpView.frame.width/2-4, Height: 35, TextColor: .black, BackColor: .clear)
        txtEndDate.layer.borderWidth = 1.0
        txtEndDate.layer.borderColor = bordeColor
        txtEndDate.placeholder = "Select End Date"
        txtEndDate.textAlignment = .center
        txtEndDate.font = UIFont.systemFont(ofSize: 15)
        popUpView.addSubview(txtEndDate)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 5, Y: txtStartDate.frame.maxY-1, Width: popUpView.frame.width/2-5, Height: 30, TextColor: APPBUTTONCOLOR, BackColor: .clear, Text: "User", TextAlignment: .center, Font: .boldSystemFont(ofSize: 16))
        lblHeading.layer.borderWidth = 1.0
        lblHeading.layer.borderColor = bordeColor
        popUpView.addSubview(lblHeading)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: txtStartDate.frame.maxX-1, Y: txtEndDate.frame.maxY-1, Width: popUpView.frame.width/2-4, Height: 30, TextColor: APPBUTTONCOLOR, BackColor: .clear, Text: "User Type", TextAlignment: .center, Font: .boldSystemFont(ofSize: 16))
        lblHeading.layer.borderWidth = 1.0
        lblHeading.layer.borderColor = bordeColor
        
        popUpView.addSubview(lblHeading)
        
        
        txtSelectUSer = UITextField()
        txtSelectUSer.setTextField(X: 5, Y: lblHeading.frame.maxY-1, Width: popUpView.frame.width/2-5, Height: 35, TextColor: .black, BackColor: .clear)
        txtSelectUSer.layer.borderWidth = 1.0
        txtSelectUSer.layer.borderColor = bordeColor
        txtSelectUSer.placeholder = "Select User"
        txtSelectUSer.textAlignment = .center
        txtSelectUSer.font = UIFont.systemFont(ofSize: 15)
        txtSelectUSer.delegate = self
        txtSelectUSer.roundCorners(corners: [.layerMinXMaxYCorner], radius: 5)
        popUpView.addSubview(txtSelectUSer)
        
        txtUserType = UITextField()
        txtUserType.setTextField(X: txtSelectUSer.frame.maxX-1, Y: lblHeading.frame.maxY-1, Width: popUpView.frame.width/2-4, Height: 35, TextColor: .black, BackColor: .clear)
        txtUserType.layer.borderWidth = 1.0
        txtUserType.layer.borderColor = bordeColor
        txtUserType.placeholder = "User Type"
        txtUserType.textAlignment = .center
        txtUserType.font = UIFont.systemFont(ofSize: 15)
        txtUserType.isEnabled = false
        txtUserType.roundCorners(corners: [.layerMaxXMaxYCorner], radius: 5)
        popUpView.addSubview(txtUserType)
        
        btnOk = UIButton(type: .system)
        btnOk.setButton(X: popUpView.frame.width/2-(popUpView.frame.width/4)/2, Y: txtUserType.frame.maxY+10, Width: popUpView.frame.width/4, Height: 30, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Ok", Align: .center, Font: .systemFont(ofSize: 16.0))
        btnOk.tag = 1
        btnOk.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        popUpView.addSubview(btnOk)
        
        
        userView = UIView()
        userView.frame = CGRect(x: 10, y: mainView.frame.height/2-200, width: mainView.frame.width-20, height: 300)
        userView.backgroundColor = UIColor.white
        userView.layer.cornerRadius = 10.0
        userView.clipsToBounds = true
        mainView.addSubview(userView)
        
        topView = UIView()
        topView.frame = CGRect(x: 0, y: 0, width: userView.frame.width, height: 50)
        topView.backgroundColor = APPBUTTONCOLOR
        userView.addSubview(topView)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 0, Y: 10, Width: popUpView.frame.width, Height: 30, TextColor: .white, BackColor: .clear, Text: "Select User", TextAlignment: .center, Font: .boldSystemFont(ofSize: 16))
        topView.addSubview(lblHeading)
        
        btnCancel = UIButton(type: .system)
        btnCancel.setButton(X: popUpView.frame.width-30, Y: 5, Width: 24, Height: 24, TextColor: .white, BackColor: .clear, Title: "X", Align: .center, Font: .boldSystemFont(ofSize: 22))
        btnCancel.tag = 2
        btnCancel.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        topView.addSubview(btnCancel)
        
        tableView = UITableView()
        tableView.frame = CGRect(x: 0, y: topView.frame.maxY+10, width: userView.frame.width, height: userView.frame.height-50)
        tableView.backgroundColor = UIColor.white
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        userView.addSubview(tableView)
        tableView.tableFooterView = UIView()
        tableView.layer.cornerRadius = 10.0
        userView.isHidden = true
        
        datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        txtStartDate.inputView = datePicker
        datePicker.maximumDate = Date()
        var toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 44))
        toolBar.tintColor = UIColor.blue
        var cancelBtn = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatepicker))
        var doneBtn = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(showStartDate))
        var space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.items = [cancelBtn, space, doneBtn]
        txtStartDate.inputAccessoryView = toolBar
        
        datePicker1 = UIDatePicker()
        datePicker1.datePickerMode = .date
        txtEndDate.inputView = datePicker1
        datePicker.maximumDate = Date()
        toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 44))
        toolBar.tintColor = UIColor.blue
        cancelBtn = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatepicker))
        doneBtn = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(showEdnDate))
        space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.items = [cancelBtn, space, doneBtn]
        txtEndDate.inputAccessoryView = toolBar
        
        
        resultView = UIView()
        resultView.frame = CGRect(x: 10, y: 10, width: SWIDTH-20, height: 580)
        resultView.backgroundColor = UIColor.white
        resultView.layer.cornerRadius = 10.0
        resultView.clipsToBounds = true
        mainView.addSubview(resultView)
        
        topView = UIView()
        topView.frame = CGRect(x: 0, y: 0, width: resultView.frame.width, height: 50)
        topView.backgroundColor = APPBUTTONCOLOR
        resultView.addSubview(topView)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 0, Y: 10, Width: topView.frame.width, Height: 30, TextColor: .white, BackColor: .clear, Text: "My Report Result", TextAlignment: .center, Font: .boldSystemFont(ofSize: 18))
        topView.addSubview(lblHeading)
        
        btnCancel = UIButton(type: .system)
        btnCancel.setButton(X: topView.frame.width-30, Y: 5, Width: 24, Height: 24, TextColor: .white, BackColor: .clear, Title: "X", Align: .center, Font: .boldSystemFont(ofSize: 22))
        btnCancel.tag = 2
        btnCancel.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        topView.addSubview(btnCancel)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 5, Y: topView.frame.maxY+10, Width: resultView.frame.width-10, Height: 30, TextColor: APPBUTTONCOLOR, BackColor: .clear, Text: "Favorability", TextAlignment: .center, Font: .boldSystemFont(ofSize: 17))
        lblHeading.layer.borderWidth = 1.0
        lblHeading.layer.borderColor = bordeColor
        lblHeading.roundCorners(corners: [.layerMinXMinYCorner,.layerMaxXMinYCorner], radius: 5)
        resultView.addSubview(lblHeading)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 5, Y: topView.frame.maxY+39, Width: resultView.frame.width-10, Height: 35, TextColor: APPBUTTONCOLOR, BackColor: .clear, Text: " Favor", TextAlignment: .left, Font: .systemFont(ofSize: 17.0))
        lblHeading.layer.borderWidth = 1.0
        lblHeading.layer.borderColor = bordeColor
        resultView.addSubview(lblHeading)
        
        btnResult = UIButton(type: .system)
        btnResult.setButton(X: resultView.frame.width-70, Y: topView.frame.maxY+41, Width: 60, Height: 31, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "0", Align: .center, Font: .systemFont(ofSize: 15))
        btnResult.tag = 3
        btnResult.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        resultView.addSubview(btnResult)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 5, Y: topView.frame.maxY+73, Width: resultView.frame.width-10, Height: 35, TextColor: APPBUTTONCOLOR, BackColor: .clear, Text: " Not Favor", TextAlignment: .left, Font: .systemFont(ofSize: 17.0))
        lblHeading.layer.borderWidth = 1.0
        lblHeading.layer.borderColor = bordeColor
        resultView.addSubview(lblHeading)
        
        btnResult = UIButton(type: .system)
        btnResult.setButton(X: resultView.frame.width-70, Y: topView.frame.maxY+75, Width: 60, Height: 31, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "0", Align: .center, Font: .systemFont(ofSize: 15))
        btnResult.tag = 4
        btnResult.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        resultView.addSubview(btnResult)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 5, Y: topView.frame.maxY+107, Width: resultView.frame.width-10, Height: 35, TextColor: APPBUTTONCOLOR, BackColor: .clear, Text: " Undecided", TextAlignment: .left, Font: .systemFont(ofSize: 17.0))
        lblHeading.layer.borderWidth = 1.0
        lblHeading.layer.borderColor = bordeColor
        resultView.addSubview(lblHeading)
        
        btnResult = UIButton(type: .system)
        btnResult.setButton(X: resultView.frame.width-70, Y: topView.frame.maxY+109, Width: 60, Height: 31, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "0", Align: .center, Font: .systemFont(ofSize: 15))
        btnResult.tag = 5
        btnResult.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        resultView.addSubview(btnResult)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 5, Y: topView.frame.maxY+141, Width: resultView.frame.width-10, Height: 35, TextColor: APPBUTTONCOLOR, BackColor: .clear, Text: " Not Available", TextAlignment: .left, Font: .systemFont(ofSize: 17.0))
        lblHeading.layer.borderWidth = 1.0
        lblHeading.layer.borderColor = bordeColor
        lblHeading.roundCorners(corners: [.layerMinXMaxYCorner,.layerMaxXMaxYCorner], radius: 5)
        resultView.addSubview(lblHeading)
        
        btnResult = UIButton(type: .system)
        btnResult.setButton(X: resultView.frame.width-70, Y: topView.frame.maxY+143, Width: 60, Height: 31, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "0", Align: .center, Font: .systemFont(ofSize: 15))
        btnResult.tag = 6
        btnResult.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        resultView.addSubview(btnResult)
        
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 5, Y: topView.frame.maxY+241, Width: resultView.frame.width-10, Height: 30, TextColor: APPBUTTONCOLOR, BackColor: .clear, Text: "Yard Sign", TextAlignment: .center, Font: .boldSystemFont(ofSize: 17))
        lblHeading.layer.borderWidth = 1.0
        lblHeading.layer.borderColor = bordeColor
        lblHeading.roundCorners(corners: [.layerMinXMinYCorner,.layerMaxXMinYCorner], radius: 5)
        resultView.addSubview(lblHeading)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 5, Y: topView.frame.maxY+270, Width: resultView.frame.width-10, Height: 35, TextColor: APPBUTTONCOLOR, BackColor: .clear, Text: " Yes", TextAlignment: .left, Font: .systemFont(ofSize: 17.0))
        lblHeading.layer.borderWidth = 1.0
        lblHeading.layer.borderColor = bordeColor
        resultView.addSubview(lblHeading)
        
        btnResult = UIButton(type: .system)
        btnResult.setButton(X: resultView.frame.width-70, Y: topView.frame.maxY+272, Width: 60, Height: 31, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "0", Align: .center, Font: .systemFont(ofSize: 15))
        btnResult.tag = 7
        btnResult.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        resultView.addSubview(btnResult)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 5, Y: topView.frame.maxY+304, Width: resultView.frame.width-10, Height: 35, TextColor: APPBUTTONCOLOR, BackColor: .clear, Text: " No", TextAlignment: .left, Font: .systemFont(ofSize: 17.0))
        lblHeading.layer.borderWidth = 1.0
        lblHeading.layer.borderColor = bordeColor
        resultView.addSubview(lblHeading)
        
        btnResult = UIButton(type: .system)
        btnResult.setButton(X: resultView.frame.width-70, Y: topView.frame.maxY+306, Width: 60, Height: 31, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "0", Align: .center, Font: .systemFont(ofSize: 15))
        btnResult.tag = 8
        btnResult.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        resultView.addSubview(btnResult)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 5, Y: topView.frame.maxY+338, Width: resultView.frame.width-10, Height: 35, TextColor: APPBUTTONCOLOR, BackColor: .clear, Text: " Placed", TextAlignment: .left, Font: .systemFont(ofSize: 17.0))
        lblHeading.layer.borderWidth = 1.0
        lblHeading.layer.borderColor = bordeColor
        lblHeading.roundCorners(corners: [.layerMinXMaxYCorner,.layerMaxXMaxYCorner], radius: 5)
        resultView.addSubview(lblHeading)
        
        btnResult = UIButton(type: .system)
        btnResult.setButton(X: resultView.frame.width-70, Y: topView.frame.maxY+340, Width: 60, Height: 31, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "0", Align: .center, Font: .systemFont(ofSize: 15))
        btnResult.tag = 9
        btnResult.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        resultView.addSubview(btnResult)
        
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 5, Y: topView.frame.maxY+438, Width: resultView.frame.width-10, Height: 30, TextColor: APPBUTTONCOLOR, BackColor: .clear, Text: "Comments", TextAlignment: .center, Font: .boldSystemFont(ofSize: 17))
        lblHeading.layer.borderWidth = 1.0
        lblHeading.layer.borderColor = bordeColor
        lblHeading.roundCorners(corners: [.layerMinXMinYCorner,.layerMaxXMinYCorner], radius: 5)
        resultView.addSubview(lblHeading)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 5, Y: topView.frame.maxY+467, Width: resultView.frame.width-10, Height: 35, TextColor: APPBUTTONCOLOR, BackColor: .clear, Text: " Added", TextAlignment: .left, Font: .systemFont(ofSize: 17.0))
        lblHeading.layer.borderWidth = 1.0
        lblHeading.layer.borderColor = bordeColor
        lblHeading.roundCorners(corners: [.layerMinXMaxYCorner,.layerMaxXMaxYCorner], radius: 5)
        resultView.addSubview(lblHeading)
        
        btnResult = UIButton(type: .system)
        btnResult.setButton(X: resultView.frame.width-70, Y: topView.frame.maxY+469, Width: 60, Height: 31, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "0", Align: .center, Font: .systemFont(ofSize: 15))
        btnResult.tag = 10
        btnResult.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        resultView.addSubview(btnResult)
        resultView.isHidden = true
        var hight = 210
        if usertype == "3"{
            hight = 160
        }
        
        
        buttonView = UIView()
        buttonView.frame = CGRect(x: 10, y: mainView.frame.height/2-150, width: mainView.frame.width-20, height: CGFloat(hight))
        buttonView.backgroundColor = UIColor.white
        buttonView.layer.cornerRadius = 10.0
        buttonView.clipsToBounds = true
        mainView.addSubview(buttonView)
        
        topView = UIView()
        topView.frame = CGRect(x: 0, y: 0, width: userView.frame.width, height: 50)
        topView.backgroundColor = APPBUTTONCOLOR
        buttonView.addSubview(topView)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 0, Y: 10, Width: buttonView.frame.width, Height: 30, TextColor: .white, BackColor: .clear, Text: "", TextAlignment: .center, Font: .boldSystemFont(ofSize: 16))
        topView.addSubview(lblHeading)
        
        btnCancel = UIButton(type: .system)
        btnCancel.setButton(X: buttonView.frame.width-30, Y: 5, Width: 24, Height: 24, TextColor: .white, BackColor: .clear, Title: "X", Align: .center, Font: .boldSystemFont(ofSize: 22))
        btnCancel.tag = 2
        btnCancel.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        topView.addSubview(btnCancel)
        
        btnResult = UIButton(type: .system)
        btnResult.setButton(X: 15, Y: topView.frame.maxY+10, Width: buttonView.frame.width-30, Height: 40, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Alphabetically", Align: .center, Font: .systemFont(ofSize: 15))
        btnResult.tag = 11
        btnResult.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        buttonView.addSubview(btnResult)
        
        btnResult = UIButton(type: .system)
        btnResult.setButton(X: 15, Y: topView.frame.maxY+60, Width: buttonView.frame.width-30, Height: 40, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Map", Align: .center, Font: .systemFont(ofSize: 15))
        btnResult.tag = 12
        btnResult.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        buttonView.addSubview(btnResult)
        
        btnResult = UIButton(type: .system)
        btnResult.setButton(X: 15, Y: topView.frame.maxY+110, Width: buttonView.frame.width-30, Height: 40, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Email CSV Report", Align: .center, Font: .systemFont(ofSize: 15))
        btnResult.tag = 13
        btnResult.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        buttonView.addSubview(btnResult)
        
        buttonView.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onClickRearView), name: NSNotification.Name(rawValue: "onClickRearView"), object: nil)
        
    }
    @objc func onClickRearView(notif: NSNotification) {
        self.isHidden = true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        if txtSelectUSer == textField{
            if (!(txtStartDate.text!.isEmpty) && !(txtStartDate.text!.isEmpty)){
                callGet_all_usertype()
            }
        }
    }
    
    @objc func showStartDate() {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/YYYY"
        txtStartDate.text = "\(formatter.string(from: datePicker.date))"
        datePicker1.minimumDate = datePicker.date
        txtStartDate.resignFirstResponder()
    }
    @objc func showEdnDate() {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/YYYY"
        txtEndDate.text = "\(formatter.string(from: datePicker1.date))"
        txtEndDate.resignFirstResponder()
        
    }
    @objc func cancelDatepicker() {
        txtStartDate.resignFirstResponder()
        txtEndDate.resignFirstResponder()
    }
    
    @objc func onClickButton(_ sender: UIButton) {
        if sender.tag == 2{
            if userView.isHidden == false{
                userView.isHidden = true
            }else if resultView.isHidden == false{
                popUpView.isHidden = false
                resultView.isHidden = true
            }else if buttonView.isHidden == false{
                resultView.isHidden = false
                buttonView.isHidden = true
            }else{
                self.isHidden = true
                HomeViewController.VC.navigationItem.rightBarButtonItem = HomeViewController.VC.btnEditPerson
            }
        }else if sender.tag == 1{
            
            if (txtStartDate.text!.isEmpty) || (txtEndDate.text!.isEmpty) {
                let alert = UIAlertController(title: "Alert!", message: "Please Select Start Date & End Date", preferredStyle: .alert)
                let yesButton = UIAlertAction(title: "Ok", style: .default, handler: { action in
                    //Handle your yes please button action here
                })
                alert.addAction(yesButton)
                HomeViewController.VC.present(alert, animated: true)
            } else if (txtSelectUSer.text!.isEmpty) {
                let alert = UIAlertController(title: "Alert!", message: "Please Select User", preferredStyle: .alert)
                let yesButton = UIAlertAction(title: "Ok", style: .default, handler: { action in
                    //Handle your yes please button action here
                })
                alert.addAction(yesButton)
                HomeViewController.VC.present(alert, animated: true)
            } else {
                callReports_user_wise()
            }
        }else if sender.tag == 3{
            if sender.titleLabel?.text == "0"{
                HomeViewController.VC.alert(message: "No Record Found", title: "Alert!")
            }else{
                popUpView.isHidden = true
                resultView.isHidden = true
                buttonView.isHidden = false
                lblHeading.text = "Favor"
                search_field_value = "Favor";
                field_name = "Favorability";
            }
        }else if sender.tag == 4{
            if sender.titleLabel?.text == "0"{
                HomeViewController.VC.alert(message: "No Record Found", title: "Alert!")
            }else{
                popUpView.isHidden = true
                resultView.isHidden = true
                buttonView.isHidden = false
                lblHeading.text = "Not Favor"
                search_field_value = "Not Favor";
                field_name = "Favorability";
            }
        }else if sender.tag == 5{
            if sender.titleLabel?.text == "0"{
                HomeViewController.VC.alert(message: "No Record Found", title: "Alert!")
            }else{
                popUpView.isHidden = true
                resultView.isHidden = true
                buttonView.isHidden = false
                lblHeading.text = "Undecided"
                search_field_value = "Undecided";
                field_name = "Favorability";
            }
        }else if sender.tag == 6{
            if sender.titleLabel?.text == "0"{
                HomeViewController.VC.alert(message: "No Record Found", title: "Alert!")
            }else{
                popUpView.isHidden = true
                resultView.isHidden = true
                buttonView.isHidden = false
                lblHeading.text = "Not Available"
                search_field_value = "Not Available";
                field_name = "Favorability";
            }
        }else if sender.tag == 7{
            if sender.titleLabel?.text == "0"{
                HomeViewController.VC.alert(message: "No Record Found", title: "Alert!")
            }else{
                popUpView.isHidden = true
                resultView.isHidden = true
                buttonView.isHidden = false
                lblHeading.text = "Yes"
                search_field_value = "Yes";
                field_name = "Yard_Sign";
            }
        }else if sender.tag == 8{
            if sender.titleLabel?.text == "0"{
                HomeViewController.VC.alert(message: "No Record Found", title: "Alert!")
            }else{
                popUpView.isHidden = true
                resultView.isHidden = true
                buttonView.isHidden = false
                lblHeading.text = "No"
                search_field_value = "No";
                field_name = "Yard_Sign";
            }
        }else if sender.tag == 9{
            if sender.titleLabel?.text == "0"{
                HomeViewController.VC.alert(message: "No Record Found", title: "Alert!")
            }else{
                popUpView.isHidden = true
                resultView.isHidden = true
                buttonView.isHidden = false
                lblHeading.text = "Placed"
                search_field_value = "Placed";
                field_name = "Yard_Sign";
            }
        }else if sender.tag == 10{
            if sender.titleLabel?.text == "0"{
                HomeViewController.VC.alert(message: "No Record Found", title: "Alert!")
            }else{
                popUpView.isHidden = true
                resultView.isHidden = true
                buttonView.isHidden = false
                lblHeading.text = "Comments"
                search_field_value = "Comments";
                field_name = "Comments";
            }
        }else if sender.tag == 11{
            type = 12
            paramStr = ""
            paramStr = "selected_user_id=\(selectedUserID)&startdate=\(txtStartDate.text ?? "")&enddate=\(txtEndDate.text ?? "")&search_field_value=\(search_field_value)&field_name=\(field_name)"
            HomeViewController.VC.callGetExcel(u_id: u_id, cp_id: cp_id, startRow: 0)
            self.isHidden = true
            HomeViewController.VC.navigationItem.rightBarButtonItem = HomeViewController.VC.btnEditPerson
        }else if sender.tag == 13{
            HomeViewController.VC.field_startDate = txtStartDate.text ?? ""
            HomeViewController.VC.field_endDate = txtEndDate.text ?? ""
            HomeViewController.VC.selectedUserID = selectedUserID
            HomeViewController.VC.search_field_value = search_field_value
            HomeViewController.VC.field_name = field_name
            HomeViewController.VC.callSendCSVApi(typ: "", id: 4)
            self.isHidden = true
            HomeViewController.VC.navigationItem.rightBarButtonItem = HomeViewController.VC.btnEditPerson
        }else if sender.tag == 12{
            mapType = 3
            let vc = MapViewController()
            vc.field_startDate = txtStartDate.text ?? ""
            vc.field_endDate = txtEndDate.text ?? ""
            vc.selectedUserID = selectedUserID
            vc.search_field_value = search_field_value
            vc.field_name = field_name
            HomeViewController.VC.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return firstNameArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as UITableViewCell!
        cell.textLabel?.text = "\(lastNameArr[indexPath.row]),\(firstNameArr[indexPath.row]) (\(userFavorArr[indexPath.row]))"
        cell.textLabel?.textColor = APPBUTTONCOLOR
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        txtSelectUSer.text = "\(lastNameArr[indexPath.row]),\(firstNameArr[indexPath.row])"
        txtUserType.text = "\(userTypArr[indexPath.row])"
        selectedUserID = "\(userArr[indexPath.row])"
        userView.isHidden = true
    }
    
    func callGet_all_usertype(){
        let authToken = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
        postWithoutImage(url_string: "get_all_usertype", parameters: "cid=\(cp_id)&id=\(u_id)&startdate=\(txtStartDate.text ?? "")&enddate=\(txtEndDate.text ?? "")&authToken=\(authToken)",CompletionHandler: { json, error in
            if let json = json {
                print("json~~~~~~~",json)
                let statusmassege = json.value(forKey: "status") as! Bool
                if (statusmassege == true){
                    self.firstNameArr.removeAllObjects()
                    self.lastNameArr.removeAllObjects()
                    self.userArr.removeAllObjects()
                    self.userTypArr.removeAllObjects()
                    self.userFavorArr.removeAllObjects()
                    let authToken = json.value(forKey: "authToken") as! String
                    USER_DEFAULTS.set(authToken, forKey: "authToken")
                    let arr = json.value(forKey: "data") as! NSArray
                    for i in 0..<arr.count{
                        var data = NSArray()
                        data = arr[i] as! NSArray
                        let fn = data[1]
                        let ln = data[2]
                        let un = data[3]
                        let ut = data[4]
                        let fav = data[5]
                        self.firstNameArr.add(fn)
                        self.lastNameArr.add(ln)
                        self.userArr.add(un)
                        self.userTypArr.add(ut)
                        self.userFavorArr.add(fav)
                    }
                    self.tableView.reloadData()
                    self.userView.isHidden = false
                }else{
                    var is_changed: String? = nil
                    if let object = json["is_changed"] {
                        is_changed = "\(object)"
                    }
                    if is_changed?.isEqual("1") ?? false {
                        logoutAlertMassege()
                    } else {
                        HomeViewController.VC.alert(message: "No Record Found!", title: "Alert!")
                    }
                }
            }
        })
    }
    
    func callReports_user_wise(){
        let authToken = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
        postWithoutImage(url_string: "my_Reports_user_wise", parameters: "cid=\(cp_id)&id=\(u_id)&selected_user_id=\(selectedUserID)&startdate=\(txtStartDate.text ?? "")&enddate=\(txtEndDate.text ?? "")&authToken=\(authToken)",CompletionHandler: { json, error in
            if let json = json {
                print("json~~~~~~~",json)
                let statusmassege = json.value(forKey: "status") as! Bool
                if (statusmassege == true){
                    let authToken = json.value(forKey: "authToken") as! String
                    USER_DEFAULTS.set(authToken, forKey: "authToken")
                    self.resultView.isHidden = false
                    let arr = json.value(forKey: "data") as! NSArray
                    
                    for vw in self.resultView.subviews {
                        let btn = vw as? UIButton
                        if btn?.tag == 3{
                            btn?.setTitle(arr[0] as? String, for: .normal)
                        }else if btn?.tag == 4{
                            btn?.setTitle(arr[1] as? String, for: .normal)
                        }else if btn?.tag == 5{
                            btn?.setTitle(arr[2] as? String, for: .normal)
                        }else if btn?.tag == 6{
                            btn?.setTitle(arr[3] as? String, for: .normal)
                        }else if btn?.tag == 7{
                            btn?.setTitle(arr[4] as? String, for: .normal)
                        }else if btn?.tag == 8{
                            btn?.setTitle(arr[6] as? String, for: .normal)
                        }else if btn?.tag == 9{
                            btn?.setTitle(arr[5] as? String, for: .normal)
                        }else if btn?.tag == 10{
                            btn?.setTitle(arr[7] as? String, for: .normal)
                        }
                        
                    }
                }else{
                    var is_changed: String? = nil
                    if let object = json["is_changed"] {
                        is_changed = "\(object)"
                    }
                    if is_changed?.isEqual("1") ?? false {
                        logoutAlertMassege()
                    } else {
                        HomeViewController.VC.alert(message: "No Record Found!", title: "Alert!")
                    }
                }
            }
        })
    }

}
