//
//  PollWatchingModeView.swift
//  ExelToApp
//
//  Created by baps on 30/01/20.
//  Copyright © 2020 Osiya. All rights reserved.
//

import UIKit

class PollWatchingModeView: UIView ,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{
    var mainView:UIView!
    var lblHeading:UILabel!
    var lblHeading1:UILabel!
    var popUpView:UIView!
    var topView:UIView!
    var btnCancel:UIButton!
    var btnSubmit:UIButton!
    var btnCross:UIButton!
    var txtParish:UITextField!
    var txtWard:UITextField!
    var txtPrecinct:UITextField!
    
    var popupView1:UIView!
    var TableView: UITableView = UITableView()
    let cellReuseIdentifier = "cell"
    var btnCancel1:UIButton!
    var btnSelect1:UIButton!
    
    var pollView:UIView!
    var votedView:UIView!
    var btnPYes:UIButton!
    var btnPNo:UIButton!
    var txtFirstName:UITextField!
    var txtLastName:UITextField!
    var btnPGo:UIButton!
    var btnPListVoted:UIButton!
    var btnPListNotVoted:UIButton!
    var btnPMapGoTv:UIButton!
    var btnPSave:UIButton!
    
    var viewSaveBack:UIView!
    var viewSave:UIView!
    var btnSaveYes:UIButton!
    var lblHead:UILabel!
    
    var selectTitle = ""
    var pollWatchMode = 0
    var u_id = USER_DEFAULTS.string(forKey: "u_id") ?? ""
    var cp_id = USER_DEFAULTS.string(forKey: "cp_id") ?? ""
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpView()
    }
    
    func setUpView() {
        mainView = UIView()
        mainView.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT-LoginViewController.VC.NAVHEIGHT-STATUSBARHEIGHT)
        mainView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.addSubview(mainView)
        
        popUpView = UIView()
        popUpView.frame = CGRect(x: 10, y: 80, width: mainView.frame.width-20, height: 270)
        popUpView.backgroundColor = UIColor.white
        popUpView.layer.cornerRadius = 10.0
        popUpView.clipsToBounds = true
        mainView.addSubview(popUpView)
        
        topView = UIView()
        topView.frame = CGRect(x: 0, y: 0, width: popUpView.frame.width, height: 50)
        topView.backgroundColor = APPBUTTONCOLOR
        popUpView.addSubview(topView)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 30, Y: 0, Width: topView.frame.width-60, Height: 50, TextColor: .white, BackColor: .clear, Text: "Search By Poll Watching Mode", TextAlignment: .center, Font: .boldSystemFont(ofSize: 18.0))
        lblHeading.numberOfLines = 0
        lblHeading.lineBreakMode = .byWordWrapping
        topView.addSubview(lblHeading)
        
        btnCross = UIButton(type: .system)
        btnCross.setButton(X: topView.frame.width-30, Y: 5, Width: 24, Height: 24, TextColor: .white, BackColor: .clear, Title: "X", Align: .center, Font: .boldSystemFont(ofSize: 22))
        btnCross.tag = 3
        btnCross.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        topView.addSubview(btnCross)
        
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 0, Y: topView.frame.maxY+10, Width: popUpView.frame.width/2-5, Height: 20, TextColor: .black, BackColor: .clear, Text: "County/Parish#", TextAlignment: .center, Font: .systemFont(ofSize: 15.0))
        popUpView.addSubview(lblHeading)
        
        txtParish = UITextField()
        txtParish.setTextField(X: 5, Y: lblHeading.frame.maxY+5, Width: popUpView.frame.width/2-5, Height: 40, TextColor: .black, BackColor: .clear)
        txtParish.borderStyle = .roundedRect
        txtParish.font = UIFont.systemFont(ofSize: 15)
        txtParish.placeholder = "Select Parish"
        txtParish.text = "All"
        txtParish.delegate = self
        txtParish.textAlignment = .center
        popUpView.addSubview(txtParish)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: txtParish.frame.maxX+5, Y: topView.frame.maxY+10, Width: popUpView.frame.width/2-5, Height: 20, TextColor: .black, BackColor: .clear, Text: "Ward", TextAlignment: .center, Font: .systemFont(ofSize: 15.0))
        popUpView.addSubview(lblHeading)
        
        txtWard = UITextField()
        txtWard.setTextField(X: txtParish.frame.maxX+5, Y: lblHeading.frame.maxY+5, Width: popUpView.frame.width/2-10, Height: 40, TextColor: .black, BackColor: .clear)
        txtWard.borderStyle = .roundedRect
        txtWard.font = UIFont.systemFont(ofSize: 15)
        txtWard.placeholder = "Select Ward"
        txtWard.text = "All"
        txtWard.delegate = self
        txtWard.textAlignment = .center
        popUpView.addSubview(txtWard)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 0, Y: txtWard.frame.maxY+10, Width: popUpView.frame.width, Height: 20, TextColor: .black, BackColor: .clear, Text: "Precinct", TextAlignment: .center, Font: .systemFont(ofSize: 15.0))
        popUpView.addSubview(lblHeading)
        
        txtPrecinct = UITextField()
        txtPrecinct.setTextField(X: 5, Y: lblHeading.frame.maxY+5, Width: popUpView.frame.width-10, Height: 40, TextColor: .black, BackColor: .clear)
        txtPrecinct.borderStyle = .roundedRect
        txtPrecinct.font = UIFont.systemFont(ofSize: 15)
        txtPrecinct.placeholder = "Select Precinct"
        txtPrecinct.text = "All"
        txtPrecinct.delegate = self
        txtPrecinct.textAlignment = .center
        popUpView.addSubview(txtPrecinct)
        
        btnSubmit = UIButton(type: .system)
        btnSubmit.setButton(X: (popUpView.frame.width/3)/2.5, Y: txtPrecinct.frame.maxY+20, Width: popUpView.frame.width/3, Height: 35, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Submit", Align: .center, Font: .systemFont(ofSize: 16.0))
        btnSubmit.tag = 1
        btnSubmit.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        popUpView.addSubview(btnSubmit)
        
        btnCancel = UIButton(type: .system)
        btnCancel.setButton(X: btnSubmit.frame.maxX+30, Y: txtPrecinct.frame.maxY+20, Width: popUpView.frame.width/3, Height: 35, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Cancel", Align: .center, Font: .systemFont(ofSize: 16.0))
        btnCancel.tag = 2
        btnCancel.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        popUpView.addSubview(btnCancel)
        
        popupView1 = UIView()
        popupView1.frame = CGRect(x: 10, y: 5, width: mainView.frame.width-20, height: mainView.frame.height-10)
        popupView1.backgroundColor = UIColor.white
        popupView1.layer.cornerRadius = 10.0
        popupView1.layer.shadowRadius  = 1.5
        popupView1.layer.shadowColor   = UIColor(red: 176.0/255.0, green: 199.0/255.0, blue: 226.0/255.0, alpha: 1.0).cgColor
        popupView1.layer.shadowOffset  = CGSize(width: 0.0, height: 0.0)
        popupView1.layer.shadowOpacity = 0.9
        popupView1.layer.masksToBounds = false
        popupView1.clipsToBounds = true
        mainView.addSubview(popupView1)
        
        
        topView = UIView()
        topView.frame = CGRect(x: 0, y: 0, width: popupView1.frame.width, height: 50)
        topView.backgroundColor = APPBUTTONCOLOR
        popupView1.addSubview(topView)
        
        lblHeading1 = UILabel()
        lblHeading1.setLabel(X: 30, Y: 0, Width: topView.frame.width-60, Height: 50, TextColor: .white, BackColor: .clear, Text: selectTitle, TextAlignment: .center, Font: .boldSystemFont(ofSize: 18.0))
        lblHeading1.numberOfLines = 0
        lblHeading1.lineBreakMode = .byWordWrapping
        topView.addSubview(lblHeading1)
        
        btnCross = UIButton(type: .system)
        btnCross.setButton(X: topView.frame.width-30, Y: 5, Width: 24, Height: 24, TextColor: .white, BackColor: .clear, Title: "X", Align: .center, Font: .boldSystemFont(ofSize: 22))
        btnCross.tag = 3
        btnCross.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        topView.addSubview(btnCross)
        
        TableView.frame = CGRect(x: 0, y: topView.frame.maxY+10, width: popupView1.frame.width, height: popupView1.frame.height-110)
        TableView.backgroundColor = UIColor.white
        TableView.delegate = self
        TableView.dataSource = self
        TableView.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        popupView1.addSubview(TableView)
        TableView.tableFooterView = UIView()
        TableView.layer.cornerRadius = 10.0
        
        btnCancel1 = UIButton()
        btnCancel1.setButton(X: popupView1.frame.width/4, Y: TableView.frame.maxY+10, Width: popupView1.frame.width/4, Height: 30, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Submit", Align: .center, Font: .systemFont(ofSize: 17.0))
        btnCancel1.tag = 5
        btnCancel1.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        popupView1.addSubview(btnCancel1)
        
        btnSelect1 = UIButton()
        btnSelect1.setButton(X: btnCancel1.frame.maxX+5, Y: TableView.frame.maxY+10, Width: popupView1.frame.width/4, Height: 30, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Cancel", Align: .center, Font: .systemFont(ofSize: 17.0))
        btnSelect1.tag = 4
        btnSelect1.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        popupView1.addSubview(btnSelect1)
        popupView1.isHidden = true
        
        if(isSelectPollParishWard == 0){
            isSelectPollParishWard = 1
            c_v.callPollPerishnWardList(u_id: u_id, cp_id: cp_id)
        }
        
        
        
        pollView = UIView()
        pollView.frame = CGRect(x: 0, y: SHEIGHT-400, width: SWIDTH, height: 400)
        pollView.backgroundColor = UIColor(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        mainView.addSubview(pollView)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 0, Y: 8, Width: pollView.frame.width, Height: 25, TextColor: .black, BackColor: .clear, Text: "Poll Watching Mode", TextAlignment: .center, Font: .systemFont(ofSize: 17.0))
        pollView.addSubview(lblHeading)
        
        votedView = UIView()
        votedView.frame = CGRect(x: pollView.frame.width/2-(pollView.frame.width/3)/2, y: lblHeading.frame.maxY+15, width: pollView.frame.width/3, height: 70)
        votedView.backgroundColor = APPBUTTONCOLOR
        votedView.layer.cornerRadius = 15.0
        pollView.addSubview(votedView)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 0, Y: 5, Width: votedView.frame.width, Height: 20, TextColor: .black, BackColor: .clear, Text: "Voted?", TextAlignment: .center, Font: .systemFont(ofSize: 17.0))
        votedView.addSubview(lblHeading)
        
        btnPYes = UIButton()
        btnPYes.setButton(X: 10, Y: lblHeading.frame.maxY+5, Width: votedView.frame.width/2, Height: 35, TextColor: .white, BackColor: .clear, Title: " Yes", Align: .left,Font: .systemFont(ofSize: 14))
        btnPYes.setImage(UIImage(named: "unchecked.png"), for: .normal)
        btnPYes.setImage(UIImage(named: "checked.png"), for: .selected)
        btnPYes.tag = 6
        btnPYes.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        votedView.addSubview(btnPYes)
        
        btnPNo = UIButton()
        btnPNo.setButton(X: btnPYes.frame.maxX+5, Y: lblHeading.frame.maxY+5, Width: votedView.frame.width/2, Height: 35, TextColor: .white, BackColor: .clear, Title: " No", Align: .left,Font: .systemFont(ofSize: 14))
        btnPNo.setImage(UIImage(named: "unchecked.png"), for: .normal)
        btnPNo.setImage(UIImage(named: "checked.png"), for: .selected)
        btnPNo.tag = 7
        btnPNo.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        votedView.addSubview(btnPNo)
        
        
        txtFirstName = UITextField()
        txtFirstName.setTextField(X: 8, Y: votedView.frame.maxY+15, Width: pollView.frame.width/2.6, Height: 30, TextColor: .black, BackColor: .clear)
        txtFirstName.backgroundColor = UIColor.white
        txtFirstName.borderStyle = .roundedRect
        txtFirstName.font = UIFont.systemFont(ofSize: 15)
        txtFirstName.placeholder = "First Name"
        pollView.addSubview(txtFirstName)
        
        txtLastName = UITextField()
        txtLastName.setTextField(X: txtFirstName.frame.maxX+10, Y: votedView.frame.maxY+15, Width: pollView.frame.width/2.6, Height: 30, TextColor: .black, BackColor: .clear)
        txtLastName.borderStyle = .roundedRect
        txtLastName.backgroundColor = UIColor.white
        txtLastName.font = UIFont.systemFont(ofSize: 15)
        txtLastName.placeholder = "Last Name"
        pollView.addSubview(txtLastName)
        
        
        btnPGo = UIButton(type: .system)
        btnPGo.setButton(X: txtLastName.frame.maxX+8, Y: votedView.frame.maxY+15, Width: pollView.frame.width/6.5, Height: 30, TextColor: .black, BackColor: .clear, Title: "Go", Align: .center,Font: .systemFont(ofSize: 14))
        btnPGo.layer.cornerRadius = 8.0
        btnPGo.layer.borderColor = UIColor.lightGray.cgColor
        btnPGo.layer.borderWidth = 1.0
        btnPGo.tag = 8
        btnPGo.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        pollView.addSubview(btnPGo)
        
        btnPListVoted = UIButton(type: .system)
        btnPListVoted.setButton(X: pollView.frame.width/2-(pollView.frame.width/1.5)/2, Y: txtLastName.frame.maxY+15, Width: pollView.frame.width/1.5, Height: 40, TextColor: .black, BackColor: .clear, Title: "List Voted", Align: .center,Font: .systemFont(ofSize: 14))
        btnPListVoted.layer.cornerRadius = 10.0
        btnPListVoted.layer.borderColor = UIColor.lightGray.cgColor
        btnPListVoted.layer.borderWidth = 1.0
        btnPListVoted.tag = 9
        btnPListVoted.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        pollView.addSubview(btnPListVoted)
        
        btnPListNotVoted = UIButton(type: .system)
        btnPListNotVoted.setButton(X: pollView.frame.width/2-(pollView.frame.width/1.5)/2, Y: btnPListVoted.frame.maxY+10, Width: pollView.frame.width/1.5, Height: 40, TextColor: .black, BackColor: .clear, Title: "List Not Voted", Align: .center,Font: .systemFont(ofSize: 14))
        btnPListNotVoted.layer.cornerRadius = 10.0
        btnPListNotVoted.layer.borderColor = UIColor.lightGray.cgColor
        btnPListNotVoted.layer.borderWidth = 1.0
        btnPListNotVoted.tag = 10
        btnPListNotVoted.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        pollView.addSubview(btnPListNotVoted)
        
        btnPMapGoTv = UIButton(type: .system)
        btnPMapGoTv.setButton(X: pollView.frame.width/2-(pollView.frame.width/1.5)/2, Y: btnPListNotVoted.frame.maxY+10, Width: pollView.frame.width/1.5, Height: 40, TextColor: .black, BackColor: .clear, Title: "Map GOTV", Align: .center,Font: .systemFont(ofSize: 14))
        btnPMapGoTv.layer.cornerRadius = 10.0
        btnPMapGoTv.layer.borderColor = UIColor.lightGray.cgColor
        btnPMapGoTv.layer.borderWidth = 1.0
        btnPMapGoTv.tag = 11
        btnPMapGoTv.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        pollView.addSubview(btnPMapGoTv)
        
        btnPSave = UIButton(type: .system)
        btnPSave.setButton(X: pollView.frame.width/8.5, Y: btnPMapGoTv.frame.maxY+10, Width: pollView.frame.width/3, Height: 40, TextColor: .black, BackColor: .clear, Title: "Save", Align: .center,Font: .systemFont(ofSize: 14))
        btnPSave.layer.cornerRadius = 10.0
        btnPSave.layer.borderColor = UIColor.lightGray.cgColor
        btnPSave.layer.borderWidth = 1.0
        btnPSave.tag = 12
        btnPSave.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        pollView.addSubview(btnPSave)
        
        btnCancel = UIButton(type: .system)
        btnCancel.setButton(X: btnPSave.frame.maxX+40, Y: btnPMapGoTv.frame.maxY+10, Width: pollView.frame.width/3, Height: 40, TextColor: .black, BackColor: .clear, Title: "Cancel", Align: .center,Font: .systemFont(ofSize: 14))
        btnCancel.layer.cornerRadius = 10.0
        btnCancel.layer.borderColor = UIColor.lightGray.cgColor
        btnCancel.layer.borderWidth = 1.0
        btnCancel.tag = 2
        btnCancel.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        pollView.addSubview(btnCancel)
        pollView.isHidden = true
        
        
        
        viewSaveBack = UIView()
        viewSaveBack.frame = CGRect(x: 0, y: 0, width: mainView.frame.width, height: mainView.frame.height)
        viewSaveBack.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        mainView.addSubview(viewSaveBack)
        
        viewSave = UIView()
        viewSave.frame = CGRect(x: 40, y: 0, width: SWIDTH-80, height: 160)
        viewSave.backgroundColor = UIColor.white
        viewSave.layer.cornerRadius = 10.0
        viewSave.clipsToBounds = true
        viewSaveBack.addSubview(viewSave)
        
        topView = UIView()
        topView.frame = CGRect(x: 0, y: 0, width: viewSave.frame.width, height: 50)
        topView.backgroundColor = APPBUTTONCOLOR
        viewSave.addSubview(topView)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 0, Y: 2, Width: viewSave.frame.width, Height: 46, TextColor: .white, BackColor: .clear, Text: "Save", TextAlignment: .center, Font: .boldSystemFont(ofSize: 18))
        topView.addSubview(lblHeading)
        
        btnSaveYes = UIButton(type: .system)
        btnSaveYes.setButton(X: topView.frame.width-30, Y: 13, Width: 24, Height: 24, TextColor: .white, BackColor: .clear, Title: "X", Align: .center, Font: .boldSystemFont(ofSize: 22))
        btnSaveYes.tag = 13
        btnSaveYes.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        topView.addSubview(btnSaveYes)
        
        lblHead = UILabel()
        lblHead.setLabel(X: 0, Y: topView.frame.maxY+10, Width: viewSave.frame.width, Height: 50, TextColor: APPBUTTONCOLOR, BackColor: .clear, Text: "Do You Want To Save Your Changes To This Case?", TextAlignment: .center, Font: .boldSystemFont(ofSize: 16))
        lblHead.numberOfLines = 0
        lblHead.lineBreakMode = .byWordWrapping
        viewSave.addSubview(lblHead)
        
        btnSaveYes = UIButton(type: .system)
        btnSaveYes.setButton(X: viewSave.frame.width/8, Y: topView.frame.maxY+70, Width: viewSave.frame.width/3, Height: 35, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Cancel", Align: .center, Font: .systemFont(ofSize: 16.0))
        btnSaveYes.tag = 13
        btnSaveYes.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        viewSave.addSubview(btnSaveYes)
        
        btnSaveYes = UIButton(type: .system)
        btnSaveYes.setButton(X: viewSave.frame.width/1.8, Y: topView.frame.maxY+70, Width: viewSave.frame.width/3, Height: 35, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Save", Align: .center, Font: .systemFont(ofSize: 16.0))
        btnSaveYes.tag = 14
        btnSaveYes.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        viewSave.addSubview(btnSaveYes)
        viewSaveBack.isHidden = true
        
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.Notify), name: NSNotification.Name(rawValue: "pollwatchingmode"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onClickRearView), name: NSNotification.Name(rawValue: "onClickRearView"), object: nil)
        
    }
    @objc func onClickRearView(notif: NSNotification) {
        self.isHidden = true
    }
    
    @objc func Notify(notif: NSNotification) {
        print("pollwatchingmode",SHEIGHT/1.9)
        self.frame = CGRect(x: 0, y: SHEIGHT-400, width: SWIDTH, height: 400)
        mainView.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: 400)
        pollView.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: 400)
        self.isHidden = false
       // mainView.isHidden = true
        popUpView.isHidden = true
        mainView.backgroundColor = UIColor.clear
        let results = jsonObj.value(forKey: "data") as! NSArray?
        let obj = results?[Int(USER_DEFAULTS.string(forKey: "case") ?? "")!] as! NSArray?
        if obj?.count ?? 0 > 22 {
            let voted = "\(obj?[22] ?? "")"
            if voted.caseInsensitiveCompare("YES") == .orderedSame {
                btnPYes.isSelected = true
                btnPNo.isSelected = false
            } else if voted.caseInsensitiveCompare("NO") == .orderedSame {
                btnPNo.isSelected = true
                btnPYes.isSelected = false
            } else {
                btnPYes.isSelected = false
                btnPNo.isSelected = false
            }
        }
        
        pollView.isHidden = false
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        if txtParish == textField {
            lblHeading1.text = "County/Parish#"
            pollWatchMode = 1
            if (popupView1.isHidden == true) {
                popupView1.isHidden = false;
            }else{
                popupView1.isHidden = true;
            }
            TableView.reloadData()
        }
        if txtWard == textField {
            lblHeading1.text = "Ward"
            pollWatchMode = 2
            if (popupView1.isHidden == true) {
                popupView1.isHidden = false;
            }else{
                popupView1.isHidden = true;
            }
            TableView.reloadData()
        }
        if txtPrecinct == textField {
            lblHeading1.text = "Precinct"
            pollWatchMode = 3
            if (popupView1.isHidden == true) {
                popupView1.isHidden = false;
            }else{
                popupView1.isHidden = true;
            }
            TableView.reloadData()
        }
        
    }
    
    @objc func onClickButton(_ sender: UIButton) {
        if sender.tag == 1{// onclick Submit
            type = 8
            paramStr = ""
            paramStr = "parish_id=\(txtParish.text ?? "")&precinct_id=\(txtPrecinct.text ?? "")&ward=\(txtWard.text ?? "")&search_firstname=&search_lastname=&voted=&poll_mode=YES"
            HomeViewController.VC.max = 0
            HomeViewController.VC.callGetExcel(u_id: u_id, cp_id: cp_id, startRow: 0)
            self.isHidden = true
        }else if sender.tag == 2{// onclick Cancel
            self.isHidden = true
            NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "pollwatchingmode"), object: nil)
            HomeViewController.VC.navigationItem.rightBarButtonItem = HomeViewController.VC.btnEditPerson
        }else if sender.tag == 3{// onclick Cross
            if popupView1.isHidden == false{
                popupView1.isHidden = true
            }else{
                self.isHidden = true
                HomeViewController.VC.navigationItem.rightBarButtonItem = HomeViewController.VC.btnEditPerson
            }
        }else if sender.tag == 4{// onclick cancel ppopup
            popUpView.isHidden = false
            popupView1.isHidden = true
        }else if sender.tag == 5{// onclick select popup
            var nsm = NSMutableArray()
            if pollWatchMode == 1 {
                nsm = parishArray
            }else if pollWatchMode == 2 {
                nsm = wardArray
            }else {
                nsm = precinctArray
            }
            var j = 0
            var select = ""
            for i in 0..<(nsm.count) {
                let obj = nsm[i] as? multiSelcted
                if ((obj?.isSelected)!){
                    if j == 0 {
                        if let txt = obj?.txt {
                            select = "\(txt)"
                        }
                    } else {
                        if let txt = obj?.txt {
                            select = "\(select),\(txt)"
                        }
                    }
                    j += 1
                }
            }
            if j == 0{
                HomeViewController.VC.alert(message: "Select At Least One", title: "Alert")
            }else{
                if pollWatchMode == 1 {
                    txtParish.text = select.contains("All") ? "All" : select
                    txtWard.text = "All"
                    txtPrecinct.text = "All"
                    for i in 0..<wardArray.count{
                        let comObj = wardArray[i] as! multiSelcted
                        comObj.isSelected = true
                    }
                    c_v.callPollPerishnWardUpdateList(u_id: u_id, cp_id: cp_id)
                }else if pollWatchMode == 2 {
                    txtWard.text = select.contains("All") ? "All" : select
                    txtPrecinct.text = "All"
                    c_v.callPollPerishnWardUpdateList(u_id: u_id, cp_id: cp_id)
                }else {
                    txtPrecinct.text = select.contains("All") ? "All" : select
                }
                popupView1.isHidden = true
            }
            
            
        }else if sender.tag == 6{// onclick Yes
            sender.isSelected = !sender.isSelected
            btnPNo.isSelected = false
//            let alert = UIAlertController(title: "Message", message: "Do You Want To Save Your Changes To This Case?", preferredStyle: .alert)
//            let saveServer = UIAlertAction(title: "YES", style: .default, handler: { action in
                isSelectedVotedPoll = "Yes"
            viewSaveBack.isHidden = false
            
                
//            })
//            alert.addAction(saveServer)
            
//            let No = UIAlertAction(title: "NO", style: .default, handler: { action in
//            })
//            alert.addAction(No)
//            HomeViewController.VC.present(alert, animated: true)
        }else if sender.tag == 7{// onclick No
            sender.isSelected = !sender.isSelected
            btnPYes.isSelected = false
//            let alert = UIAlertController(title: "Message", message: "Do You Want To Save Your Changes To This Case?", preferredStyle: .alert)
//            let saveServer = UIAlertAction(title: "YES", style: .default, handler: { action in
                isSelectedVotedPoll = "No"
                viewSaveBack.isHidden = false
//            })
//            alert.addAction(saveServer)
//
//            let No = UIAlertAction(title: "NO", style: .default, handler: { action in
//            })
//            alert.addAction(No)
//            HomeViewController.VC.present(alert, animated: true)
        }else if sender.tag == 8{ // onclick GO
            self.isHidden = true
            paramStr = "parish_id=\(txtParish.text ?? "")&precinct_id=\(txtPrecinct.text ?? "")&ward=\(txtWard.text ?? "")&search_firstname=\(txtFirstName.text ?? "")&search_lastname=\(txtLastName.text ?? "")&voted=&poll_mode=YES"
            HomeViewController.VC.isTableViewHidden = false
            HomeViewController.VC.callGetExcel(u_id: u_id, cp_id: cp_id, startRow: 0)
            
        }else if sender.tag == 9{ // onclick ListVote
            print("onclick ListVoted~~~~~~")
            self.isHidden = true
            pollView.isHidden = true
            HomeViewController.VC.isTableViewHidden = false
            paramStr = "parish_id=\(txtParish.text ?? "")&precinct_id=\(txtPrecinct.text ?? "")&ward=\(txtWard.text ?? "")&search_firstname=&search_lastname=&voted=YES&poll_mode=YES"
            HomeViewController.VC.callGetExcel(u_id: u_id, cp_id: cp_id, startRow: 0)
        }else if sender.tag == 10{ // onclick List Not Voted
            self.isHidden = true
            pollView.isHidden = true
            HomeViewController.VC.isTableViewHidden = false
            print("onclick List Not Voted~~~~~~")
            paramStr = "parish_id=\(txtParish.text ?? "")&precinct_id=\(txtPrecinct.text ?? "")&ward=\(txtWard.text ?? "")&search_firstname=&search_lastname=&voted=NO&poll_mode=YES"
            HomeViewController.VC.callGetExcel(u_id: u_id, cp_id: cp_id, startRow: 0)
            
        }else if sender.tag == 12{ // onclick Save
//            let alert = UIAlertController(title: "Message", message: "Do You Want To Save Your Changes To This Case?", preferredStyle: .alert)
//            let saveServer = UIAlertAction(title: "YES", style: .default, handler: { action in
                if(self.btnPYes.isSelected){
                    isSelectedVotedPoll = "Yes"
                }else if(self.btnPNo.isSelected){
                    isSelectedVotedPoll = "No"
                }else{
                    isSelectedVotedPoll = ""
                }
                viewSaveBack.isHidden = false
//            })
//            alert.addAction(saveServer)
//
//            let No = UIAlertAction(title: "NO", style: .default, handler: { action in
//            })
//            alert.addAction(No)
//            HomeViewController.VC.present(alert, animated: true)
        }else if sender.tag == 11 {//on click Map gotv
            let vc = MapViewController()
            mapType = 2
            HomeViewController.VC.navigationController?.pushViewController(vc, animated: true)
        }else if sender.tag == 13 {//on click Cancel Alert popup
            viewSaveBack.isHidden = true
        }else if sender.tag == 14 {//on click Save Alert popup
            self.Update()
            viewSaveBack.isHidden = true
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if pollWatchMode == 1 {
            return parishArray.count
        }else if pollWatchMode == 2 {
            return wardArray.count
        }else {
            return precinctArray.count
        }
    }
    
//    func newCross(_ str: String?, type isSel: Bool) -> multiSelcted? {
//        let comment = multiSelcted()
//        comment.txt = str ?? ""
//        comment.isSelected = isSel
//        return comment
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as UITableViewCell!
        var obj: multiSelcted! = nil
        if pollWatchMode == 1 {
            obj = parishArray[indexPath.row] as? multiSelcted
           // print("obj.txt~~~~~",obj.txt)
            cell.textLabel?.text = obj.txt
            cell.imageView?.image = UIImage(named: obj.isSelected == false ? "diselect128.png" : "select128.png")
        }else if pollWatchMode == 2 {
            obj = wardArray[indexPath.row] as? multiSelcted
            cell.textLabel?.text = obj.txt
            cell.imageView?.image = UIImage(named: obj.isSelected == false ? "diselect128.png" : "select128.png")
        }else if pollWatchMode == 3 {
            obj = precinctArray[indexPath.row] as? multiSelcted
            cell.textLabel?.text = obj.txt
            cell.imageView?.image = UIImage(named: obj.isSelected == false ? "diselect128.png" : "select128.png")
        }
        
        cell.textLabel?.textColor = UIColor.black
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var obj = NSMutableArray()
        if pollWatchMode == 1 {
            obj = parishArray
        }else if pollWatchMode == 2 {
            obj = wardArray
        }else if pollWatchMode == 3 {
            obj = precinctArray
        }
        var com = obj[indexPath.row] as! multiSelcted
        if (com.txt == "All") {
            let comObj = obj[0] as! multiSelcted
            if indexPath.row == 0 && comObj.isSelected == false {
                for i in 0..<obj.count {
                    let comObj = obj[i] as! multiSelcted
                    comObj.isSelected = i == 0 ? !(comObj.isSelected) : true
                }
            } else {
                for i in 0..<obj.count {
                    let comObj = obj[i] as! multiSelcted
                    comObj.isSelected = i == 0 ? !(comObj.isSelected) : false
                }
            }
        } else {
            com.isSelected = !com.isSelected
            com = obj[0] as! multiSelcted
            if (com.txt == "All") {
                com.isSelected = false
            }
        }
        TableView.reloadData()
    }
    
    func Update(){
        let no = Int(USER_DEFAULTS.string(forKey: "case") ?? "")!
        print("no~~~~~",no)
        let heding = jsonObj.value(forKey: "heading") as! NSArray
        let results = jsonObj.value(forKey: "data") as! NSArray
        //let obj = results[Int(USER_DEFAULTS.string(forKey: "case") ?? "")!] as! NSArray
        let allDataArr = NSMutableArray()
        for i in 0..<results.count{
            let chidchid2 = NSMutableArray()
            let valus3 = results[i] as! NSArray
            for j in 0..<valus3.count{
                if(i == Int(USER_DEFAULTS.string(forKey: "case") ?? "")!){
                    if j == 22{
                        chidchid2.add(isSelectedVotedPoll)
                    }else{
                        chidchid2.add(valus3[j])
                    }
                }else{
                    chidchid2.add(valus3[j])
                }
            }
            allDataArr.add(chidchid2)
        }
        
        var grade: [AnyHashable : Any] = [:]
        grade = ["data": allDataArr,"status": "true","msg": "Successfully","cid": self.cp_id,"heading": heding]
        jsonObj = grade as NSDictionary
        
        var sendreq: [AnyHashable : Any] = [:]
        var result = jsonObj.value(forKey: "data") as? [AnyHashable]
        var bp: [AnyHashable] = []
        if let no = result?[no] {
            bp.append(no)
        }
        sendreq = ["data": bp,"status": "true","msg": "Successfully","cid": self.cp_id,"id": self.u_id,"heading": heding]
        var _: Error? = nil
        var jsonData: Data? = nil
        do {
            jsonData = try JSONSerialization.data(withJSONObject: sendreq, options: .prettyPrinted)
        } catch {
        }
        var jsonString: String? = nil
        if let jsonData = jsonData {
            jsonString = String(data: jsonData, encoding: .utf8)
        }
        let postData = jsonString?.data(using: .ascii, allowLossyConversion: true)
        if postData != nil {
            let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
            let documentsDirectory = paths[0]
            let filePath = "\(documentsDirectory)/\("filename.doc")"
            do {
                try postData?.write(to: URL(fileURLWithPath: filePath), options: .atomic)
            } catch {
                print(error)
            }
        }
        
        
        HomeViewController.VC.callUpdateExcel(jsonData: postData!)
        
    }
    
    
}
