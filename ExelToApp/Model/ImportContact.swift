//
//  ImportContact.swift
//  ExelToApp
//
//  Created by baps on 10/02/20.
//  Copyright © 2020 Osiya. All rights reserved.
//

import UIKit
import Contacts
import SwiftGifOrigin
import LinearProgressBar


class SimDetail{
    var strName = ""
    var strEmail = ""
    var strHomePhone = ""
    var strMobilePhone = ""
    var appContactArr = [Any]()
    var isOpen = false
}
class AppDetail{
    var strFirstName = ""
    var strLastName = ""
    var strEmail = ""
    var strHomePhone = ""
    var strMobilePhone = ""
    var strexcel_id = ""
}

class AppContactCell: UICollectionViewCell {
    var backView:UIView!
    var lblName:UILabel!
    var lblEmail:UILabel!
    var lblHPhone:UILabel!
    var lblMPhone:UILabel!
    var lblCity:UILabel!
    var lblAge:UILabel!
    var btnAccept:UIButton!
    var btnDecline:UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backView = UIView()
        backView.frame = CGRect(x: 5, y: 5, width: SWIDTH-60, height: 185)
        backView.backgroundColor = UIColor.clear
        backView.layer.cornerRadius = 5.0
        backView.layer.borderColor = UIColor.lightGray.cgColor
        backView.layer.borderWidth = 1.0
        backView.clipsToBounds = true
        contentView.addSubview(backView)
        
        lblName = UILabel()
        lblName.setLabel(X: 8, Y: 10, Width: backView.frame.width, Height: 25, TextColor: APPBUTTONCOLOR, BackColor: .clear, Text: "", TextAlignment: .left, Font: .systemFont(ofSize: 20.0))
        backView.addSubview(lblName)
        
        lblEmail = UILabel()
        lblEmail.setLabel(X: 8, Y: lblName.frame.maxY+5, Width: backView.frame.width, Height: 25, TextColor: APPBUTTONCOLOR, BackColor: .clear, Text: "", TextAlignment: .left, Font: .systemFont(ofSize: 20.0))
        backView.addSubview(lblEmail)
        
        lblHPhone = UILabel()
        lblHPhone.setLabel(X: 8, Y: lblEmail.frame.maxY+5, Width: backView.frame.width, Height: 25, TextColor: APPBUTTONCOLOR, BackColor: .clear, Text: "", TextAlignment: .left, Font: .systemFont(ofSize: 20.0))
        backView.addSubview(lblHPhone)
        
        lblMPhone = UILabel()
        lblMPhone.setLabel(X: 8, Y: lblHPhone.frame.maxY+5, Width: backView.frame.width, Height: 25, TextColor: APPBUTTONCOLOR, BackColor: .clear, Text: "", TextAlignment: .left, Font: .systemFont(ofSize: 20.0))
        backView.addSubview(lblMPhone)
        
        lblCity = UILabel()
        lblCity.setLabel(X: 8, Y: lblMPhone.frame.maxY+5, Width: backView.frame.width-105, Height: 20, TextColor: APPBUTTONCOLOR, BackColor: .clear, Text: "", TextAlignment: .left, Font: .systemFont(ofSize: 19.0))
        backView.addSubview(lblCity)
        
        lblAge = UILabel()
        lblAge.setLabel(X: 8, Y: lblCity.frame.maxY+5, Width: backView.frame.width-105, Height: 20, TextColor: APPBUTTONCOLOR, BackColor: .clear, Text: "", TextAlignment: .left, Font: .systemFont(ofSize: 19.0))
        backView.addSubview(lblAge)
        
        btnAccept = UIButton()
        btnAccept.setButton(X: backView.frame.width-140, Y: lblMPhone.frame.maxY+15, Width: 60, Height: 30, TextColor: .white, BackColor: APPBUTTONCOLOR1, Title: "Import", Align: .center, Font: .boldSystemFont(ofSize: 16.0))
        //btnAccept.setImage(UIImage(named: "yes.png"), for: .normal)
        backView.addSubview(btnAccept)
        
        btnDecline = UIButton()
        btnDecline.setButton(X: btnAccept.frame.maxX+10, Y: lblMPhone.frame.maxY+15, Width: 60, Height: 30, TextColor: .white, BackColor: APPBUTTONCOLOR1, Title: "Undo", Align: .center, Font: .boldSystemFont(ofSize: 16.0))
        //btnDecline.setImage(UIImage(named: "no.png"), for: .normal)
        backView.addSubview(btnDecline)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}

class ContactCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource {
    var backView:UIView!
    var backView1:UIView!
    var topView:UIView!
    var lblHeading:UILabel!
    var lblName:UILabel!
    var lblEmail:UILabel!
    var lblHPhone:UILabel!
    var lblMPhone:UILabel!
    var btnDropDown:UIButton!
    var imgView:UIImageView!
    var imgDropDown:UIImageView!
    var imgGif:UIImageView!
    var collectionView:UICollectionView!
    var btnSelect:UIButton!
    var AppcontactArr = [Any]()
    var u_id = USER_DEFAULTS.string(forKey: "u_id") ?? ""
    var cp_id = USER_DEFAULTS.string(forKey: "cp_id") ?? ""
    var mainView:UIView!
    var popUpView:UIView!
    var btnCancel:UIButton!
    var btnYes:UIButton!
    var btnNo:UIButton!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        //print(contentView.frame.width)
        
        backView = UIView()
        backView.frame = CGRect(x: 5, y: 5, width: SWIDTH-30, height: 180)
        backView.backgroundColor = APPPBACKCOLOR
        backView.layer.cornerRadius = 5.0
        backView.clipsToBounds = true
        contentView.addSubview(backView)
        
        topView = UIView()
        topView.frame = CGRect(x: 5, y: 5, width: backView.frame.width-10, height: 40)
        topView.backgroundColor = UIColor.white
        topView.layer.cornerRadius = 5.0
        backView.addSubview(topView)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 0, Y: 5, Width: topView.frame.width, Height: 30, TextColor: APPBUTTONCOLOR, BackColor: .clear, Text: "Phone Contact", TextAlignment: .center, Font: .boldSystemFont(ofSize: 25))
        topView.addSubview(lblHeading)
        
        imgView = UIImageView()
        imgView.frame = CGRect(x: 5, y: 5, width: 30, height: 30)
        imgView.image = UIImage(named: "call.png")
        imgView.setImageColor(color: APPBUTTONCOLOR)
        topView.addSubview(imgView)
        
        lblName = UILabel()
        lblName.setLabel(X: 8, Y: topView.frame.maxY+10, Width: topView.frame.width-16, Height: 25, TextColor: .white, BackColor: .clear, Text: "", TextAlignment: .left, Font: .systemFont(ofSize: 20.0))
        backView.addSubview(lblName)
        
        lblEmail = UILabel()
        lblEmail.setLabel(X: 8, Y: lblName.frame.maxY+5, Width: topView.frame.width-36, Height: 25, TextColor: .white, BackColor: .clear, Text: "", TextAlignment: .left, Font: .systemFont(ofSize: 20.0))
        backView.addSubview(lblEmail)
        lblEmail.lineBreakMode = .byClipping
        
        lblHPhone = UILabel()
        lblHPhone.setLabel(X: 8, Y: lblEmail.frame.maxY+5, Width: topView.frame.width-16, Height: 25, TextColor: .white, BackColor: .clear, Text: "", TextAlignment: .left, Font: .systemFont(ofSize: 20.0))
        backView.addSubview(lblHPhone)
        
        lblMPhone = UILabel()
        lblMPhone.setLabel(X: 8, Y: lblHPhone.frame.maxY+5, Width: topView.frame.width-16, Height: 25, TextColor: .white, BackColor: .clear, Text: "", TextAlignment: .left, Font: .systemFont(ofSize: 20.0))
        backView.addSubview(lblMPhone)
        
        imgDropDown = UIImageView()
        imgDropDown.setImageView(X: backView.frame.width-35, Y: backView.frame.height/2, Width: 24, Height: 24, img: UIImage(named: "down-arrow.png")!)
        backView.addSubview(imgDropDown)
        
        imgGif = UIImageView()
        imgGif.frame = CGRect(x: backView.frame.width-35, y: backView.frame.height/2, width: 30, height: 30)
        imgGif.image = UIImage(named: "0.png")
        backView.addSubview(imgGif)
        imgGif.setImageColor(color: .white)
        imgGif.isHidden = true
        
        
        btnSelect = UIButton()
        btnSelect.setButton(X: 5, Y: 5, Width: SWIDTH-30, Height: 180, TextColor: .clear, BackColor: .clear, Title: "", Align: .center, Font: .systemFont(ofSize: 15.0))
        backView.addSubview(btnSelect)
        
        backView1 = UIView()
        backView1.frame = CGRect(x: 5, y: lblMPhone.frame.maxY + 25, width: SWIDTH-30, height: 250)
        backView1.backgroundColor = APPPPOPUPCOLOR
        backView1.layer.cornerRadius = 5.0
        backView1.clipsToBounds = true
        contentView.addSubview(backView1)
        
        topView = UIView()
        topView.frame = CGRect(x: 5, y: 5, width: backView1.frame.width-10, height: 40)
        topView.backgroundColor = APPPBACKCOLOR
        topView.layer.cornerRadius = 5.0
        backView1.addSubview(topView)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 0, Y: 5, Width: topView.frame.width, Height: 30, TextColor: .white, BackColor: .clear, Text: "App Contact", TextAlignment: .center, Font: .boldSystemFont(ofSize: 25))
        topView.addSubview(lblHeading)
        
        imgView = UIImageView()
        imgView.frame = CGRect(x: 5, y: 5, width: 30, height: 30)
        imgView.image = UIImage(named: "icon0.png")
        topView.addSubview(imgView)
        
        
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        layout.itemSize = CGSize(width: backView1.frame.width-20, height: 190)
        collectionView = UICollectionView(frame: CGRect(x: 5, y: topView.frame.maxY, width: backView1.frame.width-10, height: 200), collectionViewLayout: layout)
        layout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        collectionView.setCollectionViewLayout(layout, animated: true)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(AppContactCell.self, forCellWithReuseIdentifier: "cell")
        collectionView.backgroundColor = UIColor.clear
        collectionView.showsHorizontalScrollIndicator = true
        collectionView.isPagingEnabled = true
        backView1.addSubview(collectionView)
        backView1.isHidden = true
        
        mainView = UIView()
        mainView.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT)
        mainView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        HomeViewController.VC.view.addSubview(mainView)
        
        popUpView = UIView()
        popUpView.frame = CGRect(x: 15, y: mainView.frame.height/2-70, width: mainView.frame.width-30, height: 140)
        popUpView.backgroundColor = UIColor.white
        popUpView.layer.cornerRadius = 10.0
        popUpView.clipsToBounds = true
        mainView.addSubview(popUpView)
        
        topView = UIView()
        topView.frame = CGRect(x: 0, y: 0, width: popUpView.frame.width, height: 50)
        topView.backgroundColor = APPBUTTONCOLOR
        popUpView.addSubview(topView)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 0, Y: 1, Width: popUpView.frame.width-30, Height: 48, TextColor: .white, BackColor: .clear, Text: "Do You Want To Merge The Contact Information For This Case?", TextAlignment: .center, Font: .boldSystemFont(ofSize: 18))
        lblHeading.numberOfLines = 0
        lblHeading.lineBreakMode = .byWordWrapping
        topView.addSubview(lblHeading)
        
        
        
        btnCancel = UIButton(type: .system)
        btnCancel.setButton(X: popUpView.frame.width-30, Y: 5, Width: 24, Height: 24, TextColor: .white, BackColor: .clear, Title: "X", Align: .center, Font: .boldSystemFont(ofSize: 22))
        btnCancel.addTarget(self, action: #selector(onClickNo1(_:)), for: .touchUpInside)
        topView.addSubview(btnCancel)
        
        btnYes = UIButton(type: .system)
        btnYes.setButton(X: popUpView.frame.width/2-(popUpView.frame.width/3)/2, Y: topView.frame.maxY+10, Width: popUpView.frame.width/3, Height: 30, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Yes", Align: .center, Font: .boldSystemFont(ofSize: 18))
        btnYes.addTarget(self, action: #selector(onClickYes1(_:)), for: .touchUpInside)
        popUpView.addSubview(btnYes)
        
        btnNo = UIButton(type: .system)
        btnNo.setButton(X: popUpView.frame.width/2-(popUpView.frame.width/3)/2, Y: btnYes.frame.maxY+10, Width: popUpView.frame.width/3, Height: 30, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "No", Align: .center, Font: .boldSystemFont(ofSize: 18))
        btnNo.addTarget(self, action: #selector(onClickNo1(_:)), for: .touchUpInside)
        popUpView.addSubview(btnNo)
        mainView.isHidden = true
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        //print("ContactCell")
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return AppcontactArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell",for: indexPath) as! AppContactCell
        let dic = AppcontactArr[indexPath.row] as! NSDictionary
        cell.lblName.text = "Name : \(dic.value(forKey: "Personal_FirstName") as? String ?? "") \(dic.value(forKey: "Personal_MiddleName") as? String ?? "") \(dic.value(forKey: "Personal_LastName") as? String ?? "")"
        cell.lblEmail.text = "Email : \(dic.value(forKey: "Email") as? String ?? "")"
        
        var mutableString = dic.value(forKey: "Personal_Phone") as? String ?? ""
        for p in 0..<mutableString.count {
            if p == 0 {
                mutableString.insert("(", at: mutableString.startIndex)
            }
            if p == 4 {
                mutableString.insert(contentsOf: ") ", at: mutableString.index(mutableString.startIndex, offsetBy: p))
            }
            if p == 9 {
                mutableString.insert(contentsOf: "-", at: mutableString.index(mutableString.startIndex, offsetBy: p))
            }
        }
        cell.lblHPhone.text = "Home Phone : \(mutableString)"
        mutableString = dic.value(forKey: "Mobile_Phone") as? String ?? ""
        for p in 0..<mutableString.count {
            if p == 0 {
                mutableString.insert("(", at: mutableString.startIndex)
            }
            if p == 4 {
                mutableString.insert(contentsOf: ") ", at: mutableString.index(mutableString.startIndex, offsetBy: p))
            }
            if p == 9 {
                mutableString.insert(contentsOf: "-", at: mutableString.index(mutableString.startIndex, offsetBy: p))
            }
        }
        cell.lblMPhone.text = "Mobile Phone : \(mutableString)"
        cell.lblCity.text = "\(dic.value(forKey: "Residence_City") as? String ?? "")"
        cell.lblAge.text = "Age : \(dic.value(forKey: "Personal_Age") as? String ?? "")"
        cell.backgroundColor = .clear
        //print("isImport----\(dic.value(forKey: "isContactUpdate") as? String ?? "")")//isImportOld
        
        let isImport = "\(dic.value(forKey: "isContactUpdate") as? String ?? "")"
        let isImportOld = "\(dic.value(forKey: "isContactUndo") as? String ?? "")"
        if isImport == "0" || isImportOld == "1"{
            cell.btnDecline.isHidden = true
        }else{
            cell.btnDecline.isHidden = false
        }
        cell.btnAccept.tag = indexPath.row
        cell.btnDecline.tag = indexPath.row
        cell.btnAccept.addTarget(self, action: #selector(onClickYes(_:)), for: .touchUpInside)
        cell.btnDecline.addTarget(self, action: #selector(onClickNo(_:)), for: .touchUpInside)
        return cell
    }
    @objc func onClickYes1(_ sender: UIButton) {
        //print("onClickYes1~~~~~~",sender.tag)
//        NotificationCenter.default.addObserver(self, selector: #selector(self.onClickCollectionViewCell), name: NSNotification.Name(rawValue: "onClickCollCell"), object: nil)
        let dic = AppcontactArr[sender.tag] as! NSDictionary
        let exlId = dic.value(forKey: "excel_id") as? String ?? ""
        let appId = dic.value(forKey: "app_id") as? String ?? ""
        let Mobile_Phone = dic.value(forKey: "Mobile_Phone") as? String ?? ""
        let home_Phone = dic.value(forKey: "Personal_Phone") as? String ?? ""
        let email = dic.value(forKey: "Email") as? String ?? ""
        //print("appId",appId)
        btnSelect.isHidden = true
        imgDropDown.isHidden = true
        imgGif.isHidden = false
        imgGif.image = UIImage(named: "yes")
        imgGif.setImageColor(color: .white)
        NotificationCenter.default.post(name: Notification.Name("onClickCollCell"), object: nil, userInfo: ["exlId":"\(exlId)","onClickYes":"Yes","appId":"\(appId)","mPhone":"\(Mobile_Phone)","hPhone":"\(home_Phone)","email":"\(email)"])
        mainView.isHidden = true
    }
    @objc func onClickNo1(_ sender: UIButton) {
        mainView.isHidden = true
    }
    
    
    @objc func onClickYes(_ sender: UIButton) {
        btnYes.tag = sender.tag
        mainView.isHidden = false
    }
    
    @objc func onClickNo(_ sender: UIButton) {
        //print("onClickNo~~~~~~",sender.tag)
        let dic = AppcontactArr[sender.tag] as! NSDictionary
        let exlId = dic.value(forKey: "excel_id") as? String ?? ""
        let appId = dic.value(forKey: "app_id") as? String ?? ""
        let Mobile_Phone = dic.value(forKey: "Mobile_Phone") as? String ?? ""
        let home_Phone = dic.value(forKey: "Personal_Phone") as? String ?? ""
        let email = dic.value(forKey: "Email") as? String ?? ""
        btnSelect.isHidden = true
        imgDropDown.isHidden = true
        imgGif.isHidden = false
        imgGif.image = UIImage(named: "yes")
        imgGif.setImageColor(color: .white)
        NotificationCenter.default.post(name: Notification.Name("onClickCollCell"), object: nil, userInfo: ["exlId":"\(exlId)","onClickYes":"No","appId":"\(appId)","mPhone":"\(Mobile_Phone)","hPhone":"\(home_Phone)","email":"\(email)"])
    }
    
//    func callUpdateContact(exl_id:String,email:String,hPhone:String,mPhone:String){
//        let authToken = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
//        postWithoutImage(url_string: "update_voterinfo_from_import_contact", parameters: "id=\(u_id)&cid=\(cp_id)&excel_id=\(exl_id)&email=\(email)&Personal_Phone=\(hPhone)&Mobile_Phone=\(mPhone)&authToken=\(authToken)",CompletionHandler: { json, error in
//            if let json = json {
//                print("json~~~~~~",json)
//            }
//        })
//    }
    
}


class ImportContact: UIView,UITableViewDelegate,UITableViewDataSource {
    
    var mainView:UIView!
    var lblHeading:UILabel!
    var popUpView:UIView!
    var btnCancel:UIButton!
    var btnOk:UIButton!
    var topView:UIView!
    var TableView: UITableView = UITableView()
    var msgView:UIView!
    var lblmsg:UILabel!
    
    var popupMainView:UIView!
    var btnaalreadyImport:UIButton!
    
    var cellReuseIdentifier = "Cell"
    var fPointer: UnsafeMutablePointer<Int>!
    var fPointer1: UnsafeMutablePointer<Int>!
    var u_id = USER_DEFAULTS.string(forKey: "u_id") ?? ""
    var cp_id = USER_DEFAULTS.string(forKey: "cp_id") ?? ""
    var btnNext:UIButton!
    var btnPrev:UIButton!
    var progressBar:LinearProgressBar!
    var btnReImport:UIButton!
    var lblTotalCount:UILabel!
    var actIndicator:UIActivityIndicatorView!
    
    var phoneContactArr = [SimDetail]()
    var AppContactArr = [AppDetail]()
    
    var jsonOBJ = NSDictionary()
    var selectedIndexPath : Int!
    var index = 0
    var startIndex = 0
    var endIndex = 0
    var startRow = 0
    var endRow = 0
    var isActIndiCator = false
    var scrollView:UIScrollView!
    var isReImport = 1
    var percentgae = 0
    var percent = 1
    let contactStore = CNContactStore()
    var batchCount = 200
    
    lazy var contacts: [CNContact] = {
        
        let keysToFetch = [
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
            CNContactEmailAddressesKey,
            CNContactPhoneNumbersKey,
            CNContactImageDataAvailableKey,
            CNContactThumbnailImageDataKey] as [Any]
        
        // Get all the containers
        var allContainers: [CNContainer] = []
        do {
            allContainers = try contactStore.containers(matching: nil)
        } catch {
            print("Error fetching containers")
        }
        
        var results: [CNContact] = []
        
        // Iterate all containers and append their contacts to our results array
        for container in allContainers {
            let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
            
            do {
                let containerResults = try     contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch as! [CNKeyDescriptor])
                results.append(contentsOf: containerResults)
            } catch {
                print("Error fetching results for container")
            }
        }
        
        return results
    }()
    
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
        
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpView()
    }
    
    func setUpView() {
        mainView = UIView()
        mainView.frame = CGRect(x: 10, y: 10, width: SWIDTH-20, height: SHEIGHT-LoginViewController.VC.NAVHEIGHT-STATUSBARHEIGHT-20)
        mainView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        mainView.layer.cornerRadius = 10.0
        self.addSubview(mainView)
        
        popupMainView = UIView()
        popupMainView.frame = CGRect(x: 0, y: mainView.frame.height/2-110, width: mainView.frame.width, height: 160)
        popupMainView.backgroundColor = UIColor.white
        popupMainView.layer.cornerRadius = 10.0
        popupMainView.clipsToBounds = true
        mainView.addSubview(popupMainView)
        
        topView = UIView()
        topView.frame = CGRect(x: 0, y: 0, width: popupMainView.frame.width, height: 50)
        topView.backgroundColor = APPBUTTONCOLOR
        popupMainView.addSubview(topView)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 0, Y: 10, Width: popupMainView.frame.width, Height: 25, TextColor: .white, BackColor: .clear, Text: "Do You Want?", TextAlignment: .center, Font: .boldSystemFont(ofSize: 18))
        topView.addSubview(lblHeading)
        
        btnaalreadyImport = UIButton(type: .system)
        btnaalreadyImport.setButton(X: 30, Y: topView.frame.maxY+10, Width: popupMainView.frame.width-60, Height: 40, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Already Imported", Align: .center, Font: .boldSystemFont(ofSize: 22))
        btnaalreadyImport.tag = 1
        btnaalreadyImport.addTarget(self, action: #selector(onClickIndexButton(_:)), for: .touchUpInside)
        popupMainView.addSubview(btnaalreadyImport)
        
        btnReImport = UIButton(type: .system)
        btnReImport.setButton(X: 30, Y: btnaalreadyImport.frame.maxY+10, Width: popupMainView.frame.width-60, Height: 40, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Re Import", Align: .center, Font: .boldSystemFont(ofSize: 22))
        btnReImport.tag = 2
        btnReImport.addTarget(self, action: #selector(onClickIndexButton(_:)), for: .touchUpInside)
        popupMainView.addSubview(btnReImport)
        popupMainView.isHidden = true
        
        
        popUpView = UIView()
        popUpView.frame = CGRect(x: 0, y: 0, width: mainView.frame.width, height: mainView.frame.height)
        popUpView.backgroundColor = UIColor.white
        popUpView.layer.cornerRadius = 10.0
        popUpView.clipsToBounds = true
        mainView.addSubview(popUpView)
        //popUpView.isHidden = true
        
        topView = UIView()
        topView.frame = CGRect(x: 0, y: 0, width: popUpView.frame.width, height: 50)
        topView.backgroundColor = APPBUTTONCOLOR
        popUpView.addSubview(topView)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 0, Y: 12, Width: popUpView.frame.width, Height: 25, TextColor: .white, BackColor: .clear, Text: "Import My Contacts", TextAlignment: .center, Font: .boldSystemFont(ofSize: 20))
        topView.addSubview(lblHeading)
        
        btnCancel = UIButton(type: .system)
        btnCancel.setButton(X: popUpView.frame.width-30, Y: 5, Width: 24, Height: 24, TextColor: .white, BackColor: .clear, Title: "X", Align: .center, Font: .boldSystemFont(ofSize: 22))
        btnCancel.tag = 2
        btnCancel.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        topView.addSubview(btnCancel)
        
        TableView.frame = CGRect(x: 0, y: topView.frame.maxY, width: popUpView.frame.width, height: popUpView.frame.height-100)
        TableView.backgroundColor = UIColor.white
        TableView.delegate = self
        TableView.dataSource = self
        TableView.register(ContactCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        popUpView.addSubview(TableView)
        TableView.separatorStyle = .none
        TableView.layer.cornerRadius = 10.0
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 0, Y: TableView.frame.maxY+1, Width: popupMainView.frame.width, Height: 1, TextColor: .white, BackColor: .lightGray, Text: "", TextAlignment: .center, Font: .boldSystemFont(ofSize: 18))
        popUpView.addSubview(lblHeading)
        
        lblTotalCount = UILabel()
        lblTotalCount.setLabel(X: 0, Y: TableView.frame.maxY+20, Width: popUpView.frame.width, Height: 25, TextColor: APPBUTTONCOLOR, BackColor: .clear, Text: "", TextAlignment: .center, Font: .boldSystemFont(ofSize: 18))
        popUpView.addSubview(lblTotalCount)
        
        
        btnPrev = UIButton(type: .system)
        btnPrev.setButton(X: 10, Y: TableView.frame.maxY+8, Width: 100, Height: 30, TextColor: .white, BackColor: APPBUTTONCOLOR1, Title: "Previous", Align: .center, Font: .boldSystemFont(ofSize: 22))
        btnPrev.tag = 4
        btnPrev.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        popUpView.addSubview(btnPrev)
        
        btnNext = UIButton(type: .system)
        btnNext.setButton(X: popUpView.frame.width-110, Y: TableView.frame.maxY+8, Width: 100, Height: 30, TextColor: .white, BackColor: APPBUTTONCOLOR1, Title: "Next", Align: .center, Font: .boldSystemFont(ofSize: 22))
        btnNext.tag = 3
        btnNext.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        popUpView.addSubview(btnNext)
        
        btnNext.isHidden = true
        btnPrev.isHidden = true
        
        progressBar = LinearProgressBar()
        progressBar.frame = CGRect(x: popUpView.frame.minX, y: TableView.frame.maxY+5, width: popUpView.frame.width, height: 15)
        progressBar.barColor = APPBUTTONCOLOR1
        progressBar.trackColor = UIColor.white
        progressBar.capType = 2
        progressBar.progressValue = 0
        popUpView.addSubview(progressBar)
        
        
        
        

        
        scrollView = UIScrollView()
        scrollView.frame = CGRect(x: 0, y: TableView.frame.maxY+8, width: popUpView.frame.width, height: 50)
        scrollView.backgroundColor = UIColor.white
        scrollView.bounces = false
        scrollView.contentSize.width = SWIDTH
        popUpView.addSubview(scrollView)
        scrollView.isHidden = true
        
        msgView = UIView()
        msgView.frame = CGRect(x: 0, y: 0, width: mainView.frame.width, height: 250)
        msgView.backgroundColor = UIColor.white
        msgView.layer.cornerRadius = 10.0
        msgView.clipsToBounds = true
        mainView.addSubview(msgView)
        //msgView.isHidden = true
        
        topView = UIView()
        topView.frame = CGRect(x: 0, y: 0, width: popUpView.frame.width, height: 50)
        topView.backgroundColor = APPBUTTONCOLOR
        msgView.addSubview(topView)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 0, Y: 10, Width: msgView.frame.width, Height: 30, TextColor: .white, BackColor: .clear, Text: "Message", TextAlignment: .center, Font: .boldSystemFont(ofSize: 18))
        topView.addSubview(lblHeading)
        
        lblmsg = UILabel()
        lblmsg.setLabel(X: 0, Y: topView.frame.maxY, Width: msgView.frame.width, Height: 90, TextColor: APPBUTTONCOLOR1, BackColor: .clear, Text: "Please Wait\nWhile We Identify Contact Information  For You To Import Into The Campaign Database.", TextAlignment: .center, Font: .boldSystemFont(ofSize: 18))
        lblmsg.numberOfLines = 0
        lblmsg.lineBreakMode = .byWordWrapping
        msgView.addSubview(lblmsg)
        
        actIndicator = UIActivityIndicatorView()
        actIndicator = UIActivityIndicatorView(frame: CGRect.init(x: msgView.frame.width/2-40, y: lblmsg.frame.maxY+20, width: 80, height: 80))
        //actIndicator.center = mainView.center
        actIndicator.style = UIActivityIndicatorView.Style.whiteLarge
        actIndicator.color = UIColor.black//UIColor(red:0.00, green:0.00, blue:0.50, alpha:1.0)
       
        actIndicator.hidesWhenStopped = true
        msgView.addSubview(actIndicator)
        
//        let dispatchAfter = DispatchTimeInterval.seconds(Int(2.0))
//        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + dispatchAfter, execute: {
//            self.actIndicator.startAnimating()
//        })
        
        popupMainView.isHidden = true
        //let isImposrtContactStatus = USER_DEFAULTS.value(forKey: "iSImportContactStatus") as! Int
        //print("isICFirstTime..\(USER_DEFAULTS.value(forKey: "iSImportContactStatus"))..",isICFirstTime,isICShowAI)
//        if isImposrtContactStatus == 0{
//            if isICShowAI{
//                //actIndicator.stopAnimating()
//            }
            DispatchQueue.main.async {
                var results: [CNContact] = []
                let data1 = NSKeyedArchiver.archivedData(withRootObject: "")
                UserDefaults.standard.set(data1, forKey: "ContactArr")
                
                let outData1 = UserDefaults.standard.data(forKey: "ContactArr")
                let dict1 = NSKeyedUnarchiver.unarchiveObject(with: outData1!)
                
                //print("dict..........",dict1)
                self.percentgae = 0
                for i in 0..<self.contacts.count {
                    results.append(self.contacts[i])
                }
                let data = NSKeyedArchiver.archivedData(withRootObject: results)
                UserDefaults.standard.set(data, forKey: "ContactArr")
                
                var postData = Data()
                let arr = NSMutableArray()
                let dispatchAfter = DispatchTimeInterval.seconds(Int(0.0))
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + dispatchAfter, execute: {
                    //
                    let outData = UserDefaults.standard.data(forKey: "ContactArr")
                    let dict = NSKeyedUnarchiver.unarchiveObject(with: outData!) as! NSArray;
                    let con = dict as! [CNContact]
                    var sortedNames = con.sorted { $0.familyName.localizedCaseInsensitiveCompare($1.familyName) == ComparisonResult.orderedAscending }
                    //print("con.count~~~~\(con.count)~~\(sortedNames)~~~~~")
                    self.percent = sortedNames.count/self.batchCount
                    
                    
                    //                    var pos = 8
                    //                    var btnPos = UIButton()
                    
                    //                for i in 0..<sortedNames.count/100 {
                    //                    btnPos = UIButton(type: .system)
                    //                    btnPos.setButton(X: CGFloat(pos), Y: 5, Width: 50, Height: 30, TextColor: .white, BackColor: APPBUTTONCOLOR1, Title: "\(i)", Align: .center, Font: .boldSystemFont(ofSize: 22))
                    //                    btnPos.tag = i
                    //                    btnPos.addTarget(self, action: #selector(self.onClickIndexButton(_:)), for: .touchUpInside)
                    //                    self.scrollView.addSubview(btnPos)
                    //                    pos = pos+58
                    //                    print("~~~~~~~~~~~~~~~~~~",i)
                    //                }
                    //                self.scrollView.contentSize.width = CGFloat(pos+10)
                    self.lblTotalCount.text = "Total Contacts-\(sortedNames.count)"
                    
                    
                    var stIndex = Int(USER_DEFAULTS.string(forKey: "phoneImportContact") ?? "")!
                    if stIndex > sortedNames.count{
                        stIndex = sortedNames.count
                    }
                    self.startIndex = stIndex
                    self.endIndex = stIndex+self.batchCount
                    
                    
                    for i in self.startIndex..<self.endIndex {
                        //print("ContactArr~~~~~~\(i)~~~~~")
                        if sortedNames.count == i{
                           // print("con~~1--\(i)")
                            break
                        }
                        var harr = [String]()
                        var marr = [String]()
                        for phoneNumber in sortedNames[i].phoneNumbers {
                            guard let phoneNumber = phoneNumber as? CNLabeledValue else {
                                continue
                            }
                            let lable :String  =  CNLabeledValue<NSString>.localizedString(forLabel: phoneNumber.label ?? "" )
                            //print("\(lable)  \(phoneNumber.value)")
                            
                            if lable == "home"{
                                var phone = phoneNumber.value.stringValue.withoutSpecialCharacters
                                phone = phone.replacingOccurrences(of: " ", with: "")
                                phone = phone.replacingOccurrences(of: "-", with: "")
                                phone = phone.replacingOccurrences(of: "(", with: "")
                                phone = phone.replacingOccurrences(of: ")", with: "")
                                let phno = String(phone.suffix(10))
                                harr.append(phno)
                            }
                            if lable == "mobile" || lable == "iPhone"{
                                var phone = phoneNumber.value.stringValue.withoutSpecialCharacters
                                phone = phone.replacingOccurrences(of: " ", with: "")
                                phone = phone.replacingOccurrences(of: "-", with: "")
                                phone = phone.replacingOccurrences(of: "(", with: "")
                                phone = phone.replacingOccurrences(of: ")", with: "")
                                let phno = String(phone.suffix(10))
                                marr.append(phno)
                            }
                            
                        }
                        if marr == []{
                            marr.append("")
                        }
                        if harr == []{
                            harr.append("")
                        }
                        let string = "\(sortedNames[i].givenName) \(sortedNames[i].familyName)"
                        
                        if string.components(separatedBy: " ").filter({ !$0.isEmpty}).count == 1 {
                        } else {
                            
                            
                            if harr != [""] || marr != [""]{
                                //print("harr~~~\(harr)--marr---\(marr)")
                                var grade: [AnyHashable : Any] = [:]
                                grade = ["Name": "\(sortedNames[i].givenName) \(sortedNames[i].familyName)" ,"sim_Email": sortedNames[i].emailAddresses.first?.value ?? "","sim_HomePhone": harr[0] == "" ? "" : harr[0],"sim_MobilePhone": marr[0] == "" ? "" : marr[0]]
                                arr.add(grade)
                                var grade1: [AnyHashable : Any] = [:]
                                grade1 = ["data": arr]
                                var jsonData: Data? = nil
                                do {
                                    jsonData = try JSONSerialization.data(withJSONObject: grade1, options: .prettyPrinted)
                                } catch {
                                }
                                var jsonString: String? = nil
                                if let jsonData = jsonData {
                                    jsonString = String(data: jsonData, encoding: .utf8)
                                    //print("jsonString",jsonString!)
                                }
                                postData = (jsonString?.data(using: .ascii, allowLossyConversion: true))!
                                if postData != nil {
                                    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
                                    let documentsDirectory = paths[0]
                                    let filePath = "\(documentsDirectory)/\("filename.doc")"
                                    do {
                                        try postData.write(to: URL(fileURLWithPath: filePath), options: .atomic)
                                    } catch {
                                        //print(error)
                                    }
                                }
                            }
                        }
                    }
                    self.callGetContact(jsonData: postData)
                    self.actIndicator.startAnimating()
                })
            }
//        }else{
//            actIndicator.isHidden = false
//            isImportContactAll = true
//            let stRow = Int(USER_DEFAULTS.string(forKey: "importContactCount") ?? "")!
//
//            callSearchContact(startRow: stRow, endRow: 50)
//        }
        
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onClickCollectionViewCell), name: NSNotification.Name(rawValue: "onClickCollCell"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onClickRearView), name: NSNotification.Name(rawValue: "onClickRearView"), object: nil)
        
    }
    
    
    
    @objc func onClickRearView(notif: NSNotification) {
        let dispatchAfter = DispatchTimeInterval.seconds(Int(2.0))
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + dispatchAfter, execute: {
            if isImportContactAll == false{
                task.cancel()
                rootView?.activityStopAnimating()
            }
        })
        rootView?.activityStopAnimating()
        self.isHidden = true
    }
    
    @objc func onClickIndexButton(_ sender: UIButton) {
        
        if sender.tag == 1{
            popUpView.isHidden = false
            msgView.isHidden = false
            popupMainView.isHidden = true
            let isImposrtContactStatus = USER_DEFAULTS.value(forKey: "iSImportContactStatus") as! Int
            //print("isICFirstTime..\(USER_DEFAULTS.value(forKey: "iSImportContactStatus"))..",isICFirstTime,isICShowAI)
            if isImposrtContactStatus == 0{
                if isICShowAI{
                    //actIndicator.stopAnimating()
                }
                DispatchQueue.main.async {
                    var results: [CNContact] = []
                    let data1 = NSKeyedArchiver.archivedData(withRootObject: "")
                    UserDefaults.standard.set(data1, forKey: "ContactArr")
                    
                    let outData1 = UserDefaults.standard.data(forKey: "ContactArr")
                    let dict1 = NSKeyedUnarchiver.unarchiveObject(with: outData1!)
                    
                    //print("dict..........",dict1)
                    for i in 0..<self.contacts.count {
                        results.append(self.contacts[i])
                    }
                    let data = NSKeyedArchiver.archivedData(withRootObject: results)
                    UserDefaults.standard.set(data, forKey: "ContactArr")
                    
                    var postData = Data()
                    let arr = NSMutableArray()
                    let dispatchAfter = DispatchTimeInterval.seconds(Int(1.0))
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + dispatchAfter, execute: {
                        //
                        let outData = UserDefaults.standard.data(forKey: "ContactArr")
                        let dict = NSKeyedUnarchiver.unarchiveObject(with: outData!) as! NSArray;
                        let con = dict as! [CNContact]
                        var sortedNames = con.sorted { $0.givenName.localizedCaseInsensitiveCompare($1.givenName) == ComparisonResult.orderedAscending }
                        //print("con.count~~~~~~\(sortedNames.count/self.batchCount)~~~~~")
                        self.percent = sortedNames.count/self.batchCount
                        //                    var pos = 8
                        //                    var btnPos = UIButton()
                        
                        //                for i in 0..<sortedNames.count/100 {
                        //                    btnPos = UIButton(type: .system)
                        //                    btnPos.setButton(X: CGFloat(pos), Y: 5, Width: 50, Height: 30, TextColor: .white, BackColor: APPBUTTONCOLOR1, Title: "\(i)", Align: .center, Font: .boldSystemFont(ofSize: 22))
                        //                    btnPos.tag = i
                        //                    btnPos.addTarget(self, action: #selector(self.onClickIndexButton(_:)), for: .touchUpInside)
                        //                    self.scrollView.addSubview(btnPos)
                        //                    pos = pos+58
                        //                    print("~~~~~~~~~~~~~~~~~~",i)
                        //                }
                        //                self.scrollView.contentSize.width = CGFloat(pos+10)
                        self.lblTotalCount.text = "Total Contacts-\(sortedNames.count)"
                        
                        
                        var stIndex = Int(USER_DEFAULTS.string(forKey: "phoneImportContact") ?? "")!
                        if stIndex > sortedNames.count{
                            stIndex = sortedNames.count
                        }
                        self.startIndex = stIndex
                        self.endIndex = stIndex+self.batchCount
                        
                        
                        for i in self.startIndex..<self.endIndex {
                            //print("ContactArr~~~~~~\(i)~~~~~")
                            if sortedNames.count == i{
                                //print("con~~1--\(i)")
                                break
                            }
                            var harr = [String]()
                            var marr = [String]()
                            for phoneNumber in sortedNames[i].phoneNumbers {
                                guard let phoneNumber = phoneNumber as? CNLabeledValue else {
                                    continue
                                }
                                let lable :String  =  CNLabeledValue<NSString>.localizedString(forLabel: phoneNumber.label ?? "" )
                                //print("\(lable)  \(phoneNumber.value)")
                                
                                if lable == "home"{
                                    var phone = phoneNumber.value.stringValue.withoutSpecialCharacters
                                    phone = phone.replacingOccurrences(of: " ", with: "")
                                    phone = phone.replacingOccurrences(of: "-", with: "")
                                    phone = phone.replacingOccurrences(of: "(", with: "")
                                    phone = phone.replacingOccurrences(of: ")", with: "")
                                    let phno = String(phone.suffix(10))
                                    harr.append(phno)
                                }
                                if lable == "mobile" || lable == "iPhone"{
                                    var phone = phoneNumber.value.stringValue.withoutSpecialCharacters
                                    phone = phone.replacingOccurrences(of: " ", with: "")
                                    phone = phone.replacingOccurrences(of: "-", with: "")
                                    phone = phone.replacingOccurrences(of: "(", with: "")
                                    phone = phone.replacingOccurrences(of: ")", with: "")
                                    let phno = String(phone.suffix(10))
                                    marr.append(phno)
                                }
                                
                            }
                            if marr == []{
                                marr.append("")
                            }
                            if harr == []{
                                harr.append("")
                            }
                            let string = "\(sortedNames[i].givenName) \(sortedNames[i].familyName)"
                            
                            if string.components(separatedBy: " ").filter({ !$0.isEmpty}).count == 1 {
                            } else {
                                var grade: [AnyHashable : Any] = [:]
                                grade = ["Name": "\(sortedNames[i].givenName) \(sortedNames[i].familyName)" ,"sim_Email": sortedNames[i].emailAddresses.first?.value ?? "","sim_HomePhone": harr[0] == "" ? "" : harr[0],"sim_MobilePhone": marr[0] == "" ? "" : marr[0]]
                                arr.add(grade)
                                var grade1: [AnyHashable : Any] = [:]
                                grade1 = ["data": arr]
                                var jsonData: Data? = nil
                                do {
                                    jsonData = try JSONSerialization.data(withJSONObject: grade1, options: .prettyPrinted)
                                } catch {
                                }
                                var jsonString: String? = nil
                                if let jsonData = jsonData {
                                    jsonString = String(data: jsonData, encoding: .utf8)
                                    //print("jsonString",jsonString!)
                                }
                                postData = (jsonString?.data(using: .ascii, allowLossyConversion: true))!
                                if postData != nil {
                                    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
                                    let documentsDirectory = paths[0]
                                    let filePath = "\(documentsDirectory)/\("filename.doc")"
                                    do {
                                        try postData.write(to: URL(fileURLWithPath: filePath), options: .atomic)
                                    } catch {
                                        print(error)
                                    }
                                }
                            }
                        }
                        self.callGetContact(jsonData: postData)
                    })
                }
            }else{
                actIndicator.isHidden = false
                let stRow = Int(USER_DEFAULTS.string(forKey: "importContactCount") ?? "")!
                
                callSearchContact(startRow: stRow, endRow: 50)
            }
        }else{
            popupMainView.isHidden = true
            self.percentgae = 0
            isReImport = 1
            isFlag = true
            var postData = Data()
            let arr = NSMutableArray()
            let outData = UserDefaults.standard.data(forKey: "ContactArr")
            let dict = NSKeyedUnarchiver.unarchiveObject(with: outData!) as! NSArray;
            let con = dict as! [CNContact]
            var sortedNames = con.sorted { $0.givenName.localizedCaseInsensitiveCompare($1.givenName) == ComparisonResult.orderedAscending }
            self.percent = sortedNames.count/self.batchCount
            startIndex = 0
            endIndex = self.batchCount
            print("startIndex",startIndex,endIndex)
            USER_DEFAULTS.set(startIndex, forKey: "phoneImportContact")
            
            
            for i in startIndex...endIndex{
                //print("con~~\(i)")
                
                if sortedNames.count == i{
                    //print("con~~1--\(i)")
                    break
                }
                
                var harr = [String]()
                var marr = [String]()
                for phoneNumber in sortedNames[i].phoneNumbers {
                    guard let phoneNumber = phoneNumber as? CNLabeledValue else {
                        continue
                    }
                    let lable :String  =  CNLabeledValue<NSString>.localizedString(forLabel: phoneNumber.label ?? "" )
                    //print("\(lable)  \(phoneNumber.value)")
                    //print("contact~~~~~~~~",phoneNumber.label)
                    
                    if lable == "home"{
                        var phone = phoneNumber.value.stringValue.withoutSpecialCharacters
                        phone = phone.replacingOccurrences(of: " ", with: "")
                        phone = phone.replacingOccurrences(of: "-", with: "")
                        phone = phone.replacingOccurrences(of: "(", with: "")
                        phone = phone.replacingOccurrences(of: ")", with: "")
                        let phno = String(phone.suffix(10))
                        harr.append(phno)
                    }
                    if lable == "mobile" || lable == "iPhone"{
                        var phone = phoneNumber.value.stringValue.withoutSpecialCharacters
                        phone = phone.replacingOccurrences(of: " ", with: "")
                        phone = phone.replacingOccurrences(of: "-", with: "")
                        phone = phone.replacingOccurrences(of: "(", with: "")
                        phone = phone.replacingOccurrences(of: ")", with: "")
                        let phno = String(phone.suffix(10))
                        marr.append(phno)
                    }
                    
                }
                //print("marr!!!----\(marr)---harr----\(harr)")
                if marr == []{
                    marr.append("")
                }
                if harr == []{
                    harr.append("")
                }
                //                    print("arr1!!!--\(arr1)")
                
                let string = "\(sortedNames[i].givenName) \(sortedNames[i].familyName)"
                
                if string.components(separatedBy: " ").filter({ !$0.isEmpty}).count == 1 {
                    // print("One word")
                    
                } else {
                    // print("Not one word")
                    
                    var grade: [AnyHashable : Any] = [:]
                    grade = ["Name": "\(sortedNames[i].givenName) \(sortedNames[i].familyName)" ,"sim_Email": sortedNames[i].emailAddresses.first?.value ?? "","sim_HomePhone": harr[0] == "" ? "" : harr[0],"sim_MobilePhone": marr[0] == "" ? "" : marr[0]]
                    arr.add(grade)
                    var grade1: [AnyHashable : Any] = [:]
                    grade1 = ["data": arr]
                    var jsonData: Data? = nil
                    do {
                        jsonData = try JSONSerialization.data(withJSONObject: grade1, options: .prettyPrinted)
                    } catch {
                    }
                    var jsonString: String? = nil
                    if let jsonData = jsonData {
                        jsonString = String(data: jsonData, encoding: .utf8)
                        //print("jsonString",jsonString!)
                    }
                    postData = (jsonString?.data(using: .ascii, allowLossyConversion: true))!
                    if postData != nil {
                        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
                        let documentsDirectory = paths[0]
                        let filePath = "\(documentsDirectory)/\("filename.doc")"
                        do {
                            try postData.write(to: URL(fileURLWithPath: filePath), options: .atomic)
                        } catch {
                            print(error)
                        }
                    }
                }
            }
            self.callGetContact(jsonData: postData)
        }
    }
    
    @objc func onClickButton(_ sender: UIButton) {
        print("onClickButton......")
        if sender.tag == 2{
            self.isHidden = true
            
            if isImportContactAll == false{
               task.cancel()
            }
            //URLSession.shared.invalidateAndCancel()
            NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "onClickCollCell"), object: nil)
            HomeViewController.VC.navigationItem.rightBarButtonItem = HomeViewController.VC.btnEditPerson
        }
        if sender.tag == 3{
            print("onClickButton...next...")
            index = index+50
            startRow = index
            endRow = 50+index
            print("startIndex",startRow,endRow)
            selectedIndexPath = nil
            callSearchContact(startRow: startRow, endRow: 50)
        }
        if sender.tag == 4{
            print("onClickButton...prev...")
            index = index-50
            startRow = index
            endRow = 50+index
            print("startIndex",startRow,endRow)
            USER_DEFAULTS.set(startRow, forKey: "importContactCount")
            selectedIndexPath = nil
            callSearchContact(startRow: startRow, endRow: 50)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //let data = jsonOBJ.value(forKey: "data") as! NSArray
        if phoneContactArr.count <= 50{
            return phoneContactArr.count
        }else{
            return 50
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! ContactCell
        cell.AppcontactArr = phoneContactArr[indexPath.row].appContactArr
        cell.lblName.text = "Name : \(phoneContactArr[indexPath.row].strName)"
        cell.lblEmail.text = "Email : \(phoneContactArr[indexPath.row].strEmail)"
        var mutableString = phoneContactArr[indexPath.row].strHomePhone
        for p in 0..<mutableString.count {
            if p == 0 {
                mutableString.insert("(", at: mutableString.startIndex)
            }
            if p == 4 {
                mutableString.insert(contentsOf: ") ", at: mutableString.index(mutableString.startIndex, offsetBy: p))
            }
            if p == 9 {
                mutableString.insert(contentsOf: "-", at: mutableString.index(mutableString.startIndex, offsetBy: p))
            }
        }
        cell.lblHPhone.text = "Home Phone : \(mutableString)"
        mutableString = phoneContactArr[indexPath.row].strMobilePhone
        for p in 0..<mutableString.count {
            if p == 0 {
                mutableString.insert("(", at: mutableString.startIndex)
            }
            if p == 4 {
                mutableString.insert(contentsOf: ") ", at: mutableString.index(mutableString.startIndex, offsetBy: p))
            }
            if p == 9 {
                mutableString.insert(contentsOf: "-", at: mutableString.index(mutableString.startIndex, offsetBy: p))
            }
        }
        cell.lblMPhone.text = "Mobile Phone : \(mutableString)"
        cell.btnSelect.tag = indexPath.row
        cell.btnSelect.addTarget(self, action: #selector(onClickTabCell(_:)), for: .touchUpInside)
        cell.selectionStyle = .none
        if phoneContactArr[indexPath.row].appContactArr.count == 0 {
            fPointer[indexPath.row] = 0
            fPointer1[indexPath.row] = 0
        }
        cell.btnSelect.isHidden = false
        //print("cell.btnSelect.isHidden",cell.btnSelect.isHidden,self.fPointer1[indexPath.row])
        if phoneContactArr[indexPath.row].isOpen == false{
            if self.fPointer1[indexPath.row] == 0{
                cell.imgDropDown.isHidden = false
                cell.imgGif.isHidden = true
                // cell.btnSelect.isHidden = false
                cell.imgDropDown.image = UIImage(named: "down-arrow.png")
            }else{
                cell.imgGif.isHidden = false
                cell.imgDropDown.isHidden = true
                //                cell.imgGif.loadGif(asset: "right")
                cell.imgGif.image = UIImage(named: "yes")
                cell.imgGif.setImageColor(color: .white)
                // cell.btnSelect.isHidden = true
            }
            cell.backView1.isHidden = true
        }else{
            if !cell.btnSelect.isHidden{
                cell.imgDropDown.image = UIImage(named: "up-Arrow.png")
            }
            if self.fPointer1[indexPath.row] == 1{
                cell.imgGif.isHidden = false
                //                cell.imgGif.loadGif(asset: "right")
                cell.imgGif.image = UIImage(named: "yes")
                cell.imgGif.setImageColor(color: .white)
            }
            cell.backView1.isHidden = false
        }
        cell.collectionView.reloadData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if phoneContactArr[indexPath.row].isOpen{
            return 460.0
        }else{
            return 190.0
        }
    }
    
    @objc func onClickTabCell(_ sender: UIButton) {
        //print("onClickTabCell")
        selectedIndexPath = sender.tag
        //print(fPointer[sender.tag])
        phoneContactArr[sender.tag].isOpen = phoneContactArr[sender.tag].isOpen == false ? true : false
        TableView.reloadData()
    }
    
    @objc func onClickCollectionViewCell(notif: NSNotification) {
        if let exlId = notif.userInfo?["exlId"] as? String {
            print(exlId,selectedIndexPath)
            let onclick = notif.userInfo?["onClickYes"] as? String ?? ""
            let appID = notif.userInfo?["appId"] as? String ?? ""
            let Email = notif.userInfo?["email"] as? String ?? ""
            let h_phone = notif.userInfo?["hPhone"] as? String ?? ""
            let m_phone = notif.userInfo?["mPhone"] as? String ?? ""
            if (onclick == "Yes"){
                print(phoneContactArr[selectedIndexPath].appContactArr)
                let email = phoneContactArr[selectedIndexPath].strEmail
                let hphone = phoneContactArr[selectedIndexPath].strHomePhone
                let mphone = phoneContactArr[selectedIndexPath].strMobilePhone
                
                if Email != email || h_phone != hphone || m_phone != mphone{
                    callUpdateContact(exl_id: exlId, email: email, hPhone: hphone, mPhone: mphone, app_id: appID)
                }else{
                    HomeViewController.VC.alert(message: "New And Old Records Are Same", title: "Alert")
                }
                phoneContactArr[selectedIndexPath].isOpen = false
                fPointer1[selectedIndexPath] = fPointer1[selectedIndexPath] == 0 ? 1 : 0
                self.TableView.reloadData()
            }else{
                callCancelContact(exl_id: exlId, app_id: appID)
                //self.fPointer[selectedIndexPath] = 0
                phoneContactArr[selectedIndexPath].isOpen = false
                fPointer1[selectedIndexPath] = 0
                self.TableView.reloadData()
                print("hello~~~~~~")
            }
        }
    }
    
    func callUpdateContact(exl_id:String,email:String,hPhone:String,mPhone:String,app_id:String){
        //NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "onClickCollCell"), object: nil)
        let deviceID = UserDefaults.standard.value(forKey: "deviceID") as? String ?? ""
        let authToken = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
        postWithoutImage(url_string: "update_voterinfo_from_import_contact_v2", parameters: "id=\(u_id)&cid=\(cp_id)&excel_id=\(exl_id)&email=\(email)&Personal_Phone=\(hPhone)&Mobile_Phone=\(mPhone)&app_id=\(app_id)&deviceId=\(deviceID)&authToken=\(authToken)",CompletionHandler: { json, error in
            if let json = json {
                print("json~~~~~~",json)
                let statusmassege = json.value(forKey: "status") as! Bool
                let msg = json.value(forKey: "msg") as? String ?? ""
                if (statusmassege == true){
                    
                    var arr = [AnyHashable]()
                    let data = json.value(forKey: "data") as! NSDictionary
                    let mPhone = data.value(forKey: "Mobile_Phone") as? String ?? ""
                    let hPhone = data.value(forKey: "Personal_Phone") as? String ?? ""
                    let Email = data.value(forKey: "Email") as? String ?? ""
                    let isImport = json.value(forKey: "isContactUpdate") as? String ?? ""
                    let isImportOld = json.value(forKey: "isContactUndo") as? String ?? ""
                    print("isContactUpdate",isImport)
                    for i in 0..<self.phoneContactArr[self.selectedIndexPath].appContactArr.count{
                        let dic = (self.phoneContactArr[self.selectedIndexPath].appContactArr[i] as! NSDictionary).mutableCopy() as! NSMutableDictionary
                        let exalID = dic.value(forKey: "excel_id") as? String ?? ""
                        print("exalID~~~~~~",exalID)
                        if exalID == exl_id{
                            if hPhone != ""{
                                dic["Personal_Phone"] = hPhone
                            }
                            if mPhone != ""{
                                dic["Mobile_Phone"] = mPhone
                            }
                            if Email != ""{
                                dic["Email"] = Email
                            }
                            dic["isContactUpdate"] = isImport
                            dic["isContactUndo"] = isImportOld
                        }
                        print("dic~~~~~~",dic)
                        arr.append(dic)
                    }
                    DispatchQueue.main.async {
                        self.phoneContactArr[self.selectedIndexPath].appContactArr = arr
                        //print("appContactArr~~~~",self.phoneContactArr[self.selectedIndexPath].appContactArr)
                        self.TableView.reloadData()
                        if msg == "New and Old records are same"{
                            HomeViewController.VC.alert(message: "New and Old records are same", title: "Alert!")
                        }
                    }
                    
                }else{
                    var is_changed: String? = nil
                    if let object = json["is_changed"] {
                        is_changed = "\(object)"
                    }
                    if is_changed?.isEqual("1") ?? false {
                        logoutAlertMassege()
                    } else {
                        HomeViewController.VC.alert(message: "No Record Found!", title: "Alert!")
                    }
                }
            }
        })
    }
    
    func callCancelContact(exl_id:String,app_id:String){
        let authToken = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
        let deviceID = UserDefaults.standard.value(forKey: "deviceID") as? String ?? ""
        postWithoutImage(url_string: "select_update_old_contact_v2", parameters: "id=\(u_id)&cid=\(cp_id)&excel_id=\(exl_id)&isUpdate=1&app_id=\(app_id)&deviceId=\(deviceID)&authToken=\(authToken)",CompletionHandler: { json, error in
            if let json = json {
                print("json~~~~~~",json)
                let statusmassege = json.value(forKey: "status") as! Bool
                if (statusmassege == true){
                    //print("phoneContactArr~~~~~~~~~~~~~",self.phoneContactArr[0].appContactArr)
                    // self.phoneContactArr[selectedIndexPath].appContactArr
                    var arr = [AnyHashable]()
                    let data = json.value(forKey: "data") as! NSDictionary
                    let mPhone = data.value(forKey: "Mobile_Phone") as? String ?? ""
                    let hPhone = data.value(forKey: "Personal_Phone") as? String ?? ""
                    let Email = data.value(forKey: "Email") as? String ?? ""
                    let isImportOld = json.value(forKey: "isContactUndo") as? String ?? ""
                    let isImport = json.value(forKey: "isContactUpdate") as? String ?? ""
                    for i in 0..<self.phoneContactArr[self.selectedIndexPath].appContactArr.count{
                        let dic = (self.phoneContactArr[self.selectedIndexPath].appContactArr[i] as! NSDictionary).mutableCopy() as! NSMutableDictionary
                        let exalID = dic.value(forKey: "excel_id") as? String ?? ""
                        if exalID == exl_id{
                            if hPhone != ""{
                                dic["Personal_Phone"] = hPhone
                            }
                            if hPhone == "Empty"{
                                dic["Personal_Phone"] = ""
                            }
                            if mPhone != ""{
                                dic["Mobile_Phone"] = mPhone
                            }
                            if mPhone == "Empty"{
                                dic["Mobile_Phone"] = ""
                            }
                            if Email != ""{
                                dic["Email"] = Email
                            }
                            if Email == "Empty"{
                                dic["Email"] = ""
                            }
                            dic["isContactUpdate"] = isImport
                            dic["isContactUndo"] = isImportOld
                            
                        }
                        arr.append(dic)
                    }
                    DispatchQueue.main.async {
                        self.phoneContactArr[self.selectedIndexPath].appContactArr = arr
                        self.TableView.reloadData()
                    }
                    
                }else{
                    var is_changed: String? = nil
                    if let object = json["is_changed"] {
                        is_changed = "\(object)"
                    }
                    if is_changed?.isEqual("1") ?? false {
                        logoutAlertMassege()
                    } else {
                        HomeViewController.VC.alert(message: "No Record Found!", title: "Alert!")
                    }
                }
            }
        })
    }
    
    var isFlag = true
    var isSelectCellFlag = true
    
    func callGetContact(jsonData:Data){
        
        //[UIApplication sharedApplication].idleTimerDisabled = YES;
        UIApplication.shared.isIdleTimerDisabled = true
        
        let authToken = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
        let deviceID = UserDefaults.standard.value(forKey: "deviceID") as? String ?? ""
        let param = ["authToken":authToken,"id":u_id,"cid":cp_id,"isReimport":"\(isReImport)","deviceId":"\(deviceID)"]
        postWithImageforImportContact(url_string: "get_voterlist_for_import_contact_v2", parameters: param, filePathKey: "file", jsonData: jsonData, CompletionHandler: { json, error in
            
            if let json = json {
                let statusmassege = json.value(forKey: "status") as! Bool
                if self.isReImport == 1{
                    self.isReImport = 0
                }
                
                self.percentgae = self.percentgae+1
                print("percentage~~~~~~\(self.percentgae)~\(self.percent)~~~~")
                if self.percent == 0{
                    self.percent = 1
                }

                if (statusmassege == true){
                    DispatchQueue.main.async {
                        self.msgView.isHidden = true
                        self.actIndicator.stopAnimating()
                        UIApplication.shared.isIdleTimerDisabled = false
                    }
                    
                    let authToken = json.value(forKey: "authToken") as! String
                    USER_DEFAULTS.set(authToken, forKey: "authToken")
//                    self.phoneContactArr.removeAll()
//                    self.AppContactArr.removeAll()
                    let data = json.value(forKey: "data") as! NSArray
                    self.jsonOBJ = json
                    for i in 0..<data.count{
                        let dic = data[i] as! NSDictionary
                        let simDatail = dic.value(forKey: "SimDetails") as! NSDictionary
                        let s_detail = SimDetail()
                        s_detail.strName = simDatail.value(forKey: "Name") as? String ?? ""
                        s_detail.strEmail = simDatail.value(forKey: "sim_Email") as? String ?? ""
                        s_detail.strHomePhone = simDatail.value(forKey: "sim_HomePhone") as? String ?? ""
                        s_detail.strMobilePhone = simDatail.value(forKey: "sim_MobilePhone") as? String ?? ""
                        s_detail.appContactArr = (dic.value(forKey: "DatabaseContact") as! NSArray) as! [Any]
                        self.phoneContactArr.append(s_detail)
//                        self.fPointer = UnsafeMutablePointer.allocate(capacity: self.phoneContactArr.count)
//                        for i in 0..<self.phoneContactArr.count {
//                            self.fPointer[i] = 0
//                        }
                        self.fPointer1 = UnsafeMutablePointer.allocate(capacity: self.phoneContactArr.count)
                        for i in 0..<self.phoneContactArr.count {
                            self.fPointer1[i] = 0
                        }
                    }
                    DispatchQueue.main.async {
                        self.isSelectCellFlag = false
                        if self.selectedIndexPath != nil{
                            self.phoneContactArr[self.selectedIndexPath].isOpen = true
                        }

//                        if self.phoneContactArr.count > 50{
//                            self.btnPrev.isHidden = false
//                        }
//                        if self.phoneContactArr.count < 50{
//                            self.btnNext.isHidden = true
//                            self.btnPrev.isHidden = true
//                        }
                        self.TableView.reloadData()
                    }
                    var postData = Data()
                    let arr = NSMutableArray()
                    let dispatchAfter = DispatchTimeInterval.seconds(Int(0.0))
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + dispatchAfter, execute: {
                        let outData = UserDefaults.standard.data(forKey: "ContactArr")
                        let dict = NSKeyedUnarchiver.unarchiveObject(with: outData!) as! NSArray;
                        let con = dict as! [CNContact]
                        var sortedNames = con.sorted { $0.familyName.localizedCaseInsensitiveCompare($1.familyName) == ComparisonResult.orderedAscending }
                        if sortedNames.count < self.batchCount{
                            self.isFlag = false
                            isImportContactAll = true
                            if self.phoneContactArr.count >= 50{
                                self.btnNext.isHidden = false
                            }
                        }
                        
                    if self.isFlag == true{
                        let stIndex = Int(USER_DEFAULTS.string(forKey: "phoneImportContact") ?? "")!
                        self.startIndex = stIndex+self.batchCount
                        self.endIndex = self.startIndex+self.batchCount
                        USER_DEFAULTS.set(self.startIndex, forKey: "phoneImportContact")
                        //print("startIndex",self.startIndex,self.endIndex)
                        
                        
                        if sortedNames.count < self.startIndex{
                            self.isFlag = false
                            isImportContactAll = true
                        }
                        print("self.isFlag~~~~~~~",self.isFlag)
                        
                        
                        
                            for i in self.startIndex..<self.endIndex {
                                if sortedNames.count == i{
                                    break
                                }
                                var harr = [String]()
                                var marr = [String]()
                                for phoneNumber in sortedNames[i].phoneNumbers {
                                    guard let phoneNumber = phoneNumber as? CNLabeledValue else {
                                        continue
                                    }
                                    let lable :String  =  CNLabeledValue<NSString>.localizedString(forLabel: phoneNumber.label ?? "" )
                                    //print("\(lable)  \(phoneNumber.value)")
                                    //print("contact~~~~~~~~",phoneNumber.label)
                                    
                                    if lable == "home"{
                                        var phone = phoneNumber.value.stringValue.withoutSpecialCharacters
                                        phone = phone.replacingOccurrences(of: " ", with: "")
                                        phone = phone.replacingOccurrences(of: "-", with: "")
                                        phone = phone.replacingOccurrences(of: "(", with: "")
                                        phone = phone.replacingOccurrences(of: ")", with: "")
                                        let phno = String(phone.suffix(10))
                                        harr.append(phno)
                                    }
                                    if lable == "mobile" || lable == "iPhone"{
                                        var phone = phoneNumber.value.stringValue.withoutSpecialCharacters
                                        phone = phone.replacingOccurrences(of: " ", with: "")
                                        phone = phone.replacingOccurrences(of: "-", with: "")
                                        phone = phone.replacingOccurrences(of: "(", with: "")
                                        phone = phone.replacingOccurrences(of: ")", with: "")
                                        let phno = String(phone.suffix(10))
                                        marr.append(phno)
                                    }
                                    
                                }
                                //print("marr!!!----\(marr)---harr----\(harr)")
                                if marr == []{
                                    marr.append("")
                                }
                                if harr == []{
                                    harr.append("")
                                }
                                //                    print("arr1!!!--\(arr1)")
                                
                                let string = "\(sortedNames[i].givenName) \(sortedNames[i].familyName)"
                                
                                if string.components(separatedBy: " ").filter({ !$0.isEmpty}).count == 1 {
                                    // print("One word")
                                    
                                } else {
                                    // print("Not one word")
                                    if harr != [""] || marr != [""]{
                                        var grade: [AnyHashable : Any] = [:]
                                        grade = ["Name": "\(sortedNames[i].givenName) \(sortedNames[i].familyName)" ,"sim_Email": sortedNames[i].emailAddresses.first?.value ?? "","sim_HomePhone": harr[0] == "" ? "" : harr[0],"sim_MobilePhone": marr[0] == "" ? "" : marr[0]]
                                        arr.add(grade)
                                        var grade1: [AnyHashable : Any] = [:]
                                        grade1 = ["data": arr]
                                        var jsonData: Data? = nil
                                        do {
                                            jsonData = try JSONSerialization.data(withJSONObject: grade1, options: .prettyPrinted)
                                        } catch {
                                        }
                                        var jsonString: String? = nil
                                        if let jsonData = jsonData {
                                            jsonString = String(data: jsonData, encoding: .utf8)
                                            //print("jsonString",jsonString!)
                                        }
                                        postData = (jsonString?.data(using: .ascii, allowLossyConversion: true))!
                                        if postData != nil {
                                            let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
                                            let documentsDirectory = paths[0]
                                            let filePath = "\(documentsDirectory)/\("filename.doc")"
                                            do {
                                                try postData.write(to: URL(fileURLWithPath: filePath), options: .atomic)
                                            } catch {
                                                print(error)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if self.isFlag == true{
                            self.callGetContact(jsonData: postData)
                        }
                        
                        for i in self.startIndex..<self.endIndex {
                            if sortedNames.count == i{
                                self.isFlag = false
                                isImportContactAll = true
                                if self.phoneContactArr.count <= 50{
                                    self.callSearchContact(startRow: 0, endRow: 50)
                                }
                                
                                break
                            }
                        }
                    })
                    print("cool---true")
                }else{
                    print("cool---false")
                    var is_changed: String? = nil
                    if let object = json["is_changed"] {
                        is_changed = "\(object)"
                    }
                    if is_changed?.isEqual("1") ?? false {
                        logoutAlertMassege()
                    } else {
                        DispatchQueue.main.async {
                            //self.isHidden = true
                            //HomeViewController.VC.alert(message: "No Record Found! Do You Want Match Again Please Click On Next ", title: "Alert!")
                            var postData = Data()
                            let arr = NSMutableArray()
                            let dispatchAfter = DispatchTimeInterval.seconds(Int(0.0))
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + dispatchAfter, execute: {
                                
                                let outData = UserDefaults.standard.data(forKey: "ContactArr")
                                let dict = NSKeyedUnarchiver.unarchiveObject(with: outData!) as! NSArray;
                                let con = dict as! [CNContact]
                                
                                var sortedNames = con.sorted { $0.familyName.localizedCaseInsensitiveCompare($1.familyName) == ComparisonResult.orderedAscending }
                                //print("con.count~~~~~~\(sortedNames)~~~~~")
                                
                                //self.lblTotalCount.text = "Total Contact-\(sortedNames.count)"
                                
                                if sortedNames.count < self.batchCount{
                                    self.isFlag = false
                                    isImportContactAll = true
                                }
                                if self.isFlag == true{
                                    let stIndex = Int(USER_DEFAULTS.string(forKey: "phoneImportContact") ?? "")!
                                    self.startIndex = stIndex+self.batchCount
                                    self.endIndex = self.startIndex+self.batchCount
                                    USER_DEFAULTS.set(self.startIndex, forKey: "phoneImportContact")
                                    print("startIndex",self.startIndex,self.endIndex)
                                    
                                    if sortedNames.count < self.startIndex{
                                        self.isFlag = false
                                        isImportContactAll = true
                                    }
                                    
                                    for i in self.startIndex..<self.endIndex {
                                        if sortedNames.count == i{
                                            break
                                        }
                                        
                                        var harr = [String]()
                                        var marr = [String]()
                                        
                                        
                                        for phoneNumber in sortedNames[i].phoneNumbers {
                                            guard let phoneNumber = phoneNumber as? CNLabeledValue else {
                                                continue
                                            }
                                            let lable :String  =  CNLabeledValue<NSString>.localizedString(forLabel: phoneNumber.label ?? "" )
                                            //print("\(lable)  \(phoneNumber.value)")
                                            //print("contact~~~~~~~~",phoneNumber.label)
                                            
                                            if lable == "home"{
                                                var phone = phoneNumber.value.stringValue.withoutSpecialCharacters
                                                phone = phone.replacingOccurrences(of: " ", with: "")
                                                phone = phone.replacingOccurrences(of: "-", with: "")
                                                phone = phone.replacingOccurrences(of: "(", with: "")
                                                phone = phone.replacingOccurrences(of: ")", with: "")
                                                let phno = String(phone.suffix(10))
                                                harr.append(phno)
                                            }
                                            if lable == "mobile" || lable == "iPhone"{
                                                var phone = phoneNumber.value.stringValue.withoutSpecialCharacters
                                                phone = phone.replacingOccurrences(of: " ", with: "")
                                                phone = phone.replacingOccurrences(of: "-", with: "")
                                                phone = phone.replacingOccurrences(of: "(", with: "")
                                                phone = phone.replacingOccurrences(of: ")", with: "")
                                                let phno = String(phone.suffix(10))
                                                marr.append(phno)
                                            }
                                            
                                        }
                                        //print("marr!!!----\(marr)---harr----\(harr)")
                                        if marr == []{
                                            marr.append("")
                                        }
                                        if harr == []{
                                            harr.append("")
                                        }
                                        //                    print("arr1!!!--\(arr1)")
                                        
                                        let string = "\(sortedNames[i].givenName) \(sortedNames[i].familyName)"
                                        
                                        if string.components(separatedBy: " ").filter({ !$0.isEmpty}).count == 1 {
                                            // print("One word")
                                            
                                        } else {
                                            // print("Not one word")
                                            if harr != [""] || marr != [""]{
                                                var grade: [AnyHashable : Any] = [:]
                                                grade = ["Name": "\(sortedNames[i].givenName) \(sortedNames[i].familyName)" ,"sim_Email": sortedNames[i].emailAddresses.first?.value ?? "","sim_HomePhone": harr[0] == "" ? "" : harr[0],"sim_MobilePhone": marr[0] == "" ? "" : marr[0]]
                                                arr.add(grade)
                                                var grade1: [AnyHashable : Any] = [:]
                                                grade1 = ["data": arr]
                                                var jsonData: Data? = nil
                                                do {
                                                    jsonData = try JSONSerialization.data(withJSONObject: grade1, options: .prettyPrinted)
                                                } catch {
                                                }
                                                var jsonString: String? = nil
                                                if let jsonData = jsonData {
                                                    jsonString = String(data: jsonData, encoding: .utf8)
                                                    //print("jsonString",jsonString!)
                                                }
                                                postData = (jsonString?.data(using: .ascii, allowLossyConversion: true))!
                                                if postData != nil {
                                                    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
                                                    let documentsDirectory = paths[0]
                                                    let filePath = "\(documentsDirectory)/\("filename.doc")"
                                                    do {
                                                        try postData.write(to: URL(fileURLWithPath: filePath), options: .atomic)
                                                    } catch {
                                                        print(error)
                                                    }
                                                }
                                            }
                                        }
                                    }}
                                if self.isFlag == true{
                                    self.callGetContact(jsonData: postData)
                                }
                                for i in self.startIndex..<self.endIndex {
                                    if sortedNames.count == i{
                                        self.isFlag = false
                                        isImportContactAll = true
                                        if self.phoneContactArr.count < 50{
                                            self.callSearchContact(startRow: 0, endRow: 50)
                                        }
                                        break
                                    }
                                }
                            })
                        }
                    }
                }
                
                //anushka
                let percentage = (self.endIndex * 100) / self.contacts.count
                print("anushka percentage: \(percentage) \(self.endIndex) \(self.contacts.count)")//self.phoneContactArr.count
                
                DispatchQueue.main.async {
                    if percentage < 100{
                        //                        self.lblTotalCount.text = "\(per) % Complete"
                        self.lblTotalCount.text = "\(percentage) % Complete"
                    }else{
                        self.lblTotalCount.text = "100 % Complete"
                    }
                    self.progressBar.progressValue = CGFloat(percentage < 100 ? percentage : 100)
                    
                    if percentage >= 100{
                        self.lblTotalCount.isHidden = true
                        self.progressBar.isHidden = true
                        if self.phoneContactArr.count >= 50{
                            self.btnNext.isHidden = false
                        }
                    }
                }
            }else{
                DispatchQueue.main.async {
                    print("faild-------ImportContact")
                    //self.isHidden = true
                }
            }
        })
    }
    
    func callSearchContact(startRow:Int,endRow:Int){
        isICFirstTime = false
        actIndicator.stopAnimating()
//        DispatchQueue.main.async {
//            self.activityStopAnimating()
//        }
        self.msgView.isHidden = true
        let authToken = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
        postWithoutImage(url_string: "search_match_contact", parameters: "id=\(u_id)&cid=\(cp_id)&startrow=\(startRow)&endrow=\(endRow)&authToken=\(authToken)",CompletionHandler: { json, error in
            if let json = json {
               // print("json~~~~~~~",json)
                let statusmassege = json.value(forKey: "status") as! Bool
                if (statusmassege == true){
                    USER_DEFAULTS.set(startRow, forKey: "importContactCount")
                    DispatchQueue.main.async {
                        self.msgView.isHidden = true
                        self.lblTotalCount.isHidden = true
                        self.progressBar.isHidden = true
                        //self.btnNext.isHidden = false
                        if startRow >= 50{
                            self.btnPrev.isHidden = false
                        }else{
                            self.btnPrev.isHidden = true
                        }
                    }
                    let authToken = json.value(forKey: "authToken") as! String
                    USER_DEFAULTS.set(authToken, forKey: "authToken")
                    self.phoneContactArr.removeAll()
                    self.AppContactArr.removeAll()
                    let data = json.value(forKey: "data") as! NSArray
                    self.jsonOBJ = json
                    if data.count < 50{
                        DispatchQueue.main.async {
                         self.btnNext.isHidden = true
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.btnNext.isHidden = false
                        }
                    }
                    for i in 0..<data.count{
                        let dic = data[i] as! NSDictionary
                        let simDatail = dic.value(forKey: "SimDetails") as! NSDictionary
                        let s_detail = SimDetail()
                        s_detail.strName = simDatail.value(forKey: "Name") as? String ?? ""
                        s_detail.strEmail = simDatail.value(forKey: "sim_Email") as? String ?? ""
                        s_detail.strHomePhone = simDatail.value(forKey: "sim_HomePhone") as? String ?? ""
                        s_detail.strMobilePhone = simDatail.value(forKey: "sim_MobilePhone") as? String ?? ""
                        s_detail.appContactArr = (dic.value(forKey: "DatabaseContact") as! NSArray) as! [Any]
                        self.phoneContactArr.append(s_detail)
//                        self.fPointer = UnsafeMutablePointer.allocate(capacity: self.phoneContactArr.count)
//                        for i in 0..<self.phoneContactArr.count {
//                            self.fPointer[i] = 0
//                        }
                        self.fPointer1 = UnsafeMutablePointer.allocate(capacity: self.phoneContactArr.count)
                        for i in 0..<self.phoneContactArr.count {
                            self.fPointer1[i] = 0
                        }
                    }
                    DispatchQueue.main.async {
                        if self.selectedIndexPath != nil{
                            self.phoneContactArr[self.selectedIndexPath].isOpen = true
                        }
                        self.TableView.reloadData()
                        self.TableView.scrollToTop()
                    }
                    
                    
                    
                }else{
                    var is_changed: String? = nil
                    if let object = json["is_changed"] {
                        is_changed = "\(object)"
                    }
                    if is_changed?.isEqual("1") ?? false {
                        logoutAlertMassege()
                    } else {
                        DispatchQueue.main.async {
                            self.btnNext.isHidden = true
                        }
                    }
                }
            }else{
                DispatchQueue.main.async {
                    //self.isHidden = true
                    print("----false----")
                    self.btnNext.isHidden = true
                }
            }
        })
    }
    
    @objc func onClickNo(_ sender: UIButton) {
        print("-------Hello-------")
    }
    
}

