//
//  StreetName.swift
//  ExelToApp
//
//  Created by baps on 28/01/20.
//  Copyright © 2020 Osiya. All rights reserved.
//

import UIKit


class streetName {
    var strDir = ""
    var strName = ""
    var strCount = ""
    var strCombine = ""
}

class StreetName: UIView,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UISearchBarDelegate {

    var mainView:UIView!
    var lblHeading:UILabel!
    var popUpView:UIView!
    var txtHouseNo:UITextField!
    var txtStreetName:UITextField!
    var txtZipCode:UITextField!
    var txtRating:UITextField!
    var btnCancel:UIButton!
    var btnOk:UIButton!
    var topView:UIView!
    var arrowImg:UIImageView!
    
    var strNameArr = NSMutableArray()
    var strDirArr = NSMutableArray()
    var strCountArr = NSMutableArray()
    
    var strNameView:UIView!
    var TableView: UITableView = UITableView()
    var searchBar:UISearchBar!
    var btnAtoZ:UIButton!
    var btnSubmit:UIButton!
    var btnCancel1:UIButton!
    var cellReuseIdentifier = "Cell"
    
    var arrayStreetNameBySection = NSMutableArray()
    var arrayStreetNameByRow = NSMutableArray()
    var arrayStreetCountBySection = NSMutableArray()
    var arrayStreetDirBySection = NSMutableArray()
    var arrayAotz = NSMutableArray()
    var concatArr = [String]()
    var searchStrArr = [String]()
    
    
    var strNmaeArray = [streetName]()
    
    var arrayZipcode = NSMutableArray()
    var arrayRating = NSMutableArray()
    
    
    var u_id = USER_DEFAULTS.string(forKey: "u_id") ?? ""
    var cp_id = USER_DEFAULTS.string(forKey: "cp_id") ?? ""
    
    var isSteetName = 1
    var strDirection = ""
    var strName = ""
    var isHeading = 0
    var HeadingName = ""
    var isResidential = 0
    
    var isSelectTable = 0
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
        
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpView()
    }
    
    func setUpView() {
        mainView = UIView()
        mainView.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT)
        mainView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.addSubview(mainView)
        
        popUpView = UIView()
        popUpView.frame = CGRect(x: 15, y: 80, width: mainView.frame.width-30, height: 230)
        popUpView.backgroundColor = UIColor.white
        popUpView.layer.cornerRadius = 10.0
        popUpView.clipsToBounds = true
        mainView.addSubview(popUpView)
        
        topView = UIView()
        topView.frame = CGRect(x: 0, y: 0, width: popUpView.frame.width, height: 50)
        topView.backgroundColor = APPBUTTONCOLOR
        popUpView.addSubview(topView)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 0, Y: 10, Width: popUpView.frame.width, Height: 30, TextColor: .white, BackColor: .clear, Text: "Search By Street Name", TextAlignment: .center, Font: .boldSystemFont(ofSize: 18))
        topView.addSubview(lblHeading)
        
        txtHouseNo = UITextField()
        txtHouseNo.borderStyle = .roundedRect
        txtHouseNo.setTextField(X: 10, Y: topView.frame.maxY+15, Width: popUpView.frame.width-20, Height: 30, TextColor: .black, BackColor: .clear)
        txtHouseNo.placeholder = "House Number"
        popUpView.addSubview(txtHouseNo)
        
        txtStreetName = UITextField()
        txtStreetName.setTextField(X: 10, Y: txtHouseNo.frame.maxY+8, Width: popUpView.frame.width-20, Height: 30, TextColor: .black, BackColor: .clear)
        txtStreetName.borderStyle = .roundedRect
        txtStreetName.placeholder = "Street Name"
        txtStreetName.delegate = self
        popUpView.addSubview(txtStreetName)
        
        arrowImg = UIImageView()
        arrowImg.setImageView(X: txtStreetName.frame.maxX-28, Y: txtHouseNo.frame.maxY+8, Width: 24, Height: 24, img: UIImage(named: "downArrow.png")!)
        popUpView.addSubview(arrowImg)
        
        txtZipCode = UITextField()
        txtZipCode.setTextField(X: 10, Y: txtStreetName.frame.maxY+8, Width: popUpView.frame.width/2-15, Height: 30, TextColor: .black, BackColor: .clear)
        txtZipCode.borderStyle = .roundedRect
        txtZipCode.placeholder = "Zip Code"
        txtZipCode.delegate = self
        popUpView.addSubview(txtZipCode)
        
        arrowImg = UIImageView()
        arrowImg.setImageView(X: txtZipCode.frame.maxX-28, Y: txtStreetName.frame.maxY+8, Width: 24, Height: 24, img: UIImage(named: "downArrow.png")!)
        popUpView.addSubview(arrowImg)
        
        txtRating = UITextField()
        txtRating.setTextField(X: txtZipCode.frame.maxX+9, Y: txtStreetName.frame.maxY+8, Width: popUpView.frame.width/2-15, Height: 30, TextColor: .black, BackColor: .clear)
        txtRating.borderStyle = .roundedRect
        txtRating.placeholder = "Rating"
        txtRating.delegate = self
        popUpView.addSubview(txtRating)
        
        arrowImg = UIImageView()
        arrowImg.setImageView(X: txtRating.frame.maxX-28, Y: txtStreetName.frame.maxY+8, Width: 24, Height: 24, img: UIImage(named: "downArrow.png")!)
        popUpView.addSubview(arrowImg)
        
        btnCancel = UIButton(type: .system)
        btnCancel.setButton(X: popUpView.frame.width-30, Y: 5, Width: 24, Height: 24, TextColor: .white, BackColor: .clear, Title: "X", Align: .center, Font: .boldSystemFont(ofSize: 22))
        btnCancel.tag = 2
        btnCancel.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        topView.addSubview(btnCancel)
        
        btnOk = UIButton(type: .system)
        btnOk.setButton(X: popUpView.frame.width/2-(popUpView.frame.width/3)/2, Y: txtRating.frame.maxY+15, Width: popUpView.frame.width/3, Height: 30, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Search", Align: .center, Font: .boldSystemFont(ofSize: 18))
        btnOk.tag = 1
        btnOk.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        popUpView.addSubview(btnOk)
        
        
        strNameView = UIView()
        strNameView.frame = CGRect(x: 10, y: 10, width: mainView.frame.width-20, height: SHEIGHT-84)
        strNameView.backgroundColor = UIColor.white
        strNameView.layer.cornerRadius = 10.0
        strNameView.clipsToBounds = true
        mainView.addSubview(strNameView)
        
        topView = UIView()
        topView.frame = CGRect(x: 0, y: 0, width: strNameView.frame.width, height: 40)
        topView.backgroundColor = APPBUTTONCOLOR
        strNameView.addSubview(topView)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 0, Y: 10, Width: popUpView.frame.width, Height: 30, TextColor: .white, BackColor: .clear, Text: "Street Name", TextAlignment: .center, Font: .boldSystemFont(ofSize: 18))
        topView.addSubview(lblHeading)
        
        btnCancel = UIButton(type: .system)
        btnCancel.setButton(X: popUpView.frame.width-30, Y: 8, Width: 24, Height: 24, TextColor: .white, BackColor: .clear, Title: "X", Align: .center, Font: .boldSystemFont(ofSize: 22))
        btnCancel.tag = 3
        btnCancel.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        topView.addSubview(btnCancel)
        
        
        searchBar = UISearchBar()
        searchBar.frame = CGRect(x: 0, y: topView.frame.maxY, width: strNameView.frame.width, height: 50)
        searchBar.placeholder = "Search Street Name"
        searchBar.backgroundImage = UIImage(named: "trans.png")
        searchBar.backgroundColor = APPBUTTONCOLOR
        searchBar.delegate = self
        strNameView.addSubview(searchBar)
        
        TableView.frame = CGRect(x: 0, y: searchBar.frame.maxY, width: strNameView.frame.width, height: strNameView.frame.height-100)
        TableView.backgroundColor = UIColor.white
        TableView.delegate = self
        TableView.dataSource = self
        TableView.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        strNameView.addSubview(TableView)
        TableView.tableFooterView = UIView()
        TableView.layer.cornerRadius = 10.0
        strNameView.isHidden = true
        
        btnAtoZ = UIButton()
        btnAtoZ.setButton(X: strNameView.frame.width-50, Y: searchBar.frame.maxY+2, Width: 40, Height: 30, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "#", Align: .center, Font: .systemFont(ofSize: 17.0))
        btnAtoZ.tag = 4
        btnAtoZ.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        strNameView.addSubview(btnAtoZ)
        
        
        btnSubmit = UIButton()
        btnSubmit.setButton(X: strNameView.frame.width/4, Y: TableView.frame.maxY+5, Width: strNameView.frame.width/4, Height: 30, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Submit", Align: .center, Font: .systemFont(ofSize: 17.0))
        btnSubmit.tag = 5
        btnSubmit.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        strNameView.addSubview(btnSubmit)
        
        btnCancel1 = UIButton()
        btnCancel1.setButton(X: btnSubmit.frame.maxX+5, Y: TableView.frame.maxY+5, Width: strNameView.frame.width/4, Height: 30, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Cancel", Align: .center, Font: .systemFont(ofSize: 17.0))
        btnCancel1.tag = 6
        btnCancel1.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        strNameView.addSubview(btnCancel1)
        
        let keyboardDoneButtonView = UIToolbar()
        keyboardDoneButtonView.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClicked(_:)))
        keyboardDoneButtonView.items = [flexBarButton, doneButton]
        searchBar.inputAccessoryView = keyboardDoneButtonView
        
        
        callStreetName()
        NotificationCenter.default.addObserver(self, selector: #selector(self.onClickRearView), name: NSNotification.Name(rawValue: "onClickRearView"), object: nil)
        
    }
    @objc func onClickRearView(notif: NSNotification) {
        self.isHidden = true
    }
    
    @IBAction func doneClicked(_ sender: Any) {
        print("Done Clicked.")
        self.endEditing(true)
    }
    
    @objc func onClickButton(_ sender: UIButton) {
        print("-----StreetName-----")
        if sender.tag == 2{// onClick Cross
            self.isHidden = true
            HomeViewController.VC.navigationItem.rightBarButtonItem = HomeViewController.VC.btnEditPerson
        }else if sender.tag == 3{// onClick Cancel
            self.strNameView.isHidden = true
        }else if sender.tag == 4{// onClick A-Z
            if btnAtoZ.titleLabel?.text == "ABC"{
                searchBar.text = ""
                self.streetNameDropDown()
                btnAtoZ.setTitle("#", for: .normal)
            }else{
                searchBar.text = ""
                self.streetCountDropDown()
                btnAtoZ.setTitle("ABC", for: .normal)
            }
            
            
        }else if sender.tag == 5{// onClick Submit
            var j = 0
            var select = ""
            for i in 0..<(arrayRating.count) {
                let obj = arrayRating[i] as? multiSelcted
                    if ((obj?.isSelected)!){
                        if j == 0 {
                            if let txt = obj?.txt {
                                select = "\(txt)"
                            }
                        } else {
                            if let txt = obj?.txt {
                                select = "\(select),\(txt)"
                            }
                        }
                        j += 1
                    }
                }
            
            txtRating.text = select.contains("All") ? "All" : select
            strNameView.isHidden = true
            
        }else if sender.tag == 6{// onClick popup Cancel
            if isSelectTable == 1{
            }else if isSelectTable == 2{
                txtZipCode.text = ""
            }else{
                txtRating.text = ""
            }
            self.strNameView.isHidden = true
            
        }else{// onClick search
            if((txtStreetName.text?.isEmpty)!){
                HomeViewController.VC.alert(message: "Please Enter Street Name", title: "Message")
            }else{
                type = 2
                isSaerchStrName = 0
                paramStr = ""
                nextPrevStr = ""
                nextPrevStr = "next=&previous="
                paramStr = "streetname=\(strName)&streetdirection=\(strDirection)&housenumber=\(txtHouseNo.text ?? "")&zipcode=\(txtZipCode.text ?? "")&Rating=\(txtRating.text ?? "")"
                HomeViewController.VC.callGetExcel(u_id: u_id, cp_id: cp_id, startRow: 0)
                self.isHidden = true
                HomeViewController.VC.navigationItem.rightBarButtonItem = HomeViewController.VC.btnEditPerson
            }
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if txtStreetName == textField{
            isSelectTable = 1
            btnAtoZ.isHidden = false
            btnAtoZ.setTitle("#", for: .normal)
            strNameView.frame = CGRect(x: 10, y: 10, width: mainView.frame.width-20, height: SHEIGHT-84)
            TableView.frame = CGRect(x: 0, y: searchBar.frame.maxY, width: strNameView.frame.width, height: strNameView.frame.height-100)
            btnSubmit.isHidden = true
            btnCancel1.isHidden = true
            lblHeading.text = "Street Name"
            searchBar.isHidden = false
            
            streetNameDropDown()
            self.strNameView.isHidden = false
            TableView.reloadData()
            return false
        }
        if txtZipCode == textField{
            isSelectTable = 2
            btnAtoZ.isHidden = true
            strNameView.frame = CGRect(x: 10, y: SHEIGHT/2-(SHEIGHT/2)/2, width: mainView.frame.width-20, height: SHEIGHT/2)
            TableView.frame = CGRect(x: 0, y: topView.frame.maxY, width: strNameView.frame.width, height: strNameView.frame.height-100)
            btnSubmit.isHidden = true
            btnCancel1.isHidden = false
            btnCancel1.setButton(X: strNameView.frame.width/2-(strNameView.frame.width/4)/2, Y: TableView.frame.maxY+5, Width: strNameView.frame.width/4, Height: 30, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Cancel", Align: .center, Font: .systemFont(ofSize: 17.0))
            lblHeading.text = "ZipCode"
            searchBar.isHidden = true
            print("arrayZipcode.count-----",arrayZipcode.count)
            if arrayZipcode.count > 0{
                self.strNameView.isHidden = false
                TableView.reloadData()
            }
            
            
            return false
        }
        if txtRating == textField{
            isSelectTable = 3
            btnAtoZ.isHidden = true
            strNameView.frame = CGRect(x: 10, y: SHEIGHT/2-(SHEIGHT/2)/2, width: mainView.frame.width-20, height: SHEIGHT/2)
            TableView.frame = CGRect(x: 0, y: topView.frame.maxY, width: strNameView.frame.width, height: strNameView.frame.height-100)
            btnSubmit.isHidden = false
            btnCancel1.isHidden = false
            btnSubmit.setButton(X: strNameView.frame.width/4, Y: TableView.frame.maxY+5, Width: strNameView.frame.width/4, Height: 30, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Submit", Align: .center, Font: .systemFont(ofSize: 17.0))
            btnCancel1.setButton(X: btnSubmit.frame.maxX+5, Y: TableView.frame.maxY+5, Width: strNameView.frame.width/4, Height: 30, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Cancel", Align: .center, Font: .systemFont(ofSize: 17.0))
            
            lblHeading.text = "Rating"
            searchBar.isHidden = true
            
            if arrayRating.count > 0{
                self.strNameView.isHidden = false
                TableView.reloadData()
            }
            return false
        }
        TableView.reloadData()
        return true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        var arr = NSMutableArray()
        var arrDir = NSMutableArray()
        var arrCount = NSMutableArray()
        var arrstName = NSMutableArray()
        arrayStreetNameBySection.removeAllObjects()
        arrayStreetDirBySection.removeAllObjects()
        arrayStreetCountBySection.removeAllObjects()
        arrayStreetNameByRow.removeAllObjects()
        arrayAotz.removeAllObjects()
        
        var streetNameArr : [streetName] = []
        for i in 0..<strNameArr.count {
            let str = streetName()
            str.strName = strNameArr[i] as! String
            str.strCount = strCountArr[i] as! String
            str.strDir = strDirArr[i] as! String
            str.strCombine = concatArr[i]
            streetNameArr.append(str)
        }
        var sortedArray = btnAtoZ.titleLabel?.text == "#" ? streetNameArr.sorted{ ($0.strCombine).localizedCaseInsensitiveCompare($1.strCombine) == ComparisonResult.orderedAscending} : streetNameArr.sorted{ ($0.strCount).localizedCaseInsensitiveCompare($1.strCount) == ComparisonResult.orderedAscending}
        sortedArray = sortedArray.filter { ($0.strCombine).localizedCaseInsensitiveContains(searchText) }
        
        print("strName~~~~\(searchText)~")
        
//        for i in 0..<sortedArray.count{
//            let strName = sortedArray[i].strName
//            print("strName~~~~`",strName)
//        }
        
        var str = ""
        if (sortedArray.count > 0) {
            str = String(sortedArray[0].strCombine.prefix(1))
            arrayAotz.add(str)
            arrayStreetNameBySection.add(arr)
            arrayStreetDirBySection.add(arrDir)
            arrayStreetCountBySection.add(arrCount)
            arrayStreetNameByRow.add(arrstName)
        }
        for i in 0..<sortedArray.count{
            let str1 = String(sortedArray[i].strCombine.prefix(1))
            let strName = sortedArray[i].strCombine
            let strDir = sortedArray[i].strDir
            let strCount = sortedArray[i].strCount
            let stName = sortedArray[i].strName
            if str != str1{
                arr = NSMutableArray()
                arrayStreetNameBySection.add(arr)
                arrDir = NSMutableArray()
                arrayStreetDirBySection.add(arrDir)
                arrCount = NSMutableArray()
                arrayStreetCountBySection.add(arrCount)
                arrstName = NSMutableArray()
                arrayStreetNameByRow.add(arrstName)
                str = str1
                arrayAotz.add(str)
            }
            arr.add(strName)
            arrDir.add(strDir)
            arrCount.add(strCount)
            arrstName.add(stName)
        }
        TableView.reloadData()
        
        if searchText == ""{
            if btnAtoZ.titleLabel?.text == "#"{
                searchBar.text = ""
                self.streetNameDropDown()
            }else{
                searchBar.text = ""
                self.streetCountDropDown()
            }
        }
    }
    
    
    
    
    func streetNameDropDown(){
        var arr = NSMutableArray()
        var arrDir = NSMutableArray()
        var arrCount = NSMutableArray()
        var arrstName = NSMutableArray()
        arrayStreetNameBySection.removeAllObjects()
        arrayStreetDirBySection.removeAllObjects()
        arrayStreetCountBySection.removeAllObjects()
        arrayStreetNameByRow.removeAllObjects()
        arrayAotz.removeAllObjects()
        var streetNameArr : [streetName] = []
        for i in 0..<strNameArr.count {
            let str = streetName()
            str.strName = strNameArr[i] as! String
            str.strCount = strCountArr[i] as! String
            str.strDir = strDirArr[i] as! String
            str.strCombine = concatArr[i]
            streetNameArr.append(str)
        }
        let sortedArray = streetNameArr.sorted{ ($0.strCombine).localizedCaseInsensitiveCompare($1.strCombine) == ComparisonResult.orderedAscending}
        var str = ""
        if (sortedArray.count > 0) {
            str = String(sortedArray[0].strCombine.prefix(1)).uppercased()
            arrayAotz.add(str)
            arrayStreetNameBySection.add(arr)
            arrayStreetDirBySection.add(arrDir)
            arrayStreetCountBySection.add(arrCount)
            arrayStreetNameByRow.add(arrstName)
        }
        for i in 0..<sortedArray.count{
            let str1 = String(sortedArray[i].strCombine.prefix(1)).uppercased()
            let strName = sortedArray[i].strCombine
            let strDir = sortedArray[i].strDir
            let strCount = sortedArray[i].strCount
            let stName = sortedArray[i].strName
            //print("str----\(str),str1----\(str1),\(str.caseInsensitiveCompare(str1) == .orderedSame)")
            
            
            if str != str1{
                arr = NSMutableArray()
                arrayStreetNameBySection.add(arr)
                arrDir = NSMutableArray()
                arrayStreetDirBySection.add(arrDir)
                arrCount = NSMutableArray()
                arrayStreetCountBySection.add(arrCount)
                arrstName = NSMutableArray()
                arrayStreetNameByRow.add(arrstName)
                str = str1
                
                arrayAotz.add(str)
            }
            arr.add(strName)
            arrDir.add(strDir)
            arrCount.add(strCount)
            arrstName.add(stName)
        }
        TableView.reloadData()
    }
    
    func streetCountDropDown(){
        var arr = NSMutableArray()
        var arrDir = NSMutableArray()
        var arrCount = NSMutableArray()
        var arrstName = NSMutableArray()
        arrayStreetNameBySection.removeAllObjects()
        arrayStreetDirBySection.removeAllObjects()
        arrayStreetCountBySection.removeAllObjects()
        arrayStreetNameByRow.removeAllObjects()
        arrayAotz.removeAllObjects()
        var streetNameArr : [streetName] = []
        for i in 0..<strNameArr.count {
            let str = streetName()
            str.strName = strNameArr[i] as! String
            str.strCount = strCountArr[i] as! String
            str.strDir = strDirArr[i] as! String
            str.strCombine = concatArr[i]
            streetNameArr.append(str)
        }
        let sortedArray = streetNameArr.sorted{ ($0.strCount).localizedStandardCompare($1.strCount) == .orderedDescending}
        
        for i in 0..<sortedArray.count{
            print("sortedArray~~~~~~~",sortedArray[i].strCount)
        }
        var str = ""
        if (sortedArray.count > 0) {
            str = String(sortedArray[0].strCombine.prefix(1)).uppercased()
            arrayAotz.add(str)
            arrayStreetNameBySection.add(arr)
            arrayStreetDirBySection.add(arrDir)
            arrayStreetCountBySection.add(arrCount)
            arrayStreetNameByRow.add(arrstName)
        }
        for i in 0..<sortedArray.count{
            let str1 = String(sortedArray[i].strCombine.prefix(1)).uppercased()
            let strName = sortedArray[i].strCombine
            let strDir = sortedArray[i].strDir
            let strCount = sortedArray[i].strCount
            let stName = sortedArray[i].strName
            if str != str1{
                arr = NSMutableArray()
                arrayStreetNameBySection.add(arr)
                arrDir = NSMutableArray()
                arrayStreetDirBySection.add(arrDir)
                arrCount = NSMutableArray()
                arrayStreetCountBySection.add(arrCount)
                arrstName = NSMutableArray()
                arrayStreetNameByRow.add(arrstName)
                str = str1
                arrayAotz.add(str)
            }
            arr.add(strName)
            arrDir.add(strDir)
            arrCount.add(strCount)
            arrstName.add(stName)
        }
        TableView.reloadData()
    }
    
    func newCross(_ str: String?, type isSel: Bool) -> multiSelcted? {
        let comment = multiSelcted()
        comment.txt = str ?? ""
        comment.isSelected = isSel
        return comment
    }
    

    
    func numberOfSections(in tableView: UITableView) -> Int {
        if isSelectTable == 1{
            return arrayStreetNameBySection.count
        }else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSelectTable == 1{
            let arr = arrayStreetNameBySection[section] as! NSArray
            return arr.count
        }else if isSelectTable == 2{
            return arrayZipcode.count
        }else{
            return arrayRating.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as UITableViewCell!
        if isSelectTable == 1{
            let arr = arrayStreetNameBySection[indexPath.section] as! NSArray
            let arrCount = arrayStreetCountBySection[indexPath.section] as! NSArray
            cell.textLabel?.text = "\(arr[indexPath.row]) (\(arrCount[indexPath.row]))"
            cell.imageView?.image = UIImage(named:"0.png")
        }else if isSelectTable == 2{
            cell.textLabel?.text = "\(arrayZipcode[indexPath.row])"
            cell.imageView?.image = UIImage(named:"0.png")
        }else if isSelectTable == 3{
            let obj = arrayRating[indexPath.row] as? multiSelcted
            cell.textLabel?.text = obj?.txt
            cell.imageView?.image = UIImage(named: obj?.isSelected == false ? "diselect128.png" : "select128.png")
            //cell.textLabel?.text = "\(arrayRating[indexPath.row])"
        }
        
        cell.textLabel?.textColor = APPBUTTONCOLOR
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isSelectTable == 1{
            let arr = arrayStreetNameBySection[indexPath.section] as! NSArray
            txtStreetName.text = "\(arr[indexPath.row])"
            let arrDir = arrayStreetDirBySection[indexPath.section] as! NSArray
            strDirection = "\(arrDir[indexPath.row])"
            let arrName = arrayStreetNameByRow[indexPath.section] as! NSArray
            txtZipCode.text = ""
            txtRating.text = ""
            strName = "\(arrName[indexPath.row])"
            print("strName----",strName)
            isSteetName = 0
            isHeading = 1
            isResidential = 1
            self.HeadingName = "Residence_ZipCode5"
            strNameView.isHidden = true
            callStreetName()
            
        }else if isSelectTable == 2{
            txtZipCode.text = "\(arrayZipcode[indexPath.row])"
            strNameView.isHidden = true
        }else if isSelectTable == 3{
            var com = arrayRating[indexPath.row] as! multiSelcted
            if (com.txt == "All") {
                let comObj = arrayRating[0] as! multiSelcted
                if indexPath.row == 0 && comObj.isSelected == false {
                    for i in 0..<arrayRating.count {
                        let comObj = arrayRating[i] as! multiSelcted
                        comObj.isSelected = i == 0 ? !(comObj.isSelected) : true
                    }
                } else {
                    for i in 0..<arrayRating.count {
                        let comObj = arrayRating[i] as! multiSelcted
                        comObj.isSelected = i == 0 ? !(comObj.isSelected) : false
                    }
                }
            } else {
                com.isSelected = !com.isSelected
                com = arrayRating[0] as! multiSelcted
                if (com.txt == "All") {
                    com.isSelected = false
                }
            }
            TableView.reloadData()
        }
        
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        if isSelectTable == 1{
            return arrayAotz as? [String]
        }else{
            return [""]
        }
    }
    
    func callStreetName(){
        let authToken = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
        postWithoutImage(url_string: "crosstabsearch_filter", parameters: "id=\(u_id)&cid=\(cp_id)&isCanvaser=0&Residence_StreetName=\(strName)&Residence_StreetDirection=\(strDirection)&isStreetName=\(isSteetName)&isHeading=\(isHeading)&heading_name=\(HeadingName)&isFirstTime=0&authToken=\(authToken)",CompletionHandler: { json, error in
            if let json = json {
                let statusmassege = json.value(forKey: "status") as! Bool
                if (statusmassege == true){
                    let authToken = json.value(forKey: "authToken") as! String
                    USER_DEFAULTS.set(authToken, forKey: "authToken")
                    let strDirNameArr = json.value(forKey: "street_direction_name") as! NSArray
                    if self.isSteetName == 1{
                        self.strNameArr.removeAllObjects()
                        self.strDirArr.removeAllObjects()
                        self.strCountArr.removeAllObjects()
                        self.concatArr.removeAll()
                        for i in 0..<strDirNameArr.count{
                            let dic = strDirNameArr[i] as! NSDictionary
                            self.strNameArr.add(dic.value(forKey: "StreetName") as? String ?? "")
                            let trimmed = dic.value(forKey: "StreetDirection") as? String ?? "".trimmingCharacters(in: CharacterSet.whitespaces)
                            self.strDirArr.add(trimmed)
                            self.strCountArr.add(dic.value(forKey: "StreetCount") as? String ?? "")
                            var trimmedString = dic.value(forKey: "concatString") as? String ?? "".trimmingCharacters(in: .whitespaces)
                            trimmedString = trimmedString.replacingOccurrences(of: "  ", with: " ")
                            trimmedString = trimmedString.replacingOccurrences(of: "   ", with: " ")
                            self.concatArr.append(trimmedString)
                        }
                        self.TableView.reloadData()
                    }else if self.isHeading == 1{
                        self.isSteetName = 0
                        self.isHeading = 1
                        self.isResidential = 0
                        self.HeadingName = "Rating"
                        let dispatchAfter = DispatchTimeInterval.seconds(Int(0.5))
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + dispatchAfter, execute: {
                            self.callStreetName()
                            self.isHeading = 0
                        })
                        print("Residence_ZipCode5~~~~~~",json)
                        self.arrayZipcode.removeAllObjects()
                        let data = json.value(forKey: "data") as! NSArray
                        for i in 0..<data.count{
                            let dic = data[i] as! NSDictionary
                            let zip = dic.value(forKey: "Residence_ZipCode5") as? String ?? ""
                            if zip != ""{
                                self.arrayZipcode.add(zip)
                            }
                        }
                        self.TableView.reloadData()
                    }else if self.isHeading == 0{
                        self.arrayRating.removeAllObjects()
                        let data = json.value(forKey: "data") as! NSArray
                        self.arrayRating.add(self.newCross("All", type: false)!)
                        for i in 0..<data.count{
                            let dic = data[i] as! NSDictionary
                            let Rating = dic.value(forKey: "Rating") as? String ?? ""
                            if Rating != ""{
                                self.arrayRating.add(self.newCross(Rating, type: false)!)
                            }
                        }
                        self.TableView.reloadData()
                    }
                }else{
                    var is_changed: String? = nil
                    if let object = json["is_changed"] {
                        is_changed = "\(object)"
                    }
                    if is_changed?.isEqual("1") ?? false {
                        logoutAlertMassege()
                    } else {
                        HomeViewController.VC.alert(message: "No Record Found!", title: "Alert!")
                    }
                }
            }
        })
    }
}
