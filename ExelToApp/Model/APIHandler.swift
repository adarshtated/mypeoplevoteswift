//
//  APIHandler.swift
//  ExelToApp
//
//  Created by baps on 09/01/20.
//  Copyright © 2020 Osiya. All rights reserved.
//

import UIKit
import MKProgress


let APPBUTTONCOLOR = UIColor(red: 22/255, green: 128/255, blue: 196/255, alpha: 1.0)
let APPBUTTONCOLOR1 = UIColor(red: 34/255, green: 123/255, blue: 187/255, alpha: 1.0)
let APPPBACKCOLOR = UIColor(red: 65/255.0, green: 157/255.0, blue: 212/255.0, alpha: 1.0)
let APPPPOPUPCOLOR = UIColor(red: 240.0/255.0, green: 245.0/255.0, blue: 247.0/255.0, alpha: 1.0)
let baseUrl = "https://lapublicopinion.com/Excel_api_v9/"//"domain = @"https://lapublicopinion.com/Excel_api_v8/";"

let SWIDTH = UIScreen.main.bounds.width
let SHEIGHT = UIScreen.main.bounds.height
let STATUSBARHEIGHT = UIApplication.shared.statusBarFrame.size.height

let rootView = UIApplication.shared.keyWindow

let USER_DEFAULTS = UserDefaults.standard

var type = 0
var mapType = 0
var isMapHome = 0
var paramStr = ""
var nextPrevStr = ""
var noRow = 100
var start = 0
var jsonObj = NSDictionary()
var mapjsonObj = NSDictionary()
var isSelectPopup = 0
var selectPopupText = ""
var isSelectPollParishWard = 0
var isSelectCustomVar = 0

var selectCustVarHomeTxt1 = ""
var selectCustVarHomeTxt2 = ""
var selectCustVarHomeTxt3 = ""

var isSelectedVotedPoll = ""

let parishArray = NSMutableArray()
let wardArray = NSMutableArray()
let precinctArray = NSMutableArray()

var isAppMyPeople = false

var isResidential = 0
var isSaerchStrName = 0
var isICFirstTime = true
var isICShowAI = true
var isImportContactAll = false
var task = URLSessionDataTask()

public func
    postWithoutImage(url_string: String, parameters : String, CompletionHandler: @escaping (NSDictionary?, String?) -> Void){
    rootView?.activityStartAnimating()
//    MKProgress.config.hudType = .activityIndicator
//    MKProgress.show()
    print("parameters -",parameters)
    let url = URL(string: baseUrl+url_string)!
    print("url : ",url)
    var request = URLRequest(url: url)
    request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
    request.httpMethod = "POST"
    
    if parameters != "" {
        request.httpBody = parameters.data(using: .utf8)
        let hValue = USER_DEFAULTS.string(forKey: "header_value")
        request.addValue(hValue!, forHTTPHeaderField: "MypeopleHeader")
    }
    let configuration = URLSessionConfiguration.default
    configuration.timeoutIntervalForRequest = TimeInterval(120)
    configuration.timeoutIntervalForResource = TimeInterval(120)
    let session = URLSession(configuration: configuration)
    
    let task = session.dataTask(with: request) { data, response, error in
        guard let data = data, error == nil else {
            DispatchQueue.main.async {
                if let err = error as NSError? {
                    if err.code == -1009{
                        
                    }
                }
                
            }
            DispatchQueue.main.async {
                rootView?.activityStopAnimating()
//                MKProgress.hide()
                rootView?.rootViewController?.alert(message: "Network error. Please try again.", title: "Alert!")
            }
            print("error~~~~~",error,response)
            return
        }
        let string = String(data: data, encoding: String.Encoding.utf8)
        //print("string~~~~~",string)
        if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
            print("statusCode should be 200, but is \(httpStatus.statusCode)")
            print("response = \(response!)")
            DispatchQueue.main.async {
                rootView?.rootViewController?.alert(message: "Network error. Please try again.", title: "Alert!")
                rootView?.activityStopAnimating()
//                MKProgress.hide()
            }
            return
        }
        
        DispatchQueue.main.async {
            if let data = string?.replacingOccurrences(of: "\n", with: "").data(using: String.Encoding.utf8) {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! NSDictionary
                    // print("json of ---\(url_string) :",json)
                    let statusmessage1 = String(describing: json["status"])
                    let msg = json.value(forKey: "msg") as? String ?? ""
                    if (statusmessage1.contains("1")){
                        print("success")
                        CompletionHandler(json, nil)
                    }else{
                        print("false-----",msg)
                        CompletionHandler(nil, msg)
                        var is_changed: String? = nil
                        if let object = json["is_changed"] {
                            is_changed = "\(object)"
                        }
                        if is_changed?.isEqual("1") ?? false {
                            logoutAlertMassege()
                        }else {
                            rootView?.rootViewController?.alert(message: msg, title: "Alert!")
                        }
                    }
                }catch{
                    print("data~~ \(data) ~~~~ response === \(String(describing: response))")
                    print("error on decode: \(error.localizedDescription) \n \(error)\n\n")
                    let currentDate: Date = Date()
                    let stringDate: String = currentDate.app_stringFromDate()
                    var sendreq: [AnyHashable : Any] = [:]
                    
                    sendreq = ["from": "ios","error": "\(error)","date_time": "\(stringDate)","api_name": "\(baseUrl+url_string)"]
                    var _: Error? = nil
                    var jsonData: Data? = nil
                    do {
                        jsonData = try JSONSerialization.data(withJSONObject: sendreq, options: .prettyPrinted)
                    } catch {
                    }
                    var jsonString: String? = nil
                    if let jsonData = jsonData {
                        jsonString = String(data: jsonData, encoding: .utf8)
                    }
                    callsaveExceptionLogApi(parameters: "json=\(jsonString ?? "")", CompletionHandler: { json, error in
                        if let json = json {
                            print("save exception final json ~~~~~~~",json)
                        }
                    })
                }
            }
            let dispatchAfter = DispatchTimeInterval.seconds(Int(1.2))
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + dispatchAfter, execute: {
                rootView?.activityStopAnimating()
            })
//            MKProgress.hide()
        }
    }
    task.resume()
}

public func postMethod(url_string: String, CompletionHandler: @escaping (NSDictionary?, String?) -> Void){
    rootView?.activityStopAnimating()
    let url = NSURL(string: baseUrl+url_string)
    let request = NSMutableURLRequest(url: url! as URL)
    print("url - - - ",url!)
    request.httpMethod = "POST"
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    request.addValue("application/json", forHTTPHeaderField: "Accept")
    // request.addValue("apple_ghjkl_planet_5541236", forHTTPHeaderField: "Authtoken")
    let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
        guard let data = data, error == nil else {
            DispatchQueue.main.async {
                if let err = error as NSError? {
                    if err.code == -1009{
                        rootView?.rootViewController?.alert(message: "Network error. Please try again.", title: "Alert!")
                        rootView?.activityStopAnimating()
                    }
                }
            }
            return
        }
        let string = String(data: data, encoding: String.Encoding.utf8)
        print("string~~~~~~~~~~",string)
        DispatchQueue.main.async {
            if let data = string?.replacingOccurrences(of: "\n", with: "").data(using: String.Encoding.utf8) {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! NSDictionary
                    //print("json of --- \(url_string) :",json)
                    let statusmessage1 = String(describing: json["status"])
                    let msg = json.value(forKey: "msg") as? String ?? ""
                    if (statusmessage1.contains("1")){
                        print("success")
                        CompletionHandler(json, nil)
                    } else {
                        print("false")
                        CompletionHandler(nil, msg)
                    }
                }catch{
                    let string = String(data: data, encoding: String.Encoding.utf8)
                    print("response string : ",string as Any)
                    print("error on decode: \(error.localizedDescription)")
                    
                    let currentDate: Date = Date()
                    let stringDate: String = currentDate.app_stringFromDate()
                    
                    var sendreq: [AnyHashable : Any] = [:]
                    
                    sendreq = ["from": "ios","error": "\(error)","date_time": "\(stringDate)","api_name": "\(baseUrl+url_string)"]
                    var _: Error? = nil
                    var jsonData: Data? = nil
                    do {
                        jsonData = try JSONSerialization.data(withJSONObject: sendreq, options: .prettyPrinted)
                    } catch {
                    }
                    var jsonString: String? = nil
                    if let jsonData = jsonData {
                        jsonString = String(data: jsonData, encoding: .utf8)
                    }
                    callsaveExceptionLogApi(parameters: "json=\(jsonString ?? "")", CompletionHandler: { json, error in
                        if let json = json {
                            print("save exception final json ~~~~~~~",json)
                        }
                    })
                }
                    rootView?.activityStopAnimating()
            }
        }
        
    })
    task.resume()
}





public func
    callsaveExceptionLogApi(parameters : String, CompletionHandler: @escaping (NSDictionary?, String?) -> Void){
    rootView?.activityStartAnimating()
    print("parameters -",parameters)
    let base1Url = "\(baseUrl)saveException"
    let url = URL(string: base1Url)!
    print("url : ",url)
    var request = URLRequest(url: url)
    request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
    request.httpMethod = "POST"
    
    if parameters != "" {
        request.httpBody = parameters.data(using: .utf8)
        let hValue = USER_DEFAULTS.string(forKey: "header_value")
        request.addValue(hValue!, forHTTPHeaderField: "MypeopleHeader")
    }
    
    let task = URLSession.shared.dataTask(with: request) { data, response, error in
        guard let data = data, error == nil else {
            DispatchQueue.main.async {
                if let err = error as NSError? {
                    if err.code == -1009{
                        rootView?.rootViewController?.alert(message: "Network error. Please try again.", title: "Alert!")
                        rootView?.activityStopAnimating()
                    }
                }
            }
            return
        }
        
        let string = String(data: data, encoding: String.Encoding.utf8)
        if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
            print("statusCode should be 200, but is \(httpStatus.statusCode)")
            print("response = \(response!)")
            return
        }
        
        DispatchQueue.main.async {
            if let data = string?.replacingOccurrences(of: "\n", with: "").data(using: String.Encoding.utf8) {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! NSDictionary
                    print("json of saveException api :",json)
                    let statusmessage1 = String(describing: json["success"])
                    if (statusmessage1.contains("true")){
                        print("success")
                        CompletionHandler(json, nil)
                    } else {
                        print("false")
                        CompletionHandler(json, nil)
                    }
                }catch{
                    print("data~~\(data)~~~~response===\(String(describing: response))")
                    print("error on decode: \(error.localizedDescription)")
                }
            }
            rootView?.activityStopAnimating()
        }
        
    }
    task.resume()
}

public func
    callLogOut(parameters : String, CompletionHandler: @escaping (NSDictionary?, String?) -> Void){
    rootView?.activityStartAnimating()
    print("parameters -",parameters)
    let base1Url = "\(baseUrl)logout"
    let url = URL(string: base1Url)!
    print("url : ",url)
    var request = URLRequest(url: url)
    request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
    request.httpMethod = "POST"
    
    if parameters != "" {
        request.httpBody = parameters.data(using: .utf8)
        let hValue = USER_DEFAULTS.string(forKey: "header_value")
        request.addValue(hValue!, forHTTPHeaderField: "MypeopleHeader")
    }
    
    let task = URLSession.shared.dataTask(with: request) { data, response, error in
        guard let data = data, error == nil else {
            DispatchQueue.main.async {
                if let err = error as NSError? {
                    if err.code == -1009{
                        rootView?.rootViewController?.alert(message: "Network error. Please try again.", title: "Alert!")
                        rootView?.activityStopAnimating()
                    }
                }
            }
            return
        }
        
        let string = String(data: data, encoding: String.Encoding.utf8)
        if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
            print("statusCode should be 200, but is \(httpStatus.statusCode)")
            print("response = \(response!)")
            return
        }
        
        DispatchQueue.main.async {
            if let data = string?.replacingOccurrences(of: "\n", with: "").data(using: String.Encoding.utf8) {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! NSDictionary
                    print("json of logut api :",json)
                    let statusmessage1 = String(describing: json["status"])
                    if (statusmessage1.contains("1")){
                        print("success")
                        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                        HomeViewController.VC.navigationController?.pushViewController(vc!, animated: true)
                        CompletionHandler(json, nil)
                    } else {
                        print("false")
                        CompletionHandler(json, nil)
                    }
                }catch{
                    print("data~~\(data)~~~~response===\(String(describing: response))")
                    print("error on decode: \(error.localizedDescription)")
                }
            }
            rootView?.activityStopAnimating()
        }
    }
    task.resume()
}

public func postWithImage(url_string: String,parameters: [String: String],filePathKey:String ,jsonData:Data, CompletionHandler: @escaping (NSDictionary?, String?) -> Void){
    
    rootView?.activityStopAnimating()
    rootView?.activityStartAnimating()
    let url = URL(string: baseUrl+url_string)
    print("url: ",url!,"parameters: ",parameters)
    var request = URLRequest(url: url!)
    request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
    request.httpMethod = "POST"
    if parameters != [:] {
        let boundary = "Boundary-\(NSUUID().uuidString)"
        request.httpBody = createBodyWithParametersImage(parameters: parameters, filePathKey: filePathKey, jsonDataKey: jsonData, boundary: boundary) as Data
        let hValue = USER_DEFAULTS.string(forKey: "header_value")
        request.addValue(hValue!, forHTTPHeaderField: "MypeopleHeader")
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.setValue("multipart/form-data", forHTTPHeaderField: "Accept")
    }
    let configuration = URLSessionConfiguration.default
    configuration.timeoutIntervalForRequest = TimeInterval(180)
    configuration.timeoutIntervalForResource = TimeInterval(180)
    let session = URLSession(configuration: configuration)
    
    task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
        guard let data = data, error == nil else {
            DispatchQueue.main.async {
                if let err = error as NSError? {
                    if err.code == -1009{
                        DispatchQueue.main.async {
                            rootView?.rootViewController?.alert(message: "Network error. Please try again.", title: "Alert!")
                            rootView?.activityStopAnimating()
                        }
                    }
                }
            }
            return
        }
        let string = String(data: data, encoding: String.Encoding.utf8)
        if let data = string?.replacingOccurrences(of: "\n", with: "").data(using: String.Encoding.utf8) {
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary {
                   // print("json of \(url_string) api is - - - ",json)
                    let statusmessage1 = String(describing: json["status"])
                    let msg = json.value(forKey: "msg") as? String ?? ""
                    if (statusmessage1.contains("1")){
                        print("success")
                        CompletionHandler(json, msg)
                    } else {
                        print("failed")
                        CompletionHandler(json, msg)
                    }
                }
            } catch let error {
                print("html response string ~~~~~~",string as Any) //JSONSerialization
                print("error - ",error ,"\n \n  ",error.localizedDescription)
                
                let currentDate: Date = Date()
                let stringDate: String = currentDate.app_stringFromDate()
                var sendreq: [AnyHashable : Any] = [:]
                
                sendreq = ["from": "ios","error": "\(error)","date_time": "\(stringDate)","api_name": "\(baseUrl+url_string)"]
                var _: Error? = nil
                var jsonData: Data? = nil
                do {
                    jsonData = try JSONSerialization.data(withJSONObject: sendreq, options: .prettyPrinted)
                } catch {
                }
                var jsonString: String? = nil
                if let jsonData = jsonData {
                    jsonString = String(data: jsonData, encoding: .utf8)
                }
                callsaveExceptionLogApi(parameters: "json=\(jsonString ?? "")", CompletionHandler: { json, error in
                    if let json = json {
                        print("save exception final json ~~~~~~~",json)
                    }
                })
            }
            DispatchQueue.main.async {
                rootView?.activityStopAnimating()
            }
        }
        
    })
    task.resume()
}

public func postWithImageforImportContact(url_string: String,parameters: [String: String],filePathKey:String ,jsonData:Data, CompletionHandler: @escaping (NSDictionary?, String?) -> Void){
    
    rootView?.activityStopAnimating()
//    rootView?.activityStartAnimating()
    let url = URL(string: baseUrl+url_string)
    print("url: ",url!,"parameters: ",parameters)
    var request = URLRequest(url: url!)
    request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
    request.httpMethod = "POST"
    if parameters != [:] {
        let boundary = "Boundary-\(NSUUID().uuidString)"
        request.httpBody = createBodyWithParametersImage(parameters: parameters, filePathKey: filePathKey, jsonDataKey: jsonData, boundary: boundary) as Data
        let hValue = USER_DEFAULTS.string(forKey: "header_value")
        request.addValue(hValue!, forHTTPHeaderField: "MypeopleHeader")
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.setValue("multipart/form-data", forHTTPHeaderField: "Accept")
    }
    let configuration = URLSessionConfiguration.default
    configuration.timeoutIntervalForRequest = TimeInterval(180)
    configuration.timeoutIntervalForResource = TimeInterval(180)
    let session = URLSession(configuration: configuration)
    
    task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
        guard let data = data, error == nil else {
            DispatchQueue.main.async {
                if let err = error as NSError? {
                    if err.code == -1009{
                        DispatchQueue.main.async {
                            rootView?.rootViewController?.alert(message: "Network error. Please try again.", title: "Alert!")
                            //rootView?.activityStopAnimating()
                        }
                    }
                }
            }
            return
        }
        let string = String(data: data, encoding: String.Encoding.utf8)
        if let data = string?.replacingOccurrences(of: "\n", with: "").data(using: String.Encoding.utf8) {
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary {
                    // print("json of \(url_string) api is - - - ",json)
                    let statusmessage1 = String(describing: json["status"])
                    let msg = json.value(forKey: "msg") as? String ?? ""
                    if (statusmessage1.contains("1")){
                        print("success")
                        CompletionHandler(json, msg)
                    } else {
                        print("failed")
                        CompletionHandler(json, msg)
                    }
                }
            } catch let error {
                print("html response string ~~~~~~",string as Any) //JSONSerialization
                print("error - ",error ,"\n \n  ",error.localizedDescription)
                
                let currentDate: Date = Date()
                let stringDate: String = currentDate.app_stringFromDate()
                var sendreq: [AnyHashable : Any] = [:]
                
                sendreq = ["from": "ios","error": "\(error)","date_time": "\(stringDate)","api_name": "\(baseUrl+url_string)"]
                var _: Error? = nil
                var jsonData: Data? = nil
                do {
                    jsonData = try JSONSerialization.data(withJSONObject: sendreq, options: .prettyPrinted)
                } catch {
                }
                var jsonString: String? = nil
                if let jsonData = jsonData {
                    jsonString = String(data: jsonData, encoding: .utf8)
                }
                callsaveExceptionLogApi(parameters: "json=\(jsonString ?? "")", CompletionHandler: { json, error in
                    if let json = json {
                        print("save exception final json ~~~~~~~",json)
                    }
                })
            }
            DispatchQueue.main.async {
                //rootView?.activityStopAnimating()
            }
        }
        
    })
    task.resume()
}


public func createBodyWithParametersImage(parameters: [String: String]?, filePathKey: String?, jsonDataKey: Data, boundary: String) -> NSData {
    let body = NSMutableData();
    if parameters != nil {
        for (key, value) in parameters! {
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
        }
    }
    let mimetype = "text/html"
    body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
    body.append("Content-Disposition: form-data; name=\"file\"; filename=\"file.doc\"\r\n".data(using: String.Encoding.utf8)!)
    body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
    body.append(jsonDataKey as Data)
    body.append("\r\n".data(using: String.Encoding.utf8)!)
    body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
    return body
}

public func createBodyWithParameters(parameters: [String: Any]) -> NSData {
    let body = NSMutableData();
    let boundary = "Boundary-\(NSUUID().uuidString)"
    for (key, value) in parameters {
        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
        body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
    }
    return body
}

public func logoutAlertMassege(){
    print("~~~~~~~logoutAlertMassege~~~~~~~")
    let alert = UIAlertController(title: "Message", message: "Your User Rights Have Changed Logout And Then Login Again", preferredStyle: .alert)
    let OK = UIAlertAction(title: "OK", style: .default, handler: { action in
        let u_id = USER_DEFAULTS.string(forKey: "u_id") ?? ""
        let authToken = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
        callLogOut(parameters: "id\(u_id)&authToken\(authToken)", CompletionHandler: { json, error in
            if let json = json {
                print("logout ~~~~~~~",json)
            }
        })
    })
    alert.addAction(OK)
    rootView?.rootViewController?.present(alert, animated: true)
}
