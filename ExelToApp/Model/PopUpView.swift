//
//  PopUpView.swift
//  ExelToApp
//
//  Created by baps on 29/01/20.
//  Copyright © 2020 Osiya. All rights reserved.
//

import UIKit

class PopUpView: UIView,UITableViewDataSource,UITableViewDelegate {
    
    var mainView:UIView!
    
    var genderView:UIView!
    var lblHeading:UILabel!
    var btnMale:UIButton!
    var btnFeMale:UIButton!
    var btnCancel:UIButton!
    
    var raceView:UIView!
    var btnA:UIButton!
    var btnB:UIButton!
    var btnH:UIButton!
    var btnI:UIButton!
    var btnO:UIButton!
    var btnW:UIButton!
    
    var partyView:UIView!
    var btnDEM:UIButton!
    var btnREP:UIButton!
    var btnOTH:UIButton!
    
    var favView:UIView!
    var btnFavor:UIButton!
    var btnNotFavor:UIButton!
    var btnUndicided:UIButton!
    var btnNotAvailabel:UIButton!
    
    var yardView:UIView!
    var btnYes:UIButton!
    var btnNo:UIButton!
    var btnPlaced:UIButton!
    
    var VotedView:UIView!
    var btnVYes:UIButton!
    var btnVNo:UIButton!
    
    var custVarView:UIView!
    var tableView:UITableView!
    let cellReuseIdentifier = "cell"
    var btnSelect:UIButton!
    var custVarMode = 0
    var topView:UIView!
    
    var custVararr1 = USER_DEFAULTS.array(forKey: "arrayCustomVarOne") as NSArray?
    var custVararr2 = USER_DEFAULTS.array(forKey: "arrayCustomVarTwo") as NSArray?
    var custVararr3 = USER_DEFAULTS.array(forKey: "arrayCustomVarThree") as NSArray?

    public override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
        
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpView()
    }
    
    func setUpView() {
        mainView = UIView()
        mainView.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT)
        mainView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.addSubview(mainView)
        
        if isSelectPopup == 0 {
            genderView = UIView()
            genderView.frame = CGRect(x: 60, y: SHEIGHT/2-80, width: SWIDTH-120, height: 145)
            genderView.backgroundColor = UIColor.white
            genderView.layer.cornerRadius = 10.0
            genderView.clipsToBounds = true
            mainView.addSubview(genderView)
            
            topView = UIView()
            topView.frame = CGRect(x: 0, y: 0, width: genderView.frame.width, height: 50)
            topView.backgroundColor = APPBUTTONCOLOR
            genderView.addSubview(topView)
            
            lblHeading = UILabel()
            lblHeading.setLabel(X: 0, Y: 10, Width: genderView.frame.width, Height: 30, TextColor: .white, BackColor: .clear, Text: "Select Personal Sex", TextAlignment: .center, Font: .boldSystemFont(ofSize: 18.0))
            topView.addSubview(lblHeading)
            
            btnMale = UIButton(type: .system)
            btnMale.setButton(X: 0, Y: topView.frame.maxY+15, Width: genderView.frame.width, Height: 40, TextColor: .black, BackColor: .clear, Title: "Male", Align: .center, Font: .systemFont(ofSize: 15.0))
            btnMale.layer.borderWidth = 1.0
            btnMale.layer.borderColor = UIColor(red: 225.0/255.0, green: 232.0/255.0, blue: 237.0/255.0, alpha: 1.0).cgColor
            btnMale.tag = 2
            btnMale.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
            genderView.addSubview(btnMale)
            
            btnFeMale = UIButton(type: .system)
            btnFeMale.setButton(X: 0, Y: btnMale.frame.maxY-1, Width: genderView.frame.width, Height: 40, TextColor: .black, BackColor: .clear, Title: "Female", Align: .center, Font: .systemFont(ofSize: 15.0))
            btnFeMale.tag = 3
            btnFeMale.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
            genderView.addSubview(btnFeMale)
            
            btnCancel = UIButton()
            btnCancel.setButton(X: genderView.frame.width-30, Y: 5, Width: 24, Height: 24, TextColor: .white, BackColor: .clear, Title: "X", Align: .center, Font: .boldSystemFont(ofSize: 22.0))
            btnCancel.tag = 1
            btnCancel.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
            topView.addSubview(btnCancel)
        }
        
        if isSelectPopup == 1 {
            raceView = UIView()
            raceView.frame = CGRect(x: 60, y: SHEIGHT/2-145, width: SWIDTH-120, height: 300)
            raceView.backgroundColor = APPPPOPUPCOLOR
            raceView.layer.cornerRadius = 10.0
            raceView.clipsToBounds = true
            mainView.addSubview(raceView)
            
            topView = UIView()
            topView.frame = CGRect(x: 0, y: 0, width: raceView.frame.width, height: 50)
            topView.backgroundColor = APPBUTTONCOLOR
            raceView.addSubview(topView)
            
            lblHeading = UILabel()
            lblHeading.setLabel(X: 0, Y: 10, Width: raceView.frame.width, Height: 30, TextColor: .white, BackColor: .clear, Text: "Select Personal Race", TextAlignment: .center, Font: .boldSystemFont(ofSize: 18.0))
            topView.addSubview(lblHeading)
            
            btnA = UIButton(type: .system)
            btnA.setButton(X: 0, Y: topView.frame.maxY+15, Width: raceView.frame.width, Height: 40, TextColor: APPBUTTONCOLOR, BackColor: .clear, Title: "A", Align: .center, Font: .systemFont(ofSize: 15.0))
            btnA.layer.borderWidth = 1.0
            btnA.layer.borderColor = UIColor(red: 225.0/255.0, green: 232.0/255.0, blue: 237.0/255.0, alpha: 1.0).cgColor
            btnA.tag = 4
            btnA.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
            raceView.addSubview(btnA)
            
            btnB = UIButton(type: .system)
            btnB.setButton(X: 0, Y: btnA.frame.maxY-1, Width: raceView.frame.width, Height: 40, TextColor: APPBUTTONCOLOR, BackColor: .clear, Title: "B", Align: .center, Font: .systemFont(ofSize: 15.0))
            btnB.layer.borderWidth = 1.0
            btnB.layer.borderColor = UIColor(red: 225.0/255.0, green: 232.0/255.0, blue: 237.0/255.0, alpha: 1.0).cgColor
            btnB.tag = 5
            btnB.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
            raceView.addSubview(btnB)
            
            btnH = UIButton(type: .system)
            btnH.setButton(X: 0, Y: btnB.frame.maxY-1, Width: raceView.frame.width, Height: 40, TextColor: APPBUTTONCOLOR, BackColor: .clear, Title: "H", Align: .center, Font: .systemFont(ofSize: 15.0))
            btnH.layer.borderWidth = 1.0
            btnH.layer.borderColor = UIColor(red: 225.0/255.0, green: 232.0/255.0, blue: 237.0/255.0, alpha: 1.0).cgColor
            btnH.tag = 6
            btnH.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
            raceView.addSubview(btnH)
            
            btnI = UIButton(type: .system)
            btnI.setButton(X: 0, Y: btnH.frame.maxY-1, Width: raceView.frame.width, Height: 40, TextColor: APPBUTTONCOLOR, BackColor: .clear, Title: "I", Align: .center, Font: .systemFont(ofSize: 15.0))
            btnI.layer.borderWidth = 1.0
            btnI.layer.borderColor = UIColor(red: 225.0/255.0, green: 232.0/255.0, blue: 237.0/255.0, alpha: 1.0).cgColor
            btnI.tag = 7
            btnI.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
            raceView.addSubview(btnI)
            
            btnO = UIButton(type: .system)
            btnO.setButton(X: 0, Y: btnI.frame.maxY-1, Width: raceView.frame.width, Height: 40, TextColor: APPBUTTONCOLOR, BackColor: .clear, Title: "O", Align: .center, Font: .systemFont(ofSize: 15.0))
            btnO.layer.borderWidth = 1.0
            btnO.layer.borderColor = UIColor(red: 225.0/255.0, green: 232.0/255.0, blue: 237.0/255.0, alpha: 1.0).cgColor
            btnO.tag = 8
            btnO.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
            raceView.addSubview(btnO)
            
            btnW = UIButton(type: .system)
            btnW.setButton(X: 0, Y: btnO.frame.maxY-1, Width: raceView.frame.width, Height: 40, TextColor: APPBUTTONCOLOR, BackColor: .clear, Title: "W", Align: .center, Font: .systemFont(ofSize: 15.0))
            btnW.tag = 9
            btnW.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
            raceView.addSubview(btnW)
            
            btnCancel = UIButton()
            btnCancel.setButton(X: raceView.frame.width-30, Y: 5, Width: 24, Height: 24, TextColor: .white, BackColor: .clear, Title: "X", Align: .center, Font: .boldSystemFont(ofSize: 22.0))
            btnCancel.tag = 1
            btnCancel.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
            topView.addSubview(btnCancel)
        }
        
        if isSelectPopup == 2 {
            partyView = UIView()
            partyView.frame = CGRect(x: 60, y: SHEIGHT/2-100, width: SWIDTH-120, height: 190)
            partyView.backgroundColor = APPPPOPUPCOLOR
            partyView.layer.cornerRadius = 10.0
            partyView.clipsToBounds = true
            mainView.addSubview(partyView)
            
            topView = UIView()
            topView.frame = CGRect(x: 0, y: 0, width: partyView.frame.width, height: 50)
            topView.backgroundColor = APPBUTTONCOLOR
            partyView.addSubview(topView)
            
            
            lblHeading = UILabel()
            lblHeading.setLabel(X: 0, Y: 0, Width: partyView.frame.width, Height: 50, TextColor: .white, BackColor: .clear, Text: "Select Registration Political Party Code", TextAlignment: .center, Font: .boldSystemFont(ofSize: 18.0))
            lblHeading.numberOfLines = 0
            lblHeading.lineBreakMode = .byWordWrapping
            topView.addSubview(lblHeading)
            
            btnDEM = UIButton(type: .system)
            btnDEM.setButton(X: 0, Y: topView.frame.maxY+15, Width: partyView.frame.width, Height: 40, TextColor: APPBUTTONCOLOR, BackColor: .clear, Title: "DEM", Align: .center, Font: .systemFont(ofSize: 15.0))
            btnDEM.layer.borderWidth = 1.0
            btnDEM.layer.borderColor = UIColor(red: 225.0/255.0, green: 232.0/255.0, blue: 237.0/255.0, alpha: 1.0).cgColor
            btnDEM.tag = 10
            btnDEM.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
            partyView.addSubview(btnDEM)
            
            btnREP = UIButton(type: .system)
            btnREP.setButton(X: 0, Y: btnDEM.frame.maxY-1, Width: partyView.frame.width, Height: 40, TextColor: APPBUTTONCOLOR, BackColor: .clear, Title: "REP", Align: .center, Font: .systemFont(ofSize: 15.0))
            btnREP.layer.borderWidth = 1.0
            btnREP.layer.borderColor = UIColor(red: 225.0/255.0, green: 232.0/255.0, blue: 237.0/255.0, alpha: 1.0).cgColor
            btnREP.tag = 11
            btnREP.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
            partyView.addSubview(btnREP)
            
            btnOTH = UIButton(type: .system)
            btnOTH.setButton(X: 0, Y: btnREP.frame.maxY-1, Width: partyView.frame.width, Height: 40, TextColor: APPBUTTONCOLOR, BackColor: .clear, Title: "OTH", Align: .center, Font: .systemFont(ofSize: 15.0))
            btnOTH.tag = 12
            btnOTH.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
            partyView.addSubview(btnOTH)
            
            btnCancel = UIButton()
            btnCancel.setButton(X: partyView.frame.width-30, Y: 5, Width: 24, Height: 24, TextColor: .white, BackColor: .clear, Title: "X", Align: .center, Font: .boldSystemFont(ofSize: 22.0))
            btnCancel.tag = 1
            btnCancel.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
            topView.addSubview(btnCancel)
        }
        
        if isSelectPopup == 3 {
            favView = UIView()
            favView.frame = CGRect(x: 60, y: SHEIGHT/2-100, width: SWIDTH-120, height: 225)
            favView.backgroundColor = APPPPOPUPCOLOR
            favView.layer.cornerRadius = 10.0
            favView.clipsToBounds = true
            mainView.addSubview(favView)
            
            topView = UIView()
            topView.frame = CGRect(x: 0, y: 0, width: favView.frame.width, height: 50)
            topView.backgroundColor = APPBUTTONCOLOR
            favView.addSubview(topView)
            
            lblHeading = UILabel()
            lblHeading.setLabel(X: 0, Y: 10, Width: favView.frame.width, Height: 30, TextColor: .white, BackColor: .clear, Text: "Select Favorability", TextAlignment: .center, Font: .boldSystemFont(ofSize: 18.0))
            topView.addSubview(lblHeading)
            
            btnFavor = UIButton(type: .system)
            btnFavor.setButton(X: 0, Y: topView.frame.maxY+15, Width: favView.frame.width, Height: 40, TextColor: APPBUTTONCOLOR, BackColor: .clear, Title: "Favor", Align: .center, Font: .systemFont(ofSize: 15.0))
            btnFavor.layer.borderWidth = 1.0
            btnFavor.layer.borderColor = UIColor(red: 225.0/255.0, green: 232.0/255.0, blue: 237.0/255.0, alpha: 1.0).cgColor
            btnFavor.tag = 13
            btnFavor.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
            favView.addSubview(btnFavor)
            
            btnNotFavor = UIButton(type: .system)
            btnNotFavor.setButton(X: 0, Y: btnFavor.frame.maxY-1, Width: favView.frame.width, Height: 40, TextColor: APPBUTTONCOLOR, BackColor: .clear, Title: "Not Favor", Align: .center, Font: .systemFont(ofSize: 15.0))
            btnNotFavor.layer.borderWidth = 1.0
            btnNotFavor.layer.borderColor = UIColor(red: 225.0/255.0, green: 232.0/255.0, blue: 237.0/255.0, alpha: 1.0).cgColor
            btnNotFavor.tag = 14
            btnNotFavor.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
            favView.addSubview(btnNotFavor)
            
            btnUndicided = UIButton(type: .system)
            btnUndicided.setButton(X: 0, Y: btnNotFavor.frame.maxY-1, Width: favView.frame.width, Height: 40, TextColor: APPBUTTONCOLOR, BackColor: .clear, Title: "Undecided", Align: .center, Font: .systemFont(ofSize: 15.0))
            btnUndicided.layer.borderWidth = 1.0
            btnUndicided.layer.borderColor = UIColor(red: 225.0/255.0, green: 232.0/255.0, blue: 237.0/255.0, alpha: 1.0).cgColor
            btnUndicided.tag = 15
            btnUndicided.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
            favView.addSubview(btnUndicided)
            
            btnNotAvailabel = UIButton(type: .system)
            btnNotAvailabel.setButton(X: 0, Y: btnUndicided.frame.maxY-1, Width: favView.frame.width, Height: 40, TextColor: APPBUTTONCOLOR, BackColor: .clear, Title: "Not Available", Align: .center, Font: .systemFont(ofSize: 15.0))
            btnNotAvailabel.tag = 16
            btnNotAvailabel.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
            favView.addSubview(btnNotAvailabel)
            
            btnCancel = UIButton()
            btnCancel.setButton(X: favView.frame.width-30, Y: 5, Width: 24, Height: 24, TextColor: .white, BackColor: .clear, Title: "X", Align: .center, Font: .boldSystemFont(ofSize: 22.0))
            btnCancel.tag = 1
            btnCancel.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
            topView.addSubview(btnCancel)
        }
        
        if isSelectPopup == 4 {
            yardView = UIView()
            yardView.frame = CGRect(x: 60, y: SHEIGHT/2-90, width: SWIDTH-120, height: 185)
            yardView.backgroundColor = APPPPOPUPCOLOR
            yardView.layer.cornerRadius = 10.0
            yardView.clipsToBounds = true
            mainView.addSubview(yardView)
            
            topView = UIView()
            topView.frame = CGRect(x: 0, y: 0, width: yardView.frame.width, height: 50)
            topView.backgroundColor = APPBUTTONCOLOR
            yardView.addSubview(topView)
            
            lblHeading = UILabel()
            lblHeading.setLabel(X: 0, Y: 10, Width: yardView.frame.width, Height: 20, TextColor: .white, BackColor: .clear, Text: "Select Yard Sign", TextAlignment: .center, Font: .boldSystemFont(ofSize: 18.0))
            topView.addSubview(lblHeading)
            
            btnYes = UIButton(type: .system)
            btnYes.setButton(X: 0, Y: topView.frame.maxY+15, Width: yardView.frame.width, Height: 40, TextColor: APPBUTTONCOLOR, BackColor: .clear, Title: "Yes", Align: .center, Font: .systemFont(ofSize: 15.0))
            btnYes.layer.borderWidth = 1.0
            btnYes.layer.borderColor = UIColor(red: 225.0/255.0, green: 232.0/255.0, blue: 237.0/255.0, alpha: 1.0).cgColor
            btnYes.tag = 17
            btnYes.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
            yardView.addSubview(btnYes)
            
            btnNo = UIButton(type: .system)
            btnNo.setButton(X: 0, Y: btnYes.frame.maxY-1, Width: yardView.frame.width, Height: 40, TextColor: APPBUTTONCOLOR, BackColor: .clear, Title: "No", Align: .center, Font: .systemFont(ofSize: 15.0))
            btnNo.layer.borderWidth = 1.0
            btnNo.layer.borderColor = UIColor(red: 225.0/255.0, green: 232.0/255.0, blue: 237.0/255.0, alpha: 1.0).cgColor
            btnNo.tag = 18
            btnNo.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
            yardView.addSubview(btnNo)
            
            btnPlaced = UIButton(type: .system)
            btnPlaced.setButton(X: 0, Y: btnNo.frame.maxY-1, Width: yardView.frame.width, Height: 40, TextColor: APPBUTTONCOLOR, BackColor: .clear, Title: "Placed", Align: .center, Font: .systemFont(ofSize: 15.0))
            btnPlaced.tag = 19
            btnPlaced.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
            yardView.addSubview(btnPlaced)
            
            btnCancel = UIButton()
            btnCancel.setButton(X: yardView.frame.width-30, Y: 5, Width: 24, Height: 24, TextColor: .white, BackColor: .clear, Title: "X", Align: .center, Font: .boldSystemFont(ofSize: 22.0))
            btnCancel.tag = 1
            btnCancel.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
            topView.addSubview(btnCancel)
        }
        
        if isSelectPopup == 5 {
            VotedView = UIView()
            VotedView.frame = CGRect(x: 60, y: SHEIGHT/2-75, width: SWIDTH-120, height: 145)
            VotedView.backgroundColor = APPPPOPUPCOLOR
            VotedView.layer.cornerRadius = 10.0
            VotedView.clipsToBounds = true
            mainView.addSubview(VotedView)
            
            topView = UIView()
            topView.frame = CGRect(x: 0, y: 0, width: VotedView.frame.width, height: 50)
            topView.backgroundColor = APPBUTTONCOLOR
            VotedView.addSubview(topView)
            
            lblHeading = UILabel()
            lblHeading.setLabel(X: 0, Y: 10, Width: VotedView.frame.width, Height: 20, TextColor: .white, BackColor: .clear, Text: "Select Voted", TextAlignment: .center, Font: .boldSystemFont(ofSize: 18.0))
            topView.addSubview(lblHeading)
            
            btnVYes = UIButton(type: .system)
            btnVYes.setButton(X: 0, Y: topView.frame.maxY+15, Width: VotedView.frame.width, Height: 40, TextColor: APPBUTTONCOLOR, BackColor: .clear, Title: "Yes", Align: .center, Font: .systemFont(ofSize: 15.0))
            btnVYes.layer.borderWidth = 1.0
            btnVYes.layer.borderColor = UIColor(red: 225.0/255.0, green: 232.0/255.0, blue: 237.0/255.0, alpha: 1.0).cgColor
            btnVYes.tag = 20
            btnVYes.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
            VotedView.addSubview(btnVYes)
            
            btnVNo = UIButton(type: .system)
            btnVNo.setButton(X: 0, Y: btnVYes.frame.maxY-1, Width: VotedView.frame.width, Height: 40, TextColor: APPBUTTONCOLOR, BackColor: .clear, Title: "No", Align: .center, Font: .systemFont(ofSize: 15.0))
            btnVNo.tag = 21
            btnVNo.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
            VotedView.addSubview(btnVNo)
            
            btnCancel = UIButton()
            btnCancel.setButton(X: VotedView.frame.width-30, Y: 5, Width: 24, Height: 24, TextColor: .white, BackColor: .clear, Title: "X", Align: .center, Font: .boldSystemFont(ofSize: 22.0))
            btnCancel.tag = 1
            btnCancel.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
            topView.addSubview(btnCancel)
        }
        
        if isSelectPopup == 6{
            custVarView = UIView()
            custVarView.frame = CGRect(x: 10, y: 80, width: SWIDTH-20, height: SHEIGHT/1.8)
            custVarView.backgroundColor = UIColor.white
            custVarView.layer.cornerRadius = 10.0
            custVarView.layer.shadowRadius  = 1.5
            custVarView.layer.shadowColor   = UIColor(red: 176.0/255.0, green: 199.0/255.0, blue: 226.0/255.0, alpha: 1.0).cgColor
            custVarView.layer.shadowOffset  = CGSize(width: 0.0, height: 0.0)
            custVarView.layer.shadowOpacity = 0.9
            custVarView.layer.masksToBounds = false
            custVarView.clipsToBounds = true
            self.addSubview(custVarView)
            
            topView = UIView()
            topView.frame = CGRect(x: 0, y: 0, width: custVarView.frame.width, height: 50)
            topView.backgroundColor = APPBUTTONCOLOR
            custVarView.addSubview(topView)
            
            lblHeading = UILabel()
            lblHeading.setLabel(X: 0, Y: 10, Width: custVarView.frame.width, Height: 20, TextColor: .white, BackColor: .clear, Text: "Select Cust VAr1", TextAlignment: .center, Font: .boldSystemFont(ofSize: 18.0))
            topView.addSubview(lblHeading)
            
            tableView = UITableView()
            tableView.frame = CGRect(x: 5, y: topView.frame.maxY+10, width: custVarView.frame.width-10, height: custVarView.frame.height-50)
            tableView.backgroundColor = UIColor.white
            tableView.delegate = self
            tableView.dataSource = self
            tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
            custVarView.addSubview(tableView)
            tableView.tableFooterView = UIView()
            tableView.layer.cornerRadius = 10.0
            
            btnCancel = UIButton()
            btnCancel.setButton(X: custVarView.frame.width-30, Y: 5, Width: 24, Height: 24, TextColor: .white, BackColor: .clear, Title: "X", Align: .center, Font: .boldSystemFont(ofSize: 22.0))
            btnCancel.tag = 1
            btnCancel.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
            topView.addSubview(btnCancel)
            
            btnSelect = UIButton()
            btnSelect.setButton(X: custVarView.frame.width/2-(custVarView.frame.width/2)/2, Y: tableView.frame.maxY+5, Width: custVarView.frame.width/2, Height: 30, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Select", Align: .center, Font: .systemFont(ofSize: 17.0))
            btnSelect.tag = 4
            btnSelect.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
            custVarView.addSubview(btnSelect)
            //custVarView.isHidden = true
        }
    }
    
    @objc func onClickButton(_ sender: UIButton) {
        //print("-----Name-----")
        let addVC = AddEditViewController.VC
        if sender.tag == 1{
            self.isHidden = true
            selectPopupText = ""
        }else if sender.tag == 2{
            let txtSex = addVC.textFieldArr[11] as? UITextField
            txtSex!.text = "M"
            selectPopupText = "M"
            self.isHidden = true
        }else if sender.tag == 3{
            selectPopupText = "F"
            let txtSex = addVC.textFieldArr[11] as? UITextField
            txtSex!.text = "F"
            self.isHidden = true
        }else if sender.tag == 4{
            let txt = addVC.textFieldArr[12] as? UITextField
            txt!.text = "A"
            selectPopupText = "A"
            self.isHidden = true
        }else if sender.tag == 5{
            let txt = addVC.textFieldArr[12] as? UITextField
            txt!.text = "B"
            selectPopupText = "B"
            self.isHidden = true
        }else if sender.tag == 6{
            let txt = addVC.textFieldArr[12] as? UITextField
            txt!.text = "H"
            selectPopupText = "H"
            self.isHidden = true
        }else if sender.tag == 7{
            let txt = addVC.textFieldArr[12] as? UITextField
            txt!.text = "I"
            selectPopupText = "I"
            self.isHidden = true
        }else if sender.tag == 8{
            let txt = addVC.textFieldArr[12] as? UITextField
            txt!.text = "O"
            selectPopupText = "O"
            self.isHidden = true
        }else if sender.tag == 9{
            let txt = addVC.textFieldArr[12] as? UITextField
            txt!.text = "W"
            selectPopupText = "W"
            self.isHidden = true
        }else if sender.tag == 10{
            let txt = addVC.textFieldArr[13] as? UITextField
            txt!.text = "DEM"
            selectPopupText = "DEM"
            self.isHidden = true
        }else if sender.tag == 11{
            let txt = addVC.textFieldArr[13] as? UITextField
            txt!.text = "REP"
            selectPopupText = "REP"
            self.isHidden = true
        }else if sender.tag == 12{
            let txt = addVC.textFieldArr[13] as? UITextField
            txt!.text = "OTH"
            selectPopupText = "OTH"
            self.isHidden = true
        }else if sender.tag == 13{
            let txt = addVC.textFieldArr[20] as? UITextField
            txt!.text = "Favor"
            selectPopupText = "Favor"
            self.isHidden = true
        }else if sender.tag == 14{
            let txt = addVC.textFieldArr[20] as? UITextField
            txt!.text = "Not Favor"
            selectPopupText = "Not Favor"
            self.isHidden = true
        }else if sender.tag == 15{
            let txt = addVC.textFieldArr[20] as? UITextField
            txt!.text = "Undecided"
            selectPopupText = "Undecided"
            self.isHidden = true
        }else if sender.tag == 16{
            let txt = addVC.textFieldArr[20] as? UITextField
            txt!.text = "Not Available"
            selectPopupText = "Not Available"
            self.isHidden = true
        }else if sender.tag == 17{
            let txt = addVC.textFieldArr[21] as? UITextField
            txt!.text = "Yes"
            selectPopupText = "Yes"
            self.isHidden = true
        }else if sender.tag == 18{
            let txt = addVC.textFieldArr[21] as? UITextField
            txt!.text = "No"
            selectPopupText = "No"
            self.isHidden = true
        }else if sender.tag == 19{
            let txt = addVC.textFieldArr[21] as? UITextField
            txt!.text = "Placed"
            selectPopupText = "Placed"
            self.isHidden = true
        }else if sender.tag == 20{
            let txt = addVC.textFieldArr[24] as? UITextField
            txt!.text = "Yes"
            selectPopupText = "Yes"
            self.isHidden = true
        }else if sender.tag == 21{
            let txt = addVC.textFieldArr[24] as? UITextField
            txt!.text = "No"
            selectPopupText = "No"
            self.isHidden = true
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print("custVarMode~~~~~",custVarMode)
        if custVarMode == 1 {
            return custVararr1?.count ?? 0
        }else if custVarMode == 2 {
            return custVararr2?.count ?? 0
        }else {
            return custVararr3?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as UITableViewCell!
        if custVarMode == 1 {
            cell.textLabel?.text = custVararr1?[indexPath.row] as? String
        }else if custVarMode == 2 {
            cell.textLabel?.text = custVararr2?[indexPath.row] as? String
        }else {
            cell.textLabel?.text = custVararr3?[indexPath.row] as? String
        }
        cell.textLabel?.textColor = APPBUTTONCOLOR
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let addVC = AddEditViewController.VC
        if custVarMode == 1 {
            print("H_V.isSelectCustomVar",isSelectCustomVar)
            if (isSelectCustomVar == 1){
                selectCustVarHomeTxt1 = custVararr1?[indexPath.row] as? String ?? ""
                HomeViewController.VC.btnCustVar1.setTitle(custVararr1?[indexPath.row] as? String ?? "", for: .normal)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "onClickCustomVar"), object: nil)
                self.isHidden = true
            }else{
                let txt = addVC.textFieldArr[28] as? UITextField
                txt!.text = custVararr1?[indexPath.row] as? String
                print("txt!.text---2",txt!.text)
                selectPopupText = custVararr1?[indexPath.row] as? String ?? ""
                self.isHidden = true
            }
            
        }else if custVarMode == 2 {
            if (isSelectCustomVar == 2){
                selectCustVarHomeTxt2 = custVararr2?[indexPath.row] as? String ?? ""
                HomeViewController.VC.btnCustVar2.setTitle(custVararr2?[indexPath.row] as? String ?? "", for: .normal)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "onClickCustomVar"), object: nil)
                self.isHidden = true
            }else{
                let txt = addVC.textFieldArr[29] as? UITextField
                txt!.text = custVararr2?[indexPath.row] as? String
                print("txt!.text---2",txt!.text)
                selectPopupText = custVararr2?[indexPath.row] as? String ?? ""
                self.isHidden = true
                
            }
        }else {
            if (isSelectCustomVar == 3){
                selectCustVarHomeTxt3 = custVararr3?[indexPath.row] as? String ?? ""
                HomeViewController.VC.btnCustVar3.setTitle(custVararr3?[indexPath.row] as? String ?? "", for: .normal)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "onClickCustomVar"), object: nil)
                self.isHidden = true
            }else{
                let txt = addVC.textFieldArr[30] as? UITextField
                txt!.text = custVararr3?[indexPath.row] as? String
                selectPopupText = custVararr3?[indexPath.row] as? String ?? ""
                self.isHidden = true
            }
        }
    }

}
