//
//  Home.swift
//  ExelToApp
//
//  Created by baps on 15/01/20.
//  Copyright © 2020 Osiya. All rights reserved.
//

import UIKit
import MessageUI
import Messages
var H_V = Home()


class commentCell: UITableViewCell {
    var lblTxt:UILabel!
    var imgView:UIImageView!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        lblTxt = UILabel()
        addSubview(lblTxt)
        lblTxt.anchor(top: topAnchor, paddingTop: 5, bottom: bottomAnchor, paddingBottom: 5, left: leftAnchor, paddingLeft: 5, right: nil, paddingRight: 0, width: 0, height: 0)
        lblTxt.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1, constant: -45).isActive = true
        
        imgView = UIImageView()
        addSubview(imgView)
        imgView.anchor(top: nil, paddingTop: 0, bottom: nil, paddingBottom: 0, left: lblTxt.rightAnchor, paddingLeft: 5, right: nil, paddingRight: 0, width: 40, height: 40)
        imgView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class Home: UIView ,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,MFMessageComposeViewControllerDelegate{
    
    
    var mainView:UIView!
    var bgImgView:UIImageView!
    var navcon: UINavigationController!
    var sideMenuButton: UIBarButtonItem!
    var lblName:UILabel!
    var lblHouseNo:UILabel!
    var lblStreetCity:UILabel!
    var btnHomePhoneNo:UIButton!
    var btnMobilePhoneNo:UIButton!
    var btnInfo:UIButton!
    var btnMap:UIButton!
    var lblEmail:UILabel!
    var lblLastVote:UILabel!
    var lblSex:UILabel!
    var lblParty:UILabel!
    var lblRating:UILabel!
    var lblAge:UILabel!
    var lblRace:UILabel!
    var lblVoted:UILabel!
    var lblCustVar1:UILabel!
    var lblCustVar2:UILabel!
    var lblCustVar3:UILabel!
    var btnCustVar1:UIButton!
    var btnCustVar2:UIButton!
    var btnCustVar3:UIButton!
    var arrowImg:UIImageView!
    var arrowImg1:UIImageView!
    var arrowImg2:UIImageView!
    var favrabilityView:UIView!
    var lblFavrability:UILabel!
    var btnFavor:UIButton!
    var btnNotFavor:UIButton!
    var btnUndecided:UIButton!
    var btnNotAvailable:UIButton!
    var yardSignView:UIView!
    var lblYardSign:UILabel!
    var btnYes:UIButton!
    var btnNo:UIButton!
    var btnPlaced:UIButton!
    var commentView:UIView!
    var lblComment:UILabel!
    var btnAddComm:UIButton!
    var commTableView: UITableView = UITableView()
    let cellReuseIdentifier = "cell"
    
    var btnSendCase:UIButton!
    var btnSave:UIButton!
    var btnBackToList:UIButton!
    var btnNext:UIButton!
    var btnPrevious:UIButton!
    var commView:UIView!
    var commView1:UIView!
    var txtCommView:UITextView!
    var btnOk:UIButton!
    var btnPrivate:UIButton!
    var btnPublic:UIButton!
    var btnCancel:UIButton!
    var lblMsg:UILabel!
    var viewTable:UIView!
    var TableView: UITableView = UITableView()
    let cellReuseIdentifier1 = "cell1"
    var btnClose:UIButton!
    var lblLine:UILabel!
    
    var sendCaseView1:UIView!
    var sendCaseView:UIView!
    var btnSCase:UIButton!
    var topView:UIView!
    var lblHeading:UILabel!
    
    
    var viewSaveBack:UIView!
    var viewSave:UIView!
    var btnSaveYes:UIButton!
    var lblHead:UILabel!
    
    var viewSaveChangesBack:UIView!
    var viewChangSave:UIView!
    var btnSaveChangeYes:UIButton!
    
    var u_id = USER_DEFAULTS.string(forKey: "u_id") ?? ""
    var cp_id = USER_DEFAULTS.string(forKey: "cp_id") ?? ""
    
    var max = 0
    var no = 0
    var jsonCount = 0
    
    
    var exel_Id = ""
    var isSelectFavNYard = false
    var isReqUpdate = false
    var commentArray = NSMutableArray()
    var commDataArray = NSMutableArray()
    
    var custVarHeading1 = USER_DEFAULTS.string(forKey: "custoVarHeadingOne") ?? ""
    var custVarHeading2 = USER_DEFAULTS.string(forKey: "custoVarHeadingTwo") ?? ""
    var custVarHeading3 = USER_DEFAULTS.string(forKey: "custoVarHeadingThree") ?? ""
    
    
    var address = ""
    var homePhone = ""
    var mobilePhone = ""
    
    var isSelectSaveBtn = false
    var isSelectNextPre = Bool()
    var isSelectBackToList = Bool()
    var isSelectCustomVariable = false
    var isSelectCustomVariable1 = false
    var isSelectCustomVariable2 = false
    
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
        // Home.h_v = self
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpView()
        //Home.h_v = self
    }
    
    func setUpView() {
        self.layoutIfNeeded()
        mainView = UIView()
        mainView.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT-LoginViewController.VC.NAVHEIGHT-STATUSBARHEIGHT)
        self.addSubview(mainView)
        if(isMapHome == 1){
            bgImgView = UIImageView()
            bgImgView.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT)
            bgImgView.image = UIImage(named: "bcakground.jpg")
            mainView.addSubview(bgImgView)
        }
        let height = mainView.frame.height
        print("height~~~~~~~",height,SHEIGHT)
        lblName = UILabel()
        lblName.setLabel(X: 15, Y: 5, Width: SWIDTH/1.45, Height: 20, TextColor: .white, BackColor: .clear, Text: "Name", TextAlignment: .left, Font: .boldSystemFont(ofSize: 17))
        mainView.addSubview(lblName)
        
        lblHouseNo = UILabel()
        lblHouseNo.setLabel(X: 15, Y: lblName.frame.maxY+5, Width: SWIDTH/1.7, Height: 20, TextColor: .white, BackColor: .clear, Text: "House Number", TextAlignment: .left, Font: .systemFont(ofSize: 16))
        mainView.addSubview(lblHouseNo)
        
        lblStreetCity = UILabel()
        lblStreetCity.setLabel(X: 15, Y: lblHouseNo.frame.maxY+5, Width: SWIDTH/1.7, Height: 20, TextColor: .white, BackColor: .clear, Text: "City", TextAlignment: .left, Font: .systemFont(ofSize: 16))
        mainView.addSubview(lblStreetCity)
        
        btnInfo = UIButton()
        btnInfo.setButton(X: SWIDTH*0.774, Y: 2, Width: 40, Height: 40, TextColor: .white, BackColor: .clear, Title: "", Align: .center, Font: .systemFont(ofSize: 16))
        btnInfo.setImage(UIImage(named: "info.png"), for: .normal)
        btnInfo.tag = 1
        btnInfo.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        mainView.addSubview(btnInfo)
        
        btnMap = UIButton()
        btnMap.setButton(X: btnInfo.frame.maxX, Y: -2, Width: 50, Height: 50, TextColor: .white, BackColor: .clear, Title: "", Align: .center, Font: .systemFont(ofSize: 16))
        btnMap.setImage(UIImage(named: "MAP.png"), for: .normal)
        btnMap.tag = 2
        btnMap.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        mainView.addSubview(btnMap)
        
        btnHomePhoneNo = UIButton(type: .system)
        btnHomePhoneNo.setButton(X: lblHouseNo.frame.maxX, Y: lblName.frame.maxY+15, Width: 250, Height: 15, TextColor: .white, BackColor: .clear, Title: "H : (789) 456-1230", Align: .left,Font: .systemFont(ofSize: 14))
        btnHomePhoneNo.tag = 3
        btnHomePhoneNo.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        mainView.addSubview(btnHomePhoneNo)
        btnMobilePhoneNo = UIButton(type: .system)
        btnMobilePhoneNo.setButton(X: lblStreetCity.frame.maxX, Y: btnHomePhoneNo.frame.maxY+5, Width: 250, Height: 15, TextColor: .white, BackColor: .clear, Title: "M : (789) 456-1230", Align: .left,Font: .systemFont(ofSize: 14))
        btnMobilePhoneNo.tag = 4
        btnMobilePhoneNo.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        mainView.addSubview(btnMobilePhoneNo)
        
        lblEmail = UILabel()
        lblEmail.setLabel(X: 15, Y: lblStreetCity.frame.maxY+5, Width: SWIDTH-30, Height: 20, TextColor: .white, BackColor: .clear, Text: "osiya@gmail.com", TextAlignment: .left, Font: .systemFont(ofSize: 16))
        mainView.addSubview(lblEmail)
        
        lblLine = UILabel()
        lblLine.setLabel(X: 0, Y: lblEmail.frame.maxY+9, Width: SWIDTH, Height: 1, TextColor: .clear, BackColor: APPBUTTONCOLOR1, Text: "", TextAlignment: .left, Font: .systemFont(ofSize: 16))
        mainView.addSubview(lblLine)
        
        lblLastVote = UILabel()
        lblLastVote.setLabel(X: 15, Y: lblLine.frame.maxY+9, Width: SWIDTH-30, Height: 20, TextColor: .white, BackColor: .clear, Text: "Last Vote: 15/01/2020", TextAlignment: .left, Font: .systemFont(ofSize: 16))
        var myMutableString = NSMutableAttributedString(string: "Last Vote : 15/01/2020", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 15)])
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:11))
        lblLastVote.attributedText = myMutableString
        mainView.addSubview(lblLastVote)
        
        lblSex = UILabel()
        lblSex.setLabel(X: 15, Y: lblLastVote.frame.maxY+5, Width: SWIDTH/3, Height: 20, TextColor: .white, BackColor: .clear, Text: "Sex: Female", TextAlignment: .left, Font: .systemFont(ofSize: 16))
        myMutableString = NSMutableAttributedString(string: "Sex : Female", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 15)])
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:5))
        lblSex.attributedText = myMutableString
        mainView.addSubview(lblSex)
        
        lblParty = UILabel()
        lblParty.setLabel(X: lblSex.frame.maxX, Y: lblLastVote.frame.maxY+5, Width: SWIDTH/3+5, Height: 20, TextColor: .white, BackColor: .clear, Text: "Party: Dem", TextAlignment: .left, Font: .systemFont(ofSize: 16))
        myMutableString = NSMutableAttributedString(string: "Party : Dem", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 15)])
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:7))
        lblParty.attributedText = myMutableString
        lblParty.lineBreakMode = .byClipping
        mainView.addSubview(lblParty)
        
        lblRating = UILabel()
        lblRating.setLabel(X: lblParty.frame.maxX, Y: lblLastVote.frame.maxY+5, Width: SWIDTH/3, Height: 20, TextColor: .white, BackColor: .clear, Text: "Rating: 5", TextAlignment: .left, Font: .systemFont(ofSize: 16))
        myMutableString = NSMutableAttributedString(string: "Rating : 5", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 15)])
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:8))
        lblRating.attributedText = myMutableString
        mainView.addSubview(lblRating)
        
        lblAge = UILabel()
        lblAge.setLabel(X: 15, Y: lblSex.frame.maxY+5, Width: SWIDTH/3, Height: 20, TextColor: .white, BackColor: .clear, Text: "Age: 24", TextAlignment: .left, Font: .systemFont(ofSize: 16))
        myMutableString = NSMutableAttributedString(string: "Age : 24", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 15)])
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:5))
        lblAge.attributedText = myMutableString
        mainView.addSubview(lblAge)
        
        lblRace = UILabel()
        lblRace.setLabel(X: lblAge.frame.maxX, Y: lblSex.frame.maxY+5, Width: SWIDTH/2.5+2, Height: 20, TextColor: .white, BackColor: .clear, Text: "Race: H", TextAlignment: .left, Font: .systemFont(ofSize: 16))
        myMutableString = NSMutableAttributedString(string: "Race : H", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 15)])
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:6))
        lblRace.attributedText = myMutableString
        lblRace.lineBreakMode = .byClipping
        mainView.addSubview(lblRace)
        
        lblVoted = UILabel()
        lblVoted.setLabel(X: lblRace.frame.maxX, Y: lblRating.frame.maxY+5, Width: SWIDTH/3, Height: 20, TextColor: .green, BackColor: .clear, Text: "VOTED", TextAlignment: .left, Font: .systemFont(ofSize: 16.0))
        mainView.addSubview(lblVoted)
        
        lblLine = UILabel()
        lblLine.setLabel(X: 0, Y: lblRace.frame.maxY+9, Width: SWIDTH, Height: 1, TextColor: .clear, BackColor: APPBUTTONCOLOR1, Text: "", TextAlignment: .left, Font: .systemFont(ofSize: 16))
        mainView.addSubview(lblLine)
        
        lblCustVar1 = UILabel()
        lblCustVar1.setLabel(X: 16, Y: lblLine.frame.maxY+9, Width: SWIDTH/3.2, Height: 20, TextColor: .black, BackColor: .clear, Text: "Var 11", TextAlignment: .left, Font: .systemFont(ofSize: 15))
        mainView.addSubview(lblCustVar1)
        lblCustVar2 = UILabel()
        lblCustVar2.setLabel(X: lblCustVar1.frame.maxX+1, Y: lblLine.frame.maxY+9, Width: SWIDTH/3.2, Height: 20, TextColor: .black, BackColor: .clear, Text: "Var 22", TextAlignment: .left, Font: .systemFont(ofSize: 15))
        mainView.addSubview(lblCustVar2)
        lblCustVar3 = UILabel()
        lblCustVar3.setLabel(X: lblCustVar2.frame.maxX+1, Y: lblLine.frame.maxY+9, Width: SWIDTH/3.2, Height: 20, TextColor: .black, BackColor: .clear, Text: "Var 33", TextAlignment: .left, Font: .systemFont(ofSize: 15))
        mainView.addSubview(lblCustVar3)
        
        btnCustVar1 = UIButton(type: .system)
        btnCustVar1.setButton(X: 15, Y: lblCustVar1.frame.maxY+2, Width: SWIDTH/3.5, Height: 25, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "var-1", Align: .left, Font: .systemFont(ofSize: 14))
        btnCustVar1.tag = 5
        btnCustVar1.titleEdgeInsets.left = 5;
        btnCustVar1.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        mainView.addSubview(btnCustVar1)
        
        arrowImg = UIImageView()
        arrowImg.setImageView(X: btnCustVar1.frame.maxX-25, Y: lblCustVar1.frame.maxY+5, Width: 20, Height: 20, img: UIImage(named: "down-arrow.png")!)
        mainView.addSubview(arrowImg)
        
        btnCustVar2 = UIButton(type: .system)
        btnCustVar2.setButton(X: lblCustVar1.frame.maxX, Y: lblCustVar1.frame.maxY+2, Width: SWIDTH/3.5, Height: 25, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "var-2", Align: .left,Font: .systemFont(ofSize: 14))
        btnCustVar2.tag = 6
        btnCustVar2.titleEdgeInsets.left = 5;
        btnCustVar2.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        mainView.addSubview(btnCustVar2)
        
        arrowImg1 = UIImageView()
        arrowImg1.setImageView(X: btnCustVar2.frame.maxX-25, Y: lblCustVar1.frame.maxY+5, Width: 20, Height: 20, img: UIImage(named: "down-arrow.png")!)
        mainView.addSubview(arrowImg1)
        
        btnCustVar3 = UIButton(type: .system)
        btnCustVar3.setButton(X: lblCustVar2.frame.maxX, Y: lblCustVar1.frame.maxY+2, Width: SWIDTH/3.5, Height: 25, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "var-3", Align: .left,Font: .systemFont(ofSize: 14))
        btnCustVar3.tag = 7
        btnCustVar3.titleEdgeInsets.left = 5;
        btnCustVar3.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        mainView.addSubview(btnCustVar3)
        
        arrowImg2 = UIImageView()
        arrowImg2.setImageView(X: btnCustVar3.frame.maxX-25, Y: lblCustVar1.frame.maxY+5, Width: 20, Height: 20, img: UIImage(named: "down-arrow.png")!)
        mainView.addSubview(arrowImg2)
        
        favrabilityView = UIView()
        favrabilityView.frame = CGRect(x: 15, y: btnCustVar1.frame.maxY+20, width: SWIDTH-SWIDTH/2.5-15, height: height*0.224)
        favrabilityView.backgroundColor = APPPBACKCOLOR
        favrabilityView.layer.cornerRadius = 10.0
        mainView.addSubview(favrabilityView)
        
        lblFavrability = UILabel()
        lblFavrability.setLabel(X: 0, Y: 3, Width: favrabilityView.frame.width, Height: 25, TextColor: .black, BackColor: .clear, Text: "Favorability", TextAlignment: .center, Font: .systemFont(ofSize: 18))
        favrabilityView.addSubview(lblFavrability)
        
        btnFavor = UIButton()
        btnFavor.setButton(X: 10, Y: lblFavrability.frame.maxY+5, Width: favrabilityView.frame.width-20, Height: favrabilityView.frame.height/5.5, TextColor: .white, BackColor: .clear, Title: " Favor", Align: .left,Font: .systemFont(ofSize: 14))
        btnFavor.setImage(UIImage(named: "unchecked.png"), for: .normal)
        btnFavor.setImage(UIImage(named: "checked.png"), for: .selected)
        btnFavor.tag = 1
        btnFavor.addTarget(self, action: #selector(onClickFavNYard(_:)), for: .touchUpInside)
        favrabilityView.addSubview(btnFavor)
        
        btnNotFavor = UIButton()
        btnNotFavor.setButton(X: 10, Y: btnFavor.frame.maxY, Width: favrabilityView.frame.width-20, Height: favrabilityView.frame.height/5.5, TextColor: .white, BackColor: .clear, Title: " Not Favor", Align: .left,Font: .systemFont(ofSize: 14))
        btnNotFavor.setImage(UIImage(named: "unchecked.png"), for: .normal)
        btnNotFavor.setImage(UIImage(named: "checked.png"), for: .selected)
        btnNotFavor.tag = 2
        btnNotFavor.addTarget(self, action: #selector(onClickFavNYard(_:)), for: .touchUpInside)
        favrabilityView.addSubview(btnNotFavor)
        
        btnUndecided = UIButton()
        btnUndecided.setButton(X: 10, Y: btnNotFavor.frame.maxY, Width: favrabilityView.frame.width-20, Height: favrabilityView.frame.height/5.5, TextColor: .white, BackColor: .clear, Title: " Undecided", Align: .left,Font: .systemFont(ofSize: 14))
        btnUndecided.setImage(UIImage(named: "unchecked.png"), for: .normal)
        btnUndecided.setImage(UIImage(named: "checked.png"), for: .selected)
        btnUndecided.tag = 3
        btnUndecided.addTarget(self, action: #selector(onClickFavNYard(_:)), for: .touchUpInside)
        favrabilityView.addSubview(btnUndecided)
        
        btnNotAvailable = UIButton()
        btnNotAvailable.setButton(X: 10, Y: btnUndecided.frame.maxY, Width: favrabilityView.frame.width-20, Height: favrabilityView.frame.height/5.5, TextColor: .white, BackColor: .clear, Title: " Not Available", Align: .left,Font: .systemFont(ofSize: 14))
        btnNotAvailable.setImage(UIImage(named: "unchecked.png"), for: .normal)
        btnNotAvailable.setImage(UIImage(named: "checked.png"), for: .selected)
        btnNotAvailable.tag = 4
        btnNotAvailable.addTarget(self, action: #selector(onClickFavNYard(_:)), for: .touchUpInside)
        favrabilityView.addSubview(btnNotAvailable)
        
        yardSignView = UIView()
        yardSignView.frame = CGRect(x: favrabilityView.frame.maxX+10, y: btnCustVar3.frame.maxY+20, width: SWIDTH/2.5-25, height: height*0.16)
        yardSignView.backgroundColor = APPPBACKCOLOR
        yardSignView.layer.cornerRadius = 10.0
        mainView.addSubview(yardSignView)
        
        lblYardSign = UILabel()
        lblYardSign.setLabel(X: 0, Y: 3, Width: yardSignView.frame.width, Height: 25, TextColor: .black, BackColor: .clear, Text: "Yard Sign?", TextAlignment: .center, Font: .systemFont(ofSize: 18))
        yardSignView.addSubview(lblYardSign)
        
        btnYes = UIButton()
        btnYes.setButton(X: 10, Y: lblYardSign.frame.maxY+5, Width: yardSignView.frame.width/2, Height: yardSignView.frame.height/4, TextColor: .white, BackColor: .clear, Title: " Yes", Align: .left,Font: .systemFont(ofSize: 14))
        btnYes.setImage(UIImage(named: "unchecked.png"), for: .normal)
        btnYes.setImage(UIImage(named: "checked.png"), for: .selected)
        btnYes.tag = 5
        btnYes.addTarget(self, action: #selector(onClickFavNYard(_:)), for: .touchUpInside)
        yardSignView.addSubview(btnYes)
        
        btnNo = UIButton()
        btnNo.setButton(X: btnYes.frame.maxX+2, Y: lblYardSign.frame.maxY+5, Width: yardSignView.frame.width/2, Height: yardSignView.frame.height/4, TextColor: .white, BackColor: .clear, Title: " No", Align: .left,Font: .systemFont(ofSize: 14))
        btnNo.setImage(UIImage(named: "unchecked.png"), for: .normal)
        btnNo.setImage(UIImage(named: "checked.png"), for: .selected)
        btnNo.tag = 6
        btnNo.addTarget(self, action: #selector(onClickFavNYard(_:)), for: .touchUpInside)
        yardSignView.addSubview(btnNo)
        
        btnPlaced = UIButton()
        btnPlaced.setButton(X: 10, Y: btnYes.frame.maxY+5, Width: yardSignView.frame.width-20, Height: yardSignView.frame.height/4, TextColor: .white, BackColor: .clear, Title: " Placed", Align: .left,Font: .systemFont(ofSize: 14))
        btnPlaced.setImage(UIImage(named: "unchecked.png"), for: .normal)
        btnPlaced.setImage(UIImage(named: "checked.png"), for: .selected)
        btnPlaced.tag = 7
        btnPlaced.addTarget(self, action: #selector(onClickFavNYard(_:)), for: .touchUpInside)
        yardSignView.addSubview(btnPlaced)
        
        
        commentView = UIView()
        mainView.addSubview(commentView)
        commentView.anchor(top: favrabilityView.bottomAnchor, paddingTop: 20, bottom: mainView.bottomAnchor, paddingBottom: 10, left: mainView.leftAnchor, paddingLeft: 15, right: mainView.rightAnchor, paddingRight: SWIDTH/2.5, width: 0, height: 0)
        commentView.backgroundColor = APPPBACKCOLOR
        commentView.layer.cornerRadius = 10.0
        print("commentView",commentView.heightAnchor.hashValue)
        
        
        lblComment = UILabel()
        commentView.addSubview(lblComment)
        lblComment.anchor(top: commentView.topAnchor, paddingTop: 5, bottom: nil, paddingBottom: 0, left: commentView.leftAnchor, paddingLeft: 10, right: nil, paddingRight: 0, width: commentView.frame.width/1.3, height: 20)
        lblComment.text = "Comments"
        
        btnAddComm = UIButton()
        commentView.addSubview(btnAddComm)
        btnAddComm.setButton1(TextColor: .white, BackColor: .black, Title: "+", Align: .center,Font: .boldSystemFont(ofSize: 25))
        btnAddComm.anchor(top: commentView.topAnchor, paddingTop: 5, bottom: nil, paddingBottom: 0, left: nil, paddingLeft: 0, right: commentView.rightAnchor, paddingRight: 5, width: commentView.frame.width/3, height: 20)
        btnAddComm.tag = 15
        btnAddComm.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        
        commentView.addSubview(commTableView)
        commTableView.anchor(top: lblComment.bottomAnchor, paddingTop: 5, bottom: commentView.bottomAnchor, paddingBottom: 10, left: commentView.leftAnchor, paddingLeft: 5, right: commentView.rightAnchor, paddingRight: 5, width: 0, height: 0)
        commTableView.backgroundColor = UIColor.clear
        commTableView.delegate = self
        commTableView.dataSource = self
        commTableView.register(commentCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        commTableView.tableFooterView = UIView()
        commTableView.layer.cornerRadius = 10.0
        commTableView.estimatedRowHeight = 44.0
        commTableView.rowHeight = UITableView.automaticDimension
        
        
        
        btnSendCase = UIButton(type: .system)
        mainView.addSubview(btnSendCase)
        btnSendCase.anchor(top: favrabilityView.bottomAnchor, paddingTop: 0, bottom: nil, paddingBottom: 0, left: commentView.rightAnchor, paddingLeft: 15, right: mainView.rightAnchor, paddingRight: 15, width: 0, height: SHEIGHT/16.6)
        btnSendCase.setButton1(TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Send Case", Align: .center,Font: .systemFont(ofSize: 16))
        btnSendCase.tag = 16
        btnSendCase.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        btnSendCase.tintColor = UIColor.clear
        
        
        btnSave = UIButton(type: .system)
        mainView.addSubview(btnSave)
        btnSave.anchor(top: btnSendCase.bottomAnchor, paddingTop: 8, bottom: nil, paddingBottom: 0, left: commentView.rightAnchor, paddingLeft: 15, right: mainView.rightAnchor, paddingRight: 15, width: 0, height: SHEIGHT/16.6)
        btnSave.setButton1(TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Save", Align: .center,Font: .systemFont(ofSize: 16))
        btnSave.tag = 17
        btnSave.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        btnSave.tintColor = UIColor.clear
        
        
        btnBackToList = UIButton(type: .system)
        mainView.addSubview(btnBackToList)
        btnBackToList.anchor(top: btnSave.bottomAnchor, paddingTop: 8, bottom: nil, paddingBottom: 0, left: commentView.rightAnchor, paddingLeft: 15, right: mainView.rightAnchor, paddingRight: 15, width: 0, height: SHEIGHT/16.6)
        btnBackToList.setButton1(TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Back To List", Align: .center,Font: .systemFont(ofSize: 16))
        btnBackToList.tag = 18
        btnBackToList.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        btnBackToList.tintColor = UIColor.clear
        
        print("btnBackToList~~~~~~",SHEIGHT/16.7)
        
        btnPrevious = UIButton(type: .custom)
        mainView.addSubview(btnPrevious)
        btnPrevious.anchor(top: btnBackToList.bottomAnchor, paddingTop: 1, bottom: nil, paddingBottom: 0, left: commentView.rightAnchor, paddingLeft: 15, right: nil, paddingRight: 0, width: yardSignView.frame.width/2-5, height: SHEIGHT/13)
        btnPrevious.setButton1(TextColor: .white, BackColor: .clear, Title: "", Align: .center,Font: .systemFont(ofSize: 16))
        btnPrevious.tag = 19
        btnPrevious.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        btnPrevious.setImage(UIImage(named: "LeftArrow.png"), for: .normal)
        // btnPrevious.imageView!.contentMode = .scaleAspectFit
        
        btnNext = UIButton(type: .custom)
        mainView.addSubview(btnNext)
        btnNext.anchor(top: btnBackToList.bottomAnchor, paddingTop: 1, bottom: nil, paddingBottom: 0, left: btnPrevious.rightAnchor, paddingLeft: 5, right: mainView.rightAnchor, paddingRight: 15, width: yardSignView.frame.width/2, height: SHEIGHT/13)
        btnNext.setButton1(TextColor: .white, BackColor: .clear, Title: "", Align: .center,Font: .systemFont(ofSize: 16))
        btnNext.tag = 20
        btnNext.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        btnNext.setImage(UIImage(named: "rightArrow.png"), for: .normal)
        //btnNext.imageView!.contentMode = .scaleAspectFit
        
        if isSelectPollParishWard == -1 {
            callGetExcel(u_id: u_id, cp_id: cp_id, startRow: 0)
        }
        
        
        commView = UIView()
        commView.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT)
        mainView.addSubview(commView)
        
        commView1 = UIView()
        commView1.frame = CGRect(x: 10, y: 100, width: SWIDTH-20, height: SHEIGHT/2)
        commView1.backgroundColor = UIColor.white
        commView1.layer.cornerRadius = 10
        commView1.clipsToBounds = true
        commView.addSubview(commView1)
        
        topView = UIView()
        topView.frame = CGRect(x: 0, y: 0, width: commView1.frame.width, height: 50)
        topView.backgroundColor = APPBUTTONCOLOR
        commView1.addSubview(topView)
        
        lblMsg = UILabel()
        lblMsg.setLabel(X: 0, Y: 10, Width: commView1.frame.width, Height: 30, TextColor: .white, BackColor: .clear, Text: "Add Comment", TextAlignment: .center, Font: .boldSystemFont(ofSize: 18.0))
        topView.addSubview(lblMsg)
        
        
        btnCancel = UIButton(type: .system)
        btnCancel.setButton(X: topView.frame.width-30, Y: 13, Width: 24, Height: 24, TextColor: .white, BackColor: .clear, Title: "X", Align: .center,Font: .boldSystemFont(ofSize: 22.0))
        btnCancel.tag = 22
        btnCancel.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        btnCancel.tintColor = UIColor.clear
        topView.addSubview(btnCancel)
        
        
        
        
        txtCommView = UITextView()
        txtCommView.frame = CGRect(x: 5, y: topView.frame.maxY+8, width: commView1.frame.width-10, height: commView1.frame.height/2.5)
        txtCommView.text = "Comment"
        txtCommView.font = UIFont.systemFont(ofSize: 16.0)
        txtCommView.textColor = UIColor.lightGray
        txtCommView.delegate = self
        txtCommView.layer.borderColor = UIColor.lightGray.cgColor
        txtCommView.layer.borderWidth = 1.0
        txtCommView.layer.cornerRadius = 10.0
        commView1.addSubview(txtCommView)
        
        btnPublic = UIButton()
        btnPublic.setButton(X: 15, Y: txtCommView.frame.maxY+5, Width: commView1.frame.width, Height: 30, TextColor: APPBUTTONCOLOR, BackColor: .clear, Title: " Public", Align: .left,Font: .systemFont(ofSize: 17))
        btnPublic.setImage(UIImage(named: "unchecked.png"), for: .normal)
        btnPublic.setImage(UIImage(named: "checked.png"), for: .selected)
        btnPublic.tag = 34
        btnPublic.isSelected = true
        btnPublic.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        btnPublic.tintColor = UIColor.clear
        commView1.addSubview(btnPublic)
        
        btnPrivate = UIButton()
        btnPrivate.setButton(X: 15, Y: btnPublic.frame.maxY+5, Width: commView1.frame.width, Height: 30, TextColor: APPBUTTONCOLOR, BackColor: .clear, Title: " Private", Align: .left,Font: .systemFont(ofSize: 17))
        btnPrivate.setImage(UIImage(named: "unchecked.png"), for: .normal)
        btnPrivate.setImage(UIImage(named: "checked.png"), for: .selected)
        btnPrivate.tag = 33
        btnPrivate.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        btnPrivate.tintColor = UIColor.clear
        commView1.addSubview(btnPrivate)
        
        btnOk = UIButton(type: .system)
        btnOk.setButton(X: commView1.frame.width/2-(commView1.frame.width/2-40)/2, Y: btnPrivate.frame.maxY+20, Width: commView1.frame.width/2-40, Height: 35, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Ok", Align: .center,Font: .systemFont(ofSize: 16))
        btnOk.tag = 21
        btnOk.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        btnOk.tintColor = UIColor.clear
        commView1.addSubview(btnOk)
        
        commView.isHidden = true
        
        viewTable = UIView()
        viewTable.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: height)
        viewTable.backgroundColor = APPPBACKCOLOR
        mainView.addSubview(viewTable)
        
        TableView.frame = CGRect(x: 0, y: 0, width: viewTable.frame.width, height: viewTable.frame.height)
        TableView.backgroundColor = UIColor.clear
        TableView.delegate = self
        TableView.dataSource = self
        TableView.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier1)
        viewTable.addSubview(TableView)
        TableView.tableFooterView = UIView()
        
        btnClose = UIButton()
        btnClose.setButton(X: SWIDTH-70, Y: 5, Width: 70, Height: 25, TextColor: .white, BackColor: .clear, Title: "X", Align: .center, Font: .boldSystemFont(ofSize: 22.0))
        btnClose.tag = 23
        btnClose.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        viewTable.addSubview(btnClose)
        viewTable.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.Notify), name: NSNotification.Name(rawValue: "nameOfNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateCustomVarOnHome), name: NSNotification.Name(rawValue: "onClickCustomVar"), object: nil)
        
        
        if custVarHeading1 == ""{
            lblCustVar1.isHidden = true
            btnCustVar1.isHidden = true
            arrowImg.isHidden = true
        }else{
            lblCustVar1.text = custVarHeading1
        }
        if custVarHeading2 == ""{
            lblCustVar2.isHidden = true
            btnCustVar2.isHidden = true
            arrowImg1.isHidden = true
        }else{
            lblCustVar2.text = custVarHeading2
        }
        if custVarHeading3 == ""{
            lblCustVar3.isHidden = true
            btnCustVar3.isHidden = true
            arrowImg2.isHidden = true
        }else{
            lblCustVar3.text = custVarHeading3
        }
        
        sendCaseView1 = UIView()
        sendCaseView1.frame = CGRect(x: 0, y: 0, width: mainView.frame.width, height: mainView.frame.height)
        sendCaseView1.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        mainView.addSubview(sendCaseView1)
        
        sendCaseView = UIView()
        sendCaseView.frame = CGRect(x: 40, y: 250, width: SWIDTH-80, height: 160)
        sendCaseView.backgroundColor = UIColor.white
        sendCaseView.layer.cornerRadius = 10.0
        sendCaseView.clipsToBounds = true
        sendCaseView1.addSubview(sendCaseView)
        
        topView = UIView()
        topView.frame = CGRect(x: 0, y: 0, width: sendCaseView.frame.width, height: 50)
        topView.backgroundColor = APPBUTTONCOLOR
        sendCaseView.addSubview(topView)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 0, Y: 2, Width: sendCaseView.frame.width, Height: 46, TextColor: .white, BackColor: .clear, Text: "Do You Want To Send Case?", TextAlignment: .center, Font: .boldSystemFont(ofSize: 16))
        topView.addSubview(lblHeading)
        
        btnSCase = UIButton(type: .system)
        btnSCase.setButton(X: topView.frame.width-30, Y: 13, Width: 24, Height: 24, TextColor: .white, BackColor: .clear, Title: "X", Align: .center, Font: .boldSystemFont(ofSize: 22))
        btnSCase.tag = 9
        btnSCase.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        topView.addSubview(btnSCase)
        
        btnSCase = UIButton(type: .system)
        btnSCase.setButton(X: 60, Y: topView.frame.maxY+10, Width: sendCaseView.frame.width-120, Height: 40, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Send Mail", Align: .center, Font: .systemFont(ofSize: 16.0))
        btnSCase.tag = 10
        btnSCase.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        sendCaseView.addSubview(btnSCase)
        
        btnSCase = UIButton(type: .system)
        btnSCase.setButton(X: 60, Y: topView.frame.maxY+60, Width: sendCaseView.frame.width-120, Height: 40, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Send Message", Align: .center, Font: .systemFont(ofSize: 16.0))
        btnSCase.tag = 11
        btnSCase.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        sendCaseView.addSubview(btnSCase)
        
        sendCaseView1.isHidden = true
        
        viewSaveBack = UIView()
        viewSaveBack.frame = CGRect(x: 0, y: 0, width: mainView.frame.width, height: mainView.frame.height)
        viewSaveBack.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        mainView.addSubview(viewSaveBack)
        
        viewSave = UIView()
        viewSave.frame = CGRect(x: 40, y: 250, width: SWIDTH-80, height: 160)
        viewSave.backgroundColor = UIColor.white
        viewSave.layer.cornerRadius = 10.0
        viewSave.clipsToBounds = true
        viewSaveBack.addSubview(viewSave)
        
        topView = UIView()
        topView.frame = CGRect(x: 0, y: 0, width: viewSave.frame.width, height: 50)
        topView.backgroundColor = APPBUTTONCOLOR
        viewSave.addSubview(topView)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 0, Y: 2, Width: viewSave.frame.width, Height: 46, TextColor: .white, BackColor: .clear, Text: "Save", TextAlignment: .center, Font: .boldSystemFont(ofSize: 16))
        topView.addSubview(lblHeading)
        
        btnSaveYes = UIButton(type: .system)
        btnSaveYes.setButton(X: topView.frame.width-30, Y: 13, Width: 24, Height: 24, TextColor: .white, BackColor: .clear, Title: "X", Align: .center, Font: .boldSystemFont(ofSize: 22))
        btnSaveYes.tag = 12
        btnSaveYes.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        topView.addSubview(btnSaveYes)
        
        lblHead = UILabel()
        lblHead.setLabel(X: 0, Y: topView.frame.maxY+10, Width: viewSave.frame.width, Height: 50, TextColor: APPBUTTONCOLOR, BackColor: .clear, Text: "", TextAlignment: .center, Font: .boldSystemFont(ofSize: 16))
        lblHead.numberOfLines = 0
        lblHead.lineBreakMode = .byWordWrapping
        viewSave.addSubview(lblHead)
        
        btnSaveYes = UIButton(type: .system)
        btnSaveYes.setButton(X: viewSave.frame.width/8, Y: topView.frame.maxY+70, Width: viewSave.frame.width/3, Height: 35, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Cancel", Align: .center, Font: .systemFont(ofSize: 16.0))
        btnSaveYes.tag = 13
        btnSaveYes.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        viewSave.addSubview(btnSaveYes)
        
        btnSaveYes = UIButton(type: .system)
        btnSaveYes.setButton(X: viewSave.frame.width/1.8, Y: topView.frame.maxY+70, Width: viewSave.frame.width/3, Height: 35, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Save", Align: .center, Font: .systemFont(ofSize: 16.0))
        btnSaveYes.tag = 14
        btnSaveYes.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        viewSave.addSubview(btnSaveYes)
        viewSaveBack.isHidden = true
        
        
        
        viewSaveChangesBack = UIView()
        viewSaveChangesBack.frame = CGRect(x: 0, y: 0, width: mainView.frame.width, height: mainView.frame.height)
        viewSaveChangesBack.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        mainView.addSubview(viewSaveChangesBack)
        
        viewChangSave = UIView()
        viewChangSave.frame = CGRect(x: 40, y: 250, width: SWIDTH-80, height: 160)
        viewChangSave.backgroundColor = UIColor.white
        viewChangSave.layer.cornerRadius = 10.0
        viewChangSave.clipsToBounds = true
        viewSaveChangesBack.addSubview(viewChangSave)
        
        topView = UIView()
        topView.frame = CGRect(x: 0, y: 0, width: viewChangSave.frame.width, height: 50)
        topView.backgroundColor = APPBUTTONCOLOR
        viewChangSave.addSubview(topView)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 0, Y: 2, Width: viewChangSave.frame.width, Height: 46, TextColor: .white, BackColor: .clear, Text: "Do You Want To Save Changes?", TextAlignment: .center, Font: .boldSystemFont(ofSize: 16))
        lblHeading.numberOfLines = 0
        topView.addSubview(lblHeading)
        
        btnSaveChangeYes = UIButton(type: .system)
        btnSaveChangeYes.setButton(X: topView.frame.width-30, Y: 13, Width: 24, Height: 24, TextColor: .white, BackColor: .clear, Title: "X", Align: .center, Font: .boldSystemFont(ofSize: 22))
        btnSaveChangeYes.tag = 30
        btnSaveChangeYes.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        topView.addSubview(btnSaveChangeYes)
        
        btnSaveChangeYes = UIButton(type: .system)
        btnSaveChangeYes.setButton(X: 60, Y: topView.frame.maxY+15, Width: viewChangSave.frame.width-120, Height: 35, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "Yes", Align: .center, Font: .systemFont(ofSize: 16.0))
        btnSaveChangeYes.tag = 31
        btnSaveChangeYes.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        viewChangSave.addSubview(btnSaveChangeYes)
        
        btnSaveChangeYes = UIButton(type: .system)
        btnSaveChangeYes.setButton(X: 60, Y: topView.frame.maxY+65, Width: viewChangSave.frame.width-120, Height: 35, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "No", Align: .center, Font: .systemFont(ofSize: 16.0))
        btnSaveChangeYes.tag = 32
        btnSaveChangeYes.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        viewChangSave.addSubview(btnSaveChangeYes)
        
        viewSaveChangesBack.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateOnClickMapHomeView), name: NSNotification.Name(rawValue: "onclickMap"), object: nil)
        setMyPeopleApp()
    }
    
    
    func setMyPeopleApp() {
        if isAppMyPeople {
            lblFavrability.text = "Satisfaction"
            lblYardSign.text = "Case Status"
            btnFavor.setTitle(" Satisfied", for: .normal)
            btnNotFavor.setTitle(" Not Satisfied", for: .normal)
            btnUndecided.setTitle(" Undecided", for: .normal)
            btnNotAvailable.setTitle(" Never Contacted", for: .normal)
            btnYes.setTitle(" Open", for: .normal)
            btnNo.setTitle(" Closed", for: .normal)
            btnPlaced.isHidden = true
            btnNo.frame = CGRect(x: 10, y: btnYes.frame.maxY+10, width: yardSignView.frame.width-20, height: yardSignView.frame.height/4)
        }
    }
    
    @objc func Notify(notif: NSNotification) {
        if let type = notif.userInfo?["type"] as? String {
            print("type~~~~~~",type)
            if type == "0"{
                let results = jsonObj.value(forKey: "data") as! NSArray
                max = results.count
                print("type~~~~max~~",max)
                viewTable.isHidden = false
                self.TableView.reloadData()
            }
//            else if type == "1"{
//                no = noRow-1
//                USER_DEFAULTS.set(no, forKey: "case")
//                USER_DEFAULTS.set(no, forKey: "case1")
//                self.SetData()
//            }
            
        }
    }
    @objc func updateCustomVarOnHome(notif: NSNotification) {
        print("selectCustVarHomeTxt1~~~~",selectCustVarHomeTxt1)
        if selectCustVarHomeTxt1 != "" {
            isSelectCustomVariable = true
            btnCustVar1.setTitle(selectCustVarHomeTxt1, for: .normal)
        }
        if selectCustVarHomeTxt2 != "" {
            isSelectCustomVariable1 = true
            btnCustVar2.setTitle(selectCustVarHomeTxt2, for: .normal)
        }
        if selectCustVarHomeTxt3 != "" {
            isSelectCustomVariable2 = true
            btnCustVar3.setTitle(selectCustVarHomeTxt3, for: .normal)
        }
        isReqUpdate = true;
        isSelectFavNYard = true
        isSelectSaveBtn = true
    }
    
    @objc func updateOnClickMapHomeView(notif: NSNotification) {
        if let type = notif.userInfo?["type"] as? String {
            print("type~~~~~\(notif.userInfo)~",type)
            if type == "1"{ // SendCase
                sendCaseView1.isHidden = false
                sendCaseView.frame = CGRect(x: 40, y: 80, width: SWIDTH-80, height: 160)
                HomeViewController.VC.mailDic = nil
            }else if type == "2"{//Save
                viewSaveBack.isHidden = false
                viewSave.frame = CGRect(x: 40, y: 60, width: SWIDTH-80, height: 160)
                print("isSelectSaveBtn~~~",isSelectSaveBtn)
                lblHead.text = "Do You Want To Save?"
            }else if type == "3"{//Back to Map
                let isSelectVoted = notif.userInfo?["isSelectVoted"] as? String
                if isSelectVoted == "true"{
                    viewSaveChangesBack.isHidden = false
                    viewChangSave.frame = CGRect(x: 40, y: 60, width: SWIDTH-80, height: 160)
                }else{
                    self.isHidden = true
                }
            }
        }
        
    }
    
    func comment(_ str: String?, type isSel: Bool) -> multiSelcted? {
        let comment = multiSelcted()
        comment.txt = str ?? ""
        comment.isSelected = isSel
        return comment
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txtCommView.text == "Comment" {
            txtCommView.text = ""
            txtCommView.textColor = UIColor.black
        }
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if txtCommView.text.isEmpty {
            txtCommView.text = "Comment"
            txtCommView.textColor = UIColor.lightGray
        }
        txtCommView.resignFirstResponder()
    }
    
    @objc func onClickFavNYard(_ sender: UIButton) {
        isSelectFavNYard = true
        isReqUpdate = true
        isSelectSaveBtn = true
        if (sender.tag == 1) {
            btnFavor.isSelected = !btnFavor.isSelected;
            btnNotFavor.isSelected = false;
            btnUndecided.isSelected = false;
            btnNotAvailable.isSelected = false;
        }else if (sender.tag == 2){
            btnFavor.isSelected = false;
            btnNotFavor.isSelected = !btnNotFavor.isSelected;
            btnUndecided.isSelected = false;
            btnNotAvailable.isSelected = false;
        }else if (sender.tag == 3){
            btnFavor.isSelected = false;
            btnNotFavor.isSelected = false;
            btnUndecided.isSelected = !btnUndecided.isSelected;
            btnNotAvailable.isSelected = false;
        }else if (sender.tag == 4){
            btnFavor.isSelected = false;
            btnNotFavor.isSelected = false;
            btnUndecided.isSelected = false;
            btnNotAvailable.isSelected = !btnNotAvailable.isSelected;
        }else if (sender.tag == 5){
            btnYes.isSelected = !btnYes.isSelected;
            btnPlaced.isSelected = false;
            btnNo.isSelected = false;
        }else if (sender.tag == 6){
            btnYes.isSelected = false;
            btnPlaced.isSelected = false;
            btnNo.isSelected = !btnNo.isSelected;
        }else if (sender.tag == 7){
            btnYes.isSelected = false;
            btnPlaced.isSelected = !btnPlaced.isSelected;
            btnNo.isSelected = false;
        }
    }
    
    @objc func onClickButton(_ sender: UIButton) {
        let NAVHEIGHT = LoginViewController.VC.navigationController!.navigationBar.frame.maxY
        if sender.tag == 1 { // on click Info
            var results = NSArray()
            if(isMapHome == 1){
                results = mapjsonObj.value(forKey: "data") as! NSArray
            }else{
                results = jsonObj.value(forKey: "data") as! NSArray
            }
            let obj = results[no] as? NSArray
            var query: String? = nil
            query = "https://www.google.co.in/search?q=\(lblName.text ?? "") \(obj?[4] ?? "") \(obj?[18] ?? "")"
            query = query?.replacingOccurrences(of: " ", with: "+")
            query = query?.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            if let url = URL(string: query ?? "") {
                UIApplication.shared.openURL(url)
            }
        }
        if sender.tag == 2 { // on click Map
            var addressString = "http://maps.apple.com/?q=\(address)"
            addressString = addressString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
            let url = URL(string: addressString)
            if let url = url {
                UIApplication.shared.openURL(url)
            }
        }
        if sender.tag == 3 { // on click Home Phone
            if !(homePhone == "") {
                let alert = UIAlertController(title: "Home Phone", message: "", preferredStyle: .alert)
                let saveServer = UIAlertAction(title: "Call", style: .default, handler: { action in
                    let phoneNumber = "tel://\(self.homePhone)"
                    self.call(phoneNumber: phoneNumber)
                })
                alert.addAction(saveServer)
                let saveMAssege = UIAlertAction(title: "Message", style: .default, handler: { action in
                    self.message(self.homePhone)
                })
                alert.addAction(saveMAssege)
                let No = UIAlertAction(title: "Cancel", style: .default, handler: { action in
                })
                alert.addAction(No)
                HomeViewController.VC.present(alert, animated: true)
            }
        }
        if sender.tag == 4 { // on click Mobile Phone
            if !(mobilePhone == "") {
                let alert = UIAlertController(title: "Mobile Phone", message: "", preferredStyle: .alert)
                let saveServer = UIAlertAction(title: "Call", style: .default, handler: { action in
                    let phoneNumber = "tel://\(self.mobilePhone)"
                    self.call(phoneNumber: phoneNumber)
                })
                alert.addAction(saveServer)
                let saveMAssege = UIAlertAction(title: "Message", style: .default, handler: { action in
                    self.message(self.mobilePhone)
                })
                alert.addAction(saveMAssege)
                let No = UIAlertAction(title: "Cancel", style: .default, handler: { action in
                })
                alert.addAction(No)
                HomeViewController.VC.present(alert, animated: true)
            }
        }
        if sender.tag == 5 { // on click Cust Var1
            isSelectPopup = 6
            isSelectCustomVar = 1
            let custView = PopUpView()
            custView.custVarMode = 1
            custView.lblHeading.text = custVarHeading1
            custView.isHidden = false
            UIView.transition(with: custView, duration: 0.3, options: .transitionCrossDissolve, animations: {
                custView.frame = CGRect(x: 0, y: NAVHEIGHT, width: SWIDTH, height: SHEIGHT-64)
                if(isMapHome == 1){
                    MapViewController.VC.view.addSubview(custView)
                }else{
                    HomeViewController.VC.view.addSubview(custView)
                }
            })
        }
        if sender.tag == 6 { // on click Cust Var2
            isSelectPopup = 6
            isSelectCustomVar = 2
            let custView = PopUpView()
            custView.custVarMode = 2
            custView.lblHeading.text = custVarHeading2
            custView.isHidden = false
            UIView.transition(with: custView, duration: 0.3, options: .transitionCrossDissolve, animations: {
                custView.frame = CGRect(x: 0, y: NAVHEIGHT, width: SWIDTH, height: SHEIGHT-64)
                if(isMapHome == 1){
                    MapViewController.VC.view.addSubview(custView)
                }else{
                    HomeViewController.VC.view.addSubview(custView)
                }
            })
        }
        if sender.tag == 7 { // on click Cust Var3
            isSelectPopup = 6
            isSelectCustomVar = 3
            let custView = PopUpView()
            custView.custVarMode = 3
            custView.lblHeading.text = custVarHeading3
            custView.isHidden = false
            UIView.transition(with: custView, duration: 0.3, options: .transitionCrossDissolve, animations: {
                custView.frame = CGRect(x: 0, y: NAVHEIGHT, width: SWIDTH, height: SHEIGHT-64)
                if(isMapHome == 1){
                    MapViewController.VC.view.addSubview(custView)
                }else{
                    HomeViewController.VC.view.addSubview(custView)
                }
            })
        }
        if sender.tag == 9{// on click SendCase close
            sendCaseView1.isHidden = true
        }
        if sender.tag == 10{// on click SendCase SendMail
            HomeViewController.VC.strComment = ""
            for i in 0..<commentArray.count{
                let obj = commentArray[i] as! multiSelcted
                if (obj.isSelected){
                    let str = obj.txt.slice(from: "Comment : ", to: "\nFavorability")
                    HomeViewController.VC.strComment = "\(str ?? "")\n\(HomeViewController.VC.strComment)"
                }
            }
            sendCaseView1.isHidden = true
            var results = NSArray()
            if(isMapHome == 1){
                results = mapjsonObj.value(forKey: "data") as! NSArray
            }else{
                results = jsonObj.value(forKey: "data") as! NSArray
            }
            let obj = results[no] as! NSArray
            HomeViewController.VC.callSendCSVApi(typ: obj[20] as! String, id: 11)
        }
        if sender.tag == 11{// on click SendCase SendMessage
            HomeViewController.VC.strComment = ""
            for i in 0..<commentArray.count{
                let obj = commentArray[i] as! multiSelcted
                if (obj.isSelected){
                    let str = obj.txt.slice(from: "Comment : ", to: "\nFavorability")
                    HomeViewController.VC.strComment = "\(str ?? "")\n\(HomeViewController.VC.strComment)"
                }
            }
            sendCaseView1.isHidden = true
            var results = NSArray()
            if(isMapHome == 1){
                results = mapjsonObj.value(forKey: "data") as! NSArray
            }else{
                results = jsonObj.value(forKey: "data") as! NSArray
            }
            let obj = results[no] as! NSArray
            HomeViewController.VC.callSendCSVApi(typ: obj[20] as! String, id: 12)
        }
        if sender.tag == 12{// on click close save popup
            viewSaveBack.isHidden = true
        }
        if sender.tag == 13{// on click cancel save popup
            viewSaveBack.isHidden = true
        }
        if sender.tag == 14{// on click Save popup
            if (isSelectSaveBtn == false){
                isSelectFavNYard = true
            }
            self.updateView()
            viewSaveBack.isHidden = true
        }
        if sender.tag == 16{// on click SendCase
            sendCaseView1.isHidden = false
            self.updateView()
            HomeViewController.VC.mailDic = nil
        }
        if sender.tag == 19 { // on click previous
            if (isReqUpdate){
                isSelectNextPre = false
                viewSaveChangesBack.isHidden = false
            }else{
                self.goBack()
            }
        }
        if sender.tag == 20 { // on click next
            if (isReqUpdate){
                isSelectNextPre = true
                viewSaveChangesBack.isHidden = false
            }else{
                self.goNext()
            }
            
        }
        if sender.tag == 17 { // on click save
            viewSaveBack.isHidden = false
            print("isSelectSaveBtn~~~",isSelectSaveBtn)
            if (isSelectSaveBtn == false){
                lblHead.text = "No Changes Had Been Made. Would You Like To Save?"
            }else{
                lblHead.text = "Do You Want To Save?"
            }
        }
        if sender.tag == 15 { // on click add
            commView.isHidden = false
            txtCommView.text = "Comment"
            txtCommView.textColor = UIColor.lightGray
        }
        if sender.tag == 21 { // on click ok comment
            commView.isHidden = true
            self.addComment()
            isSelectFavNYard = false
            isReqUpdate = true
            txtCommView.text = "Comment"
            txtCommView.textColor = UIColor.lightGray
            txtCommView.resignFirstResponder()
        }
        if sender.tag == 22 { // on click cancel comment
            txtCommView.text = ""
            commView.isHidden = true
            txtCommView.resignFirstResponder()
        }
        if sender.tag == 18 { // on click BackToList
            //print("---home--btnBackToList--------")
            if (isReqUpdate){
                isSelectNextPre = false
                viewSaveChangesBack.isHidden = false
                isSelectBackToList = true
            }else{
                if isMapHome == 1
                {
                    self.isHidden = true
                }else{
                    viewTable.isHidden = false
                    self.TableView.reloadData()
                }
            }
        }
        if sender.tag == 23 { // on click Close
            viewTable.isHidden = true
        }
        
        if sender.tag == 30 { // on click Close save changes popup
            viewSaveChangesBack.isHidden = true
        }
        if sender.tag == 31 { // on click Yes save changes popup
            if isSelectBackToList{
                self.updateView()
                
            }else{
                if (isSelectNextPre == true){
                    self.updateView()
                    self.goNext()
                }else if (isSelectNextPre == false){
                    self.updateView()
                    self.goBack()
                }
                self.isHidden = true
            }
            viewSaveChangesBack.isHidden = true
        }
        if sender.tag == 32 { // on click No save changes popup
            if (isSelectNextPre == true){
                self.goNext()
            }else{
                self.goBack()
            }
            viewSaveChangesBack.isHidden = true
            self.isHidden = true
        }
        
        if (sender.tag == 33) {// on click private
            if btnPublic.isSelected == true{
                btnPublic.isSelected = false;
                btnPrivate.isSelected = true;
            }
            
        }else if (sender.tag == 34){// on click Public
            if btnPrivate.isSelected == true{
                btnPrivate.isSelected = false;
                btnPublic.isSelected = true;
            }
        }
    }
    
    func call(phoneNumber: String) {
        if let url = URL(string: phoneNumber) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:],
                                          completionHandler: {
                                            (success) in
                                            print("Open \(phoneNumber): \(success)")
                })
            } else {
                let success = UIApplication.shared.openURL(url)
                print("Open \(phoneNumber): \(success)")
            }
        }
    }
    
    func message(_ msg: String?) {
        let controller = MFMessageComposeViewController()
        if MFMessageComposeViewController.canSendText() {
            controller.recipients = [msg].compactMap { $0 }
            controller.messageComposeDelegate = self
            HomeViewController.VC.present(controller, animated: true, completion: nil)
        }
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        HomeViewController.VC.dismiss(animated: true, completion: nil)
    }
    
    // callGetExcel
    func callGetExcel(u_id:String,cp_id:String,startRow:Int){
        print("type~~~",type)
        let authToken = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
        var urlStr = ""
        var parameter = ""
        urlStr = "getexcel"
        parameter = "cid=\(cp_id)&id=\(u_id)&startrow=\(startRow)&endrow=\(noRow)&sorts=Personal_LastName&authToken=\(authToken)"
        //}else
        if type == 3 {// For Name
            urlStr = "getexcel"
            parameter = "cid=\(cp_id)&id=\(u_id)&startrow=\(startRow)&endrow=\(noRow)&\(paramStr)&authToken=\(authToken)"
        }else if type == 2 { // For Street Name
            urlStr = "search_by_address"
            parameter = "cid=\(cp_id)&id=\(u_id)&startrow=\(startRow)&endrow=\(noRow)&\(paramStr)&\(nextPrevStr)&authToken=\(authToken)"
        }else if type == 8 { // For poll Watching Mode
            urlStr = "all_precinct_parish_results"
            parameter = "cid=\(cp_id)&id=\(u_id)&startrow=\(startRow)&endrow=\(noRow)&\(paramStr)&authToken=\(authToken)"
        }else if type == 12 { // For my_Reports_alphabetically_list
            urlStr = "my_Reports_alphabetically_list"
            parameter = "cid=\(cp_id)&id=\(u_id)&startrow=\(startRow)&endrow=\(noRow)&\(paramStr)&authToken=\(authToken)"
        }else if type == 13 { // For Voice
            let str = USER_DEFAULTS.string(forKey: "voiceSearch") ?? ""
            urlStr = "searchname_by_voice_search"
            parameter = "cid=\(cp_id)&id=\(u_id)&startrow=\(startRow)&endrow=\(noRow)&search_byname=\(str)&authToken=\(authToken)"
        }else if type == 14 { // CrossTab WalkList
            urlStr = "crosstabsearch_for_all"
            parameter = "cid=\(cp_id)&id=\(u_id)&startrow=\(startRow)&endrow=\(noRow)&\(paramStr)&authToken=\(authToken)"
        }
        postWithoutImage(url_string: urlStr, parameters: parameter,CompletionHandler: { json, error in
            if let json = json {
                print("json~~~~~~~",json)
                let statusmassege = json.value(forKey: "status") as! Bool
                if (statusmassege == true){
                    let authToken = json.value(forKey: "authToken") as! String
                    USER_DEFAULTS.set(authToken, forKey: "authToken")
                    // jsonObj.removeAllObjects()
                    self.no = 0
                    jsonObj = json
                    self.SetData()
                    start = startRow
                    self.TableView.reloadData()
                    if type == 8 || type == 12 || type == 3 || type == 2 || type == 14{
                        NotificationCenter.default.post(name: Notification.Name("nameOfNotification"), object: nil, userInfo: ["type":"\(isSaerchStrName)"])
                    }
                    if type == 13{
                        self.viewTable.isHidden = false
                        //type = 0
                    }
                    self.jsonCount = jsonObj.count
                }else{
                    print("else!~~~~~")
                    var is_changed: String? = nil
                    if let object = json["is_changed"] {
                        is_changed = "\(object)"
                    }
                    if is_changed?.isEqual("1") ?? false {
                        logoutAlertMassege()
                    } else {
                        HomeViewController.VC.alert(message: "No Record Found!", title: "Alert!")
                    }
                    self.jsonCount = 0
                }
            }else{
                print("error!~~~~~",error)
                self.jsonCount = 0
                if type == 8{
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "pollwatchingmode"), object: nil)
                }
            }
        })
        
    }
    
    
    func SetData(){
        //isSaerchStrName = 1
        isSelectBackToList = false
        isSelectFavNYard = false
        isReqUpdate = false
        isSelectSaveBtn = false
        isSelectCustomVariable = false
        isSelectCustomVariable1 = false
        isSelectCustomVariable2 = false
        var race = ["H","B","I","O","W","A",];
        var rName = ["Hispanic","Black","Native American","Other","White","Asian"];
        var party = ["DEM","REP","U",""," "];
        var pName = ["Democrat","Republican","Unknown","Unknown","Unknown", nil];
//        max = 0
        var results = NSArray()
        if(isMapHome == 1){
            results = mapjsonObj.value(forKey: "data") as! NSArray
        }else{
            results = jsonObj.value(forKey: "data") as! NSArray
        }
        max = results.count
        if(no >= max){
            no = max - 1
        }
        USER_DEFAULTS.set(no, forKey: "case")
        USER_DEFAULTS.set(no, forKey: "case1")
        let obj = results[no] as! NSArray
        let obj23 = obj[23] as? String ?? ""
        
        if (obj23 != "") {
            lblName.text = "\(obj[0]) \(obj[23]) \( obj[1])"
        }else{
            lblName.text = "\(obj[0]) \( obj[1])"
        }
        lblHouseNo.text = "\(obj[2]) \(obj[15]) \(obj[3]) \(obj[17])"
        lblStreetCity.text  = "\(obj[4]) \(obj[18]) \(obj[19])"
        
        
        var myMutableString = NSMutableAttributedString(string: "Age : \(obj[8])", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 16)])
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:5))
        lblAge.attributedText = myMutableString
        
        lblEmail.text = "\(obj[21])"
        
        myMutableString = NSMutableAttributedString(string: isAppMyPeople ? "Group : \(obj[24])" : "Rating : \(obj[24])", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 16)])
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:isAppMyPeople ? 7 : 8))
        lblRating.attributedText = myMutableString
        
        myMutableString = NSMutableAttributedString(string: "Last Vote : \(obj[10])", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 16)])
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:11))
        lblLastVote.attributedText = myMutableString
        
        lblRace.text = "\(obj[6])"
        lblParty.text = "\(obj[7])"
        
        for i in 0..<race.count{
            if(lblRace.text?.caseInsensitiveCompare(race[i]) == .orderedSame){
                myMutableString = NSMutableAttributedString(string: "Race : \(rName[i])", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 16)])
                myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:6))
                lblRace.attributedText = myMutableString
                break;
            }
            if(race.count - 1 == i){
                myMutableString = NSMutableAttributedString(string: "Race : Unknown", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 16)])
                myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:6))
                lblRace.attributedText = myMutableString
            }
        }
        for i in 0..<party.count{
            if(lblParty.text?.caseInsensitiveCompare(party[i]) == .orderedSame){
                myMutableString = NSMutableAttributedString(string: "Party : \(pName[i]!)", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 16)])
                myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:7))
                lblParty.attributedText = myMutableString
                break;
            }
            if(party.count - 1 == i){
                myMutableString = NSMutableAttributedString(string: "Party : Other", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 16)])
                myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:7))
                lblParty.attributedText = myMutableString
            }
        }
        
        lblSex.text = "\(obj[5])"
        
        if(lblSex.text?.caseInsensitiveCompare("M") == .orderedSame){
            myMutableString = NSMutableAttributedString(string: "Sex : Male", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 16)])
            myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:5))
            lblSex.attributedText = myMutableString
        }else if(lblSex.text?.caseInsensitiveCompare("F") == .orderedSame){
            myMutableString = NSMutableAttributedString(string: "Sex : Female", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 16)])
            myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:5))
            lblSex.attributedText = myMutableString
        }else{
            myMutableString = NSMutableAttributedString(string: "Sex : Unknown", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 16)])
            myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:5))
            lblSex.attributedText = myMutableString
        }
        
        btnCustVar1.titleLabel?.text = ""
        btnCustVar2.titleLabel?.text = ""
        btnCustVar3.titleLabel?.text = ""
        
        
        
        btnCustVar1.setTitle("\(obj[28])", for: .normal)
        btnCustVar2.setTitle("\(obj[29])", for: .normal)
        btnCustVar3.setTitle("\(obj[30])", for: .normal)
        
        var phno = "\(obj[31])"
        mobilePhone = "\(obj[31])"
        var mutableString = ""
        phno = phno.replacingOccurrences(of: " ", with: "")
        mutableString = phno
        for p in 0..<mutableString.count {
            if p == 0 {
                mutableString.insert("(", at: mutableString.startIndex)
            }
            if p == 4 {
                mutableString.insert(contentsOf: ") ", at: mutableString.index(mutableString.startIndex, offsetBy: p))
            }
            if p == 9 {
                mutableString.insert(contentsOf: "-", at: mutableString.index(mutableString.startIndex, offsetBy: p))
            }
        }
        btnMobilePhoneNo.setTitle("M : \(mutableString)", for: .normal)
        
        var homephno = "\(obj[9])"
        homePhone = "\(obj[9])"
        homephno = homephno.replacingOccurrences(of: " ", with: "")
        mutableString = homephno
        for p in 0..<mutableString.count {
            if p == 0 {
                mutableString.insert("(", at: mutableString.startIndex)
            }
            if p == 4 {
                mutableString.insert(contentsOf: ") ", at: mutableString.index(mutableString.startIndex, offsetBy: p))
            }
            if p == 9 {
                mutableString.insert(contentsOf: "-", at: mutableString.index(mutableString.startIndex, offsetBy: p))
            }
        }
        btnHomePhoneNo.setTitle("H : \(mutableString)", for: .normal)
        btnFavor.isSelected = false
        btnNotFavor.isSelected = false
        btnUndecided.isSelected = false
        btnNotAvailable.isSelected = false
        let Favorability = "\(obj[12])"
        if Favorability.caseInsensitiveCompare("FAVOR") == .orderedSame || Favorability.caseInsensitiveCompare("Favour") == .orderedSame {
            btnFavor.isSelected = true
        } else if Favorability.caseInsensitiveCompare("NOT FAVOR") == .orderedSame || Favorability.caseInsensitiveCompare("NOT Favour") == .orderedSame {
            btnNotFavor.isSelected = true
        } else if Favorability.caseInsensitiveCompare("UNDECIDED") == .orderedSame {
            btnUndecided.isSelected = true
        } else if Favorability.caseInsensitiveCompare("NOT AVAILABLE") == .orderedSame {
            btnNotAvailable.isSelected = true
        } else {
            btnFavor.isSelected = false
            btnNotFavor.isSelected = false
            btnUndecided.isSelected = false
            btnNotAvailable.isSelected = false
        }
        btnPlaced.isSelected = false
        btnYes.isSelected = false
        btnNo.isSelected = false
        let YardSign = "\(obj[13])"
        if YardSign.caseInsensitiveCompare("PLACED") == .orderedSame {
            btnPlaced.isSelected = true
        } else if YardSign.caseInsensitiveCompare("YES") == .orderedSame {
            btnYes.isSelected = true
        } else if YardSign.caseInsensitiveCompare("No") == .orderedSame {
            btnNo.isSelected = true
        } else {
            btnPlaced.isSelected = false
            btnYes.isSelected = false
            btnNo.isSelected = false
        }
        exel_Id = obj[20] as?String ?? ""
        
        if obj.count > 22 {
            let voted = "\(obj[22])"
            let p_v = PollWatchingModeView()
            if voted.caseInsensitiveCompare("YES") == .orderedSame {
                isSelectedVotedPoll = "Yes"
                p_v.btnPYes.isSelected = true
                lblVoted.isHidden = false
            } else if voted.caseInsensitiveCompare("NO") == .orderedSame {
                isSelectedVotedPoll = "No"
                p_v.btnPNo.isSelected = true
                lblVoted.isHidden = true
            } else {
                isSelectedVotedPoll = ""
                p_v.btnPYes.isSelected = false
                p_v.btnPNo.isSelected = false
                lblVoted.isHidden = true
            }
        }
        print("voted~~~~~~~~\(obj[22])")
        
        let dic = obj[32] as! NSDictionary
        let commArr = dic.value(forKey: "data") as! NSArray
        commentArray.removeAllObjects()
        commDataArray.removeAllObjects()
        for i in 0..<commArr.count{
            let c_Array = NSMutableArray()
            let data = commArr[i] as! NSArray
            var custVar1detail = "";
            var custVar2detail = "";
            var custVar3detail = "";
            let d9 = data[9] as? String ?? ""
            let d10 = data[10] as? String ?? ""
            let d11 = data[11] as? String ?? ""
            if custVarHeading1 != ""{
                if (d9 != "") {custVar1detail = "\n\(data[9]) : \(data[12])"}
            }else{
                custVar1detail = ""
            }
            if custVarHeading2 != ""{
                if (d10 != "") {custVar2detail = "\n\(data[10]) : \(data[13])"}
            }else{
                custVar2detail = ""
            }
            if custVarHeading3 != ""{
                if (d11 != "") {custVar3detail = "\n\(data[11]) : \(data[14])"}
            }else{
                custVar3detail = ""
            }
            
            var str = ""
            if isAppMyPeople {
                var favorability = data[4] as? String ?? ""
                var casStaus = data[5] as? String ?? ""
                if favorability.caseInsensitiveCompare("FAVOR") == .orderedSame || favorability.caseInsensitiveCompare("Satisfied") == .orderedSame {
                    favorability = "Satisfied"
                } else if favorability.caseInsensitiveCompare("NOT FAVOR") == .orderedSame || favorability.caseInsensitiveCompare("Not Satisfied") == .orderedSame {
                    favorability = "Not Satisfied"
                } else if favorability.caseInsensitiveCompare("UNDECIDED") == .orderedSame || favorability.caseInsensitiveCompare("Undecided") == .orderedSame {
                    favorability = "Undecided"
                } else if favorability.caseInsensitiveCompare("NOT AVAILABLE") == .orderedSame || favorability.caseInsensitiveCompare("Never Contacted") == .orderedSame{
                    favorability = "Never Contacted"
                } else{
                    favorability = ""
                }
                if casStaus.caseInsensitiveCompare("YES") == .orderedSame || casStaus.caseInsensitiveCompare("Open") == .orderedSame {
                    casStaus = "Open"
                } else if casStaus.caseInsensitiveCompare("NO") == .orderedSame || casStaus.caseInsensitiveCompare("Closed") == .orderedSame {
                    casStaus = "Closed"
                } else if casStaus.caseInsensitiveCompare("Placed") == .orderedSame {
                    casStaus = "Placed"
                } else {
                    casStaus = ""
                }
                
                str = "Date : \(data[2])\nUser : \(data[8])\nComment : \(data[1])\nSatisfaction : \(favorability)\nCase Status : \(casStaus)\(custVar1detail)\(custVar2detail)\(custVar3detail)"
            } else {
                str = "Date : \(data[2])\nUser : \(data[8])\nComment : \(data[1])\nFavorability : \(data[4])\nYard Sign : \(data[5])\(custVar1detail)\(custVar2detail)\(custVar3detail)"
            }
            commentArray.add(comment(str, type: false)!)
            c_Array.add(data[0]);c_Array.add(data[1]);c_Array.add(data[2]);c_Array.add(data[3]);c_Array.add(data[4]);c_Array.add(data[5]);c_Array.add(data[6]);c_Array.add(data[7]);c_Array.add(data[8]);c_Array.add(data[9]);c_Array.add(data[10]);c_Array.add(data[11]);c_Array.add(data[12]);c_Array.add(data[13]);c_Array.add(data[14]);
            
            commDataArray.add(c_Array)
        }
        commTableView.reloadData()
        
        address = "\(obj[2]) \(obj[15]) \(obj[3]) \(obj[4]) \(obj[18]) \(obj[19])"
        for _ in 0..<3 {
            address = address.replacingOccurrences(of: "  ", with: " ")
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if TableView == tableView {
            if(max == noRow && start > 0){
                   print("max~~~\(start)~~",max)
                return max+2;}
            else if(max == noRow || start > 0){
                 // print("max~~2~~~",max,noRow)
                return max+1;}
            else{
                 // print("max~~3~~~",max)
                return max;
            }
        }else{
            return commentArray.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == TableView{
            let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier1, for: indexPath)
            if(start > 0 && indexPath.row == 0){
                cell.textLabel?.text = "Previous";
            }else if(max + 1 == indexPath.row || (max == indexPath.row && start == 0)){
                cell.textLabel?.text = "Next";
            }else{
                var results = NSArray()
                if(isMapHome == 1){
                    results = mapjsonObj.value(forKey: "data") as! NSArray
                }else{
                    results = jsonObj.value(forKey: "data") as! NSArray
                }
                var obj = NSArray()
                if start > 0 {
                    //print("~~~~~start~~~~~")
                    obj = results[indexPath.row - 1] as! NSArray
                } else {
                    //print("~~~~~end~~~~~",indexPath.row)obj[2]
                    obj = results[indexPath.row] as! NSArray
                }
                print("\(obj[1]) , \(obj[0])")
                cell.textLabel?.text = "\(obj[1]), \(obj[0])"
                if type == 2{
                    cell.textLabel?.text = "\(obj[1]), \(obj[0]) - \(obj[2])"
                }else if type == 14{
                    cell.textLabel?.text = "\(obj[1]), \(obj[0]) - \(obj[2])"
                }
            }
            cell.textLabel?.textColor = UIColor.white
            cell.backgroundColor = .clear
            cell.selectionStyle = .none
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! commentCell
            var obj: multiSelcted! = nil
            obj = commentArray[indexPath.row] as? multiSelcted
            //print("obj.txt~~~~~",obj.txt)
            let str = obj.txt.slice(from: "Date", to: "Comment")
            let str1 = obj.txt.slice(from: "Comment : ", to: isAppMyPeople ? "Satisfaction" :"Favorability")
            cell.lblTxt.textColor = UIColor.white
            let myMutableString = NSMutableAttributedString(string: obj.txt, attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 16)])
            myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: NSRange(location:str!.count+14,length:str1!.count))
            cell.lblTxt.attributedText = myMutableString
            cell.lblTxt.numberOfLines = 0
            cell.lblTxt.lineBreakMode = .byClipping
            cell.imgView.image = UIImage(named: obj.isSelected == false ? "diselect128.png" : "select128.png")
            cell.backgroundColor = .clear
            cell.selectionStyle = .none
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if TableView == tableView {
            if start > 0 && indexPath.row == 0 {
                if type == 2 {
                    let results = jsonObj.value(forKey: "data") as? [AnyHashable]
                    var obj = results?[0] as? [AnyHashable]
                    nextPrevStr = "next=&previous=\(obj![11])"
                    callGetExcel(u_id: u_id, cp_id: cp_id, startRow: 0)
                } else {
                    callGetExcel(u_id: u_id, cp_id: cp_id, startRow: start-noRow)
                    no = noRow - 1
                    self.SetData()
                }
            } else if max + 1 == indexPath.row || (max == indexPath.row && start == 0) {
                if type == 2 {
                    let results = jsonObj.value(forKey: "data") as? [AnyHashable]
                    var obj = results?[(results?.count ?? 0) - 1] as? [AnyHashable]
                    nextPrevStr = "next=\(obj![11])&previous="
                    callGetExcel(u_id: u_id, cp_id: cp_id, startRow: 0)
                }else{
                    callGetExcel(u_id: u_id, cp_id: cp_id, startRow: start+noRow)
                }
            }else{
                if(start > 0){
                    no = indexPath.row - 1;}
                else{
                    no = indexPath.row;
                }
                SetData()
                USER_DEFAULTS.set(no, forKey: "case")
                USER_DEFAULTS.set(no, forKey: "case1")
                USER_DEFAULTS.set(nil, forKey: "voiceSearch")
                viewTable.isHidden = true
                if type == 8{
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "pollwatchingmode"), object: nil)
                }
            }
        }else{
            var obj = NSMutableArray()
            obj = commentArray
            let com = obj[indexPath.row] as! multiSelcted
            com.isSelected = !com.isSelected
            commTableView.reloadData()
        }
    }
    
    func goBack() {
        isSaerchStrName = 1
        NotificationCenter.default.post(name: Notification.Name("nameOfNotification"), object: nil, userInfo: ["type":"\(isSaerchStrName)"])
        if (no > 0) {
            print("goBack~~~~~~~~0")
            no = no-1
            USER_DEFAULTS.set(no, forKey: "case")
            USER_DEFAULTS.set(no, forKey: "case1")
            self.SetData()
        }else{
            if type == 2 {
                print("goBack~~~~~~~~1")
                let results = jsonObj.value(forKey: "data") as? [AnyHashable]
                var obj = results?[0] as? [AnyHashable]
                nextPrevStr = "next=&previous=\(obj![11])"
                self.callGetExcel(u_id: u_id, cp_id: cp_id, startRow: 0)
                let dispatchAfter = DispatchTimeInterval.seconds(Int(2.0))
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + dispatchAfter, execute: {
                    print("jsonObj.count~~~~1~~",jsonObj.count)
                    if self.jsonCount > 0{
                        self.no = noRow-1
                        USER_DEFAULTS.set(self.no, forKey: "case")
                        USER_DEFAULTS.set(self.no, forKey: "case1")
                        self.SetData()
                    }
                })
            }else if (start > 0){
                print("goBack~~~~~~~~2")
                self.callGetExcel(u_id: u_id, cp_id: cp_id, startRow: start-noRow)
                no = noRow-1
                USER_DEFAULTS.set(no, forKey: "case")
                USER_DEFAULTS.set(no, forKey: "case1")
                self.SetData()
            }
        }
    }
    
    func goNext() {
        isSaerchStrName = 2
        NotificationCenter.default.post(name: Notification.Name("nameOfNotification"), object: nil, userInfo: ["type":"\(isSaerchStrName)"])
        if(no < max-1){
            no = no+1
            USER_DEFAULTS.set(no, forKey: "case")
            USER_DEFAULTS.set(no, forKey: "case1")
            self.SetData()
        }else{
            if type == 2 {
                let results = jsonObj.value(forKey: "data") as? [AnyHashable]
                var obj = results?[(results?.count ?? 0) - 1] as? [AnyHashable]
                nextPrevStr = "next=\(obj![11])&previous="
                callGetExcel(u_id: u_id, cp_id: cp_id, startRow: 0)
            }else{
                self.callGetExcel(u_id: u_id, cp_id: cp_id, startRow: start+noRow)
            }
        }
    }
    
    
    func updateView() {
        var heding = NSArray()
        var results = NSArray()
        if(isMapHome == 1){
            results = mapjsonObj.value(forKey: "data") as! NSArray
            heding = mapjsonObj.value(forKey: "heading") as! NSArray
        }else{
            results = jsonObj.value(forKey: "data") as! NSArray
            heding = jsonObj.value(forKey: "heading") as! NSArray
        }
        let obj = results[no] as! NSArray
        let dic = obj[32] as! NSDictionary
        let commHeding = dic.value(forKey: "heading") as! NSArray
        let allDataArr = NSMutableArray()
        for i in 0..<results.count{
            let chidchid2 = NSMutableArray()
            let valus3 = results[i] as! NSArray
            for j in 0..<valus3.count{
                if(i == no){
                    if(j == 12){
                        if(btnFavor.isSelected == true){
                            chidchid2.add("FAVOR")
                        }else if(btnNotFavor.isSelected == true){
                            chidchid2.add("NOT FAVOR")
                        }else if(btnUndecided.isSelected == true){
                            chidchid2.add("UNDECIDED")
                        }else if(btnNotAvailable.isSelected == true){
                            chidchid2.add("NOT AVAILABLE")
                        }else{
                            chidchid2.add("")
                        }
                    }else if(j == 13){
                        if(btnYes.isSelected == true){
                            chidchid2.add("YES")
                        }else if(btnNo.isSelected == true){
                            chidchid2.add("NO")
                        }else if(btnPlaced.isSelected == true){
                            chidchid2.add("PLACED")
                        }else{
                            chidchid2.add("")
                        }
                    }else if(j == 20){
                        chidchid2.add(exel_Id)
                    }else if(j == 22){
                        chidchid2.add(isSelectedVotedPoll)
                    }else if j == 28 {
                        if isSelectCustomVariable == true {
                            let trimmed = btnCustVar1.titleLabel?.text?.trimmingCharacters(in: CharacterSet.whitespaces)
                            chidchid2.add(trimmed ?? "")
                        } else {
                            chidchid2.add(valus3[28])
                        }
                    } else if j == 29 {
                        if isSelectCustomVariable1 == true {
                            let trimmed = btnCustVar2.titleLabel?.text?.trimmingCharacters(in: CharacterSet.whitespaces)
                            chidchid2.add(trimmed ?? "")
                        } else {
                            chidchid2.add(valus3[29])
                        }
                    } else if j == 30 {
                        if isSelectCustomVariable2 == true {
                            let trimmed = btnCustVar3.titleLabel?.text?.trimmingCharacters(in: CharacterSet.whitespaces)
                            chidchid2.add(trimmed ?? "")
                        } else {
                            chidchid2.add(valus3[30])
                        }
                    }else if(j == 32){
                        if(isSelectFavNYard){
                            self.addComment()
                            var commDic: [AnyHashable : Any] = [:]
                            commDic = ["heading": commHeding,"data": commDataArray]
                            chidchid2.add(commDic)
                        }else{
                            var commDic: [AnyHashable : Any] = [:]
                            commDic = ["heading": commHeding,"data": commDataArray]
                            chidchid2.add(commDic)
                        }
                    }else{
                        chidchid2.add(valus3[j])
                    }
                }else{
                    chidchid2.add(valus3[j])
                }
            }
            allDataArr.add(chidchid2)
        }
        
        var grade: [AnyHashable : Any] = [:]
        grade = ["data": allDataArr,"status": "true","msg": "Successfully","cid": cp_id,"heading": heding]
        
        if(isMapHome == 1){
            mapjsonObj = grade as NSDictionary
        }else{
            jsonObj = grade as NSDictionary
        }
        
        var sendreq: [AnyHashable : Any] = [:]
        var result: [AnyHashable]? = []
        if(isMapHome == 1){
            result = mapjsonObj.value(forKey: "data") as? [AnyHashable]
        }else{
            result = jsonObj.value(forKey: "data") as? [AnyHashable]
        }
        var bp: [AnyHashable] = []
        if let no = result?[no] {
            bp.append(no)
        }
        sendreq = ["data": bp,"status": "true","msg": "Successfully","cid": cp_id,"id": u_id,"heading": heding]
        var _: Error? = nil
        var jsonData: Data? = nil
        do {
            jsonData = try JSONSerialization.data(withJSONObject: sendreq, options: .prettyPrinted)
        } catch {
        }
        var jsonString: String? = nil
        if let jsonData = jsonData {
            jsonString = String(data: jsonData, encoding: .utf8)
        }
        //print("jsonString~~~~",jsonString)
        let postData = jsonString?.data(using: .ascii, allowLossyConversion: true)
        if postData != nil {
            let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
            let documentsDirectory = paths[0]
            let filePath = "\(documentsDirectory)/\("filename.doc")"
            do {
                try postData?.write(to: URL(fileURLWithPath: filePath), options: .atomic)
            } catch {
                print(error)
            }
        }
        callUpdateExcel(jsonData: postData!)
    }
    
    func addComment() {
        isReqUpdate = true
        isSelectFavNYard = false
        isSelectSaveBtn = true
        let addCommArr = NSMutableArray()
        let currentDate: Date = Date()
        let stringDate: String = currentDate.app_stringFromDate1()
        addCommArr.add("")
        if txtCommView.text == "Comment" {
            addCommArr.add("")
        }else{
            addCommArr.add(txtCommView.text ?? "")
        }
        addCommArr.add(stringDate)
        addCommArr.add(u_id)
        if(btnFavor.isSelected == true){
            addCommArr.add("FAVOR")
        }else if(btnNotFavor.isSelected == true){
            addCommArr.add("NOT FAVOR")
        }else if(btnUndecided.isSelected == true){
            addCommArr.add("UNDECIDED")
        }else if(btnNotAvailable.isSelected == true){
            addCommArr.add("NOT AVAILABLE")
        }else{
            addCommArr.add("")
        }
        if(btnYes.isSelected == true){
            addCommArr.add("YES")
        }else if(btnNo.isSelected == true){
            addCommArr.add("NO")
        }else if(btnPlaced.isSelected == true){
            addCommArr.add("PLACED")
        }else{
            addCommArr.add("")
        }
        addCommArr.add(exel_Id)
        addCommArr.add(cp_id)
        addCommArr.add(USER_DEFAULTS.string(forKey: "userName") ?? "")
        
        if custVarHeading1 != ""{
            addCommArr.add(custVarHeading1)
        }else{
            addCommArr.add("")
        }
        
        if custVarHeading2 != ""{
            addCommArr.add(custVarHeading2)
        }else{
            addCommArr.add("")
        }
        
        if custVarHeading3 != ""{
            addCommArr.add(custVarHeading3)
        }else{
            addCommArr.add("")
        }
        addCommArr.add(btnCustVar1.titleLabel?.text ?? "")
        addCommArr.add(btnCustVar2.titleLabel?.text ?? "")
        addCommArr.add(btnCustVar3.titleLabel?.text ?? "")
        if(btnPrivate.isSelected == true){
            addCommArr.add(true)
        }else if(btnPublic.isSelected == true){
            addCommArr.add(false)
        }else{
            addCommArr.add(false)
        }
        commDataArray.insert(addCommArr, at: 0)
        
        commentArray.removeAllObjects()
        for i in 0..<commDataArray.count{
            let data = commDataArray[i] as! NSArray
            var custVar1detail = "";
            var custVar2detail = "";
            var custVar3detail = "";
            let d9 = data[9] as? String ?? ""
            let d10 = data[10] as? String ?? ""
            let d11 = data[10] as? String ?? ""
            if (d9 != "") {custVar1detail = "\n\(data[9]) : \(data[12])"}
            if (d10 != "") {custVar2detail = "\n\(data[10]) : \(data[13])"}
            if (d11 != "") {custVar3detail = "\n\(data[11]) : \(data[14])"}
            
            var str = ""
            if isAppMyPeople {
                var favorability = data[4] as? String ?? ""
                var casStaus = data[5] as? String ?? ""
                if favorability.caseInsensitiveCompare("FAVOR") == .orderedSame || favorability.caseInsensitiveCompare("Satisfied") == .orderedSame {
                    favorability = "Satisfied"
                } else if favorability.caseInsensitiveCompare("NOT FAVOR") == .orderedSame || favorability.caseInsensitiveCompare("Not Satisfied") == .orderedSame {
                    favorability = "Not Satisfied"
                } else if favorability.caseInsensitiveCompare("UNDECIDED") == .orderedSame || favorability.caseInsensitiveCompare("Undecided") == .orderedSame {
                    favorability = "Undecided"
                } else if favorability.caseInsensitiveCompare("NOT AVAILABLE") == .orderedSame || favorability.caseInsensitiveCompare("Never Contacted") == .orderedSame{
                    favorability = "Never Contacted"
                } else{
                    favorability = ""
                }
                if casStaus.caseInsensitiveCompare("YES") == .orderedSame || casStaus.caseInsensitiveCompare("Open") == .orderedSame {
                    casStaus = "Open"
                } else if casStaus.caseInsensitiveCompare("NO") == .orderedSame || casStaus.caseInsensitiveCompare("Closed") == .orderedSame {
                    casStaus = "Closed"
                } else if casStaus.caseInsensitiveCompare("Placed") == .orderedSame {
                    casStaus = "Placed"
                } else {
                    casStaus = ""
                }
                
                str = "Date : \(data[2])\nUser : \(data[8])\nComment : \(data[1])\nSatisfaction : \(favorability)\nCase Status : \(casStaus)\(custVar1detail)\(custVar2detail)\(custVar3detail)"
            } else {
                str = "Date : \(data[2])\nUser : \(data[8])\nComment : \(data[1])\nFavorability : \(data[4])\nYard Sign : \(data[5])\(custVar1detail)\(custVar2detail)\(custVar3detail)"
            }
            commentArray.add(comment(str, type: false)!)
            // print("commentArray~~1~~~",commentArray)
            commTableView.reloadData()
        }
        // print("commentArray~~~~~",commentArray)
    }
    
    func callUpdateExcel(jsonData:Data){
        let authToken = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
        let param = ["authToken":authToken,"id":u_id,"cid":cp_id]
        postWithImage(url_string: "upload_jsonsheet?", parameters: param, filePathKey: "file", jsonData: jsonData, CompletionHandler: { json, error in
            if let json = json {
                print("json~~~~~~~",json)
                let statusmassege = json.value(forKey: "status") as! Bool
                if (statusmassege == true){
                    let authToken = json.value(forKey: "authToken") as! String
                    USER_DEFAULTS.set(authToken, forKey: "authToken")
                    self.isSelectSaveBtn = false
                    self.isSelectFavNYard = false
                    self.isReqUpdate = false
                    let data = json.value(forKey: "data") as! NSArray
                    let obj = data[0] as! NSArray
                    let dic = obj[32] as! NSDictionary
                    let commArr = dic.value(forKey: "data") as! NSArray
                    self.commentArray.removeAllObjects()
                    self.commDataArray.removeAllObjects()
                    for i in 0..<commArr.count{
                        let c_Array = NSMutableArray()
                        let data = commArr[i] as! NSArray
                        var custVar1detail = "";
                        var custVar2detail = "";
                        var custVar3detail = "";
                        let d9 = data[9] as? String ?? ""
                        let d10 = data[10] as? String ?? ""
                        let d11 = data[11] as? String ?? ""
                        if self.custVarHeading1 != ""{
                            if (d9 != "") {custVar1detail = "\n\(data[9]) : \(data[12])"}
                        }else{
                            custVar1detail = ""
                        }
                        if self.custVarHeading2 != ""{
                            if (d10 != "") {custVar2detail = "\n\(data[10]) : \(data[13])"}
                        }else{
                            custVar2detail = ""
                        }
                        if self.custVarHeading3 != ""{
                            if (d11 != "") {custVar3detail = "\n\(data[11]) : \(data[14])"}
                        }else{
                            custVar3detail = ""
                        }
                        var str = ""
                        if isAppMyPeople {
                            str = "Date : \(data[2])\nUser : \(data[8])\nComment : \(data[1])\nSatisfaction : \(data[4])\nCase Status : \(data[5])\(custVar1detail)\(custVar2detail)\(custVar3detail)"
                        } else {
                            str = "Date : \(data[2])\nUser : \(data[8])\nComment : \(data[1])\nFavorability : \(data[4])\nYard Sign : \(data[5])\(custVar1detail)\(custVar2detail)\(custVar3detail)"
                        }
                        self.commentArray.add(self.comment(str, type: false)!)
                        c_Array.add(data[0]);c_Array.add(data[1]);c_Array.add(data[2]);c_Array.add(data[3]);c_Array.add(data[4]);c_Array.add(data[5]);c_Array.add(data[6]);c_Array.add(data[7]);c_Array.add(data[8]);c_Array.add(data[9]);c_Array.add(data[10]);c_Array.add(data[11]);c_Array.add(data[12]);c_Array.add(data[13]);c_Array.add(data[14]);
                        self.commDataArray.add(c_Array)
                    }
                    
                    let heading = json["heading"] as? [AnyHashable]
                    var results: [AnyHashable]? = []
                    if(isMapHome == 1){
                        results = mapjsonObj.value(forKey: "data") as? [AnyHashable]
                    }else{
                        results = jsonObj.value(forKey: "data") as? [AnyHashable]
                    }
                    
                    
                    var baap: [AnyHashable] = []
                    for i in 0..<(results?.count ?? 0) {
                        var chidchid2: [AnyHashable] = []
                        var valus3 = results?[i] as? [AnyHashable]
                        for j in 0..<(valus3?.count ?? 0) {
                            if i == self.no {
                                chidchid2.append(obj[j] as! AnyHashable)
                            } else {
                                if let aValus3 = valus3?[j] {
                                    chidchid2.append(aValus3)
                                }
                            }
                        }
                        baap.append(chidchid2)
                    }
                    var grade: [AnyHashable : Any] = [:]
                    grade = ["data": baap,"status": "true","msg": "Successfully","cid": self.cp_id,"heading": heading!]
                    if(isMapHome == 1){
                        mapjsonObj = grade as NSDictionary
                    }else{
                        jsonObj = grade as NSDictionary
                    }
                    DispatchQueue.main.async {
                        self.commTableView.reloadData()
                        if self.isSelectBackToList{
                            self.viewTable.isHidden = false
                            self.TableView.reloadData()
                        }
                        self.SetData()
                    }
                    
                }else{
                    var is_changed: String? = nil
                    if let object = json["is_changed"] {
                        is_changed = "\(object)"
                    }
                    if is_changed?.isEqual("1") ?? false {
                        logoutAlertMassege()
                    } else {
                        HomeViewController.VC.alert(message: "No Record Found!", title: "Alert!")
                    }
                }
            }
        })
    }
}
