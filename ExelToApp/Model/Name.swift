//
//  Name.swift
//  ExelToApp
//
//  Created by baps on 28/01/20.
//  Copyright © 2020 Osiya. All rights reserved.
//

import UIKit

class Name: UIView {
    
    var mainView:UIView!
    var lblHeading:UILabel!
    var popUpView:UIView!
    var txtFirstName:UITextField!
    var txtLastName:UITextField!
    var btnCancel:UIButton!
    var btnOk:UIButton!
    var topView:UIView!
    
    var u_id = USER_DEFAULTS.string(forKey: "u_id") ?? ""
    var cp_id = USER_DEFAULTS.string(forKey: "cp_id") ?? ""

    public override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
        
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpView()
    }
    
    func setUpView() {
        mainView = UIView()
        mainView.frame = CGRect(x: 0, y: 0, width: SWIDTH, height: SHEIGHT)
        mainView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.addSubview(mainView)
        
        popUpView = UIView()
        popUpView.frame = CGRect(x: 40, y: 80, width: mainView.frame.width-80, height: 180)
        popUpView.backgroundColor = UIColor.white
        popUpView.layer.cornerRadius = 10.0
        popUpView.clipsToBounds = true
        mainView.addSubview(popUpView)
        
        topView = UIView()
        topView.frame = CGRect(x: 0, y: 0, width: popUpView.frame.width, height: 50)
        topView.backgroundColor = APPBUTTONCOLOR
        popUpView.addSubview(topView)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 0, Y: 10, Width: popUpView.frame.width, Height: 30, TextColor: .white, BackColor: .clear, Text: "Search By Name", TextAlignment: .center, Font: .boldSystemFont(ofSize: 18))
        topView.addSubview(lblHeading)
        
        txtFirstName = UITextField()
        txtFirstName.borderStyle = .roundedRect
        txtFirstName.setTextField(X: 10, Y: topView.frame.maxY+15, Width: popUpView.frame.width-20, Height: 30, TextColor: .black, BackColor: .clear)
        txtFirstName.placeholder = "Enter First Name"
        popUpView.addSubview(txtFirstName)
        
        txtLastName = UITextField()
        txtLastName.setTextField(X: 10, Y: txtFirstName.frame.maxY+8, Width: popUpView.frame.width-20, Height: 30, TextColor: .black, BackColor: .clear)
        txtLastName.borderStyle = .roundedRect
        txtLastName.placeholder = "Enter Last Name"
        popUpView.addSubview(txtLastName)
        
        btnCancel = UIButton(type: .system)
        btnCancel.setButton(X: popUpView.frame.width-30, Y: 5, Width: 24, Height: 24, TextColor: .white, BackColor: .clear, Title: "X", Align: .center, Font: .boldSystemFont(ofSize: 22))
        btnCancel.tag = 2
        btnCancel.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        topView.addSubview(btnCancel)
        
        btnOk = UIButton(type: .system)
        btnOk.setButton(X: popUpView.frame.width/2-(popUpView.frame.width/3)/2, Y: txtLastName.frame.maxY+10, Width: popUpView.frame.width/3, Height: 30, TextColor: .white, BackColor: APPBUTTONCOLOR, Title: "OK", Align: .center, Font: .boldSystemFont(ofSize: 18))
        btnOk.tag = 1
        btnOk.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        popUpView.addSubview(btnOk)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onClickRearView), name: NSNotification.Name(rawValue: "onClickRearView"), object: nil)
        
    }
    @objc func onClickRearView(notif: NSNotification) {
        self.isHidden = true
    }
    
    @objc func onClickButton(_ sender: UIButton) {
        print("-----Name-----")
        if sender.tag == 2{
            self.isHidden = true
            HomeViewController.VC.navigationItem.rightBarButtonItem = HomeViewController.VC.btnEditPerson
        }else{
            type = 3
            paramStr = ""
            paramStr = "search_firstname=\(txtFirstName.text ?? "")&search_lastname=\(txtLastName.text ?? "")"
            HomeViewController.VC.callGetExcel(u_id: u_id, cp_id: cp_id, startRow: 0)
            self.isHidden = true
            HomeViewController.VC.navigationItem.rightBarButtonItem = HomeViewController.VC.btnEditPerson
        }
        
    }

}
