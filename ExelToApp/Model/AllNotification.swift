
//
//  Notification.swift
//  ExelToApp
//
//  Created by baps on 12/02/20.
//  Copyright © 2020 Osiya. All rights reserved.
//

import UIKit

class NotifDetail{
    var strTitle = ""
    var strDescription = ""
}
class NotifCell: UITableViewCell {
    var backView:UIView!
    var lblTitle:UILabel!
    var lblDesc:UILabel!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        print(contentView.frame.width)
        
        lblTitle = UILabel()
        addSubview(lblTitle)
        lblTitle.anchor(top: topAnchor, paddingTop: 5, bottom: nil, paddingBottom: 5, left: leftAnchor, paddingLeft: 15, right: rightAnchor, paddingRight: 5, width: 0, height: 25)
        
        lblDesc = UILabel()
        addSubview(lblDesc)
        lblDesc.anchor(top: lblTitle.bottomAnchor, paddingTop: 5, bottom: bottomAnchor, paddingBottom: 8, left: leftAnchor, paddingLeft: 15, right: rightAnchor, paddingRight: 5, width: 0, height: 0)
        lblDesc.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 1, constant: -35).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class AllNotification: UIView,UITableViewDelegate,UITableViewDataSource {
    var mainView:UIView!
    var lblHeading:UILabel!
    var popUpView:UIView!
    var btnCancel:UIButton!
    var btnOk:UIButton!
    var topView:UIView!
    var TableView: UITableView = UITableView()
    var cellReuseIdentifier = "Cell"
    var fPointer: UnsafeMutablePointer<Int>!
    var u_id = USER_DEFAULTS.string(forKey: "u_id") ?? ""
    var cp_id = USER_DEFAULTS.string(forKey: "cp_id") ?? ""
    
    var notificationArr = [NotifDetail]()

    public override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
        
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpView()
    }
    
    func setUpView() {
        mainView = UIView()
        mainView.frame = CGRect(x: 10, y: 10, width: SWIDTH-20, height: SHEIGHT-LoginViewController.VC.NAVHEIGHT-STATUSBARHEIGHT-20)
        mainView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        mainView.layer.cornerRadius = 10.0
        self.addSubview(mainView)
        
        popUpView = UIView()
        popUpView.frame = CGRect(x: 0, y: 0, width: mainView.frame.width, height: mainView.frame.height)
        popUpView.backgroundColor = UIColor.white
        popUpView.layer.cornerRadius = 10.0
        popUpView.clipsToBounds = true
        mainView.addSubview(popUpView)
        
        topView = UIView()
        topView.frame = CGRect(x: 0, y: 0, width: popUpView.frame.width, height: 50)
        topView.backgroundColor = APPBUTTONCOLOR
        popUpView.addSubview(topView)
        
        lblHeading = UILabel()
        lblHeading.setLabel(X: 0, Y: 10, Width: popUpView.frame.width, Height: 30, TextColor: .white, BackColor: .clear, Text: "Notification", TextAlignment: .center, Font: .boldSystemFont(ofSize: 18))
        topView.addSubview(lblHeading)
        
        btnCancel = UIButton(type: .system)
        btnCancel.setButton(X: popUpView.frame.width-30, Y: 5, Width: 24, Height: 24, TextColor: .white, BackColor: .clear, Title: "X", Align: .center, Font: .boldSystemFont(ofSize: 22))
        btnCancel.tag = 2
        btnCancel.addTarget(self, action: #selector(onClickButton(_:)), for: .touchUpInside)
        topView.addSubview(btnCancel)
        
        TableView.frame = CGRect(x: 0, y: topView.frame.maxY, width: popUpView.frame.width, height: popUpView.frame.height-50)
        TableView.backgroundColor = UIColor.white
        TableView.delegate = self
        TableView.dataSource = self
        TableView.register(NotifCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        popUpView.addSubview(TableView)
        TableView.layer.cornerRadius = 10.0
        TableView.estimatedRowHeight = 44.0
        TableView.rowHeight = UITableView.automaticDimension
        TableView.tableFooterView = UIView()
        callAllNotification()
        NotificationCenter.default.addObserver(self, selector: #selector(self.onClickRearView), name: NSNotification.Name(rawValue: "onClickRearView"), object: nil)
        
    }
    @objc func onClickRearView(notif: NSNotification) {
        self.isHidden = true
    }
    
    @objc func onClickButton(_ sender: UIButton) {
        if sender.tag == 2{
            self.isHidden = true
            HomeViewController.VC.navigationItem.rightBarButtonItem = HomeViewController.VC.btnEditPerson
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! NotifCell
        cell.lblTitle.text = "Title : \(notificationArr[indexPath.row].strTitle)"
        cell.lblDesc.text = notificationArr[indexPath.row].strDescription
        cell.lblTitle.textColor = APPBUTTONCOLOR
        cell.lblDesc.textColor = APPBUTTONCOLOR
        cell.lblDesc.numberOfLines = 0
        cell.lblDesc.lineBreakMode = .byClipping
        cell.selectionStyle = .none
        
        return cell
    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 100
//    }
    ///https://lapublicopinion.com/Excel_api_v9i/getall_notification
    func callAllNotification(){
        let authToken = UserDefaults.standard.value(forKey: "authToken") as? String ?? ""
        postWithoutImage(url_string: "getall_notification", parameters: "id=\(u_id)&cid=\(cp_id)&authToken=\(authToken)",CompletionHandler: { json, error in
            if let json = json {
                print("json~~~~~~",json)
                let statusmassege = json.value(forKey: "status") as! Bool
                if (statusmassege == true){
                    self.notificationArr.removeAll()
                    let data = json.value(forKey: "data") as! NSArray
                    for i in 0..<data.count{
                        let dic = data[i] as! NSDictionary
                        let N_detail = NotifDetail()
                        N_detail.strTitle = dic.value(forKey: "notification_title") as? String ?? ""
                        N_detail.strDescription = dic.value(forKey: "notification_discription") as? String ?? ""
                        self.notificationArr.append(N_detail)
                    }
                    DispatchQueue.main.async {
                        self.TableView.reloadData()
                    }
                }else{
                    var is_changed: String? = nil
                    if let object = json["is_changed"] {
                        is_changed = "\(object)"
                    }
                    if is_changed?.isEqual("1") ?? false {
                        logoutAlertMassege()
                    } else {
                        HomeViewController.VC.alert(message: "No Record Found!", title: "Alert!")
                    }
                }
            }else{
                self.isHidden = true
                //HomeViewController.VC.alert(message: "No Record Found!", title: "Alert!")
            }
        })
    }
}
